package guardian;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.LinkedHashMap;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import guardian.Proficiency.CustomProficiency;

public class AddProficiency extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private LinkedHashMap<BaseInterface, Proficiency> proficiencies;
	private JPanel parent;
	private SearchableComboBox items = new SearchableComboBox();

	private boolean hasClicked = false;
	private JButton add = new JButton("Add");
	private JButton done = new JButton("Done");

	public AddProficiency(LinkedHashMap<BaseInterface, Proficiency> proficiencies, JPanel parent) {
		super("New Proficiency");
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});
		this.items.setModel(BaseInterface.getProficiencyList().values().toArray(new BaseInterface[0]));
		this.items.actionListener(this);
		this.proficiencies = proficiencies;
		this.parent = parent;

		this.add.addActionListener(this);
		this.done.addActionListener(this);

		JPanel buttonPane = new JPanel(new GridLayout(1, 0));
		buttonPane.add(this.add);
		buttonPane.add(this.done);

		this.setLayout(new GridLayout(0, 1));
		this.add(this.items);
		this.add(buttonPane);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	public JButton open(boolean characterCreation) {
		JButton open = new JButton(this.getTitle());
		open.setFont(Main.titleFont);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
				if (!hasClicked && characterCreation) {
					hasClicked = true;
					JOptionPane.showMessageDialog(open,
							"Subclass proficiencies are automatically added on character creation.");
				}
			}
		});
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.add || e.getSource() == this.done) {
			try {
				Proficiency proficiency;
				BaseInterface prof;
				if (!this.items.getSelectedItem().toString().isEmpty()) {
					if (BaseInterface.getProficiencyList().containsValue((BaseInterface) this.items.getSelectedItem()))
						proficiencies.put(prof = (BaseInterface) this.items.getSelectedItem(),
								proficiency = new Proficiency(prof, proficiencies));
					else
						proficiencies.put(prof = new CustomProficiency(this.items.getSelectedItem().toString()),
								proficiency = new Proficiency(prof, proficiencies));
					if (parent != null) {
						parent.add(proficiency.display(parent));
						parent.revalidate();
						parent.repaint();
					}
				}
				this.items.setModel(new DefaultComboBoxModel<BaseInterface>(this.items.items));
				this.items.setSelectedIndex(0);
				this.items.showPopup();

				if (e.getSource() == this.done)
					this.setVisible(false);
			} catch (RuntimeException error) {
				this.setVisible(false);
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					error.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
			}
		}
	}

}
