package guardian;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.Entity.Speed;
import guardian.Skill.Skills;
import guardian.Stat.Stats;
import guardian.items.AddArmor;
import guardian.items.NewWeapon;
import guardian.items.Weapon;

@SuppressWarnings("serial")
public class MyCharacter extends JFrame {

	public Guardian character;
	private Guardian[] characters;

	private JPanel proficienciesPane = new JPanel(new GridLayout(0, 1));
	private JPanel languagesPane = new JPanel(new GridLayout(0, 1));
	private JPanel spellPane = new JPanel(new BorderLayout());
	private JPanel spellsPane = new JPanel(new GridLayout(0, 1));
	private JPanel inventoryPane = new JPanel(new GridLayout(0, 1));
	private JPanel personalityPane = new JPanel(new GridLayout(0, 1));
	private JPanel idealsPane = new JPanel(new GridLayout(0, 1));
	private JPanel bondsPane = new JPanel(new GridLayout(0, 1));
	private JPanel flawsPane = new JPanel(new GridLayout(0, 1));
	private JPanel featuresPane = new JPanel(new GridLayout(0, 1));
	private JPanel characterNotesPane = new JPanel(new GridLayout(0, 1));
	JPanel statsPane = new JPanel(new GridLayout(0, 1));

	public JPanel weaponNamePane = new JPanel(new GridLayout(0, 1));
	public JPanel weaponAmountPane = new JPanel(new GridLayout(0, 1));
	public JPanel weaponAttackBonusPane = new JPanel(new GridLayout(0, 1));
	public JPanel weaponDamagePane = new JPanel(new GridLayout(0, 1));
	public JPanel weaponButtonPane = new JPanel(new GridLayout(0, 1));

	public JSpinner shields;
	public JTextField glimmer = new JTextField(8);
	public JLabel proficiencyBonus;

	private JPanel topPane = new JPanel();
	private JPanel leftPane = new JPanel(new GridLayout(0, 1));
	private JScrollPane leftScrollPane = new JScrollPane(leftPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	private JPanel centerPane = new JPanel(new GridLayout());
	private JScrollPane centerScrollPane = new JScrollPane(centerPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	public JPanel rightPane = new JPanel(new GridLayout());
	private JScrollPane rightScrollPane = new JScrollPane(rightPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	private JPanel bottomPane = new JPanel(new GridLayout());

	public MyCharacter(Guardian[] characters, int characterIndex) {
		super(characters[characterIndex].toString());
		this.character = characters[characterIndex];
		this.characters = characters;

		this.init();
	}

	public MyCharacter(Guardian[] characters, Guardian character) {
		super(character.toString());
		this.character = character;
		Guardian[] tempCharacters = new Guardian[characters.length + 1];
		for (int i = 0; i < characters.length; i++) {
			tempCharacters[i] = characters[i];
		}
		tempCharacters[characters.length] = this.character;
		this.characters = tempCharacters;

		this.init();
	}

	private void init() {
		this.addWindowListener(new java.awt.event.WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent closeEvent) {
				updateCharacter(getContentPane());
				Main.write(characters);

			}
		});
		this.setLayout(new BorderLayout());
		initTopPane();
		initLeftPane();
		initCenterPane();
		initRightPane();
		initBottomPane();

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.setMaximizedBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.pack();
		this.getContentPane().setBackground(character.backgroundColor);
		this.setBackground(character.backgroundColor);
		rightScrollPane.getPreferredSize().width = (int) (.3 * Main.width) - 20;
		centerScrollPane
				.getPreferredSize().width = (int) ((Main.width - leftPane.getWidth() - rightPane.getWidth() - 20)
						* 0.5);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void updateCharacter(Component component) {
		if (component instanceof JPanel) {
			for (Component newComponent : ((JPanel) component).getComponents()) {
				updateCharacter(newComponent);
			}
		} else if (component instanceof JTextField && ((JTextField) component).getActionListeners().length == 1) {
			((JTextField) component).getActionListeners()[0].actionPerformed(new ActionEvent(component, 0, null));
		} else if (component instanceof JSpinner && ((JSpinner) component).getChangeListeners().length == 1) {
			((JSpinner) component).getChangeListeners()[0].stateChanged(new ChangeEvent(component));
		}
	}

	@SuppressWarnings("unused")
	private void print(Component component, int count) {
		if (component instanceof JPanel) {
			System.out.print("{\n");
			for (Component newComponent : ((JPanel) component).getComponents()) {
				for (int i = 0; i < count; i++)
					System.out.print("    ");
				print(newComponent, count + 1);
			}
			for (int i = 0; i < count - 1; i++)
				System.out.print("    ");
			System.out.print("}\n");
		} else {
			System.out.println(component.getClass());
		}
	}

	JPanel initStatsPane() {
		statsPane.removeAll();
		statsPane.add(character.getStats().get(Stats.Strength).display());
		statsPane.add(character.getStats().get(Stats.Dexterity).display());
		statsPane.add(character.getStats().get(Stats.Constitution).display());
		statsPane.add(character.getStats().get(Stats.Intelligence).display());
		statsPane.add(character.getStats().get(Stats.Wisdom).display());
		statsPane.add(character.getStats().get(Stats.Charisma).display());
		statsPane.getPreferredSize().width = (int) (statsPane.getPreferredSize().width * 1.4);
		statsPane.setBackground(character.backgroundColor);
		return statsPane;
	}

	private JPanel initSkillsPane() {
		JPanel leftPane = new JPanel(new GridLayout(0, 1));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Acrobatics).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.AnimalHandling).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Arcana).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Athletics).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Deception).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.History).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Insight).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Intimidation).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Investigation).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Medicine).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Nature).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Perception).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Performance).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Persuasion).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Religion).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.SleightOfHand).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Stealth).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Survival).getName()));
		leftPane.add(new JLabel(character.getSkills().get(Skills.Technology).getName()));
		leftPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 7));
		leftPane.setBackground(character.backgroundColor);

		JPanel rightPane = new JPanel(new GridLayout(0, 1));
		rightPane.add(character.getSkills().get(Skills.Acrobatics).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.AnimalHandling).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Arcana).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Athletics).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Deception).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.History).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Insight).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Intimidation).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Investigation).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Medicine).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Nature).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Perception).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Performance).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Persuasion).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Religion).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.SleightOfHand).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Stealth).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Survival).displayStats(character));
		rightPane.add(character.getSkills().get(Skills.Technology).displayStats(character));
		rightPane.setBackground(character.backgroundColor);

		JPanel skillsPane = new JPanel();
		skillsPane.setLayout(new BorderLayout());
		skillsPane.add(leftPane, BorderLayout.WEST);
		skillsPane.add(rightPane, BorderLayout.CENTER);
		skillsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Skills", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		skillsPane.setBackground(character.backgroundColor);
		return skillsPane;
	}

	private JPanel initStatPane() {
		JPanel proficiencyPane = new JPanel(new GridLayout(1, 0));
		proficiencyPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(character.borderColor), "Proficiency Bonus",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		proficiencyPane.add(proficiencyBonus = new JLabel(character.getProficiencyBonus() + ""));

		JPanel inspirationPane = new JPanel(new GridLayout(1, 0));
		inspirationPane.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Inspiration",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		JSpinner inspiration = new JSpinner(new SpinnerNumberModel(character.inspiration, 0, 10, 1));
		inspiration.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				character.inspiration = (int) inspiration.getValue();
			}
		});
		inspirationPane.add(inspiration);

		JPanel passivePerceptionPane = new JPanel(new GridLayout(1, 0));
		passivePerceptionPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(character.borderColor), "Passive Wisdom (Perception)",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		proficiencyPane.setBackground(character.backgroundColor);
		inspirationPane.setBackground(character.backgroundColor);
		passivePerceptionPane.setBackground(character.backgroundColor);

		JPanel statPane = new JPanel(new BorderLayout());
		statPane.add(initStatsPane(), BorderLayout.WEST);

		JPanel statPaneRight = new JPanel();
		statPaneRight.setLayout(new BoxLayout(statPaneRight, BoxLayout.Y_AXIS));
		statPaneRight.add(inspirationPane);
		statPaneRight.add(proficiencyPane);
		statPaneRight.add(passivePerceptionPane);
		statPaneRight.add(initSkillsPane());
		passivePerceptionPane.add(character.getPassivePerception());
		statPaneRight.setBackground(character.backgroundColor);

		statPane.add(statPaneRight, BorderLayout.CENTER);
		statPane.setBackground(character.backgroundColor);
		return statPane;
	}

	private JPanel initWeaponsPane() {
		weaponNamePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Name", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		weaponAmountPane
				.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
						"Amount", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		weaponAttackBonusPane.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Attack Bonus",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		weaponDamagePane.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Damage/Type",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		weaponButtonPane
				.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
						"Info", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		for (Weapon weapon : character.getWeapons()) {
			weaponNamePane.add(weapon.getName(weaponNamePane));
			weaponAmountPane.add(weapon.getAmount(weaponAmountPane));
			weaponAttackBonusPane.add(weapon.getAttack(weaponAttackBonusPane));
			weaponDamagePane.add(weapon.getDamage(weaponDamagePane));
			weaponButtonPane.add(weapon.getButtons(weaponButtonPane));
		}

		JPanel weapons = new JPanel();
		JPanel weaponPane = new JPanel(new BorderLayout());
		weaponPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Weapons", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		weapons.setLayout(new BoxLayout(weapons, BoxLayout.X_AXIS));
		weapons.add(weaponNamePane);
		weapons.add(weaponAmountPane);
		weapons.add(weaponAttackBonusPane);
		weapons.add(weaponDamagePane);
		weapons.add(weaponButtonPane);
		weaponPane.add(weapons, BorderLayout.CENTER);
		weaponPane.add(new NewWeapon(character).open, BorderLayout.NORTH);

		weaponNamePane.setBackground(character.backgroundColor);
		weaponAmountPane.setBackground(character.backgroundColor);
		weaponAttackBonusPane.setBackground(character.backgroundColor);
		weaponDamagePane.setBackground(character.backgroundColor);
		weaponButtonPane.setBackground(character.backgroundColor);
		weapons.setBackground(character.backgroundColor);
		weaponPane.setBackground(character.backgroundColor);
		return weaponPane;

	}

	private JPanel initDescriptionPane() {
		JPanel agePane = new JPanel(new GridLayout());
		JTextField age = new JTextField(character.age);
		age.setEditable(false);
		age.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.age = age.getText();
			}
		});
		agePane.add(age);
		agePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Age",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel heightPane = new JPanel(new GridLayout());
		JTextField height = new JTextField(character.height);
		height.setEditable(false);
		height.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.height = height.getText();
			}
		});
		heightPane.add(height);
		heightPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Height", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel weightPane = new JPanel(new GridLayout());
		JTextField weight = new JTextField(character.weight);
		weight.setEditable(false);
		weight.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.weight = weight.getText();
			}
		});
		weightPane.add(weight);
		weightPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Weight", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel eyesPane = new JPanel(new GridLayout());
		JTextField eyes = new JTextField(character.eyes);
		eyes.setEditable(false);
		eyes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.eyes = eyes.getText();
			}
		});
		eyesPane.add(eyes);
		eyesPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Eyes", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel skinPane = new JPanel(new GridLayout());
		JTextField skin = new JTextField(character.skin);
		skin.setEditable(false);
		skin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.skin = skin.getText();
			}
		});
		skinPane.add(skin);
		skinPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Skin", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel hairPane = new JPanel(new GridLayout());
		JTextField hair = new JTextField(character.hair);
		hair.setEditable(false);
		hair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				character.hair = hair.getText();
			}
		});
		hairPane.add(hair);
		hairPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Hair", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel descriptionPane = new JPanel(new GridLayout(1, 0));
		descriptionPane.add(agePane);
		descriptionPane.add(heightPane);
		descriptionPane.add(weightPane);
		descriptionPane.add(eyesPane);
		descriptionPane.add(skinPane);
		descriptionPane.add(hairPane);
		agePane.setBackground(character.backgroundColor);
		heightPane.setBackground(character.backgroundColor);
		weightPane.setBackground(character.backgroundColor);
		eyesPane.setBackground(character.backgroundColor);
		skinPane.setBackground(character.backgroundColor);
		hairPane.setBackground(character.backgroundColor);
		descriptionPane.setBackground(character.backgroundColor);
		return descriptionPane;
	}

	private void initPersonalityPane() {
		personalityPane.setLayout(new BoxLayout(personalityPane, BoxLayout.Y_AXIS));
		personalityPane.removeAll();
		for (Info trait : character.information.get("personalityTraits")) {
			personalityPane.add(trait.display(personalityPane));
		}
		personalityPane.setBackground(character.backgroundColor);
	}

	private void initIdealsPane() {
		idealsPane.setLayout(new BoxLayout(idealsPane, BoxLayout.Y_AXIS));
		idealsPane.removeAll();
		for (Info trait : character.information.get("ideals")) {
			idealsPane.add(trait.display(idealsPane));
		}
		idealsPane.setBackground(character.backgroundColor);
	}

	private void initBondsPane() {
		bondsPane.setLayout(new BoxLayout(bondsPane, BoxLayout.Y_AXIS));
		bondsPane.removeAll();
		for (Info trait : character.information.get("bonds")) {
			bondsPane.add(trait.display(bondsPane));
		}
		bondsPane.setBackground(character.backgroundColor);
	}

	private void initFlawsPane() {
		flawsPane.setLayout(new BoxLayout(flawsPane, BoxLayout.Y_AXIS));
		flawsPane.removeAll();
		for (Info trait : character.information.get("flaws")) {
			flawsPane.add(trait.display(flawsPane));
		}
		flawsPane.setBackground(character.backgroundColor);
	}

	private void initLanguagesPane() {
		languagesPane.setLayout(new BoxLayout(languagesPane, BoxLayout.Y_AXIS));
		languagesPane.removeAll();
		for (Info trait : character.information.get("languages")) {
			languagesPane.add(trait.display(languagesPane));
		}
		languagesPane.setBackground(character.backgroundColor);
	}

	private void initProficienciesPane() {
		proficienciesPane.setLayout(new BoxLayout(proficienciesPane, BoxLayout.Y_AXIS));
		proficienciesPane.removeAll();
		for (Proficiency proficiency : character.getProficiencies().values()) {
			proficienciesPane.add(proficiency.display(proficienciesPane));
		}
		proficienciesPane.setBackground(character.backgroundColor);
	}

	private void initCharacterNotesPane() {
		characterNotesPane.setLayout(new BoxLayout(characterNotesPane, BoxLayout.Y_AXIS));
		characterNotesPane.removeAll();
		for (Info trait : character.information.get("notes")) {
			characterNotesPane.add(trait.display(characterNotesPane));
		}
		characterNotesPane.setBackground(character.backgroundColor);
	}

	private void initFeaturesPane() {
		featuresPane.setLayout(new BoxLayout(featuresPane, BoxLayout.Y_AXIS));
		featuresPane.removeAll();
		for (Trait feature : character.getFeatures()) {
			featuresPane.add(feature.display(featuresPane));
		}
		featuresPane.setBackground(character.backgroundColor);
	}

	private void initTopPane() {
		JPanel infoPane = new JPanel(new GridLayout(2, 0));
		infoPane.add(character.getCharacterClass().getSubClass().displayName(character));
		infoPane.add(character.getCharacterClass().displayPaths(character));
		infoPane.add(character.getLevelPanel());
		infoPane.add(character.getCharacterName());
		infoPane.add(character.getCharacterBackground());
		infoPane.add(character.getRacePanel());
		infoPane.add(character.getAlignment());
		infoPane.add(character.getExperiencePoints());

		JButton selectorButton = new JButton("Character Selector");
		selectorButton.setFont(Main.titleFont);
		selectorButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.selector.setVisible(true);
				Main.selector.characters = characters;
				dispose();
			}
		});

		JButton backstoryButton = new JButton("Character Backstory");
		backstoryButton.setFont(Main.titleFont);
		backstoryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JTextArea backstory = new JTextArea(30, 90);
				backstory.setText(character.backstory);
				backstory.setLineWrap(true);
				backstory.setWrapStyleWord(true);
				backstory.setMargin(new Insets(3, 5, 5, 5));
				JOptionPane.showMessageDialog(null, backstory, character.characterName + "'s Backstory",
						JOptionPane.PLAIN_MESSAGE);
				character.backstory = backstory.getText();
			}
		});

		JButton characteristicsButton = new JButton("Characteristics");
		characteristicsButton.setFont(Main.titleFont);
		characteristicsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				initPersonalityPane();
				JPanel personalityPane = new JPanel(new BorderLayout());
				personalityPane.add(new AddInfo("New Personality Trait", character.information.get("personalityTraits"),
						personalityPane).open(), BorderLayout.NORTH);
				personalityPane.add(MyCharacter.this.personalityPane, BorderLayout.CENTER);
				personalityPane.setBorder(BorderFactory.createTitledBorder(
						BorderFactory.createLineBorder(character.borderColor), "Personality Traits",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
				personalityPane.setBackground(character.backgroundColor);

				initIdealsPane();
				JPanel idealsPane = new JPanel(new BorderLayout());
				idealsPane.add(new AddInfo("New Ideal", character.information.get("ideals"), idealsPane).open(),
						BorderLayout.NORTH);
				idealsPane.add(MyCharacter.this.idealsPane, BorderLayout.CENTER);
				idealsPane.setBorder(BorderFactory.createTitledBorder(
						BorderFactory.createLineBorder(character.borderColor), "Ideals",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
				idealsPane.setBackground(character.backgroundColor);

				initBondsPane();
				JPanel bondsPane = new JPanel(new BorderLayout());
				bondsPane.add(new AddInfo("New Bond", character.information.get("bonds"), bondsPane).open(),
						BorderLayout.NORTH);
				bondsPane.add(MyCharacter.this.bondsPane, BorderLayout.CENTER);
				bondsPane.setBorder(
						BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Bonds",
								TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
				bondsPane.setBackground(character.backgroundColor);

				initFlawsPane();
				JPanel flawsPane = new JPanel(new BorderLayout());
				flawsPane.add(new AddInfo("New Flaw", character.information.get("flaws"), flawsPane).open(),
						BorderLayout.NORTH);
				flawsPane.add(MyCharacter.this.flawsPane, BorderLayout.CENTER);
				flawsPane.setBorder(
						BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Flaws",
								TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
				flawsPane.setBackground(character.backgroundColor);

				initLanguagesPane();
				JPanel languagesPane = new JPanel(new BorderLayout());
				languagesPane.add(
						new AddInfo("New Language", character.information.get("languages"), languagesPane).open(),
						BorderLayout.NORTH);
				languagesPane.add(MyCharacter.this.languagesPane, BorderLayout.CENTER);
				languagesPane.setBorder(BorderFactory.createTitledBorder(
						BorderFactory.createLineBorder(character.borderColor), "Languages",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
				languagesPane.setBackground(character.backgroundColor);

				JPanel panel = new JPanel();
				panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
				panel.add(personalityPane);
				panel.add(idealsPane);
				panel.add(bondsPane);
				panel.add(flawsPane);
				panel.add(languagesPane);

				JDialog characteristics = new JDialog(MyCharacter.this);
				characteristics.add(panel);
				characteristics.setIconImage(new ImageIcon(Main.logo).getImage());
				// characteristics.setExtendedState(JFrame.MAXIMIZED_BOTH);
				characteristics.pack();
				characteristics.getContentPane().setBackground(character.backgroundColor);
				// characteristics.setAlwaysOnTop(true);
				characteristics.setBackground(character.backgroundColor);
				characteristics.setResizable(false);
				characteristics.setLocationRelativeTo(null);
				characteristics.setVisible(true);
			}
		});

		JPanel buttonPane = new JPanel(new GridLayout(0, 2));
		buttonPane.add(new CharacterEditor(character).open(true));
		buttonPane.add(selectorButton);
		buttonPane.add(backstoryButton);
		buttonPane.add(characteristicsButton);

		JCheckBox advantage = new JCheckBox("Advantage");
		advantage.setFont(Main.titleFont);
		JCheckBox disadvantage = new JCheckBox("Disadvantage");
		disadvantage.setFont(Main.titleFont);

		JPanel diePane = new JPanel(new GridLayout(2, 0));
		diePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Die Rolls", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		diePane.add(advantage);
		diePane.add(new Roll(Die.d20, advantage, disadvantage, "D20", 0));
		diePane.add(new Roll(Die.d12, advantage, disadvantage, "D12", 0));
		diePane.add(new Roll(Die.d10, advantage, disadvantage, "D10", 0));
		diePane.add(disadvantage);
		diePane.add(new Roll(Die.d8, advantage, disadvantage, "D8", 0));
		diePane.add(new Roll(Die.d6, advantage, disadvantage, "D6", 0));
		diePane.add(new Roll(Die.d4, advantage, disadvantage, "D4", 0));

		topPane.setLayout(new BoxLayout(topPane, BoxLayout.X_AXIS));
		topPane.add(buttonPane);
		topPane.add(character.ghost.open(character));
		topPane.add(infoPane);
		topPane.add(diePane);

		buttonPane.setBackground(character.backgroundColor);
		infoPane.setBackground(character.backgroundColor);
		topPane.setBackground(character.backgroundColor);
		advantage.setBackground(character.backgroundColor);
		disadvantage.setBackground(character.backgroundColor);
		diePane.setBackground(character.backgroundColor);
		this.getContentPane().add(topPane, BorderLayout.NORTH);
	}

	private void initLeftPane() {
		leftPane.setLayout(new BoxLayout(leftPane, BoxLayout.Y_AXIS));
		leftPane.add(initStatPane());
		leftPane.setBackground(character.backgroundColor);
		this.getContentPane().add(leftScrollPane, BorderLayout.WEST);
		leftPane.getPreferredSize().width += 20;
	}

	private void initCenterPane() {
		centerPane.setLayout(new BoxLayout(centerPane, BoxLayout.Y_AXIS));

		JPanel initiativePane = new JPanel(new GridLayout());
		initiativePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Initiative", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		JTextField initiative = new JTextField(character.getInitiative() + "", 8);
		initiativePane.add(initiative);
		initiative.setEditable(false);
		initiative.setBackground(character.backgroundColor);
		initiative.setFont(Main.textFont);

		JPanel armorPane = new JPanel(new GridLayout());
		armorPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Armor", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		armorPane.add(new AddArmor(character, true).open());

		JPanel glimmerPane = new JPanel(new GridLayout());
		glimmerPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Glimmer", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		glimmer.setText(character.getGlimmerFormatted());
		glimmer.setFont(Main.textFont);
		glimmer.setEditable(false);
		glimmer.setBackground(character.backgroundColor);
		glimmerPane.add(glimmer);

		JPanel speedPane = new JPanel(new GridLayout(1, 0));
		speedPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Speed", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		for (Speed speedName : character.getRace().getSpeed().keySet()) {
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
					speedName.getName(), TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
					Main.titleFont));
			panel.setBackground(character.backgroundColor);
			JLabel speedLabel = new JLabel(character.getRace().getSpeed().get(speedName).toString());
			speedLabel.setFont(Main.textFont);
			panel.add(speedLabel);
			speedPane.add(panel);
		}

		JPanel armorInitiativeGlimmerPane = new JPanel();
		armorInitiativeGlimmerPane.setLayout(new BoxLayout(armorInitiativeGlimmerPane, BoxLayout.X_AXIS));
		armorInitiativeGlimmerPane.add(armorPane);
		armorInitiativeGlimmerPane.add(initiativePane);
		armorInitiativeGlimmerPane.add(glimmerPane);

		JPanel armorInitiativeSpeedPane = new JPanel();
		armorInitiativeSpeedPane.setLayout(new BoxLayout(armorInitiativeSpeedPane, BoxLayout.Y_AXIS));
		armorInitiativeSpeedPane.add(armorInitiativeGlimmerPane);
		armorInitiativeSpeedPane.add(speedPane);

		JPanel maxHitPointsPane = new JPanel(new GridLayout());
		maxHitPointsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(character.borderColor), "Maximum Hit Points",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		JSpinner maxHitPoints = new JSpinner(new SpinnerNumberModel(character.getMaxHitPoints(), 1, 1000, 1));
		JSpinner currentHitPoints = new JSpinner(
				new SpinnerNumberModel(character.getCurrentHitPoints(), 0, character.getMaxHitPoints(), 1));
		JSpinner temporaryHitPoints = new JSpinner(
				new SpinnerNumberModel(character.getTemporaryHitPoints(), 0, 1000, 1));
		shields = new JSpinner(new SpinnerNumberModel(character.shields, 0,
				character.getCharacterClass().getSubClass().getShield(character.getLevel()), 1));
		shields.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				character.shields = (int) shields.getValue();
			}
		});
		maxHitPointsPane.add(maxHitPoints);
		maxHitPoints.setModel(
				new SpinnerNumberModel((int) maxHitPoints.getValue(), (int) currentHitPoints.getValue(), 1000, 1));
		maxHitPoints.setFont(Main.textFont);
		maxHitPoints.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				currentHitPoints.setModel(new SpinnerNumberModel((int) (Integer) currentHitPoints.getValue(), 0,
						(int) (Integer) maxHitPoints.getValue(), 1));
				((DefaultEditor) currentHitPoints.getEditor()).getTextField().setColumns(10);
				character.setMaxHitPoints((int) maxHitPoints.getValue());
			}
		});

		JPanel currentHitPointsPane = new JPanel(new GridLayout());
		currentHitPointsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(character.borderColor), "Current Hit Points",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		currentHitPointsPane.add(currentHitPoints);
		currentHitPoints.setModel(new SpinnerNumberModel((int) (Integer) currentHitPoints.getValue(), 0,
				(int) (Integer) maxHitPoints.getValue(), 1));
		currentHitPoints.setFont(Main.textFont);
		currentHitPoints.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				maxHitPoints.setModel(new SpinnerNumberModel((int) (Integer) maxHitPoints.getValue(),
						(int) (Integer) currentHitPoints.getValue(), 1000, 1));
				((DefaultEditor) maxHitPoints.getEditor()).getTextField().setColumns(10);

				character.setCurrentHitPoints((int) currentHitPoints.getValue());
			}
		});

		JPanel healthPane1 = new JPanel();
		healthPane1.setLayout(new BoxLayout(healthPane1, BoxLayout.X_AXIS));
		healthPane1.add(maxHitPointsPane);
		healthPane1.add(currentHitPointsPane);
		healthPane1.add(armorInitiativeSpeedPane);

		JPanel temporaryHitPointsPane = new JPanel(new GridLayout());
		temporaryHitPointsPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(character.borderColor), "Temporary Hit Points",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		temporaryHitPointsPane.add(temporaryHitPoints);
		temporaryHitPoints.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				maxHitPoints.setModel(new SpinnerNumberModel((int) (Integer) maxHitPoints.getValue(),
						(int) (Integer) temporaryHitPoints.getValue(), 1000, 1));
				((DefaultEditor) maxHitPoints.getEditor()).getTextField().setColumns(10);
				character.setTemporaryHitPoints((int) temporaryHitPoints.getValue());
			}
		});
		temporaryHitPoints.setFont(Main.textFont);

		JPanel shieldsPane = new JPanel(new GridLayout());
		shieldsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Shield", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		shieldsPane.add(shields);
		shields.setFont(Main.textFont);

		JPanel hitDiePane = new JPanel();
		hitDiePane.setLayout(new BoxLayout(hitDiePane, BoxLayout.Y_AXIS));
		hitDiePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Hit Dice: " + character.getCharacterClass().getSubClass().getHitDie(), TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, Main.titleFont));
		hitDiePane.add(character.getCharacterClass().getHitDice());

		JCheckBox[][] deathSaves = new JCheckBox[2][3];
		deathSaves[0][0] = new JCheckBox();
		deathSaves[0][0].setSelected(character.getDeathSaves()[0][1]);
		deathSaves[0][1] = new JCheckBox();
		deathSaves[0][1].setSelected(character.getDeathSaves()[0][1]);
		deathSaves[0][2] = new JCheckBox();
		deathSaves[0][2].setSelected(character.getDeathSaves()[0][2]);
		deathSaves[1][0] = new JCheckBox();
		deathSaves[1][0].setSelected(character.getDeathSaves()[1][1]);
		deathSaves[1][1] = new JCheckBox();
		deathSaves[1][1].setSelected(character.getDeathSaves()[1][1]);
		deathSaves[1][2] = new JCheckBox();
		deathSaves[1][2].setSelected(character.getDeathSaves()[1][2]);

		JPanel deathSuccessesPane = new JPanel(new GridLayout());
		deathSuccessesPane.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Successes",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		deathSuccessesPane.add(deathSaves[0][0]);
		deathSuccessesPane.add(deathSaves[0][1]);
		deathSuccessesPane.add(deathSaves[0][2]);
		for (Component comp : deathSuccessesPane.getComponents())
			comp.setBackground(character.backgroundColor);

		JPanel deathFailsPane = new JPanel(new GridLayout());
		deathFailsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Fails", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		deathFailsPane.add(deathSaves[1][0]);
		deathFailsPane.add(deathSaves[1][1]);
		deathFailsPane.add(deathSaves[1][2]);

		JPanel deathSavesPane = new JPanel(new GridLayout(1, 0));
		deathSavesPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Death Saves", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		deathSavesPane.add(deathSuccessesPane);
		deathSavesPane.add(deathFailsPane);
		for (Component comp : deathFailsPane.getComponents())
			comp.setBackground(character.backgroundColor);

		JPanel healthPane2 = new JPanel();
		healthPane2.setLayout(new BoxLayout(healthPane2, BoxLayout.X_AXIS));
		healthPane2.add(temporaryHitPointsPane);
		healthPane2.add(shieldsPane);
		healthPane2.add(hitDiePane);
		healthPane2.add(deathSavesPane);

		spellPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Spells", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		spellPane.add(new AddTrait("New Spell", character, character.spells, this.spellsPane, true).open(),
				BorderLayout.NORTH);
		for (Trait spell : character.spells) {
			spellsPane.add(spell.display(spellsPane));
		}
		spellPane.add(spellsPane, BorderLayout.CENTER);

		centerPane.add(healthPane1);
		centerPane.add(healthPane2);
		centerPane.add(initWeaponsPane());

		centerPane.add(spellPane);

		((DefaultEditor) maxHitPoints.getEditor()).getTextField().setColumns(10);
		((DefaultEditor) currentHitPoints.getEditor()).getTextField().setColumns(10);
		((DefaultEditor) temporaryHitPoints.getEditor()).getTextField().setColumns(10);
		((DefaultEditor) shields.getEditor()).getTextField().setColumns(10);

		armorInitiativeGlimmerPane.setBackground(character.backgroundColor);
		armorPane.setBackground(character.backgroundColor);
		initiativePane.setBackground(character.backgroundColor);
		glimmerPane.setBackground(character.backgroundColor);
		speedPane.setBackground(character.backgroundColor);
		maxHitPointsPane.setBackground(character.backgroundColor);
		currentHitPointsPane.setBackground(character.backgroundColor);
		temporaryHitPointsPane.setBackground(character.backgroundColor);
		shieldsPane.setBackground(character.backgroundColor);
		hitDiePane.setBackground(character.backgroundColor);
		deathSuccessesPane.setBackground(character.backgroundColor);
		deathFailsPane.setBackground(character.backgroundColor);
		deathSavesPane.setBackground(character.backgroundColor);
		healthPane1.setBackground(character.backgroundColor);
		healthPane2.setBackground(character.backgroundColor);
		spellPane.setBackground(character.backgroundColor);
		spellsPane.setBackground(character.backgroundColor);
		this.inventoryPane.setBackground(character.backgroundColor);
		centerPane.setBackground(character.backgroundColor);
		this.getContentPane().add(centerScrollPane, BorderLayout.CENTER);
	}

	private void initRightPane() {
		inventoryPane.add(character.getInventory().display((int) (Main.height / 2)));

		initProficienciesPane();
		JPanel proficienciesPane = new JPanel(new BorderLayout());
		proficienciesPane.add(new AddProficiency(character.proficiencies, this.proficienciesPane).open(false),
				BorderLayout.NORTH);
		proficienciesPane.add(this.proficienciesPane, BorderLayout.CENTER);
		proficienciesPane.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor), "Proficiencies",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		proficienciesPane.setBackground(character.backgroundColor);

		initCharacterNotesPane();
		JPanel characterNotesPane = new JPanel(new BorderLayout());
		characterNotesPane.add(
				new AddInfo("New Character Note", character.information.get("notes"), this.characterNotesPane).open(),
				BorderLayout.NORTH);
		characterNotesPane.add(this.characterNotesPane, BorderLayout.CENTER);
		characterNotesPane.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(character.borderColor), "Character Notes",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		characterNotesPane.setBackground(character.backgroundColor);

		initFeaturesPane();
		JPanel featuresPane = new JPanel(new BorderLayout());
		featuresPane.add(new AddTrait("New Feature", character, character.features, this.featuresPane, false).open(),
				BorderLayout.NORTH);
		featuresPane.add(this.featuresPane, BorderLayout.CENTER);
		featuresPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(character.borderColor),
				"Features", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		featuresPane.setBackground(character.backgroundColor);

		rightPane.setLayout(new BoxLayout(rightPane, BoxLayout.Y_AXIS));
		rightPane.add(inventoryPane);
		rightPane.add(proficienciesPane);
		rightPane.add(featuresPane);
		rightPane.add(characterNotesPane);
		rightPane.add(Box.createVerticalGlue());

		proficienciesPane.setBackground(character.backgroundColor);
		characterNotesPane.setBackground(character.backgroundColor);
		rightPane.setBackground(character.backgroundColor);

		this.getContentPane().add(rightScrollPane, BorderLayout.EAST);
	}

	private void initBottomPane() {
		bottomPane.setLayout(new BoxLayout(bottomPane, BoxLayout.Y_AXIS));
		bottomPane.add(initDescriptionPane());

		bottomPane.setBackground(character.backgroundColor);
		this.getContentPane().add(bottomPane, BorderLayout.SOUTH);
	}

	private class Roll extends JButton implements ActionListener {
		private static final long serialVersionUID = 1L;

		private JCheckBox advantage;
		private JCheckBox disadvantage;

		private Die die;
		private int modifier;

		private Roll(Die die, JCheckBox advantage, JCheckBox disadvantage, String check, int modifier) {
			super(check);
			this.setFont(Main.titleFont);
			this.die = die;
			this.advantage = advantage;
			this.disadvantage = disadvantage;
			this.modifier = modifier;
			this.addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == this) {
				if (this.advantage.isSelected() == this.disadvantage.isSelected()) {
					int roll = die.roll();
					if (modifier == 0)
						JOptionPane.showMessageDialog(this, "You rolled " + roll + ".");
					else
						JOptionPane.showMessageDialog(this,
								"You rolled " + roll + " (" + (roll + this.modifier) + ").");
				} else {
					int[] rolls = new int[2];
					rolls[0] = this.die.roll();
					rolls[1] = this.die.roll();
					Arrays.sort(rolls);
					if (this.advantage.isSelected()) {
						JOptionPane.showMessageDialog(this,
								"You rolled " + rolls[0] + " and " + rolls[1] + " (" + rolls[1] + this.modifier + ").");
					} else {
						JOptionPane.showMessageDialog(this,
								"You rolled " + rolls[0] + " and " + rolls[1] + " (" + rolls[0] + this.modifier + ").");
					}
				}
			}
		}

	}
}