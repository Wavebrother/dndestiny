package guardian;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import guardian.Stat.Stats;

public class Skill implements ActionListener, Serializable {
	private static final long serialVersionUID = 1L;
	private Skills skill;
	Entity entity;

	private JButton check = new JButton();
	final JCheckBox proficiency = new JCheckBox();
	final JCheckBox expertise = new JCheckBox();
	private JLabel modifier = new JLabel();

	public Skill(Skills skill, boolean proficiency, boolean expertise, Entity entity) {
		this.skill = skill;
		this.proficiency.setSelected(proficiency);
		this.proficiency.addActionListener(this);
		this.expertise.setSelected(expertise);
		this.expertise.addActionListener(this);
		this.entity = entity;

		this.check.addActionListener(this);
	}

	String getName() {
		return skill.name;
	}

	JPanel displayStats(Entity entity) {
		JPanel panel = new JPanel(new GridLayout(1, 0));
		panel.add(new JLabel("(" + skill.parent.toString().substring(0, 3) + ")"));
		panel.add(this.proficiency);
		panel.add(this.expertise);
		updateModifierLabel();
		panel.add(this.modifier);
		this.modifier.setFont(Main.textFont);
		this.proficiency.setBackground(entity.backgroundColor);
		this.expertise.setBackground(entity.backgroundColor);
		panel.setBackground(entity.backgroundColor);
		panel.add(check);
		return panel;
	}

	int getModifier() {
		int modifier = this.entity.getStats().get(skill.parent).getModifier();
		if (this.proficiency.isSelected())
			modifier += entity.getProficiencyBonus();
		if (this.expertise.isSelected())
			modifier += entity.getProficiencyBonus();
		return modifier;
	}

	void updateModifierLabel() {
		if (this.getModifier() > 0)
			this.modifier.setText("+" + this.getModifier());
		else
			this.modifier.setText(this.getModifier() + "");
		this.modifier.repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.check) {
			int roll = Die.d20.roll();
			StringBuilder sb = new StringBuilder();
			sb.append("You Rolled ");
			sb.append(roll);
			sb.append(" (");
			sb.append(roll + this.getModifier());
			sb.append(") ");
			JOptionPane.showMessageDialog(check, sb.toString());
		} else if (e.getSource() == this.proficiency || e.getSource() == this.expertise) {
			updateModifierLabel();
			this.entity.getPassivePerception();
		}
	}

	public enum Skills implements BaseInterface {
		Acrobatics("Acrobatics", Stats.Dexterity), 
		AnimalHandling("Animal Handling", Stats.Wisdom), 
		Arcana("Arcana", Stats.Intelligence), 
		Athletics("Athletics", Stats.Strength), 
		Deception("Deception", Stats.Charisma), 
		History("History", Stats.Intelligence), 
		Insight("Insight", Stats.Wisdom), 
		Intimidation("Intimidation", Stats.Charisma),
		Investigation("Investigation", Stats.Intelligence), 
		Medicine("Medicine", Stats.Wisdom), 
		Nature("Nature", Stats.Intelligence), 
		Perception("Perception", Stats.Wisdom), 
		Performance("Performance", Stats.Charisma), 
		Persuasion("Persuasion", Stats.Charisma), 
		Religion("Religion", Stats.Intelligence), 
		SleightOfHand("Sleight of Hand", Stats.Dexterity), 
		Stealth("Stealth", Stats.Dexterity),
		Survival("Survival", Stats.Wisdom), 
		Technology("Technology", Stats.Intelligence);
		
		protected final String name;
		protected final Stats parent;

		Skills(String name, Stats parent) {
			this.name = name;
			this.parent = parent;
		}
		
		@Override
		public String toString() {
			return name;
		}
	}

}
