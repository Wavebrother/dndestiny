package guardian;

import java.util.Random;
import java.util.stream.IntStream;

public enum Die {
	d4(4), d6(6), d8(8), d10(10), d12(12), d20(20), d100(100), PERCENTILE(10, 100, 10);
	private Random rand = new Random();

	private final int start;
	public final int max;
	private final int multiplier;

	private Die(int start, int max, int increment) {
		this.multiplier = increment;
		this.start = start / increment;
		this.max = max / increment;
	}

	Die(int start, int max) {
		this(start, max, 1);
	}

	Die(int max) {
		this(1, max);
	}

	public String toString() {
		if (this == PERCENTILE)
			return "Percentile";
		return super.toString();
	}

	public static Die[] classValues() {
		return new Die[] { d6, d8, d10, d12 };
	}

	public static Die[] weaponValues() {
		return new Die[] { d4, d6, d8, d10, d12 };
	}

	public Die increaseDie() {
		switch (this) {
		case d4:
			return d6;
		case d6:
			return d8;
		case d8:
			return d10;
		case d10:
			return d12;
		default:
			return this;
		}
	}

	@SuppressWarnings("unused")
	private Die decreaseDie() {
		switch (this) {
		case d6:
			return d4;
		case d8:
			return d6;
		case d10:
			return d8;
		case d12:
			return d10;
		default:
			return this;
		}
	}

	public int roll() {
		IntStream stream = this.rand.ints(10000000, 0, max);
		int[] rolls = stream.toArray();
		return (rolls[this.rand.nextInt(rolls.length)] + start) * multiplier;
	}

}
