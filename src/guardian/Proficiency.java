package guardian;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Proficiency extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	JTextArea info;
	private BaseInterface proficiency;
	private LinkedHashMap<BaseInterface, Proficiency> list;
	private JPanel parentPanel = new JPanel();
	private JButton delete = new JButton("X");

	Proficiency(BaseInterface proficiency, LinkedHashMap<BaseInterface, Proficiency> proficiencies) {
		super(new BorderLayout());
		this.info = new JTextArea(proficiency.toString());
		this.info.setMargin(new Insets(3, 5, 5, 5));
		this.proficiency = proficiency;
		this.list = proficiencies;
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		if (BaseInterface.getProficiencyList().containsKey(proficiency.toString()))
			info.setEditable(false);
		else {
			info.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent arg0) {
				}

				@Override
				public void keyReleased(KeyEvent arg0) {
					proficiency.setName(info.getText());
				}

				@Override
				public void keyTyped(KeyEvent arg0) {
				}
			});
		}
		this.delete.addActionListener(this);
		this.delete.setForeground(Color.RED);
		this.add(this.info, BorderLayout.CENTER);
		this.add(delete, BorderLayout.EAST);
		this.info.setLineWrap(true);
		this.info.setWrapStyleWord(true);
	}

	JPanel display(JPanel panel) {
		this.parentPanel = panel;
		this.info.setFont(Main.textFont);
		return this;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == delete) {
			this.list.remove(this.proficiency);
			this.parentPanel.remove(this);
		}
		Main.character.getContentPane().revalidate();
		Main.character.getContentPane().repaint();
	}

	public static class CustomProficiency implements BaseInterface {
		private String name;

		CustomProficiency(String name) {
			this.name = name;
		}

		@Override
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public String name() {
			return toString();
		}
	}

}
