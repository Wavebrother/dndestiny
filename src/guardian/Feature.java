package guardian;

public interface Feature extends BaseInterface {
	default boolean hasUpgraded() {
		return getUpgraded() != null;
	}

	default Feature getUpgraded() {
		return null;
	}
	
}
