package guardian;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class Trait extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	JDialog dialog = new JDialog();
	JTextArea trait = new JTextArea();
	String name;

	private ArrayList<Trait> list;
	private JPanel parentPanel = new JPanel();
	private JButton delete = new JButton("X");
	private JButton moveUp = new JButton("\u2191");
	private JButton moveDown = new JButton("\u2193");

	Trait(Feature feature, ArrayList<Trait> list) {
		super(new BorderLayout());
		this.list = list;
		this.setAlignmentX(CENTER_ALIGNMENT);
		trait.setText(feature.getDescription());
		trait.setMargin(new Insets(3, 5, 5, 5));
		trait.setTabSize(2);
		trait.setFont(Main.textFont);
		trait.setLineWrap(true);
		trait.setWrapStyleWord(true);
		this.name = feature.toString();
		JButton open = new JButton(feature.toString());
		dialog = new JDialog(SwingUtilities.getWindowAncestor(Trait.this));
		dialog.setTitle(feature.toString());
		dialog.getContentPane().add(trait);
		dialog.getContentPane().setBackground(Main.backgroundColor);
		dialog.setBackground(Main.backgroundColor);
		dialog.setLocationRelativeTo(open);
		trait.setPreferredSize(new Dimension((int) (Main.width / 3), trait.getPreferredSize().height));
		dialog.pack();
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (dialog == null || !dialog.isVisible()) {
					Dimension dimension = dialog.getSize();
					Point location = dialog.getLocation();
					dialog = new JDialog(SwingUtilities.getWindowAncestor(Trait.this));
					dialog.setSize(dimension);
					dialog.setLocation(location);
					dialog.setTitle(feature.toString());
					dialog.getContentPane().add(trait);
					dialog.getContentPane().setBackground(Main.backgroundColor);
					dialog.setBackground(Main.backgroundColor);
					dialog.setVisible(true);
					dialog.addWindowListener(new java.awt.event.WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent closeEvent) {
						}
					});
				} else {
					dialog.dispose();
				}
			}
		});
		this.add(open);
		this.delete.addActionListener(this);
		this.delete.setForeground(Color.RED);
		this.moveUp.addActionListener(this);
		this.moveUp.setFont(new Font(this.moveUp.getFont().getName(), this.moveUp.getFont().getStyle(),
				(this.moveUp.getFont().getSize() * 3) / 4));
		this.moveDown.addActionListener(this);
		this.moveDown.setFont(new Font(this.moveDown.getFont().getName(), this.moveDown.getFont().getStyle(),
				(this.moveDown.getFont().getSize() * 3) / 4));
		JPanel movePanel = new JPanel(new GridLayout(0, 1));
		movePanel.add(this.moveUp);
		movePanel.add(this.moveDown);
		JPanel buttonPanel = new JPanel(new GridLayout());
		buttonPanel.add(this.delete);
		buttonPanel.add(movePanel);
		this.add(buttonPanel, BorderLayout.EAST);
	}

	JPanel display(JPanel panel) {
		this.parentPanel = panel;
		return this;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == delete) {
			this.list.remove(this);
			this.parentPanel.remove(this);
		} else if (e.getSource() == this.moveUp && this.list.indexOf(this) != 0) {
			Collections.swap(this.list, this.list.indexOf(this), this.list.indexOf(this) - 1);
			Component[] panel = this.parentPanel.getComponents();
			int index = Arrays.asList(panel).indexOf(this) - 1;
			JPanel swappedTrait = (JPanel) panel[index];
			panel[index] = this;
			panel[index + 1] = swappedTrait;
			this.parentPanel.removeAll();
			for (Component component : panel)
				this.parentPanel.add(component);
		} else if (e.getSource() == this.moveDown && this.list.indexOf(this) != this.list.size() - 1) {
			Collections.swap(this.list, this.list.indexOf(this), this.list.indexOf(this) + 1);
			Component[] panel = this.parentPanel.getComponents();
			int index = Arrays.asList(panel).indexOf(this) + 1;
			JPanel swappedTrait = (JPanel) panel[index];
			panel[index] = this;
			panel[index - 1] = swappedTrait;
			this.parentPanel.removeAll();
			for (Component component : panel)
				this.parentPanel.add(component);
		}
		Main.character.getContentPane().revalidate();
		Main.character.getContentPane().repaint();
	}
}
