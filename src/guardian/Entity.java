package guardian;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import guardian.Ghost.GhostBackground;
import guardian.Races.RaceFeature;
import guardian.Skill.Skills;
import guardian.Stat.Stats;
import guardian.classes.CharacterClass;
import guardian.items.CustomArmor;
import guardian.items.Firearm.Firearms;
import guardian.items.Inventory;
import guardian.items.Weapon;
import guardian.items.interfaces.ArmorInterface;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.itemEnums.Armor;

public class Entity {
	public Color backgroundColor = Main.backgroundColor;
	public Color borderColor = new Color(Color.HSBtoRGB(0, .7f, 1));

	private CharacterClass characterClass;
	protected String characterName = new String();
	protected Races race;
	private Background background;

	LinkedHashMap<BaseInterface, Proficiency> proficiencies = new LinkedHashMap<BaseInterface, Proficiency>();
	ArrayList<Trait> features = new ArrayList<Trait>();
	private final Inventory inventory;
	private final ArrayList<Weapon> weapons = new ArrayList<Weapon>();
	final ArrayList<Trait> spells = new ArrayList<Trait>();
	int maxHitPoints = 1;
	int currentHitPoints = 1;
	int temporaryHitPoints = 0;
	int shields = 0;
	// Successes, Failures
	private final boolean[][] deathSaves;

	public ArmorInterface armor = new CustomArmor("Shitty Armor", "A pile of shitty armor", Armor.ArmorLevel.LIGHT, 10,
			10, 0, 1, 0, false, null);
	int initiativeBonus;

	private final Map<Stats, Stat> stats = new LinkedHashMap<Stats, Stat>();
	private final Map<Skills, Skill> skills = new LinkedHashMap<Skills, Skill>();
	boolean advancedSheet;

	private JLabel passivePerception = new JLabel();

	public Entity(String characterName, Inventory inventory, ArrayList<Trait> spells, ArrayList<Trait> features,
			LinkedHashMap<BaseInterface, Proficiency> proficiencies, int maxHitPoints, int currentHitPoints,
			int temporaryHitPoints, int shields, boolean[][] deathSaves, ArmorInterface armor, int initiativeBonus,
			Map<Stats, Stat> stats, Map<Skills, Skill> skills, boolean advancedSheet) {
		this.inventory = inventory;
		this.inventory.init(this.backgroundColor, this.borderColor, this);
		this.characterName = characterName;

		this.spells.addAll(spells);
		this.features.addAll(features);
		this.proficiencies = proficiencies;
		this.maxHitPoints = maxHitPoints;
		this.currentHitPoints = currentHitPoints;
		this.temporaryHitPoints = temporaryHitPoints;
		this.shields = shields;
		this.deathSaves = deathSaves;

		this.armor = armor;
		this.initiativeBonus = initiativeBonus;

		this.stats.putAll(stats);
		this.skills.putAll(skills);
		this.advancedSheet = advancedSheet;
	}

	protected Entity() {
		inventory = new Inventory("Guardian", 0, true);
		inventory.init(backgroundColor, borderColor, this);
		deathSaves = new boolean[2][3];
	}

	protected Entity(Entity character) {
		this(character.characterName, character.getInventory(), character.spells, character.features,
				character.proficiencies, character.maxHitPoints, character.currentHitPoints,
				character.temporaryHitPoints, character.shields, character.getDeathSaves(), character.armor,
				character.initiativeBonus, character.getStats(), character.getSkills(), character.advancedSheet);
		for (Stat stat : getStats().values()) {
			stat.entity = this;
		}
		for (Skill skill : getSkills().values()) {
			skill.entity = this;
		}
		for (Weapon weapon : character.getWeapons()) {
			this.weapons.add(weapon);
			weapon.entity = this;
		}
	}

	public CharacterClass getCharacterClass() {
		return characterClass;
	}

	public void setCharacterClass(CharacterClass characterClass) {
		this.characterClass = characterClass;
	}

	void setHealthStats(int initiativeBonus) {
		this.initiativeBonus = initiativeBonus;
	}

	void setStats(Map<Stats, JSpinner[]> stats, boolean ghost) {
		for (Stats stat : stats.keySet()) {
			if (!ghost && characterClass.getSubClass().getSavingThrows().contains(stat))
				this.stats.put(stat, new Stat(stat, (int) stats.get(stat)[0].getValue(),
						(int) stats.get(stat)[1].getValue(), true, this));
			else
				this.stats.put(stat, new Stat(stat, (int) stats.get(stat)[0].getValue(),
						(int) stats.get(stat)[1].getValue(), false, this));
			this.stats.get(stat).raceModifier = (int) stats.get(stat)[2].getValue();
		}
	}

	void setSkills(Map<Skills, JCheckBox[]> skills) {
		for (Skills skill : skills.keySet())
			this.skills.put(skill,
					new Skill(skill, skills.get(skill)[0].isSelected(), skills.get(skill)[1].isSelected(), this));
	}

	Races getRace() {
		return race;
	}

	JPanel getCharacterName() {
		JPanel panel = new JPanel(new GridLayout());
		JTextField characterNameComponent = new JTextField(characterName);
		characterNameComponent.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				characterName = characterNameComponent.getText();
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		panel.add(characterNameComponent);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Character Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		characterNameComponent.setFont(Main.textFont);
		return panel;
	}

	JPanel getCharacterBackground() {
		JPanel panel = new JPanel(new GridLayout());
		if (this instanceof Guardian) {
			JTextField backgroundComponent = new JTextField(getBackground().toString());
			backgroundComponent.setEditable(false);
			panel.add(backgroundComponent);
			backgroundComponent.setFont(Main.textFont);
		} else {
			JComboBox<Background> backgroundComponent = new JComboBox<Background>(GhostBackground.values());
			backgroundComponent.setSelectedItem(getBackground());
			backgroundComponent.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					setBackground((Background) backgroundComponent.getSelectedItem());
					backgroundComponent.setToolTipText(getBackground().getDescription());
				}
			});
			backgroundComponent.setEditable(false);
			panel.add(backgroundComponent);
			backgroundComponent.setFont(Main.textFont);
		}
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Background",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		return panel;
	}

	public Background getBackground() {
		return background;
	}

	public void setBackground(Background background) {
		this.background = background;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public ArrayList<Trait> getFeatures() {
		ArrayList<Trait> features = new ArrayList<Trait>();
		features.addAll(this.features);
		if (advancedSheet) {
			for (Feature feature : getCharacterClass().getSubClass().getFeatures(20))
				features.add(new Trait(feature, features));
		}
		return features;
	}

	public LinkedHashMap<BaseInterface, Proficiency> getProficiencies() {
		LinkedHashMap<BaseInterface, Proficiency> proficiencies = new LinkedHashMap<BaseInterface, Proficiency>();
		proficiencies.putAll(this.proficiencies);
		if (advancedSheet) {
			for (BaseInterface proficiency : getCharacterClass().getSubClass().getProficiencies())
				proficiencies.put(proficiency, new Proficiency(proficiency, proficiencies));
			if (race.getFeatures().contains(RaceFeature.WEAPONFAMILIARITY))
				proficiencies.put(Firearms.Sidearm, new Proficiency(Firearms.Sidearm, proficiencies));
		}
		return proficiencies;
	}

	public boolean hasProficiency(WeaponInterface weapon) {
		if (getProficiencies().containsKey(weapon))
			return true;
		if (getProficiencies().containsKey(weapon.getWeaponType()))
			return true;
		return false;
	}

	public boolean hasProficiency(ArmorInterface armor) {
		return getProficiencies().containsKey(armor.getLevel());
	}

	public boolean hasProficiency(BaseInterface proficiency) {
		return getProficiencies().containsKey(proficiency);
	}

	public ArrayList<Weapon> getWeapons() {
		return weapons;
	}

	public ArrayList<Trait> getSpells() {
		return spells;
	}

	public LinkedHashSet<Language> getLanguages() {
		LinkedHashSet<Language> languages = new LinkedHashSet<Language>();
		languages.addAll(getRace().getLanguages());
		return languages;
	}

	public int getMaxHitPoints() {
		int maxHitPoints = this.maxHitPoints;
		if (advancedSheet) {
			maxHitPoints += getCharacterClass().getSubClass().getHitDie().max;
			maxHitPoints += (getLevel()) * stats.get(Stats.Constitution).getModifier();
		}
		return maxHitPoints;
	}

	public void setMaxHitPoints(int maxHitPoints) {
		this.maxHitPoints = maxHitPoints - (getMaxHitPoints() - this.maxHitPoints);
	}

	public int getCurrentHitPoints() {
		return currentHitPoints;
	}

	public void setCurrentHitPoints(int currentHitPoints) {
		this.currentHitPoints = currentHitPoints;
	}

	public int getTemporaryHitPoints() {
		return temporaryHitPoints;
	}

	public void setTemporaryHitPoints(int temporaryHitPoints) {
		this.temporaryHitPoints = temporaryHitPoints;
	}

	public boolean[][] getDeathSaves() {
		return deathSaves;
	}

	public int getArmorClass() {
		int ac = armor.getBaseAC();
		if ((ac += getStats().get(Stats.Dexterity).getModifier()) >= armor.getMaxAC() && armor.getMaxAC() > 0)
			return armor.getMaxAC();
		return ac;
	}

	int getInitiative() {
		return this.getStats().get(Stats.Dexterity).getModifier() + this.initiativeBonus;
	}

	public int getLevel() {
		return 0;
	}

	JLabel getPassivePerception() {
		passivePerception.setText((10 + this.getSkills().get(Skills.Perception).getModifier()) + "");
		passivePerception.repaint();
		return passivePerception;
	}

	public Map<Stats, Stat> getStats() {
		return stats;
	}

	public Map<Skills, Skill> getSkills() {
		return skills;
	}

	public int getProficiencyBonus() {
		return 0;
	}

	public interface Background extends BaseInterface {
		String getDescription();

		void setSkills(Entity entity);
	}

	public enum Alignment {
		LAWFUL_GOOD, LAWFUL_NEUTRAL, LAWFUL_EVIL, NEUTRAL_GOOD, NEUTRAL, NEUTRAL_EVIL, CHAOTIC_GOOD, CHAOTIC_NEUTRAL,
		CHAOTIC_EVIL;
		static Alignment[] getLawful() {
			Alignment[] lawful = { LAWFUL_GOOD, LAWFUL_NEUTRAL, LAWFUL_EVIL };
			return lawful;
		}

		static Alignment[] getNeutral_() {
			Alignment[] lawful = { NEUTRAL_GOOD, NEUTRAL, NEUTRAL_EVIL };
			return lawful;
		}

		static Alignment[] getChaotic() {
			Alignment[] lawful = { CHAOTIC_GOOD, CHAOTIC_NEUTRAL, CHAOTIC_EVIL };
			return lawful;
		}

		static Alignment[] getGood() {
			Alignment[] lawful = { LAWFUL_GOOD, NEUTRAL_GOOD, CHAOTIC_GOOD };
			return lawful;
		}

		static Alignment[] get_Neutral() {
			Alignment[] lawful = { LAWFUL_NEUTRAL, NEUTRAL, CHAOTIC_NEUTRAL };
			return lawful;
		}

		static Alignment[] getEvil() {
			Alignment[] lawful = { LAWFUL_EVIL, NEUTRAL_EVIL, CHAOTIC_EVIL };
			return lawful;
		}

		@Override
		public String toString() {
			switch (this) {
			case NEUTRAL:
				return "True Neutral";
			default:
				return super.toString().substring(0, 1)
						+ super.toString().substring(1, super.toString().indexOf("_")).toLowerCase() + " "
						+ super.toString().substring(super.toString().indexOf("_") + 1,
								super.toString().indexOf("_") + 2)
						+ super.toString().substring(super.toString().indexOf("_") + 2).toLowerCase();
			}
		}
	}

	public enum Speed {
		WALKING, SWIMMING, FLYING;

		public String getName() {
			switch (this) {
			case WALKING:
				return "Walking";
			case SWIMMING:
				return "Swimming";
			case FLYING:
				return "Flying";
			}
			return null;
		}
	}

}
