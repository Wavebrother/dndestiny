package guardian.classes;

import guardian.BaseInterface;

public interface CharacterSubClassPath extends BaseInterface {
	public enum SubClassPaths implements CharacterSubClassPath {
		THE_SHARPSHOOTER("The Sharpshooter"), THE_OUTLAW("The Outlaw"), THE_SHOWMAN("The Showman"),
		WAY_OF_THE_SWIFTCUTTER("Way of the Swiftcutter"), WAY_OF_THE_ARCSTRIDER("Way of the Arcstrider"),
		WAY_OF_THE_WHISPER("Way of the Whisper"), THE_PATHFINDER("The Pathfinder"), THE_BOWFIGHTER("The Bowfighter"),
		THE_WRAITH("The Wraith"), THE_VANGUARD("The Vanguard"), THE_GLADIATOR("The Gladiator"),
		CALLING_OF_THE_SHIELD("Calling of the Shield"), CALLING_OF_THE_LIGHT("Calling of the Light"),
		CALLING_OF_THE_SENTINEL("Calling of the Sentinel"), CODE_OF_THE_DEVASTATOR("Code of the Devastator"),
		CODE_OF_THE_SIEGEBREAKER("Code of the Siegebreaker"), CODE_OF_THE_FORGEMASTER("Code of the Forgemaster"),
		THE_HARBINGER_OF_DESTRUCTION("The Harbinger of Destruction"),
		THE_HARBINGER_OF_KNOWLEDGE("The Harbinger of Knowledge"), THE_HARBINGER_OF_MADNESS("The Harbinger of Madness"),
		BALLAD_OF_THE_PHOENIX("Ballad of the Phoenix"), BALLAD_OF_THE_PACK("Ballad of the Pack"),
		BALLAD_OF_THE_VALKYRIE("Ballad of the Valkyrie"), MASTER_OF_THUNDER("Master of Thunder"),
		MASTER_OF_WIND("Master of Wind"), MASTER_OF_LIGHTNING("Master of Lightning");

		String name;

		SubClassPaths(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}
}
