package guardian.classes;

public enum CharacterSuperClass {
	HUNTER, TITAN, WARLOCK;

	@Override
	public String toString() {
		return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
	}

	String getJump() {
		switch (this) {
		case HUNTER:
			return "Hunters can make a long jump or a high jump in a direction of their choosing using their Light ability modifier to calculate their airborne jump instead of Strength.";
		case TITAN:
			return "When making a jump boosted by their Light, Titans can move a number of feet equal to 5 x their Light ability modifier in the same direction as their initial jump.";
		case WARLOCK:
			return "Warlocks jumping with their Light can move a number of feet equal to 5 x their Light ability modifier. Changing direction, either from their initial jump or during their jump, costs 5 additional feet of movement.\r\n"
					+ "\tWhile you are airborne, you have disadvantage on attack rolls, and creatures have advantage on their attack rolls against you.";
		default:
			return "";
		}
	}

	String getFeature() {
		switch (this) {
		case HUNTER:
			return "When you take the Dodge action, you can reload your currently equipped firearm with the same action.";
		case TITAN:
			return "Your unarmed strikes deal 1d4 + your Strength modifier in bludgeoning damage.";
		case WARLOCK:
			return "You have advantage on saving throws against Darkness effects.";
		default:
			return "";
		}
	}
}
