package guardian.classes;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import guardian.BaseInterface;
import guardian.Die;
import guardian.Entity;
import guardian.Feature;
import guardian.Main;
import guardian.Spell.CastingTime;
import guardian.Stat.Stats;
import guardian.classes.CharacterSubClassPath.SubClassPaths;
import guardian.items.Firearm.Firearms;
import guardian.items.Tool.Tools;
import guardian.items.Vehicle.Vehicles.VehicleType;
import guardian.items.Weapon.Weapons;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.itemEnums.Armor.ArmorLevel;

public interface CharacterSubClass extends BaseInterface {
	JPanel displayName(Entity entity);

	ArrayList<Stats> getSavingThrows();

	ArrayList<Feature> getFeatures(int guardianLevel);

	default LinkedHashSet<BaseInterface> getProficiencies() {
		return new LinkedHashSet<BaseInterface>();
	}

	String getDescription();

	Die getHitDie();

	Grenade[] getGrenades(int level);

	int getShield(int level);

	Stats getLightAbility();

	CharacterSubClassPath[] getPaths();

	public enum SubClasses implements CharacterSubClass {
		GUNSLINGER("", new Stats[] { Stats.Dexterity, Stats.Charisma }),
		BLADEDANCER("", new Stats[] { Stats.Dexterity, Stats.Intelligence }),
		NIGHTSTALKER("", new Stats[] { Stats.Strength, Stats.Dexterity }),
		STRIKER("", new Stats[] { Stats.Strength, Stats.Constitution }),
		DEFENDER("", new Stats[] { Stats.Constitution, Stats.Charisma }),
		SUNBREAKER("", new Stats[] { Stats.Strength, Stats.Wisdom }),
		VOIDWALKER("", new Stats[] { Stats.Intelligence, Stats.Charisma }),
		SUNSINGER("", new Stats[] { Stats.Constitution, Stats.Charisma }),
		STORMCALLER("", new Stats[] { Stats.Dexterity, Stats.Wisdom });

		final String description;
		final ArrayList<Stats> savingThrows = new ArrayList<Stats>();

		SubClasses(String description, Stats[] savingThrows) {
			this.description = description;
			for (Stats stat : savingThrows)
				this.savingThrows.add(stat);
		}

		@Override
		public ArrayList<Stats> getSavingThrows() {
			return savingThrows;
		}

		@Override
		public String toString() {
			return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
		}

		@Override
		public ArrayList<Feature> getFeatures(int guardianLevel) {
			LinkedHashMap<SubClassFeature, Integer> classFeatures = new LinkedHashMap<SubClassFeature, Integer>();
			switch (this) {
			case GUNSLINGER:
				classFeatures.put(SubClassFeature.Gunslingers_Trance, 1);
				classFeatures.put(SubClassFeature.Weaponized_Personality, 1);
				classFeatures.put(SubClassFeature.Fancy_Shooting, 2);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Additional_Trick_Shots, 7);
				classFeatures.put(SubClassFeature.Volley, 10);
				classFeatures.put(SubClassFeature.Flow_Caster, 15);
				classFeatures.put(SubClassFeature.Crowd_Pleaser, 18);
				break;
			case BLADEDANCER:
				classFeatures.put(SubClassFeature.Expertise, 1);
				classFeatures.put(SubClassFeature.Expert_Attack, 1);
				classFeatures.put(SubClassFeature.Cunning_Action, 2);
				classFeatures.put(SubClassFeature.Opportunity_Strikes, 5);
				classFeatures.put(SubClassFeature.EXPERTISE_LV6, 6);
				classFeatures.put(SubClassFeature.Evasion, 7);
				classFeatures.put(SubClassFeature.Reliable_Talent, 11);
				classFeatures.put(SubClassFeature.Blindsense, 14);
				classFeatures.put(SubClassFeature.Slippery_Mind, 15);
				classFeatures.put(SubClassFeature.Elusive, 18);
				classFeatures.put(SubClassFeature.Stroke_of_Luck, 20);
				break;
			case NIGHTSTALKER:
				classFeatures.put(SubClassFeature.Nightstalkers_Focus, 1);
				classFeatures.put(SubClassFeature.Born_of_the_Wild, 1);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Draw_From_the_Void, 6);
				classFeatures.put(SubClassFeature.Subterfuge, 10);
				classFeatures.put(SubClassFeature.Sure_Strikes, 11);
				classFeatures.put(SubClassFeature.Vanish, 14);
				classFeatures.put(SubClassFeature.Feral_Senses, 18);
				classFeatures.put(SubClassFeature.Lucid_Hunter, 20);
				break;
			case STRIKER:
				classFeatures.put(SubClassFeature.Brawler, 1);
				classFeatures.put(SubClassFeature.Reckless_Attack, 1);
				classFeatures.put(SubClassFeature.Danger_Sense, 2);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Fast_Movement, 5);
				classFeatures.put(SubClassFeature.Reversal, 7);
				classFeatures.put(SubClassFeature.Enduring_Havoc, 11);
				classFeatures.put(SubClassFeature.Fight_Response, 15);
				classFeatures.put(SubClassFeature.Indomitable_Might, 18);
				classFeatures.put(SubClassFeature.Bring_the_Thunder, 20);
				break;
			case DEFENDER:
				classFeatures.put(SubClassFeature.Selfless_Defense, 1);
				classFeatures.put(SubClassFeature.Defenders_Voice, 1);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Aura_of_Protection, 6);
				classFeatures.put(SubClassFeature.Aura_of_Courage, 10);
				classFeatures.put(SubClassFeature.Selfless_Defense_Improvement, 14);
				classFeatures.put(SubClassFeature.Aura_of_Protection_Improvement, 18);
				classFeatures.put(SubClassFeature.Aura_of_Courage_Improvement, 18);
				break;
			case SUNBREAKER:
				classFeatures.put(SubClassFeature.Detect_Light_and_Dark, 1);
				classFeatures.put(SubClassFeature.Inner_Flame, 2);
				classFeatures.put(SubClassFeature.CHANNEL_LIGHT_TITAN, 3);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Sculpt_Light, 6);
				classFeatures.put(SubClassFeature.Improved_Inner_Flame, 11);
				classFeatures.put(SubClassFeature.Titans_Defense, 15);
				break;
			case VOIDWALKER:
				classFeatures.put(SubClassFeature.Depths_of_the_Void, 1);
				classFeatures.put(SubClassFeature.Invocations_of_the_Void, 2);
				classFeatures.put(SubClassFeature.Field_of_Study, 3);
				classFeatures.put(SubClassFeature.Sculpt_Light, 6);
				classFeatures.put(SubClassFeature.Enhanced_Lightcasting, 11);
				classFeatures.put(SubClassFeature.Arcane_Protection, 14);
				classFeatures.put(SubClassFeature.Master_of_the_Void, 20);
				break;
			case SUNSINGER:
				classFeatures.put(SubClassFeature.Channel_Light_WARLOCK, 2);
				classFeatures.put(SubClassFeature.Sculpt_Light, 3);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Patron_of_the_Arts, 6);
				classFeatures.put(SubClassFeature.Channel_Light_2_Rest_, 6);
				classFeatures.put(SubClassFeature.Solar_Prominence, 11);
				classFeatures.put(SubClassFeature.Heliocentric, 14);
				classFeatures.put(SubClassFeature.Channel_Light_3_Rest_, 18);
				classFeatures.put(SubClassFeature.The_Light_in_the_Dark, 20);
				break;
			case STORMCALLER:
				classFeatures.put(SubClassFeature.Harmony_Within, 1);
				classFeatures.put(SubClassFeature.Arc_Charge, 1);
				classFeatures.put(SubClassFeature.Extra_Attack, 5);
				classFeatures.put(SubClassFeature.Faraday_Cage, 6);
				classFeatures.put(SubClassFeature.Mind_Over_Energy, 10);
				classFeatures.put(SubClassFeature.Elementary_Particles, 14);
				classFeatures.put(SubClassFeature.Boundless_Energy, 20);
				break;
			}
			ArrayList<Feature> features = new ArrayList<Feature>();
			for (SubClassFeature feature : classFeatures.keySet()) {
				feature.addToList(classFeatures.get(feature), guardianLevel, features);
			}
			return features;
		}

		@Override
		public LinkedHashSet<BaseInterface> getProficiencies() {
			LinkedHashSet<BaseInterface> proficiencies = new LinkedHashSet<BaseInterface>();
			switch (this) {
			case GUNSLINGER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(Tools.WEAPONSMITHING_TOOLS);
				break;
			case BLADEDANCER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(Weapons.LONGSWORD);
				proficiencies.add(Weapons.SHORTSWORD);
				proficiencies.add(Firearms.Sniper_Rifle);
				proficiencies.add(Firearms.Fusion_Rifle);
				proficiencies.add(Firearms.Linear_Fusion_Rifle);
				proficiencies.add(Tools.THIEVES_TOOLS);
				break;
			case NIGHTSTALKER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(WeaponType.MARTIAL_MELEE);
				proficiencies.add(Firearms.Combat_Bow);
				proficiencies.add(Firearms.Sniper_Rifle);
				proficiencies.add(Firearms.Fusion_Rifle);
				proficiencies.add(Firearms.Linear_Fusion_Rifle);
				break;
			case STRIKER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(ArmorLevel.HEAVY);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(WeaponType.MARTIAL_MELEE);
				proficiencies.add(WeaponType.MARTIAL_RANGED);
				break;
			case DEFENDER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(ArmorLevel.HEAVY);
				proficiencies.add(ArmorLevel.SHIELD);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(WeaponType.MARTIAL_MELEE);
				proficiencies.add(WeaponType.MARTIAL_RANGED);
				proficiencies.add(Tools.MEDICAL_SUPPLIES);
				break;
			case SUNBREAKER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(ArmorLevel.HEAVY);
				proficiencies.add(ArmorLevel.SHIELD);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(WeaponType.MARTIAL_MELEE);
				proficiencies.add(WeaponType.MARTIAL_RANGED);
				proficiencies.add(Tools.ARMORSMITHING_TOOLS);
				break;
			case VOIDWALKER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				break;
			case SUNSINGER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(WeaponType.MARTIAL_MELEE);
				proficiencies.add(WeaponType.MARTIAL_RANGED);
				proficiencies.add(Tools.MEDICAL_SUPPLIES);
				break;
			case STORMCALLER:
				proficiencies.add(ArmorLevel.LIGHT);
				proficiencies.add(ArmorLevel.MEDIUM);
				proficiencies.add(WeaponType.SIMPLE_MELEE);
				proficiencies.add(WeaponType.SIMPLE_RANGED);
				proficiencies.add(WeaponType.MARTIAL_MELEE);
				proficiencies.add(Firearms.Combat_Bow);
				proficiencies.add(Firearms.Fusion_Rifle);
				proficiencies.add(Firearms.Shotgun);
				break;
			default:
				break;
			}
			proficiencies.add(VehicleType.JUMPSHIP);
			proficiencies.add(VehicleType.SPARROW);
			return proficiencies;
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public Die getHitDie() {
			switch (this) {
			case GUNSLINGER:
			case VOIDWALKER:
				return Die.d6;
			case BLADEDANCER:
			case NIGHTSTALKER:
			case STRIKER:
			case SUNBREAKER:
			case STORMCALLER:
				return Die.d8;
			case DEFENDER:
			case SUNSINGER:
				return Die.d10;
			}
			return null;
		}

		@Override
		public Grenade[] getGrenades(int level) {
			ArrayList<Grenade> grenadeList = new ArrayList<Grenade>();
			Grenade[] subClassGrenades = new Grenade[3];
			switch (this) {
			case GUNSLINGER:
				subClassGrenades[0] = Grenade.SWARM;
				subClassGrenades[1] = Grenade.TRIPMINE;
				subClassGrenades[2] = Grenade.INCENDIARY;
				break;
			case BLADEDANCER:
				subClassGrenades[0] = Grenade.SKIP;
				subClassGrenades[1] = Grenade.FLUX;
				subClassGrenades[2] = Grenade.ARCBOLT;
				break;
			case NIGHTSTALKER:
				subClassGrenades[0] = Grenade.SPIKE;
				subClassGrenades[1] = Grenade.VOIDWALL;
				subClassGrenades[2] = Grenade.VORTEX;
				break;
			case STRIKER:
				subClassGrenades[0] = Grenade.LIGHTNING;
				subClassGrenades[1] = Grenade.PULSE;
				subClassGrenades[2] = Grenade.FLASHBANG;
				break;
			case DEFENDER:
				subClassGrenades[0] = Grenade.SPIKE;
				subClassGrenades[1] = Grenade.MAGNETIC;
				subClassGrenades[2] = Grenade.SUPPRESSOR;
				break;
			case SUNBREAKER:
				subClassGrenades[0] = Grenade.FUSION;
				subClassGrenades[1] = Grenade.THERMITE;
				subClassGrenades[2] = Grenade.INCENDIARY;
				break;
			case VOIDWALKER:
				subClassGrenades[0] = Grenade.SCATTER;
				subClassGrenades[1] = Grenade.AXIONBOLT;
				subClassGrenades[2] = Grenade.VORTEX;
				break;
			case SUNSINGER:
				subClassGrenades[0] = Grenade.FUSION;
				subClassGrenades[1] = Grenade.FIREBOLT;
				subClassGrenades[2] = Grenade.SOLAR;
				break;
			case STORMCALLER:
				subClassGrenades[0] = Grenade.LIGHTNING;
				subClassGrenades[1] = Grenade.PULSE;
				subClassGrenades[2] = Grenade.STORM;
				break;
			}
			if (level >= 4)
				grenadeList.add(subClassGrenades[0]);
			if (level >= 6)
				grenadeList.add(subClassGrenades[1]);
			if (level >= 8)
				grenadeList.add(subClassGrenades[2]);
			Grenade[] grenades = new Grenade[grenadeList.size()];
			for (int i = 0; i < grenadeList.size(); i++)
				grenades[i] = grenadeList.get(i);
			return grenades;
		}

		public Melee getMelee() {
			switch (this) {
			case GUNSLINGER:
				return Melee.THROWINGKNIFE;
			case BLADEDANCER:
				return Melee.BLINKSTRIKE;
			case NIGHTSTALKER:
				return Melee.SMOKE;
			case STRIKER:
				return Melee.STORMFIST;
			case DEFENDER:
				return Melee.DISINTEGRATE;
			case SUNBREAKER:
				return Melee.SOLARSTRIKE;
			case VOIDWALKER:
				return Melee.ENERGYDRAIN;
			case SUNSINGER:
				return Melee.SCORCH;
			case STORMCALLER:
				return Melee.THUNDERSTRIKE;
			default:
				return null;
			}
		}

		private enum Melee implements BaseInterface {
			THROWINGKNIFE, BLINKSTRIKE, SMOKE, STORMFIST, DISINTEGRATE, SOLARSTRIKE, ENERGYDRAIN, SCORCH, THUNDERSTRIKE;

			@Override
			public String getDescription() {
				switch (this) {
				case THROWINGKNIFE:
					return "You craft a throwing knife with the properties and damage listed. As part of the same action, you can choose to make an attack with it. If you choose not to attack with this weapon, you can hold onto it for a duration of up to 1 minute, during which you must make concentration checks to maintain the form of the throwing knife. So long as you keep your concentration, your throwing knife can be used in a number of ways beyond attacking, such as to cut through rope or to whittle wood. If you make an attack against a creature with it, if you lose concentration, or if the duration runs out, your throwing knife dissipates. \r\n"
							+ "If you have the option to attack multiple times with the Attack action, this ability can replace one or more of your attacks.";
				case BLINKSTRIKE:
					return "When you make an attack against a hostile creature with a melee weapon you are holding in one hand or an unarmed strike, you can spend a melee ability charge to make your melee attack a Blink Strike. If you do, you can teleport up to 5 feet in order to reach your opponent as part of your melee attack, taking all carried and worn equipment with you. If your attack hits, it deals arc damage instead of its normal damage, and has a bonus to damage equal to your Light ability level if it did not already. Blink Strike movement does not cost movement on your turn.";
				case SMOKE:
					return "You spend 1 melee ability charge to create and hurl a compacted puck of void smoke at a spot you can see within range. On impact it breaks apart and releases an opaque cloud, and all creatures within 5 feet of the spot must make a Wisdom saving throw. On a failed save they are confused by the smoke and are blinded until the end of their next turn.\r\n"
							+ "If you have the option to attack multiple times with the Attack action, this ability can replace one or more of your attacks.";
				case STORMFIST:
					return "When you make an unarmed strike, you can spend a melee ability to make it a Storm Fist attack. If you do, your attack deals arc damage instead of bludgeoning, and you can add your Light ability level to the damage of your attack. You can choose to do this after your attack roll, but before you make your damage roll.";
				case DISINTEGRATE:
					return "When you make an unarmed strike, you can spend 1 melee ability to make it a Disintegrate attack. If you do, your unarmed strike deals void damage, and you can add a bonus to the damage of your unarmed strike equal to your Light ability level. You can choose to do this after the attack roll is made, but before you roll damage.";
				case SOLARSTRIKE:
					return "When you make an unarmed strike, you can spend 1 melee ability to make it a Solar Strike attack. On a hit, you deal solar damage instead of bludgeoning, and you can add a bonus to your damage roll equal to your Light ability level. You can choose to do this after your attack roll, but before you make your damage roll.";
				case ENERGYDRAIN:
					return "When you make an unarmed strike, you can spend 1 melee ability to make it an Energy Drain attack. If you do, instead of making an attack roll, your target has to make a Dexterity saving throw, with a bonus to your DC equal to your Light ability level. On a failed save, they take 1d4 + your Intelligence modifier + your Light ability level in void damage. On a success, they take half as much.";
				case SCORCH:
					return "When you make an unarmed strike, you can spend 1 melee ability to make it a Scorch attack. If you do, your target must make a Dexterity saving throw, with a bonus to your DC equal to your Light ability level. On a failed save, they take 1d4 + your Charisma modifier + your Light ability level in solar damage. On a success, they take half as much.";
				case THUNDERSTRIKE:
					return "When you make an unarmed strike, you can spend 1 melee ability to make it a Thunderstrike attack. If you do, your target must make a Dexterity saving throw against your Light save DC, with a bonus to your DC equal to your Light ability level. They take 1d8 + your Wisdom modifier + your Light ability level in arc damage on a failed save, or half as much on a success.";
				default:
					return "";
				}
			}

			public CastingTime getCastingTime() {
				switch (this) {
				case THROWINGKNIFE:
				case SMOKE:
				case ENERGYDRAIN:
					return CastingTime.ACTION;
				default:
					return CastingTime.FREEACTION;
				}
			}

		}

		@Override
		public int getShield(int level) {
			return getHitDie().max + (level - 1) * ((getHitDie().max / 2) + 1);
		}

		@Override
		public Stats getLightAbility() {
			switch (this) {
			case GUNSLINGER:
			case DEFENDER:
			case SUNSINGER:
				return Stats.Charisma;
			case BLADEDANCER:
			case VOIDWALKER:
				return Stats.Intelligence;
			case NIGHTSTALKER:
			case SUNBREAKER:
			case STORMCALLER:
				return Stats.Wisdom;
			case STRIKER:
				return Stats.Constitution;
			}
			return null;
		}

		@Override
		public JPanel displayName(Entity entity) {
			JPanel classPane = new JPanel(new GridLayout());
			JTextField name = new JTextField(this.toString());
			name.setEditable(false);
			classPane.add(name);
			classPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(entity.borderColor),
					"Class", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			classPane.setBackground(entity.backgroundColor);
			name.setFont(Main.textFont);
			return classPane;
		}

		@Override
		public CharacterSubClassPath[] getPaths() {
			switch (this) {
			case BLADEDANCER:
				return new CharacterSubClassPath[] { SubClassPaths.WAY_OF_THE_SWIFTCUTTER,
						SubClassPaths.WAY_OF_THE_ARCSTRIDER, SubClassPaths.WAY_OF_THE_WHISPER };
			case DEFENDER:
				return new CharacterSubClassPath[] { SubClassPaths.CALLING_OF_THE_SHIELD,
						SubClassPaths.CALLING_OF_THE_LIGHT, SubClassPaths.CALLING_OF_THE_SENTINEL };
			case GUNSLINGER:
				return new CharacterSubClassPath[] { SubClassPaths.THE_SHARPSHOOTER, SubClassPaths.THE_OUTLAW,
						SubClassPaths.THE_SHOWMAN };
			case NIGHTSTALKER:
				return new CharacterSubClassPath[] { SubClassPaths.THE_PATHFINDER, SubClassPaths.THE_BOWFIGHTER,
						SubClassPaths.THE_WRAITH };
			case STORMCALLER:
				return new CharacterSubClassPath[] { SubClassPaths.MASTER_OF_THUNDER, SubClassPaths.MASTER_OF_WIND,
						SubClassPaths.MASTER_OF_LIGHTNING };
			case STRIKER:
				return new CharacterSubClassPath[] { SubClassPaths.THE_VANGUARD, SubClassPaths.THE_GLADIATOR };
			case SUNBREAKER:
				return new CharacterSubClassPath[] { SubClassPaths.CODE_OF_THE_DEVASTATOR,
						SubClassPaths.CODE_OF_THE_SIEGEBREAKER, SubClassPaths.CODE_OF_THE_FORGEMASTER };
			case SUNSINGER:
				return new CharacterSubClassPath[] { SubClassPaths.BALLAD_OF_THE_PHOENIX,
						SubClassPaths.BALLAD_OF_THE_PACK, SubClassPaths.BALLAD_OF_THE_VALKYRIE };
			case VOIDWALKER:
				return new CharacterSubClassPath[] { SubClassPaths.THE_HARBINGER_OF_DESTRUCTION,
						SubClassPaths.THE_HARBINGER_OF_KNOWLEDGE, SubClassPaths.THE_HARBINGER_OF_MADNESS };
			default:
				return new CharacterSubClassPath[0];
			}
		}
	}
}
