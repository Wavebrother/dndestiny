package guardian.classes;

import java.util.ArrayList;

import guardian.Feature;

public enum SubClassFeature implements Feature {
	Gunslingers_Trance("Gunslinger's Trance"), Weaponized_Personality("Weaponized Personality"),
	Fancy_Shooting("Fancy Shooting"), Extra_Attack("Extra Attack"), Additional_Trick_Shots("Additional Trick Shots"),
	Volley("Volley"), Flow_Caster("Flow Caster"), Crowd_Pleaser("Crowd Pleaser"), Expertise("Expertise"),
	Expert_Attack("Expert Attack"), Cunning_Action("Cunning Action"), Opportunity_Strikes("Opportunity Strikes"),
	EXPERTISE_LV6("Expertise"), Evasion("Evasion"), Reliable_Talent("Reliable Talent"), Blindsense("Blindsense"),
	Slippery_Mind("Slippery Mind"), Elusive("Elusive"), Stroke_of_Luck("Stroke of Luck"),
	Nightstalkers_Focus("Nightstalker's Focus"), Born_of_the_Wild("Born of the Wild"),
	Draw_From_the_Void("Draw From the Void"), Subterfuge("Subterfuge"), Sure_Strikes("Sure Strikes"), Vanish("Vanish"),
	Feral_Senses("Feral Senses"), Lucid_Hunter("Lucid Hunter"), Brawler("Brawler"), Reckless_Attack("Reckless Attack"),
	Danger_Sense("Danger Sense"), Fast_Movement("Fast Movement"), Reversal("Reversal"),
	Enduring_Havoc("Enduring Havoc"), Fight_Response("Fight Response"), Indomitable_Might("Indomitable Might"),
	Bring_the_Thunder("Bring the Thunder"), Selfless_Defense("Selfless Defense"), Defenders_Voice("Defender's Voice"),
	Aura_of_Protection("Aura of Protection"), Aura_of_Courage("Aura of Courage"),
	Selfless_Defense_Improvement("Selfless Defense Improvement"),
	Aura_of_Protection_Improvement("Aura of Protection Improvement"),
	Aura_of_Courage_Improvement("Aura of Courage Improvement"), Detect_Light_and_Dark("Detect Light and Dark"),
	Inner_Flame("Inner Flame"), CHANNEL_LIGHT_TITAN("Channel Light"), Sculpt_Light("Sculpt Light"),
	Improved_Inner_Flame("Improved Inner Flame"), Titans_Defense("Titans Defense"),
	Depths_of_the_Void("Depths of the Void"), Invocations_of_the_Void("Invocations of the Void"),
	Field_of_Study("Field of Study"), Enhanced_Lightcasting("Enhanced Lightcasting"),
	Arcane_Protection("Arcane Protection"), Master_of_the_Void("Master of the Void"),
	Channel_Light_WARLOCK("Channel Light"), Patron_of_the_Arts("Patron of the Arts"),
	Channel_Light_2_Rest_("Channel Light (2 Per Rest)"), Solar_Prominence("Solar Prominence"),
	Heliocentric("Heliocentric"), Channel_Light_3_Rest_("Channel Light (3 Per Rest)"),
	The_Light_in_the_Dark("The Light in the Dark"), Harmony_Within("Harmony Within"), Arc_Charge("Arc Charge"),
	Faraday_Cage("Faraday Cage"), IMPROVED_HARMONY_WITHIN("Harmony Within"), Mind_Over_Energy("Mind Over Energy"),
	Elementary_Particles("Elementary Particles"), Boundless_Energy("Boundless Energy"),
	POCKET_BACKPACK("Pocket Backpack"), SLIPPERY_SAVE("Slippery Save"), GHOST_CUNNING_ACTION("Cunning Action"),
	AMMO_SYNTHESYS("Ammo Synthesis");

	private String name;

	SubClassFeature(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String getName() {
		if (hasUpgraded())
			return getUpgraded().toString();
		return name;
	}

	@Override
	public String getDescription() {
		switch (this) {
		case Gunslingers_Trance:
			return "Your skills with a firearm are unmatched. On your turn, you may enter your Gunslinger's Trance as a bonus action. When you do so, you begin Aiming, and you gain a bonus to damage rolls on firearm attacks as shown in the gunslinger table.\r\n"
					+ "  \u2022\tGunslinger's Trance lasts for 1 minute. It ends early if you stop Aiming, if you end the trance as a bonus action on your turn, or if you fall unconscious.\r\n"
					+ "  \u2022\tYou can enter this trance a number of times as shown on the gunslinger table. You regain these uses of trance when you complete a long rest. \r\n"
					+ "  \u2022\tAt 3rd level, if you use a super ability charge to cast your super ability, you can choose to enter your trance as part of the same action as casting your super. This consumes a use of your trance as normal";
		case Weaponized_Personality:
			return "As a gunslinger, you know how important a well-placed word or two can be, and you have a natural ability to make your personality as much a weapon as anything, whether it's through stone silent intimidation or an unassuming roguish charm. You can either gain proficiency in a Charisma-based skill of your choice, or you can choose to double your proficiency bonus for a Charisma-based skill you are already proficient in.";
		case Fancy_Shooting:
			return "You learn to do things with firearms that other Guardians might spend decades training for. When you make a firearm attack, you may choose to make it a trick shot. Trick shots have special effects that apply when you hit your target. You may apply one trick shot per firearm attack you make, and only one per turn. You can use this feature a number of times equal to 1 + your Charisma modifier (minimum 1), and you regain all uses of this feature after you complete a short or long rest.\r\n"
					+ "  \u2022\tDisarm. When you hit a creature with this trick shot, it must succeed on a Strength saving throw (DC = 8 + your attack bonus) or it drops one item of your choice that it is holding. The item lands in a spot 5 feet away from the creature.\r\n"
					+ "  \u2022\tHalt. Using this trick shot, you attempt to halt a creature of Medium size or smaller in its tracks. If your attack hits, your target's movement speed becomes 0 until the end of its next turn.\r\n"
					+ "  \u2022\tSnipe. For every trick shot point you spend on this trick shot, you gain +1 to hit on your next ranged weapon attack. You can choose to do this after the your attack roll, but before the results of the roll have been announced.";
		case Extra_Attack:
			return "You can attack twice, instead of once, whenever you take the Attack action on your turn. You cannot take a bonus action between these attacks unless the bonus action is the result of an attack.";
		case Additional_Trick_Shots:
			return getUpgraded().getDescription()
					+ "\r\n  \u2022\tDistracting Hit. On your turn you can spend 1 trick shot point to make a distracting attack. If you hit, your target can�t take reactions until the start of your next turn. \r\n"
					+ "  \u2022\tCovering Fire. If a hostile creature you can see attacks an ally within the effective range of your weapon, you can spend 1 trick shot point to use your reaction to grant disadvantage on the attack. If do this with a firearm, this also consumes 1 shot from your weapon�s shot capacity, though you deal no damage.";
		case Volley:
			return "You can use your action to make a ranged attack against a number of creatures within 10 feet of a point you can see within your weapon�s effective range. You must have ammunition for each target, and you make a separate attack roll for each target.";
		case Flow_Caster:
			return "You learned to cast your super ability with ease. Once per long rest, when you cast your super ability, you do not expend a super ability charge to do so.";
		case Crowd_Pleaser:
			return "Nothing thrills you more than getting to show off your skills. Whenever you start an encounter and have no trick shots remaining, you regain 1 use of your trick shots.";
		case Expertise:
			return "Choose two of your skill proficiencies, or one of your skill proficiencies and your proficiency with your thieves� tools. Your proficiency bonus is doubled for any ability check you make that uses either of the chosen proficiencies.\r\n"
					+ "  \u2022\tAt 6th level, you can choose two more of your proficiencies (in skills or with thieves� tools) to gain this benefit.";
		case Expert_Attack:
			return "You know how to strike subtly and exploit a foe's distraction. Once per turn, you can deal an extra 1d6 damage to one creature you hit with an attack if you have advantage on the attack roll. The attack must use a finesse or a ranged weapon.\r\n"
					+ "  \u2022\tYou don't need advantage on the attack roll if another enemy of the target is within 5 feet of it, that enemy isn't incapacitated, and you don't have disadvantage on the attack roll.\r\n"
					+ "  \u2022\tThe amount of the extra damage increases as you gain levels in this class, as shown in the Expert Attack column of the bladedancer table.";
		case Cunning_Action:
			return "Your quick thinking and agility allow you to move and act quickly. You can take a bonus action on each of your turns in combat. This action can be used only to take the Dash, Disengage, or Hide action.";
		case Opportunity_Strikes:
			return "You learn to make the most of openings provided to you. If a hostile creature you can see misses with an attack against yourself or an ally, you can choose to use your reaction to make an attack of opportunity against it. The creature must be within range of your weapon. \r\n"
					+ "  \u2022\tThis attack can benefit from Expert Attack damage, but only if you have not applied Expert Attack damage since the start of your most recent turn. If you do use Expert Attack damage on this attack, you cannot use Expert Attack damage again until the end of your next turn.";
		case Evasion:
			return "You can nimbly dodge out of the way of certain area effects, such as a walker�s main cannon or a missile swarm. When you are subjected to an effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.";
		case Reliable_Talent:
			return "You have refined your chosen skills until they approach perfection. Whenever you make an ability check that lets you add your proficiency bonus, you can treat a d20 roll of 9 or lower as a 10.";
		case Blindsense:
			return "If you are able to hear, you are aware of the location of any hidden or invisible creature within 10 feet of you.";
		case Slippery_Mind:
			return "You have acquired greater mental strength. You gain proficiency in Wisdom saving throws.";
		case Elusive:
			return "You are so evasive that attackers rarely gain the upper hand against you. No attack roll has advantage against you while you aren't incapacitated.";
		case Stroke_of_Luck:
			return "You have an uncanny knack for succeeding when you need to. If your attack misses a target within range, you can turn the miss into a hit. Alternatively, if you fail an ability check, you can treat the d20 roll as a 20. \r\n"
					+ "  \u2022\tOnce you use this feature, you can't use it again until you finish a short or long rest.";
		case Nightstalkers_Focus:
			return "You have a significant penchant for analyzing and exploiting the weaknesses of your enemies, which you gain through a preternatural focus. \r\n"
					+ "You have a maximum amount of focus based on your class level, as shown in the �Focus� column of the nightstalker table. Outside of combat, you regain all focus by completing a short or long rest. In combat, you regain 1 focus by hitting a target with a weapon attack on your turn. If you hit a target while aiming, you gain 2 focus instead of 1.\r\n"
					+ "Once on your turn, you may expend an amount of focus to perform a focus action, which is detailed under �Focus Actions� below. The cost of each focus action is indicated in parenthesis. \r\n\r\n"
					+ "  \u2022\tFocus Actions\r\n" + "Focus actions are listed in alphabetical order.\r\n"
					+ "  \u2022\tAmplified Hits (1 focus). You spend focus to deal an additional 1d6 damage with your attack. This damage increases to 1d8 at 11th level. You can choose to do this after you make your attack and after you gain focus for your attack, but before you roll damage.\r\n"
					+ "  \u2022\tDefensive Stance (4 focus). Choose a creature you can see. That creature has disadvantage on attack rolls against you, and you have advantage on and Strength or Dexterity saving throws it causes. This lasts until the start of your next turn.\r\n"
					+ "  \u2022\tFlatline (2 focus). When you use this focus action, you become invisible on scanners for a duration of your choice. However, for the duration, you cannot gain focus or use any other focus actions, and your maximum focus is reduced by 2. You can end this focus action at any time.\r\n"
					+ "  \u2022\tRapidstrike (3 focus). If you miss with an attack, you can spend focus to make one additional attack on your turn. You do not gain focus on this additional attack.\r\n"
					+ "  \u2022\tStabilize (3 focus). As a bonus action, you can spend focus to end an effect that is causing you to be bleeding, blinded, burning, or poisoned.\r\n"
					+ "  \u2022\tStrafe (3 focus). You begin Aiming without spending any movement and, for this turn, your movement is not reduced while Aiming. You gain no focus for your attacks on this turn.\r\n"
					+ "  \u2022\tStudy (varies). As a bonus action, you can spend an amount of focus to learn key information about one target that you can see. Make a Wisdom (Perception) check. If the target is aware of your presence, your check is contested by the target�s Wisdom (Survival) or Charisma (Deception) check, target�s choice.\r\n"
					+ "  \u2022\tOn a success, you learn one of the following:\r\n"
					+ "\t\t(2 focus) Your choice of either the target�s armor class, current hit points, damage soak value, or what the target�s readied action is, if any.\r\n"
					+ "\t\t(3 focus) The highest ability score of the target, or what the target�s highest saving throw is.\r\n"
					+ "\t\t(4 focus) Any one weakness, resistance, or immunity the target has, if any. You choose which category you wish to learn, and your GM chooses what to divulge.\r\n"
					+ "  \u2022\tStunning Blow (8 focus). You attempt to hit a target at a critical point. For your next attack, if you hit, your target has to make a Constitution saving throw (DC = 8 + your proficiency bonus + your Wisdom modifier). On a failed save, they are stunned until the end of their next turn. You have to make this attack before the end of your turn. You do not gain focus on your turn when you use this focus action.";
		case Born_of_the_Wild:
			return "You are particularly familiar with the natural environment, and typically feel more at home in the Wilds than within the confines of the City walls. You are proficient in the Nature skill. Additionally, when operating outside the influence of civilization, you gain the following benefits: \r\n"
					+ "  \u2022\tDifficult terrain doesn't slow you or your group's travel. \r\n"
					+ "  \u2022\tYou have superior vision in dim and dark conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.\r\n"
					+ "  \u2022\tYou can always recall the general layout of areas you have been to previously.\r\n"
					+ "  \u2022\tWhen you make an Intelligence or Wisdom check related to the natural surroundings, your proficiency bonus is doubled if you are using a skill you�re proficient in.\r\n"
					+ "  \u2022\tEven when you are engaged in another activity while traveling (such as foraging, navigating, or tracking), you remain alert to danger.\r\n"
					+ "  \u2022\tIf you are traveling alone, you can move stealthily at a normal pace. \r\n"
					+ "  \u2022\tWhen you forage for food or scavenge for resources, you find twice as much as you normally would. \r\n"
					+ "  \u2022\tYou can provide food and water for up to five people, including yourself, provided the local land offers options for food and water.\r\n"
					+ "  \u2022\tWhile tracking other creatures, you also learn their exact number, their sizes, and how long ago they passed through the area.";
		case Draw_From_the_Void:
			return "Your connection to the Void allows you to examine situations from another perspective. You can use your Wisdom ability when determining your modifiers for Arcana, Investigation, and Nature checks.";
		case Subterfuge:
			return "You are adept at evading creatures that rely on darkvision or scanners. While in the darkness and moving stealthily, you are invisible to any creature that relies on darkvision to see you, and creatures have disadvantage on Wisdom (Perception) checks to notice you otherwise.\r\n"
					+ "  \u2022\tAdditionally, if you use the Flatline focus action, you can now gain and spend focus at the same time, though your maximum focus is still reduced.\r\n"
					+ "  \u2022\tFinally, you can spend 1 week of downtime and 7,000 glimmer to create a masterclass camouflage for yourself based on one of the following terrain types: arctic, coast, desert, forest, grassland, mountain, or swamp. You can only wear one type of camouflage over your armor at a time. While you are in the same terrain type as your camouflage, you gain a +10 bonus to Dexterity (Stealth) checks so long as you do not move, and so long as you take no actions or reactions. Once you move or take an action or a reaction, you must take the Hide action to gain this benefit again.";
		case Sure_Strikes:
			return "You can use Amplified Hits once on each of your turns even if you use another focus action.";
		case Vanish:
			return "You can use the Hide action as a bonus action on your turn. Also, you can't be tracked by traditional survival or technology-dependant techniques unless you choose to leave a trail.";
		case Feral_Senses:
			return "You gain preternatural senses that help you fight creatures you can't see. When you attack a creature you can't see, your inability to see it doesn't impose disadvantage on your attack rolls against it. \r\n"
					+ "You are also aware of the location of any invisible creature within 30 feet of you, provided that the creature isn't hidden from you and you aren't blinded or deafened.";
		case Lucid_Hunter:
			return "You now regain all of your focus when you complete a brief rest.";
		case Brawler:
			return "You know to rely on your fists more than any other weapon. You gain the following benefits:\r\n"
					+ "  \u2022\tThe damage die of your unarmed attacks changes as you gain levels in this class, as shown in the Brawler Damage column of the striker table.\r\n"
					+ "  \u2022\tYou are proficient in improvised weapons.\r\n"
					+ "  \u2022\tOn your turn, if you attack at least once, you can make an unarmed strike as your bonus action.";
		case Reckless_Attack:
			return "You can throw aside all concern for defense to attack with fierce desperation. When you make your first attack on your turn, you can decide to attack recklessly. Doing so gives you advantage on attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn.";
		case Danger_Sense:
			return "You gain an uncanny sense of when things nearby aren't as they should be, giving you an edge when you dodge away from danger. You have advantage on Dexterity saving throws against effects that you can see, such as traps and cannon fire. To gain this benefit, you can�t be blinded, deafened, or incapacitated.";
		case Fast_Movement:
			return "Your speed increases by 10 feet while you aren�t wearing heavy armor.";
		case Reversal:
			return "Once on your turn, if you kill a creature with an unarmed strike or Storm Fist, you can make a shield recharge roll.";
		case Enduring_Havoc:
			return "The Light from your super ability can keep you fighting despite grievous wounds. If you drop to 0 hit points while using your super ability and don't die outright, you can make a DC 10 Constitution saving throw. If you succeed, you drop to 1 hit point instead. \r\n"
					+ "  \u2022\tEach time you use this feature after the first, the DC increases by 5. When you finish a short or long rest, the DC resets to 10.";
		case Fight_Response:
			return "You�re ready to brawl at the pull of a trigger. You have advantage on initiative rolls.\r\n"
					+ "Additionally, if you are surprised at the start of combat and aren�t incapacitated, you can act normally on your first turn, but only if you cast Fist of Havoc before doing anything else on that turn.";
		case Indomitable_Might:
			return "If your total for a Strength check is less than your Strength score, you can use that score in place of the total.";
		case Bring_the_Thunder:
			return "You are a constant conductor of arc energy. All of your unarmed strikes deal arc damage, and you gain an additional super ability charge.";
		case Selfless_Defense:
			return "You put the well-being of others above your own, especially in combat. When a creature is attacked within 5 feet of you, you may use your reaction to grant them a bonus +2 to their AC against the attack. This bonus increases to +3 at 10th level, and +4 at 17th level.\r\n"
					+ "  \u2022\tAlternatively, if the creature is not grappled or restrained, you can choose to simply take the attack for them. You and the creature you�re defending switch places, and the attack automatically hits you. You can also choose to do this with saving throws that only affect the creature you are defending. If you choose to take a saving throw for someone, you automatically fail the saving throw.\r\n"
					+ "  \u2022\tYou must choose to use this ability before the attack roll is made, or before the creature you are defending makes their saving throw.\r\n";
		case Defenders_Voice:
			return "You learn that your voice carries weight among the downtrodden. You gain advantage on all Charisma (Persuasion) checks when speaking to creatures allied with The Last City, and they will aid you in whatever way is most appropriate.\r\n"
					+ "  \u2022\tFor example, a settlement might offer shelter for the night if you are passing through, or a place to conduct operations if you agree to defender their settlement.";
		case Aura_of_Protection:
			return "Whenever you or a friendly creature within 10 feet of you must make a saving throw, the creature gains a bonus to the saving throw equal to your Charisma modifier (with a minimum bonus of +1). You must be conscious to grant this bonus. \r\n";
		case Aura_of_Courage:
			return "You and friendly creatures within 10 feet of you can't be frightened while you are conscious. \r\n";
		case Selfless_Defense_Improvement:
			return getUpgraded().getDescription()
					+ "\r\n  \u2022\tYou are resilient to damage taken in this way, and you halve the damage you receive when taking the attack for an ally.";
		case Aura_of_Protection_Improvement:
			return "Whenever you or a friendly creature within 30 feet of you must make a saving throw, the creature gains a bonus to the saving throw equal to your Charisma modifier (with a minimum bonus of +1). You must be conscious to grant this bonus. \r\n";
		case Aura_of_Courage_Improvement:
			return "You and friendly creatures within 30 feet of you can't be frightened while you are conscious. \r\n";
		case Detect_Light_and_Dark:
			return "The presence of Darkness registers as an uncomfortable pressure against you, and the presence of Light is like a soft singing in the wind. As an action, you can open your awareness to the ebb and flow of these forces. Until the end of your next turn, you are aware of the type of Darkness zone you are in, and you can sense if any creatures within 60 feet of you are capable of dealing Darkness damage, or if there are any wielders of the Light within the same range.\r\n"
					+ "  \u2022\tYou must complete a brief rest in order to regain use of this feature.";
		case Inner_Flame:
			return "You learn how to imbue your weapon attacks with the Light you wield. You gain a maximum number of Inner Flame uses equal to your Light ability level. When you hit with a melee weapon or close-range firearm, you can expend a use of Inner Flame to deal an additional 1d8 damage per use spent. You regain half your uses, rounded down, when you complete a short rest, and all uses when you complete a long rest.";
		case CHANNEL_LIGHT_TITAN:
			return "Your code allows you to channel your Light to fuel effects or abilities. Every sunbreaker gains the Banish the Dark Channel Light option detailed below. One addition Channel Light option is provided by your code, and your code explains how to use it. \r\n"
					+ "  \u2022\tWhen you use your Channel Light, you choose which option to use. You must then finish a short or long rest to use your Channel Light again. \r\n"
					+ "  \u2022\tSome Channel Light effects require saving throws. When you use such an effect from this class, the DC equals your sunbreaker Light save DC.\r\n\r\n"
					+ "  \u2022\tCHANNEL LIGHT: BANISH THE DARK\r\n"
					+ "As an action, you channel your Light outward, using your Channel Light. Creatures of the Darkness that can see or hear you within 30 feet of you must make a Wisdom saving throw. If the creature fails its saving throw, it is turned for 1 minute or until it takes damage. \r\n"
					+ "  \u2022\tA turned creature must spend its turns trying to move as far away from you as it can, and it can't willingly move to a space within 30 feet of you. It also can�t take reactions. For its action, it can use only the Dash action or try to escape from an effect that prevents it from moving. If there's nowhere to move, the creature can use the Dodge action.";
		case Sculpt_Light:
			return "You can give a number of creatures up to your Light ability modifier immunity against your Light abilities whenever you cast them. They take no damage and trigger no effects from your Light abilities. You can change which creatures gain this benefit on your turn.";
		case Improved_Inner_Flame:
			return "You are so suffused with solar Light that all your melee and close-range weapon attacks carry a bit of your Light with them. Whenever you hit a creature with a melee weapon or close-range firearm, the creature takes an extra 1d8 solar damage. If you also use your Inner Flame with an attack, you add this damage to the extra damage of your Inner Flame.";
		case Titans_Defense:
			return "You gain one of the following features of your choice.\r\n"
					+ "  \u2022\tEvasion. When you are subjected to an effect, such as a red dragon's fiery breath or a lightning bolt spell, that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail. \r\n"
					+ "  \u2022\tStand Against the Tide. When a hostile creature misses you with a melee attack, you can use your reaction to force that creature to repeat the same attack against another creature (other than itself) of your choice. \r\n"
					+ "  \u2022\tUncanny Dodge. When an attacker that you can see hits you with an attack, you can use your reaction to halve the attack's damage against you.";
		case Depths_of_the_Void:
			return "Your connection to the Void grants you a benefit of perspective that others may not understand. You can use your Intelligence ability when determining your modifiers for Insight, Perception, and Survival checks.";
		case Invocations_of_the_Void:
			return "In your study of the Void, you have stumbled on a source of great power: invocations of the Void, fragments of knowledge and power both awe-inspiring and terrifying. \r\n"
					+ "At 2nd level, you gain two invocations of your choice. You invocation options are detailed at the end of this class description. When you gain certain voidwalker levels, you gain additional invocations of your choice, as shown in the Invocation Known column of the voidwalker table. \r\n"
					+ "Additionally, when you gain a level in this class, you can choose one of the invocations you know and replace it with another invocation that you could learn at that level. At 20th level, you can do this when you complete a long rest in a Light zone.";
		case Field_of_Study:
			return "You begin focusing your intellect on a particular field of study. Pick from one of the following options. You cannot choose the same field of study more than once, even if you later get to choose again.\r\n"
					+ "  \u2022\tLANGUAGE\r\n" + "You gain proficiency in two languages of your choice.\r\n"
					+ "  \u2022\tTHE LIGHT\r\n"
					+ "Once per short rest, as a bonus action, you can focus your Light to regenerate half of your maximum shield capacity.\r\n"
					+ "  \u2022\tSTUDENT OF SKILL\r\n"
					+ "You can add half your proficiency bonus, rounded down, to any ability check you make that doesn�t already include your proficiency bonus.\r\n"
					+ "  \u2022\tWAR\r\n" + "You gain proficiency in fusion rifles, shotguns, and light machine guns.";
		case Enhanced_Lightcasting:
			return "Your connection to the Void grants you a boon to casting your Light abilities. When you cast a Light ability and roll a 1 or a 2 on any of the die on the damage roll, you may choose to re-roll a number of those die up to your Intelligence modifier. You must use the new rolls, even if they are 1s or 2s.";
		case Arcane_Protection:
			return "You have learned how to invoke the power of the Void to help protect yourself and an ally of your choice. As a bonus action, you may touch a creature, giving both of you bonuses to your saving throws equal to your proficiency bonus for 4 hours. You regain use of this feature after you finish a long rest.";
		case Master_of_the_Void:
			return "You have learned how to maximize your potential with Light. When you cast a Light ability, you can deal maximum damage with that ability.\r\n"
					+ "  \u2022\tThe first time you do so, you suffer no adverse effect. If you use this feature again before you finish a long rest, you take 2d12 + your Intelligence modifier in void damage as recoil directly to your health. Each time you use this feature again before finishing a long rest increases the recoil damage by 1d12. This effect ignores damage resistance and immunity.";
		case Channel_Light_WARLOCK:
			return "You learn to channel your Light through practiced mental and physiological triggers, using that Light to fuel the effects and abilities. You start off with two options: Drums of Wars, and an option determined by your sunsinger Ballad.     \r\n"
					+ "  \u2022\tWhen you use your Channel Light, you choose which effect to create. You must then finish a short or long rest to use your Channel Light again.\r\n"
					+ "  \u2022\tSome Channel Light effects require saving throws. When you use such an effect from this class, the DC equals your Light save DC.\r\n"
					+ "\r\n" + "  \u2022\tCHANNEL LIGHT: DRUMS OF WAR\r\n"
					+ "As a bonus action you embolden yourself with your solar Light, temporarily granting yourself the benefits of a fighting style. Choose a fighting style from either the nightstalker or defender class Fighting Style feature. For 1 minute, you have this fighting style.";
		case Patron_of_the_Arts:
			return "You gain proficiency in the Performance skill if you weren�t already, and you can double your proficiency bonus for it.";
		case Channel_Light_2_Rest_:
			return "You learn to channel your Light through practiced mental and physiological triggers, using that Light to fuel the effects and abilities. You start off with two options: Drums of Wars, and an option determined by your sunsinger Ballad.     \r\n"
					+ "  \u2022\tWhen you use your Channel Light, you choose which effect to create. You may use this feature twice per short or long rest.\r\n"
					+ "  \u2022\tSome Channel Light effects require saving throws. When you use such an effect from this class, the DC equals your Light save DC.\r\n"
					+ "\r\n" + "  \u2022\tCHANNEL LIGHT: DRUMS OF WAR\r\n"
					+ "As a bonus action you embolden yourself with your solar Light, temporarily granting yourself the benefits of a fighting style. Choose a fighting style from either the nightstalker or defender class Fighting Style feature. For 1 minute, you have this fighting style.";
		case Solar_Prominence:
			return "While your super ability has duration, friendly creatures that can see you within 10 feet gain advantage on Intelligence, Wisdom, and Charisma saving throws, and can add your Charisma modifier to any saving throws they make against paracausal mental effects you are aware of.";
		case Heliocentric:
			return "You gain proficiency in Wisdom saving throws. As a bonus action you can spend a use of your Channel Light to gain proficiency in a saving throw of your choice for 1 minute.";
		case Channel_Light_3_Rest_:
			return "You learn to channel your Light through practiced mental and physiological triggers, using that Light to fuel the effects and abilities. You start off with two options: Drums of Wars, and an option determined by your sunsinger Ballad.     \r\n"
					+ "  \u2022\tWhen you use your Channel Light, you choose which effect to create. You may use this feature thrice per short or long rest.\r\n"
					+ "  \u2022\tSome Channel Light effects require saving throws. When you use such an effect from this class, the DC equals your Light save DC.\r\n"
					+ "\r\n" + "  \u2022\tCHANNEL LIGHT: DRUMS OF WAR\r\n"
					+ "As a bonus action you embolden yourself with your solar Light, temporarily granting yourself the benefits of a fighting style. Choose a fighting style from either the nightstalker or defender class Fighting Style feature. For 1 minute, you have this fighting style.";
		case The_Light_in_the_Dark:
			return "When you cast your super, you can choose to, for 1 minute, emit bright light in a 60-foot radius and dim light 30 feet beyond that. Whenever a hostile creature starts their turn within the bright light, they take 10 solar damage. This damage overcomes resistances.\r\n"
					+ "  \u2022\tIn addition, for 1 minute you have advantage on saving throws imposed on you by hostile creatures.";
		case Harmony_Within:
			return "Your natural affinity for harmony and balance keeps you centered and focused, even in the midst of raging combat. Once per short rest, you can use your action to end an effect that is causing you to be charmed, frightened, blinded, bleeding, burning, or poisoned.";
		case Arc_Charge:
			return "You are a natural conduit for arc energy, allowing you to draw in arc energy and harness it in the form of arc charges, which you can use to empower yourself and your abilities.\r\n"
					+ "  \u2022\tYour stormcaller level determines the maximum amount of arc charges you can hold at once, as shown in the Max Arc Charges column of the stormcaller table. \r\n"
					+ "  \u2022\tThe number of arc charges you currently have determines your energy level, as seen in the Energy Levels table below. You start off at Energy Level 1. No matter how many Arc Charges you have, you cannot go below Energy Level 1, unless you are incapacitated.\r\n"
					+ "  \u2022\tYou can regain a number of arc charges up to twice your stormcaller level on a short rest. You can never have more than your maximum number of charges for your level. When you complete a long rest, you regain all of your spent arc charges.\r\n"
					+ "\r\n" + "  \u2022\tENERGY LEVELS\r\n" + "Level  \u2022\tMinimum Arc Charges to Maintain\r\n"
					+ "1\t\t---\r\n" + "2\t\t2\r\n" + "3\t\t8\r\n" + "4\t\t18\r\n" + "5\t\t32\r\n" + "\r\n"
					+ "  \u2022\tARC FEATURES\r\n"
					+ "While you are at Energy Level 1 or higher you gain the following features. As you progress in this class, additional arc features will become available.\r\n"
					+ "  \u2022\tBolt. Once per turn, when you take the Attack action, as a bonus action you can spend a number of arc charges up to your current energy Level to make a Bolt attack with a range of 30 feet. Make a Light attack roll for every arc charge spent. On hit, this attack does 1d6 + your Wisdom modifier in arc damage for every Bolt attack that hits.\r\n"
					+ "  \u2022\tLighting Reflexes. You can spend 1 arc charges to take the Dodge, Dash, or Disengage action as a bonus action on your turn.";
		case Faraday_Cage:
			return "You gain the following features while you are at Energy Level 2 or higher.\r\n"
					+ "  \u2022\tRapid Casting. You can spend 3 arc charges to cast your grenade or melee ability as a bonus action.\r\n"
					+ "  \u2022\tCurrent. As a bonus action you can convert one melee ability or grenade ability charge into a number of arc charges equal to your Light ability modifier. You regain use of this feature when you complete a short or long rest.";
		case IMPROVED_HARMONY_WITHIN:
			return "Your natural affinity for harmony and balance keeps you centered and focused, even in the midst of raging combat. You can use your action to end an effect that is causing you to be charmed, frightened, blinded, bleeding, burning, or poisoned.";
		case Mind_Over_Energy:
			return "Your mastery of arc energy makes you immune to disease and poison. Additionally while you are at Energy Level 3 or higher you gain access to the following features.\r\n"
					+ "  \u2022\tStatic Defense. As a bonus action you can spend a number of arc charges to give yourself an overshield equal to 2 x the charges spent, up to your stormcaller level. This overshield lasts until the start of your next turn or until it depletes, whichever happens first.";
		case Elementary_Particles:
			return "You become proficient in all saving throws. When you are at Energy Level 4 or higher you gain the following features.\r\n"
					+ "  \u2022\tGalvanism. When you fail a saving throw you can spend 7 arc charges to re-roll that saving throw once. You can choose to use either result.\r\n"
					+ "  \u2022\tShock of Inspiration. As a bonus action you can spend a number of arc charges, up to your proficiency modifier, to grant yourself or a friendly creature you touch a bonus to their next ability check equal to the number of charges you spent";
		case Boundless_Energy:
			return "When you roll initiative and have no arc charges left, you gain 8 arc charges.";
		case POCKET_BACKPACK:
			return "Ghosts have a pocket dimension attached to you, their Guardian. \r\nYour Ghost can choose to use either its action or bonus action to enter or leave the dimension at any time if it is within 10 feet of you. \r\nThe space the dimension occupies is the same space as its Guardian. \r\nGhosts cannot be harmed while in this dimension, and they cannot interact with the outside world, \r\nthough they can still communicate with you, and scan the local area for you.\r\n"
					+ "\tGhosts cannot enter or otherwise access the pocket backpack of another Ghost.\r\n"
					+ "\tIf you die either before or after your turn, your Ghost can choose to use its reaction to come out of the pocket backpack.\r\n"
					+ "\tIf you are ever permanently killed, your Ghost is removed from the pocket backpack immediately, and cannot re-enter it.";
		case SLIPPERY_SAVE:
			return "If your Ghost is within 10 feet of you and it succeeds on any saving throw it makes, or if an attack against it misses, \r\nit can choose to use its reaction to disappear into its pocket backpack, negating all damage or effects of the saving throw or attack.";
		case GHOST_CUNNING_ACTION:
			return "Your Ghost's computational mind and agility allow them to move and act quickly. \r\nThey can take a bonus action on each of their turns in combat. \r\nThis action can be used only to take the Dash, Disengage, or Hide action.";
		case AMMO_SYNTHESYS:
			return "Your Ghost can use an action to attempt to fabricate ammunition for your weapons using available resources. \r\nThey can only attempt one type of synthesis at a time (either primary, secondary, or heavy), \r\nand they must be out of their pocket backpack and have access to materials such as adequate metals and minerals to do so.\r\n"
					+ "\tRoll a d20 with a bonus to your roll equal to your proficiency bonus. \r\nThe amount of magazines synthesized is determined using the Synthesis table.\r\n"
					+ "\r\n" + "\tSYNTHESIS TABLE\r\n" + "Category\t\t\tResult\r\n"
					+ "Simple, Combat Arrows\t\t1 magazine or 10 arrows for every 5 points rolled\r\n"
					+ "Martial\t\t\t\t1 magazine for every 10 points rolled\r\n"
					+ "Rockets\t\t\t1 magazine for every 15 points rolled";
		default:
			return "";
		}
	}

	@Override
	public SubClassFeature getUpgraded() {
		switch (this) {
		case Additional_Trick_Shots:
			return Fancy_Shooting;
		case Selfless_Defense_Improvement:
			return Selfless_Defense;
		case Aura_of_Protection_Improvement:
			return Aura_of_Protection;
		case Aura_of_Courage_Improvement:
			return Aura_of_Courage;
		case Channel_Light_2_Rest_:
			return Channel_Light_WARLOCK;
		case Channel_Light_3_Rest_:
			return Channel_Light_2_Rest_;
		default:
			return null;
		}
	}

	public void addToList(int minimumLevel, int guardianLevel, ArrayList<Feature> features) {
		if (guardianLevel >= minimumLevel) {
			if (hasUpgraded())
				features.remove(getUpgraded());
			features.add(this);
		}
	}

}
