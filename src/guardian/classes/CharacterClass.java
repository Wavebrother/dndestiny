package guardian.classes;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.border.TitledBorder;

import guardian.Entity;
import guardian.Ghost.GhostSubClass;
import guardian.Main;
import guardian.classes.CharacterSubClass.SubClasses;

public class CharacterClass {
	private final CharacterSubClass subClass;
	private JComboBox<CharacterSubClassPath> paths;
	private JSpinner hitDice = new JSpinner(new SpinnerNumberModel(1, 0, 1, 1));
	private int level = 1;

	public CharacterClass(CharacterSubClass subClass) {
		this.subClass = subClass;
		if (subClass != GhostSubClass.GHOST) {
			this.paths = new JComboBox<CharacterSubClassPath>(
					new DefaultComboBoxModel<CharacterSubClassPath>(subClass.getPaths()));
			this.paths.addKeyListener(new KeyListener() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
						paths.setSelectedItem(null);
				}

				@Override
				public void keyReleased(KeyEvent e) {
				}

				@Override
				public void keyTyped(KeyEvent e) {
				}

			});
			this.paths.setSelectedItem(null);
		}
		hitDice.setFont(Main.textFont);
		((DefaultEditor) hitDice.getEditor()).getTextField().setColumns(5);
	}

	public CharacterClass(CharacterSubClass subClass, int level, int hitDice) {
		this(subClass);
		this.hitDice.setValue(hitDice);
		this.setLevel(level);
	}

	public CharacterSuperClass getSuperClass() {
		if (subClass instanceof SubClasses)
			switch ((SubClasses) subClass) {
			case GUNSLINGER:
			case BLADEDANCER:
			case NIGHTSTALKER:
				return CharacterSuperClass.HUNTER;
			case STRIKER:
			case DEFENDER:
			case SUNBREAKER:
				return CharacterSuperClass.TITAN;
			case VOIDWALKER:
			case SUNSINGER:
			case STORMCALLER:
				return CharacterSuperClass.WARLOCK;
			}
		return null;
	}

	public CharacterSubClass getSubClass() {
		return subClass;
	}

	public void setSubClassPath(CharacterSubClassPath path) {
		this.paths.setSelectedItem(path);
	}

	public CharacterSubClassPath getSubClassPath() {
		if (paths.getSelectedIndex() < 0) {
			return new CharacterSubClassPath() {
				@Override
				public String name() {
					return null;
				}
			};
		}
		return (CharacterSubClassPath) paths.getSelectedItem();
	}

	public JPanel displayPaths(Entity entity) {
		JPanel panel = new JPanel(new GridLayout());
		panel.add(paths);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(entity.borderColor),
				"Subclass Path", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(entity.backgroundColor);
		return panel;
	}

	public JSpinner getHitDice() {
		return hitDice;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
		if(getLevel() > (int) hitDice.getValue())
		this.hitDice.setModel(new SpinnerNumberModel((int) hitDice.getValue(), 0, getLevel(), 1));
		else
			this.hitDice.setModel(new SpinnerNumberModel(getLevel(), 0, getLevel(), 1));
		((DefaultEditor) hitDice.getEditor()).getTextField().setColumns(5);
	}
}
