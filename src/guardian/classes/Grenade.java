package guardian.classes;

import guardian.DamageType;
import guardian.Spell.CastingTime;
import guardian.Spell.Duration;
import guardian.Spell.SpellType;
import guardian.SpellInterface;

public enum Grenade implements SpellInterface {
	FIREBOLT, FUSION, INCENDIARY, SOLAR, SWARM, THERMITE, TRIPMINE, ARCBOLT, FLASHBANG, FLUX, LIGHTNING, PULSE, SKIP,
	STORM, AXIONBOLT, MAGNETIC, SCATTER, SPIKE, SUPPRESSOR, VOIDWALL, VORTEX;

	@Override
	public String toString() {
		if (this == AXIONBOLT)
			return "Axion Bolt";
		return toString() + " Grenade";
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public SpellType getSpellType() {
		return SpellType.GRENADE;
	}

	@Override
	public CastingTime getCastingTime() {
		return CastingTime.ACTION;
	}

	@Override
	public DamageType getDamageType() {
		switch (this) {
		case SWARM:
		case THERMITE:
		case TRIPMINE:
		case FIREBOLT:
		case FUSION:
		case INCENDIARY:
		case SOLAR:
			return DamageType.SOLAR;
		case FLASHBANG:
		case FLUX:
		case LIGHTNING:
		case PULSE:
		case SKIP:
		case STORM:
		case ARCBOLT:
			return DamageType.ARC;
		case SCATTER:
		case SPIKE:
		case AXIONBOLT:
		case MAGNETIC:
		case SUPPRESSOR:
		case VOIDWALL:
		case VORTEX:
			return DamageType.VOID;
		}
		return null;
	}

	@Override
	public Duration getDuration() {
		switch (this) {
		case SOLAR:
		case SPIKE:
		case THERMITE:
		case VOIDWALL:
		case VORTEX:
		case LIGHTNING:
		case PULSE:
			return new Duration(3, "Rounds");
		case AXIONBOLT:
		case SKIP:
		case SWARM:
			return new Duration(3, "Rounds", "Up to");
		case TRIPMINE:
			return new Duration(1, "Minute", "Up to");
		default:
			return new Duration();
		}
	}

	@Override
	public int[] getRange() {
		switch (this) {
		case ARCBOLT:
		case FLASHBANG:
		case FIREBOLT:
		case INCENDIARY:
		case SUPPRESSOR:
			return new int[] { 50 };
		case FLUX:
		case FUSION:
			return new int[] { 40, 60 };
		default:
			return new int[] { 40 };
		}
	}
}
