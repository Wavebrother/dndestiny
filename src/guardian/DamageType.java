package guardian;

public enum DamageType implements BaseInterface {
	PIERCING, SLASHING, BLUDGEONING, KINETIC, ENERGY, EXPLOSIVE, SOLAR, VOID, ARC, DARKNESS, COLD, FIRE, ACID, NECROTIC,
	POISON, RADIANT, LIGHTNING, THUNDER, PSYCHIC, FORCE;
	@Override
	public String toString() {
		return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
	}
}
