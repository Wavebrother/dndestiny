package guardian;

import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import guardian.Entity.Alignment;
import guardian.Ghost.GhostBackground;
import guardian.Proficiency.CustomProficiency;
import guardian.Races.MainRace;
import guardian.Races.Race;
import guardian.Skill.Skills;
import guardian.Stat.Stats;
import guardian.classes.CharacterSubClass.SubClasses;
import guardian.classes.CharacterSubClassPath;
import guardian.classes.CharacterSubClassPath.SubClassPaths;
import guardian.items.CustomArmor;
import guardian.items.CustomFirearm;
import guardian.items.CustomItem;
import guardian.items.CustomWeapon;
import guardian.items.Firearm;
import guardian.items.Firearm.Firearms;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Inventory;
import guardian.items.Item;
import guardian.items.Weapon;
import guardian.items.Weapon.Weapons;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.WeaponProperties;
import guardian.items.interfaces.ArmorInterface;
import guardian.items.interfaces.EnumInterface;
import guardian.items.interfaces.FirearmInterface;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.PerkInterface.CustomPerk;
import guardian.items.interfaces.PerkInterface.Perk;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.itemEnums.Armor;
import guardian.items.itemEnums.Items;

public class Main {

	public static MyCharacter character;
	public static CharacterSelector selector;

	public static final URL logo = Main.class.getResource("/resources/DnDLogo.png");
	private static final InputStream changelog = Main.class.getResourceAsStream("/resources/changelog");

	public static ImageIcon d20Icon = new ImageIcon(Main.class.getResource("/resources/d20.png"));

	public static final Color backgroundColor = new Color(169, 208, 229);

	public static final JsonObject customFeatures = new JsonObject();
	public static final JsonObject customSpells = new JsonObject();

	static final int length = 10;

	static final String charset = "ISO-8859-5";
	public static final Version version = new Version(0, 0, 8);

	public static final double width = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	public static final double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	public static Font titleFont = new Font("Dialog", 1, (int) ((Main.width / 1920.0) * 15));
	public static Font textFont = new Font("Dialog", 0, (int) ((Main.width / 1920.0) * 15));;
	public static boolean wavebrother;
	public static boolean debug;
	static {
		Perk.ACCELERATED_COILS.getClass();
		try (BufferedReader readWeapons = new BufferedReader(
				new FileReader("items\\features_" + System.getProperty("user.name").replace(":", "_") + ".json"))) {
			StringBuilder file = new StringBuilder();
			String line = readWeapons.readLine();
			do {
				file.append(line);
				line = readWeapons.readLine();
			} while (line != null);
			JsonObject array = (new GsonBuilder().setPrettyPrinting().create()).fromJson(file.toString(),
					JsonObject.class);
			for (String featureName : array.keySet()) {
				customFeatures.add(featureName, array.getAsJsonObject(featureName));
			}
		} catch (IOException e) {
			if (e.getClass() != FileNotFoundException.class)
				e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		JPanel fonts = new JPanel(new GridLayout(0, 6));
		for (Font font : GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts()) {
			JCheckBox label = new JCheckBox(1234567890 + font.getFontName());
			label.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					fonts.remove(label);
					fonts.revalidate();
					fonts.repaint();
					fonts.setSize((int) width, (int) height);
				}

			});
			label.setFont(new Font(font.getFontName(), 0, 12));
			fonts.add(label);
		}
		// JOptionPane.showMessageDialog(null, new JScrollPane(fonts,
		// JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		// JScrollPane.HORIZONTAL_SCROLLBAR_NEVER));
		d20Icon = new ImageIcon(d20Icon.getImage().getScaledInstance((int) (64 * d20Icon.getIconWidth() / Main.width),
				(int) (64 * d20Icon.getIconHeight() / Main.width), Image.SCALE_SMOOTH));
		debug = Arrays.asList(args).contains("debug");
		wavebrother = Arrays.asList(args).contains("wavebrother");
		Guardian[] characterArray = new Guardian[0];
		Version fileVersion = new Version(0, 0, 0);
		try {
			JsonArray charactersArray = new JsonArray();
			StringBuilder file = new StringBuilder();
			File charactersJson = new File("characters\\characters.json");
			try (BufferedReader readCharacters = new BufferedReader(new FileReader(charactersJson))) {
				String line = "";
				do {
					file.append(line);
					String read = readCharacters.readLine();
					if (read != null) {
						line = new String(read.getBytes(), charset);
					} else
						line = null;
				} while (line != null);
				String fileString = file.toString();
				JsonArray fileVersionArray = (new GsonBuilder().setPrettyPrinting().create())
						.fromJson(fileString.substring(0, fileString.indexOf("]") + 1), JsonArray.class);
				fileVersion = new Version(fileVersionArray.get(0).getAsInt(), fileVersionArray.get(1).getAsInt(),
						fileVersionArray.get(2).getAsInt());
				fileString = fileString.substring(fileString.indexOf("]") + 1);
				titleFont = new Font("Dialog", 1, Integer.parseInt(fileString.substring(0, fileString.indexOf("["))));
				textFont = new Font("Dialog", 0, Integer.parseInt(fileString.substring(0, fileString.indexOf("["))));
				charactersArray = (new GsonBuilder().setPrettyPrinting().create())
						.fromJson(fileString.substring(fileString.indexOf("[")), JsonArray.class);
				characterArray = read(charactersArray);
				File backup = new File("characters\\backups\\characters_" + fileVersion + ".jsonBackup");
				backup.getParentFile().mkdirs();
				try (PrintWriter writeCharacters = new PrintWriter(backup, charset)) {
					try (BufferedReader newReadCharacters = new BufferedReader(new FileReader(charactersJson))) {
						String read = newReadCharacters.readLine();
						do {
							writeCharacters.write(read);
							read = newReadCharacters.readLine();
							if (read != null)
								writeCharacters.write("\n");
						} while (read != null);
					}
				}
			}
		} catch (JsonSyntaxException | ClassCastException | IOException e) {
			if (e.getClass() != FileNotFoundException.class) {
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					e.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
				try (PrintWriter writeCharacters = new PrintWriter("characters.jsonBroken", charset)) {
					try (BufferedReader readCharacters = new BufferedReader(new FileReader("characters.json"))) {
						String read = readCharacters.readLine();
						do {
							writeCharacters.write(read);
							read = readCharacters.readLine();
							if (read != null)
								writeCharacters.write("\n");
						} while (read != null);
					}
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
		selector = new CharacterSelector(characterArray);
		if (!fileVersion.equals(version) || (args.length > 0 && args[0].equals("debug"))) {
			try (BufferedReader readChangeLog = new BufferedReader(new InputStreamReader(changelog))) {
				StringBuilder sb = new StringBuilder();
				String line;
				sb.append("Changelog version " + version);
				while ((line = readChangeLog.readLine()) != null) {
					sb.append(version.readChangeLogLine(line, fileVersion));
				}
				JOptionPane.showMessageDialog(null, sb.toString(), "Change Log", JOptionPane.INFORMATION_MESSAGE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static Entity readEntity(JsonObject entityMap) {
		boolean[][] deathSaves = new boolean[2][3];
		Map<Stats, Stat> stats = new HashMap<Stats, Stat>();
		Map<Skills, Skill> skills = new HashMap<Skills, Skill>();
		Inventory inventory = new Inventory(entityMap.get("characterName").getAsString(),
				entityMap.get("inventoryMaxSpaces").getAsInt(), entityMap.get("inventoryIconLayout").getAsBoolean());
		for (JsonElement info : entityMap.getAsJsonArray("inventory")) {
			JsonObject item = info.getAsJsonObject();
			if (Items.getItems().containsKey(item.get("name").getAsString())) {
				inventory.addItem(new Item(Items.getItems().get(item.get("name").getAsString()), inventory,
						item.get("amount").getAsInt()));
			} else {
				inventory.addItem(
						new Item(new CustomItem(item.get("name").getAsString(), item.get("description").getAsString(),
								item.get("maxStack").getAsInt(), item.get("weightPerItem").getAsDouble(),
								item.get("memorySize").getAsInt()), inventory, item.get("amount").getAsInt()));
			}
		}
		ArrayList<Trait> spells = new ArrayList<Trait>();
		for (JsonElement spell : entityMap.getAsJsonArray("spells")) {
			spells.add(new Trait(new CustomFeature(spell.getAsJsonObject().get("name").getAsString(),
					spell.getAsJsonObject().get("description").getAsString()), spells));
		}

		LinkedHashMap<BaseInterface, Proficiency> proficiencies = new LinkedHashMap<BaseInterface, Proficiency>();
		JsonArray proficienciesArray = entityMap.getAsJsonArray("proficiencies");
		for (JsonElement info : proficienciesArray) {
			BaseInterface proficiency;
			if (BaseInterface.getProficiencyList().containsKey(info.getAsString())) {
				proficiencies.put(proficiency = BaseInterface.getProficiencyList().get(info.getAsString()),
						new Proficiency(proficiency, proficiencies));
			} else {
				proficiencies.put(proficiency = new CustomProficiency(info.getAsString()),
						new Proficiency(proficiency, proficiencies));
			}
		}

		ArrayList<Trait> features = new ArrayList<Trait>();
		for (JsonElement feature : entityMap.getAsJsonArray("features")) {
			features.add(new Trait(new CustomFeature(feature.getAsJsonObject().get("name").getAsString(),
					feature.getAsJsonObject().get("description").getAsString()), features));
		}

		JsonArray successes = entityMap.getAsJsonObject("deathSaves").getAsJsonArray("successes");
		for (int i = 0; i < successes.size(); i++) {
			deathSaves[0][i] = successes.get(i).getAsBoolean();
		}
		JsonArray failures = entityMap.getAsJsonObject("deathSaves").getAsJsonArray("failures");
		for (int i = 0; i < failures.size(); i++) {
			deathSaves[1][i] = failures.get(i).getAsBoolean();
		}

		ArmorInterface armor;
		JsonObject armorArray = entityMap.getAsJsonObject("armor");
		if (Armor.getGuardianArmor().containsKey(armorArray.get("name").getAsString())
				|| Armor.getGhostArmor().containsKey(armorArray.get("name").getAsString()))
			armor = ((ArmorInterface) EnumInterface.fromString(Armor.class, armorArray.get("name").getAsString()));
		else
			armor = new CustomArmor(armorArray.get("name").getAsString(), armorArray.get("description").getAsString(),
					Armor.ArmorLevel.valueOf(armorArray.get("level").getAsString()),
					armorArray.get("baseAC").getAsInt(), armorArray.get("maxAC").getAsInt(),
					armorArray.get("minStr").getAsInt(), armorArray.get("memorySize").getAsInt(),
					armorArray.get("weight").getAsDouble(), armorArray.get("stealth").getAsBoolean(), Main.d20Icon);
		int initiativeBonus = entityMap.get("initiativeBonus").getAsInt();

		JsonObject statsArray = entityMap.getAsJsonObject("stats");
		for (String stat : statsArray.keySet()) {
			JsonObject statInfo = statsArray.getAsJsonObject(stat);
			if (statInfo.has("raceModifier"))
				stats.put(Stats.valueOf(stat),
						new Stat(Stats.valueOf(stat), statInfo.get("value").getAsInt(),
								statInfo.get("otherModifier").getAsInt(), statInfo.get("raceModifier").getAsInt(),
								statInfo.get("savingThrows").getAsBoolean(), null));
			else
				stats.put(Stats.valueOf(stat), new Stat(Stats.valueOf(stat), statInfo.get("value").getAsInt(),
						statInfo.get("otherModifier").getAsInt(), statInfo.get("savingThrows").getAsBoolean(), null));
		}
		JsonObject skillsArray = entityMap.getAsJsonObject("skills");
		for (String skill : skillsArray.keySet()) {
			JsonObject skillInfo = skillsArray.getAsJsonObject(skill);
			skills.put(Skills.valueOf(skill), new Skill(Skills.valueOf(skill),
					skillInfo.get("proficiency").getAsBoolean(), skillInfo.get("expertise").getAsBoolean(), null));
		}

		Entity entity = new Entity(entityMap.get("characterName").getAsString(), inventory, spells, features,
				proficiencies, entityMap.get("maxHitPoints").getAsInt(), entityMap.get("currentHitPoints").getAsInt(),
				entityMap.get("temporaryHitPoints").getAsInt(), entityMap.get("shields").getAsInt(), deathSaves, armor,
				initiativeBonus, stats, skills, entityMap.get("advancedSheet").getAsBoolean());

		JsonArray weaponsArray = entityMap.getAsJsonArray("weapons");
		for (int i = 0; i < weaponsArray.size(); i++) {
			JsonObject weapon = weaponsArray.get(i).getAsJsonObject();
			if (Firearms.getFirearms().containsKey(weapon.get("name").getAsString()) || weapon.has("shots")) {
				PerkInterface[] perks = new PerkInterface[3];
				for (int p = 0; p < weapon.getAsJsonArray("perks").size(); p++) {
					if (weapon.getAsJsonArray("perks").get(p).isJsonObject()) {
						PerkInterface custom = new CustomPerk(
								weapon.getAsJsonArray("perks").get(p).getAsJsonObject().get("name").getAsString(),
								weapon.getAsJsonArray("perks").get(p).getAsJsonObject().get("description")
										.getAsString());
						perks[p] = Perk.perks.get(custom.getName());
					} else if (!weapon.getAsJsonArray("perks").get(p).getAsString().equals("null"))
						perks[p] = Perk.perks.get(weapon.getAsJsonArray("perks").get(p).getAsString());
				}
				FirearmInterface baseFirearm = null;
				if (Firearms.getFirearms().containsKey(weapon.get("name").getAsString()))
					baseFirearm = Firearms.getFirearms().get(weapon.get("name").getAsString());
				else {
					ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
					for (JsonElement info : weapon.getAsJsonArray("properties"))
						properties.add(WeaponProperties.valueOf(info.getAsString()));
					baseFirearm = new CustomFirearm(weapon.get("name").getAsString(),
							weapon.get("weightPerItem").getAsDouble(), weapon.get("memorySize").getAsInt(),
							weapon.getAsJsonArray("range").get(0).getAsInt(),
							weapon.getAsJsonArray("range").get(1).getAsInt(),
							weapon.getAsJsonArray("range").get(2).getAsInt(),
							RangeBand.valueOf(weapon.get("rangeBand").getAsString()),
							weapon.get("finesse").getAsBoolean(),
							DamageType.valueOf(weapon.get("damageType").getAsString()),
							Firearms.valueOf(weapon.get("firearmBase").getAsString()),
							Die.valueOf(weapon.get("damageDie").getAsString()), weapon.get("shots").getAsInt(),
							weapon.get("damageDieNumber").getAsInt(), properties);
				}
				new Firearm(baseFirearm, weapon.get("magicalBonus").getAsInt(), weapon.get("tier").getAsInt(), perks,
						entity.getWeapons(), inventory, weapon.get("amount").getAsInt(),
						weapon.get("proficient").getAsBoolean(), entity);
			} else if (Weapons.getWeapons().containsKey(weapon.get("name").getAsString()) || weapon.has("memorySize")) {
				WeaponInterface baseWeapon;
				if (Weapons.getWeapons().containsKey(weapon.get("name").getAsString()))
					baseWeapon = Weapons.getWeapons().get(weapon.get("name").getAsString());
				else {
					ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
					baseWeapon = new CustomWeapon(weapon.get("name").getAsString(),
							weapon.get("weightPerItem").getAsDouble(), weapon.get("memorySize").getAsInt(), 1,
							weapon.getAsJsonArray("range").get(0).getAsInt(),
							weapon.getAsJsonArray("range").get(2).getAsInt(), weapon.get("finesse").getAsBoolean(),
							weapon.get("thrown").getAsBoolean(),
							DamageType.valueOf(weapon.get("damageType").getAsString()),
							WeaponType.valueOf(weapon.get("weaponType").getAsString()),
							Die.valueOf(weapon.get("damageDie").getAsString()),
							weapon.get("damageDieNumber").getAsInt(), properties);
				}
				new Weapon(baseWeapon, weapon.get("magicalBonus").getAsInt(), entity.getWeapons(), inventory,
						weapon.get("amount").getAsInt(), weapon.get("proficient").getAsBoolean(), entity);
			}
		}
		return entity;
	}

	private static Guardian[] read(JsonArray charactersArray) {
		try {
			ArrayList<Guardian> characters = new ArrayList<Guardian>();
			for (Object arrayString : charactersArray) {
				JsonObject characterMap = (JsonObject) arrayString;

				ArrayList<Info> personalityTraits = new ArrayList<Info>();
				ArrayList<Info> ideals = new ArrayList<Info>();
				ArrayList<Info> bonds = new ArrayList<Info>();
				ArrayList<Info> flaws = new ArrayList<Info>();
				ArrayList<Info> languages = new ArrayList<Info>();
				ArrayList<Info> notes = new ArrayList<Info>();

				JsonObject information = characterMap.getAsJsonObject("information");
				JsonArray personalityTraitsArray = information.getAsJsonArray("personalityTraits");
				for (JsonElement info : personalityTraitsArray)
					Guardian.newInfo(personalityTraits, new JTextArea(info.getAsString()), null);
				JsonArray idealsArray = information.getAsJsonArray("ideals");
				for (JsonElement info : idealsArray)
					Guardian.newInfo(ideals, new JTextArea(info.getAsString()), null);
				JsonArray bondsArray = information.getAsJsonArray("bonds");
				for (JsonElement info : bondsArray)
					Guardian.newInfo(bonds, new JTextArea(info.getAsString()), null);
				JsonArray flawsArray = information.getAsJsonArray("flaws");
				for (JsonElement info : flawsArray)
					Guardian.newInfo(flaws, new JTextArea(info.getAsString()), null);
				JsonArray languagesArray = information.getAsJsonArray("languages");
				for (JsonElement info : languagesArray)
					Guardian.newInfo(languages, new JTextArea(info.getAsString()), null);
				JsonArray notesArray = information.getAsJsonArray("notes");
				for (JsonElement info : notesArray)
					Guardian.newInfo(notes, new JTextArea(info.getAsString()), null);

				CharacterSubClassPath subClassPath = null;
				if (characterMap.get("subClassPath") != null)
					subClassPath = SubClassPaths.valueOf(characterMap.get("subClassPath").getAsString());
				Ghost ghost;
				if (characterMap.getAsJsonObject("ghost").has("hitDice"))
					ghost = new Ghost(readEntity(characterMap.getAsJsonObject("ghost")),
							GhostBackground
									.valueOf(characterMap.getAsJsonObject("ghost").get("background").getAsString()),
							characterMap.getAsJsonObject("ghost").get("hitDice").getAsInt());
				else
					ghost = new Ghost(readEntity(characterMap.getAsJsonObject("ghost")), GhostBackground
							.valueOf(characterMap.getAsJsonObject("ghost").get("background").getAsString()), 1);
				Guardian character = new Guardian(readEntity(characterMap),
						Race.valueOf(characterMap.get("race").getAsString()),
						SubClasses.valueOf(characterMap.get("subClass").getAsString()), subClassPath,
						characterMap.get("level").getAsInt(), characterMap.get("hitDice").getAsInt(),
						Alignment.valueOf(characterMap.get("alignment").getAsString()),
						characterMap.get("experience").getAsInt(), characterMap.get("backstory").getAsString(),
						characterMap.get("age").getAsString(), characterMap.get("height").getAsString(),
						characterMap.get("weight").getAsString(), characterMap.get("eyes").getAsString(),
						characterMap.get("skin").getAsString(), characterMap.get("hair").getAsString(),
						personalityTraits, ideals, bonds, flaws, languages, notes,
						characterMap.get("inspiration").getAsInt(), ghost);
				characters.add(character);
			}
			Guardian[] characterArray = new Guardian[characters.size()];
			for (int i = 0; i < characters.size(); i++) {
				Guardian character = characters.get(i);
				characterArray[i] = character;
			}
			return characterArray;
		} catch (ClassCastException e) {
			File crash = new File(
					"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
			crash.getParentFile().mkdirs();
			try (PrintStream ps = new PrintStream(crash)) {
				e.printStackTrace(ps);
			} catch (FileNotFoundException crashError) {
				crashError.printStackTrace();
			}
			return null;
		}
	}

	private static void writeEntity(Entity entity, JsonObject entityArray) {
		entityArray.addProperty("characterName", entity.characterName);
		entityArray.addProperty("background", entity.getBackground().name());
		entityArray.addProperty("hitDice", (int) entity.getCharacterClass().getHitDice().getValue());

		JsonArray spells = new JsonArray();
		for (Trait spell : entity.spells) {
			JsonObject spellList = new JsonObject();
			spellList.addProperty("name", spell.name);
			spellList.addProperty("description", spell.trait.getText());
			spells.add(spellList);
		}
		entityArray.add("spells", spells);

		JsonArray proficiencies = new JsonArray();
		for (BaseInterface proficiency : entity.proficiencies.keySet())
			proficiencies.add(proficiency.toString());
		entityArray.add("proficiencies", proficiencies);

		JsonArray features = new JsonArray();
		for (Trait feature : entity.features) {
			JsonObject featureList = new JsonObject();
			featureList.addProperty("name", feature.name);
			featureList.addProperty("description", feature.trait.getText());
			features.add(featureList);
		}
		entityArray.add("features", features);

		JsonArray inventory = new JsonArray();
		for (Item item : entity.getInventory().getItems()) {
			JsonObject itemArray = new JsonObject();
			itemArray.addProperty("name", item.getItem().toString());
			if (!Items.getItems().containsKey(item.getItem().toString())) {
				itemArray.addProperty("description", item.getItem().getDescription());
				itemArray.addProperty("maxStack", item.getItem().getMaxStack());
				itemArray.addProperty("weightPerItem", item.getItem().getWeightPerItem());
				itemArray.addProperty("memorySize", item.getItem().getMemorySize());
			}
			itemArray.addProperty("amount", (int) item.amount.getValue());
			inventory.add(itemArray);
		}
		entityArray.addProperty("inventoryMaxSpaces", entity.getInventory().getMaxSpaces());
		entityArray.addProperty("inventoryIconLayout", entity.getInventory().iconLayout);
		entityArray.add("inventory", inventory);
		JsonArray weapons = new JsonArray();
		for (Weapon weapon : entity.getWeapons()) {
			JsonObject weaponArray = new JsonObject();
			weaponArray.addProperty("name", weapon.getWeapon().toString());
			if (!(weapon.getWeapon() instanceof Weapons) && !(weapon.getWeapon() instanceof Firearms)) {
				weaponArray.addProperty("memorySize", weapon.getWeapon().getMemorySize());
				weaponArray.addProperty("weightPerItem", weapon.getWeapon().getWeightPerItem());
				weaponArray.addProperty("description", weapon.getWeapon().getDescription());
				JsonArray range = new JsonArray();
				for (int rangeValue : weapon.getWeapon().getRange())
					range.add(rangeValue);
				weaponArray.add("range", range);
				weaponArray.addProperty("finesse", weapon.getWeapon().isFinesse());
				weaponArray.addProperty("thrown", weapon.getWeapon().isThrown());
				weaponArray.addProperty("damageType", weapon.getWeapon().getDamageType().name());
				weaponArray.addProperty("weaponType", weapon.getWeapon().getWeaponType().name());
				weaponArray.addProperty("damageDie", weapon.getWeapon().getDamageDie().name());
				weaponArray.addProperty("damageDieNumber", weapon.getWeapon().getDamageDieNumber());
				JsonArray properties = new JsonArray();
				for (WeaponProperties propertiesValue : weapon.getWeapon().getProperties())
					properties.add(propertiesValue.name());
				weaponArray.add("properties", properties);
				if (weapon.getWeapon() instanceof CustomFirearm) {
					CustomFirearm firearm = (CustomFirearm) weapon.getWeapon();
					weaponArray.addProperty("shots", weapon.getWeapon().getMaxStack());
					weaponArray.addProperty("rangeBand", firearm.getRangeBand().name());
					weaponArray.addProperty("firearmBase", firearm.base.name());
				}
			}
			weaponArray.addProperty("amount", (int) weapon.amount.getValue());
			weaponArray.addProperty("magicalBonus", weapon.magicalBonus);
			weaponArray.addProperty("proficient", weapon.proficient);
			if (weapon instanceof Firearm) {
				weaponArray.addProperty("tier", ((Firearm) weapon).tier);
				JsonArray weaponPerks = new JsonArray();
				for (PerkInterface perk : ((Firearm) weapon).perks) {
					if (perk != null) {
						if (perk instanceof CustomPerk) {
							JsonObject customPerk = new JsonObject();
							customPerk.addProperty("name", perk.toString());
							customPerk.addProperty("description", perk.getDescription());
							weaponPerks.add(customPerk);
						} else
							weaponPerks.add(perk.toString());

					} else
						weaponPerks.add("null");
				}
				weaponArray.add("perks", weaponPerks);
			}
			weapons.add(weaponArray);
		}
		entityArray.add("weapons", weapons);
		JsonObject deathSaves = new JsonObject();
		JsonArray successes = new JsonArray();
		successes.add(entity.getDeathSaves()[0][0]);
		successes.add(entity.getDeathSaves()[0][1]);
		successes.add(entity.getDeathSaves()[0][2]);
		deathSaves.add("successes", successes);
		JsonArray failures = new JsonArray();
		failures.add(entity.getDeathSaves()[1][0]);
		failures.add(entity.getDeathSaves()[1][1]);
		failures.add(entity.getDeathSaves()[1][2]);
		deathSaves.add("failures", failures);

		entityArray.add("deathSaves", deathSaves);
		entityArray.addProperty("maxHitPoints", entity.maxHitPoints);
		entityArray.addProperty("currentHitPoints", entity.currentHitPoints);
		entityArray.addProperty("temporaryHitPoints", entity.temporaryHitPoints);
		entityArray.addProperty("shields", entity.shields);

		JsonObject armorArray = new JsonObject();
		armorArray.addProperty("name", entity.armor.name());
		if (!(entity.armor instanceof Armor)) {
			armorArray.addProperty("description", entity.armor.getDescription());
			armorArray.addProperty("level", entity.armor.getLevel().name());
			armorArray.addProperty("baseAC", entity.armor.getBaseAC());
			armorArray.addProperty("maxAC", entity.armor.getMaxAC());
			armorArray.addProperty("minStr", entity.armor.getMinStr());
			armorArray.addProperty("memorySize", entity.armor.getMemorySize());
			armorArray.addProperty("weight", entity.armor.getWeightPerItem());
			armorArray.addProperty("stealth", entity.armor.isStealth());
		}
		entityArray.add("armor", armorArray);
		entityArray.addProperty("initiativeBonus", entity.initiativeBonus);

		JsonObject stats = new JsonObject();
		for (Stats stat : entity.getStats().keySet()) {
			JsonObject statArray = new JsonObject();
			statArray.addProperty("value", entity.getStats().get(stat).value);
			statArray.addProperty("otherModifier", entity.getStats().get(stat).otherModifier);
			if (entity.race.getMainRace() == MainRace.HUMAN || entity.race.getName().equals("Ghost"))
				statArray.addProperty("raceModifier", entity.getStats().get(stat).raceModifier);
			statArray.addProperty("savingThrows", entity.getStats().get(stat).savingThrowsBox.isSelected());
			stats.add(stat.toString(), statArray);
		}
		entityArray.add("stats", stats);

		JsonObject skills = new JsonObject();
		for (Skills skill : entity.getSkills().keySet()) {
			JsonObject skillArray = new JsonObject();
			skillArray.addProperty("proficiency", entity.getSkills().get(skill).proficiency.isSelected());
			skillArray.addProperty("expertise", entity.getSkills().get(skill).expertise.isSelected());
			skills.add(skill.name(), skillArray);
		}
		entityArray.add("skills", skills);
		entityArray.addProperty("advancedSheet", entity.advancedSheet);
	}

	public static String write(Guardian[] characters) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonArray jsonCharacters = new JsonArray();
		for (Guardian character : characters) {
			JsonObject characterArray = new JsonObject();
			writeEntity(character, characterArray);
			characterArray.addProperty("race", character.getRace().name());
			characterArray.addProperty("subClass", character.getCharacterClass().getSubClass().name());
			characterArray.addProperty("level", character.getLevel());
			if (character.getCharacterClass().getSubClassPath().name() != null)
				characterArray.addProperty("subClassPath", character.getCharacterClass().getSubClassPath().name());
			characterArray.addProperty("alignment", ((Alignment) character.alignment.getSelectedItem()).name());
			characterArray.addProperty("experience", character.experiencePoints);
			characterArray.addProperty("backstory", character.backstory);

			characterArray.addProperty("age", character.age);
			characterArray.addProperty("height", character.height);
			characterArray.addProperty("weight", character.weight);
			characterArray.addProperty("eyes", character.eyes);
			characterArray.addProperty("skin", character.skin);
			characterArray.addProperty("hair", character.hair);

			JsonObject information = new JsonObject();
			JsonArray personalityTraits = new JsonArray();
			for (Info personalityTrait : character.information.get("personalityTraits"))
				personalityTraits.add(personalityTrait.info.getText());
			information.add("personalityTraits", personalityTraits);
			JsonArray ideals = new JsonArray();
			for (Info ideal : character.information.get("ideals"))
				ideals.add(ideal.info.getText());
			information.add("ideals", ideals);
			JsonArray bonds = new JsonArray();
			for (Info bond : character.information.get("bonds"))
				bonds.add(bond.info.getText());
			information.add("bonds", bonds);
			JsonArray flaws = new JsonArray();
			for (Info flaw : character.information.get("flaws"))
				flaws.add(flaw.info.getText());
			information.add("flaws", flaws);
			JsonArray languages = new JsonArray();
			for (Info language : character.information.get("languages"))
				languages.add(language.info.getText());
			information.add("languages", languages);
			JsonArray notes = new JsonArray();
			for (Info note : character.information.get("notes"))
				notes.add(note.info.getText());
			information.add("notes", notes);
			characterArray.add("information", information);

			characterArray.addProperty("inspiration", character.inspiration);
			JsonObject ghost = new JsonObject();
			writeEntity(character.ghost, ghost);
			characterArray.add("ghost", ghost);

			jsonCharacters.add(characterArray);
		}
		JsonArray version = new JsonArray();
		version.add(Main.version.release);
		version.add(Main.version.major);
		version.add(Main.version.minor);

		if (!customFeatures.keySet().isEmpty()) {
			File file = new File("items\\weapons_" + System.getProperty("user.name").replace(":", "_") + ".json");
			file.getParentFile().mkdirs();
			try (PrintWriter writeweapons = new PrintWriter(file, charset)) {
				writeweapons.print(gson.toJson(customFeatures));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		File charactersJsonFile = new File("characters\\characters.json");
		charactersJsonFile.getParentFile().mkdirs();
		try (PrintWriter writeCharacters = new PrintWriter(charactersJsonFile, charset)) {
			writeCharacters.println(gson.toJson(version));
			writeCharacters.println(gson.toJson(Main.textFont.getSize()));
			writeCharacters.println(gson.toJson(jsonCharacters));
			return gson.toJson(jsonCharacters);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
		return null;

	}

	public static String keyOfMap(Map<String, Stat> map, Object object) {
		return (String) map.keySet().toArray()[Arrays.asList(map.values().toArray()).indexOf(object)];
	}

	public static String enumString(String enumString) {
		String input = enumString.replace("_OF", " of").replace("_THE", " the");
		String output = input.substring(0, 1);
		while (input.contains("_")) {
			output += input.substring(1, input.indexOf("_")).toLowerCase() + " ";
			input = input.substring(input.indexOf("_") + 1);
			output += input.substring(0, 1);
		}
		output += input.substring(1, input.length()).toLowerCase();
		return output;
	}

	public static class Version {
		int release;
		int major;
		int minor;

		Version(int release, int major, int minor) {
			this.release = release;
			this.major = major;
			this.minor = minor;
		}

		String readChangeLogLine(String line, Version fileVersion) {
			StringBuilder sb = new StringBuilder();
			if (!line.isEmpty() && checkVersion(line.substring(0, line.indexOf("/")), fileVersion)) {
				line = line.substring(line.indexOf("/") + 1);
				sb.append("\n  ");
				while (line.startsWith("    ")) {
					sb.append("        ");
					line = line.substring(4);
				}
				while (line.startsWith("\t")) {
					sb.append("        ");
					line = line.substring(1);
				}
				sb.append("\u2022 ");
				sb.append(line);
			}
			return sb.toString();
		}

		@Override
		public boolean equals(Object object) {
			Version version = (Version) object;
			return version.release == this.release && version.major == this.major && version.minor == this.minor;
		}

		private boolean checkVersion(String lineVersion, Version fileVersion) {
			int lineReleaseVer = Integer.parseInt(lineVersion.substring(0, lineVersion.indexOf('.')));
			int lineMajorVer = Integer
					.parseInt(lineVersion.substring(lineVersion.indexOf('.') + 1, lineVersion.lastIndexOf('.')));
			int lineMinorVer = Integer.parseInt(lineVersion.substring(lineVersion.lastIndexOf('.') + 1));
			if (fileVersion.release < lineReleaseVer) {
				return true;
			} else if (lineReleaseVer == fileVersion.release) {
				if (fileVersion.major < lineMajorVer) {
					return true;
				} else if (lineMajorVer == fileVersion.major) {
					if (fileVersion.minor < lineMinorVer) {
						return true;
					}
				}
			}
			return false;
		}

		@Override
		public String toString() {
			return release + "." + major + "." + minor;
		}
	}
}
