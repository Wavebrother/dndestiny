package guardian;

import java.awt.GridLayout;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.Races.MainRace;
import guardian.classes.CharacterClass;
import guardian.classes.CharacterSubClass;
import guardian.classes.CharacterSubClassPath;
import guardian.items.Item;
import guardian.items.itemEnums.Items;

public final class Guardian extends Entity {

	private static final int[] proficiencyBonusByLevel = { 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6 };

	private static final int[] lightAbilityByLevel = { 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5 };

	Ghost ghost;
	Map<String, ArrayList<Info>> information = new HashMap<String, ArrayList<Info>>();
	JComboBox<Alignment> alignment = new JComboBox<Alignment>(Alignment.values());
	int experiencePoints;
	String backstory = "";

	String age;
	String height;
	String weight;
	String eyes;
	String skin;
	String hair;

	int inspiration;

	public Guardian(Entity entity, Races race, CharacterSubClass subClass, CharacterSubClassPath subClassPath,
			int level, int hitDice, Alignment alignment, int experiencePoints, String backstory, String age,
			String height, String weight, String eyes, String skin, String hair, ArrayList<Info> personalityTraits,
			ArrayList<Info> ideals, ArrayList<Info> bonds, ArrayList<Info> flaws, ArrayList<Info> languages,
			ArrayList<Info> notes, int inspiration, Ghost ghost) {
		super(entity);
		this.race = race;
		if (race.getMainRace() == MainRace.VEX)
			this.alignment.setModel(new DefaultComboBoxModel<Alignment>(Alignment.getLawful()));
		this.alignment.setMaximumRowCount(9);
		this.setBackground(new Background() {
			@Override
			public String name() {
				return "RISEN";
			}

			@Override
			public String toString() {
				return "Risen";
			}

			@Override
			public String getDescription() {
				return "Chosen from the dead by the Travelerís Ghosts, Risen are those rare few able to wield the Light as a weapon. Risen are the only ones who can, and must, choose a Guardian class.\r\n"
						+ "\tSome Risen have been dead for centuries; others, merely minutes. Regardless, determine what sort of person you were in your past life and work with your GM to determine what, if anything, carried over. For example, if your race grants you a language, do you still remember that language? Do you still have memories of your past life, or living relatives? It can also be good to determine when and where you died, and how your Ghost found you.\r\n";
			}

			@Override
			public void setSkills(Entity entity) {
			}
		});
		this.setCharacterClass(new CharacterClass(subClass, level, hitDice));
		this.getCharacterClass().setSubClassPath(subClassPath);
		this.levelUp(level);
		this.alignment.setSelectedItem(alignment);

		this.experiencePoints = experiencePoints;
		this.backstory = backstory;
		this.age = age;
		this.height = height;
		this.weight = weight;
		this.eyes = eyes;
		this.skin = skin;
		this.hair = hair;
		this.inspiration = inspiration;

		this.information.put("personalityTraits", personalityTraits);
		this.information.put("ideals", ideals);
		this.information.put("bonds", bonds);
		this.information.put("flaws", flaws);
		this.information.put("languages", languages);
		this.information.put("notes", notes);

		this.ghost = ghost;
	}

	private Guardian(Guardian character) {
		this(character, character.getRace(), character.getCharacterClass().getSubClass(),
				character.getCharacterClass().getSubClassPath(), character.getLevel(),
				(int) character.getCharacterClass().getHitDice().getValue(),
				(Alignment) character.alignment.getSelectedItem(), character.experiencePoints, character.backstory,
				character.age, character.height, character.weight, character.eyes, character.skin, character.hair,
				character.information.get("personalityTraits"), character.information.get("ideals"),
				character.information.get("bonds"), character.information.get("flaws"),
				character.information.get("languages"), character.information.get("notes"), character.inspiration,
				character.ghost);
	}

	Guardian() {
		super();
		this.setBackground(new Background() {
			@Override
			public String name() {
				return "RISEN";
			}

			@Override
			public String toString() {
				return "Risen";
			}

			@Override
			public String getDescription() {
				return "Chosen from the dead by the Travelerís Ghosts, Risen are those rare few able to wield the Light as a weapon. Risen are the only ones who can, and must, choose a Guardian class.\r\n"
						+ "\tSome Risen have been dead for centuries; others, merely minutes. Regardless, determine what sort of person you were in your past life and work with your GM to determine what, if anything, carried over. For example, if your race grants you a language, do you still remember that language? Do you still have memories of your past life, or living relatives? It can also be good to determine when and where you died, and how your Ghost found you.\r\n";
			}

			@Override
			public void setSkills(Entity entity) {
			}
		});
		this.alignment.setMaximumRowCount(9);
		this.information.put("personalityTraits", new ArrayList<Info>());
		this.information.put("ideals", new ArrayList<Info>());
		this.information.put("bonds", new ArrayList<Info>());
		this.information.put("flaws", new ArrayList<Info>());
		this.information.put("otherProficiencies", new ArrayList<Info>());
		this.information.put("languages", new ArrayList<Info>());
		this.information.put("notes", new ArrayList<Info>());
		this.information.put("equipment", new ArrayList<Info>());
		this.ghost = new Ghost();
	}

	public static void newInfo(ArrayList<Info> list, JTextArea area, JPanel panel) {
		list.add(new Info(area, list));
		if (Main.character != null) {
			if (panel != null) {
				panel.add(list.get(list.size() - 1).display(panel));
			}
			Main.character.getContentPane().revalidate();
			Main.character.getContentPane().repaint();
		}
	}

	void setCharacterDescription(int age, int heightFeet, int heightInches, int weight, String eyes, String skin,
			String hair) {
		this.age = age + "";
		this.height = heightFeet + "\'" + heightInches + "\"";
		this.weight = weight + "";
		this.eyes = eyes;
		this.skin = skin;
		this.hair = hair;
	}

	void levelUp(int level) {
		this.getCharacterClass().setLevel(level);
		if (Main.character != null) {
			Main.character.shields.setModel(new SpinnerNumberModel(getCharacterClass().getSubClass().getShield(level), 0,
					getCharacterClass().getSubClass().getShield(level), 1));
			shields = getCharacterClass().getSubClass().getShield(level);
			((DefaultEditor) Main.character.shields.getEditor()).getTextField().setColumns(10);
			Main.character.proficiencyBonus.setText(getProficiencyBonus() + "");
			Main.character.repaint();
		}
	}

	JPanel getRacePanel() {
		JPanel panel = new JPanel(new GridLayout());
		JTextField raceComponent = new JTextField(getRace().toString());
		raceComponent.setEditable(false);
		panel.add(raceComponent);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Race",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		raceComponent.setFont(Main.textFont);
		return panel;
	}

	JPanel getAlignment() {
		JPanel panel = new JPanel(new GridLayout());
		panel.add(alignment);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Alignment",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		return panel;
	}

	JPanel getExperiencePoints() {
		JPanel panel = new JPanel(new GridLayout());
		JSpinner experiencePointsComponent = new JSpinner(new SpinnerNumberModel(experiencePoints, 0, 10, 1));
		experiencePointsComponent.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				experiencePoints = (int) experiencePointsComponent.getValue();
			}
		});
		panel.add(experiencePointsComponent);
		panel.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Experience Points",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		experiencePointsComponent.setFont(Main.textFont);
		return panel;
	}

	JPanel getLevelPanel() {
		JPanel panel = new JPanel(new GridLayout());
		JSpinner levelComponent = new JSpinner(new SpinnerNumberModel(getLevel(), 1, 20, 1));
		levelComponent.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				levelUp((int) levelComponent.getValue());
				ghost.getCharacterClass().setLevel(getLevel());
			}
		});
		panel.add(levelComponent);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Level",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		levelComponent.setFont(Main.textFont);
		return panel;
	}

	public int getProficiencyBonus() {
		return proficiencyBonusByLevel[getLevel() - 1];
	}

	public int getLightAblityLevel() {
		return lightAbilityByLevel[getLevel() - 1];
	}

	public int getLevel() {
		return getCharacterClass().getLevel();
	}

	public long getGlimmer() {
		long glimmer = 0;
		for (Item item : getInventory().getItems()) {
			if (item.getItem() == Items.GLIMMER)
				glimmer += (int) item.amount.getValue();
		}
		for (Item item : ghost.getInventory().getItems()) {
			if (item.getItem() == Items.GLIMMER)
				glimmer += (int) item.amount.getValue();
		}
		return glimmer;
	}

	public String getGlimmerFormatted() {
		return NumberFormat.getNumberInstance().format(getGlimmer()) + " gl";
	}

	int newStat(Die die) {
		int rolls[] = new int[4];
		rolls[0] = die.roll();
		rolls[1] = die.roll();
		rolls[2] = die.roll();
		rolls[3] = die.roll();
		Arrays.sort(rolls);
		return rolls[1] + rolls[2] + rolls[3];
	}

	public String toString() {
		return this.characterName + " - Level " + this.getLevel() + " " + this.getRace().toString() + " "
				+ this.getCharacterClass().getSubClass().toString();
	}

	Guardian copy() {
		try {
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			ObjectOutputStream characters = new ObjectOutputStream(byteStream);
			characters.writeObject(this);

			characters.close();
			ByteArrayInputStream inStream = new ByteArrayInputStream(byteStream.toByteArray());
			ObjectInputStream readCharacters = new ObjectInputStream(inStream);

			Guardian character = new Guardian((Guardian) readCharacters.readObject());
			return character;
		} catch (IOException | ClassNotFoundException e) {
			return null;
		}
	}
}
