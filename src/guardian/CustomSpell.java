package guardian;

import guardian.Spell.CastingTime;
import guardian.Spell.Duration;
import guardian.Spell.SpellType;

public class CustomSpell implements SpellInterface {
	private String name;
	private String description;
	private final SpellType spellType;
	private final DamageType damageType;
	private final CastingTime castingTime;
	private final Duration duration;
	private final int[] range;

	CustomSpell(String name, String description, SpellType spellType, DamageType damageType, CastingTime castingTime, Duration duration,
			int... range) {
		this.name = name;
		this.description = description;
		this.spellType = spellType;
		this.damageType = damageType;
		this.castingTime = castingTime;
		this.duration = duration;
		this.range = range;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public SpellType getSpellType() {
		return spellType;
	}

	@Override
	public CastingTime getCastingTime() {
		return castingTime;
	}

	@Override
	public DamageType getDamageType() {
		return damageType;
	}

	@Override
	public Duration getDuration() {
		return duration;
	}

	public int[] getRange() {
		return range;
	}

	public int getCost() {
		return 0;
	}
}
