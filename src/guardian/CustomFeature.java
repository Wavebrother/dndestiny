package guardian;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class CustomFeature implements Feature {
	final private String name;
	private String description;

	CustomFeature(String name, String description) {
		this.name = name;
		this.description = description;
		if (!Main.customFeatures.has(name)) {
			JsonArray descriptions = new JsonArray();
			descriptions.add(description);
			Main.customFeatures.add(name, descriptions);
		} else if (description.length() > 10) {
			boolean equals = false;
			for (JsonElement element : Main.customFeatures.getAsJsonArray(name)) {
				if (equals = element.getAsString().contains(description))
					break;
			}
			if (!equals)
				Main.customFeatures.getAsJsonArray(name).add(description);
		}
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public String name() {
		return toString();
	}
}
