package guardian;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import guardian.Stat.Stats;

@SuppressWarnings("serial")
public class CharacterEditor extends JFrame implements ActionListener {

	Entity character;
	boolean guardian;

	JTabbedPane tabbedPane = new JTabbedPane();
	JButton done = new JButton("Done");
	// JButton characterClass;

	JSpinner age = new JSpinner(new SpinnerNumberModel(10, 3, 100000, 1));
	JSpinner heightFeet = new JSpinner(new SpinnerNumberModel(0, 0, 30, 1));
	JSpinner heightInches = new JSpinner(new SpinnerNumberModel(0, 0, 11, 1));
	JSpinner weight = new JSpinner(new SpinnerNumberModel(100, 1, 100000, 1));
	JTextField eyes = new JTextField();
	JTextField skin = new JTextField();
	JTextField hair = new JTextField();

	JSpinner initiativeBonus = new JSpinner(new SpinnerNumberModel(0, 0, 20, 1));

	Map<Stats, JSpinner[]> stats = new LinkedHashMap<Stats, JSpinner[]>();

	public CharacterEditor(Entity character) {
		super("Character Editor");
		this.character = character;
		this.guardian = character instanceof Guardian;

		this.init();
	}

	void init() {
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});

		tabbedPane.addTab("Character Info", initInfoPane());
		if (guardian)
			tabbedPane.addTab("Character Description", initDescriptionPane());
		tabbedPane.addTab("Character Stats", initStatsPane());

		done.addActionListener(this);

		this.setLayout(new BorderLayout());
		this.add(tabbedPane, BorderLayout.CENTER);
		this.add(done, BorderLayout.SOUTH);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setResizable(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}

	JPanel initInfoPane() {
		JPanel infoPane = new JPanel(new GridLayout(0, 1));
		JPanel initiativePane = new JPanel(new GridLayout());
		initiativePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Initiative Bonus", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		initiativeBonus.setModel(new SpinnerNumberModel(character.initiativeBonus, 0, 20, 1));
		initiativePane.add(initiativeBonus);

		// infoPane.add(characterClass);
		infoPane.add(initiativePane);
		infoPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		return infoPane;
	}

	JPanel initDescriptionPane() {
		JPanel descriptionPane = new JPanel(new GridLayout(0, 1));
		Guardian character = (Guardian) this.character;
		age = new JSpinner(new SpinnerNumberModel(Integer.parseInt(character.age), 3, 100000, 1));
		heightFeet = new JSpinner(new SpinnerNumberModel(
				Integer.parseInt(character.height.substring(0, character.height.indexOf("\'"))), 0, 30, 1));
		heightInches = new JSpinner(new SpinnerNumberModel(
				Integer.parseInt(
						character.height.substring(character.height.indexOf('\'') + 1, character.height.indexOf("\""))),
				0, 11, 1));
		weight = new JSpinner(new SpinnerNumberModel(Integer.parseInt(character.weight), 1, 100000, 1));
		eyes = new JTextField(character.eyes);
		skin = new JTextField(character.skin);
		hair = new JTextField(character.hair);

		JPanel agePane = new JPanel(new GridLayout());
		agePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Age",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		agePane.add(age);

		JPanel heightPane = new JPanel();
		heightPane.setLayout(new BoxLayout(heightPane, BoxLayout.X_AXIS));
		heightPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Height",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		heightPane.add(heightFeet);
		heightPane.add(new JLabel("'"));
		heightPane.add(heightInches);
		heightPane.add(new JLabel("\""));

		JPanel weightPane = new JPanel(new GridLayout());
		weightPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Weight",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		weightPane.add(weight);

		JPanel eyesPane = new JPanel(new GridLayout());
		eyesPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Eyes",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		eyesPane.add(eyes);

		JPanel skinPane = new JPanel(new GridLayout());
		skinPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Skin",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		skinPane.add(skin);

		JPanel hairPane = new JPanel(new GridLayout());
		hairPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Hair",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		hairPane.add(hair);

		descriptionPane.add(agePane);
		descriptionPane.add(heightPane);
		descriptionPane.add(weightPane);
		descriptionPane.add(eyesPane);
		descriptionPane.add(skinPane);
		descriptionPane.add(hairPane);
		return descriptionPane;
	}

	JPanel initStatsPane() {
		JPanel statsPane = new JPanel(new GridLayout(0, 2));

		for (Stats key : Stats.values()) {
			stats.put(key, new JSpinner[2]);
			JSpinner[] stat = stats.get(key);
			JPanel statPanel = new JPanel(new GridLayout(0, 2));
			statPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			statPanel.add(new JLabel(key.getName()));
			stat[0] = new JSpinner(new SpinnerNumberModel(10, 3, 30, 1));
			stat[0].setValue(character.getStats().get(key).value);
			statPanel.add(stat[0]);
			statPanel.add(new JLabel("Bonus"));
			stat[1] = new JSpinner(new SpinnerNumberModel(0, -5, 10, 1));
			stat[1].setValue(character.getStats().get(key).otherModifier);
			statPanel.add(stat[1]);
			statsPane.add(statPanel);
		}
		return statsPane;
	}

	public JButton open(boolean guardian) {
		JButton open;
		if (guardian)
			open = new JButton("Open Guardian Editor");
		else
			open = new JButton("Open Ghost Editor");
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
			}

		});
		open.setFont(Main.titleFont);
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == done) {
			if (guardian) {
				Guardian character = (Guardian) this.character;
				character.setCharacterDescription((int) age.getValue(), (int) heightFeet.getValue(),
						(int) heightInches.getValue(), (int) weight.getValue(), eyes.getText(), skin.getText(),
						hair.getText());
				character.initiativeBonus = (Integer) initiativeBonus.getValue();
			}
			for (Stats stat : character.getStats().keySet()) {
				character.getStats().get(stat).value = (Integer) stats.get(stat)[0].getValue();
				character.getStats().get(stat).otherModifier = (Integer) stats.get(stat)[1].getValue();
			}
			Main.character.initStatsPane();

			Main.character.getContentPane().revalidate();
			Main.character.getContentPane().repaint();

			for (Skill skill : character.getSkills().values())
				skill.updateModifierLabel();

			this.setVisible(false);
		}
	}
}