package guardian;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import guardian.Entity.Speed;
import guardian.Language.AncientLanguages;
import guardian.Language.ExoticLanguages;
import guardian.Language.ModernLanguages;
import guardian.Skill.Skills;
import guardian.Stat.Stats;
import guardian.items.Tool.Tools;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.WeaponProperties;
import guardian.items.interfaces.WeaponInterface;

public interface Races {

	LinkedHashMap<Speed, Integer> getSpeed();

	String name();

	String getName();

	MainRace getMainRace();

	default LinkedHashSet<Language> getLanguages() {
		return new LinkedHashSet<Language>();
	}

	default LinkedHashSet<Language> getLanguageChoices() {
		return new LinkedHashSet<Language>();
	}

	default LinkedHashSet<BaseInterface> getProficiencyChoices() {
		return new LinkedHashSet<BaseInterface>();
	}

	Size getSize();

	LinkedHashSet<RaceFeature> getFeatures();

	default HashMap<Stats, Integer> getAbilityScoreIncreases() {
		LinkedHashMap<Stats, Integer> stats = new LinkedHashMap<Stats, Integer>();
		for (Stats stat : Stats.values())
			stats.put(stat, 0);
		return stats;
	}

	WeaponInterface getUnarmedStrikes();

	enum MainRace implements BaseInterface {
		AWOKEN, HUMAN, EXO, CABAL, PSION, ELIKSNI, VEX, KRILL;

		@Override
		public String toString() {
			return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
		}

		public LinkedHashMap<Speed, Integer> getSpeed() {
			LinkedHashMap<Speed, Integer> speed = new LinkedHashMap<Speed, Integer>();
			speed.put(Speed.WALKING, 0);
			speed.put(Speed.SWIMMING, 0);
			speed.put(Speed.FLYING, 0);
			switch (this) {
			case AWOKEN:
			case HUMAN:
			case EXO:
			case ELIKSNI:
			case KRILL:
				speed.put(Speed.WALKING, 30);
				break;
			case CABAL:
			case PSION:
				speed.put(Speed.WALKING, 25);
				break;
			case VEX:
				break;
			}
			return speed;
		}

		public LinkedHashSet<Language> getLanguages() {
			LinkedHashSet<Language> languages = new LinkedHashSet<Language>();
			switch (this) {
			case AWOKEN:
			case HUMAN:
			case EXO:
				languages.add(ModernLanguages.HUMANCOMMON);
				break;
			case CABAL:
			case PSION:
				languages.add(ExoticLanguages.ULURANT);
				break;
			case ELIKSNI:
				languages.add(ExoticLanguages.ELIKSNI);
				break;
			case KRILL:
				languages.add(ExoticLanguages.KRILL);
				break;
			case VEX:
				languages.add(ExoticLanguages.HEXINARY);
				break;
			default:
				break;
			}
			return languages;
		}

		public LinkedHashSet<RaceFeature> getFeatures() {
			LinkedHashSet<RaceFeature> features = new LinkedHashSet<RaceFeature>();
			switch (this) {
			case AWOKEN:
				features.add(RaceFeature.AWOKENANCESTRY);
				features.add(RaceFeature.AWOKENDARKVISION);
				break;
			case EXO:
				features.add(RaceFeature.EXODARKVISION);
				features.add(RaceFeature.EXOLIVINGMACHINE);
				break;
			case CABAL:
				features.add(RaceFeature.POWERFULSTATURE);
				features.add(RaceFeature.HEAVYWEIGHT);
				features.add(RaceFeature.STRONGGRIP);
				break;
			case ELIKSNI:
				features.add(RaceFeature.BRANCHINGNERVES);
				break;
			case PSION:
				features.add(RaceFeature.PSYCHOKINETICS);
				features.add(RaceFeature.NATURALLYSTEALTHY);
				features.add(RaceFeature.PSIONNIMBLENESS);
				break;
			case VEX:
				features.add(RaceFeature.VEXLIVINGMACHINE);
				features.add(RaceFeature.SUPERIORMEMORY);
				features.add(RaceFeature.TELEPORT);
				break;
			default:
				break;
			}
			if (this == ELIKSNI || this == KRILL || this == PSION || this == VEX)
				features.add(RaceFeature.DARKVISION);
			return features;
		}

		public Size getSize() {
			switch (this) {
			case AWOKEN:
			case HUMAN:
			case EXO:
			case CABAL:
			case ELIKSNI:
			case KRILL:
			case VEX:
				return Size.MEDIUM;
			case PSION:
				return Size.SMALL;
			}
			return null;
		}

		public HashMap<Stats, Integer> getAbilityScoreIncreases() {
			HashMap<Stats, Integer> stats = new LinkedHashMap<Stats, Integer>();
			for (Stats stat : Stats.values())
				stats.put(stat, 0);
			switch (this) {
			case CABAL:
				stats.put(Stats.Strength, 1);
				stats.put(Stats.Constitution, 2);
				break;
			case ELIKSNI:
				stats.put(Stats.Wisdom, 1);
				break;
			case PSION:
				stats.put(Stats.Dexterity, 2);
				stats.put(Stats.Intelligence, 1);
				break;
			case VEX:
				stats.put(Stats.Intelligence, 2);
				break;
			default:
				break;
			}
			return stats;
		}
	}

	public enum Race implements Races {
		EARTHBORN, REEFBORN, CITYBORN, PILGRIM, MODEL_ABU, MODEL_CTHD, MODEL_JSYK, CABAL, CAPTAIN, VANDAL, KRILL,
		MOTHERMORPH, KNIGHTMORPH, PSION, GOBLIN, HOBGOBLIN;

		@Override
		public String getName() {
			switch (this) {
			case MODEL_ABU:
				return "Model \"AB-U\"";
			case MODEL_CTHD:
				return "Model \"CT-HD\"";
			case MODEL_JSYK:
				return "Model \"JSYK\"";
			case MOTHERMORPH:
				return "Mother Morph";
			case KNIGHTMORPH:
				return "Knight Morph";
			default:
				return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
			}
		}

		public String toString() {
			switch (this) {
			case CABAL:
			case KRILL:
			case PSION:
				return getName();
			case CAPTAIN:
			case VANDAL:
			case GOBLIN:
			case HOBGOBLIN:
			case MODEL_ABU:
			case MODEL_CTHD:
			case MODEL_JSYK:
			case PILGRIM:
				return getMainRace().toString() + " " + getName();
			default:
				return getName() + " " + getMainRace().toString();
			}
		}

		public MainRace getMainRace() {
			switch (this) {
			case EARTHBORN:
			case REEFBORN:
				return MainRace.AWOKEN;
			case CITYBORN:
			case PILGRIM:
				return MainRace.HUMAN;
			case MODEL_ABU:
			case MODEL_CTHD:
			case MODEL_JSYK:
				return MainRace.EXO;
			case CAPTAIN:
			case VANDAL:
				return MainRace.ELIKSNI;
			case MOTHERMORPH:
			case KNIGHTMORPH:
				return MainRace.KRILL;
			case GOBLIN:
			case HOBGOBLIN:
				return MainRace.VEX;
			case CABAL:
				return MainRace.CABAL;
			case PSION:
				return MainRace.PSION;
			case KRILL:
				return MainRace.KRILL;
			}
			return null;
		}

		@Override
		public LinkedHashMap<Speed, Integer> getSpeed() {
			LinkedHashMap<Speed, Integer> speed = getMainRace().getSpeed();
			switch (this) {
			case GOBLIN:
				speed.put(Speed.WALKING, 15);
				break;
			case HOBGOBLIN:
				speed.put(Speed.WALKING, 20);
				break;
			default:
				break;
			}
			return speed;
		}

		@Override
		public LinkedHashSet<Language> getLanguages() {
			return getMainRace().getLanguages();
		}

		@Override
		public LinkedHashSet<Language> getLanguageChoices() {
			LinkedHashSet<Language> languages = new LinkedHashSet<Language>();
			switch (this) {
			case CITYBORN:
				languages.addAll(Arrays.asList(ModernLanguages.values()));
				languages.addAll(Arrays.asList(AncientLanguages.values()));
				break;
			default:
				break;
			}
			return languages;
		}

		@Override
		public LinkedHashSet<BaseInterface> getProficiencyChoices() {
			LinkedHashSet<BaseInterface> proficiencies = new LinkedHashSet<BaseInterface>();
			switch (this) {
			case MODEL_JSYK:
				proficiencies.add(Skills.Perception);
				proficiencies.add(Skills.Investigation);
				break;
			case CAPTAIN:
				proficiencies.add(Skills.Intimidation);
				proficiencies.add(Skills.Athletics);
				break;
			case VANDAL:
				proficiencies.add(Skills.Acrobatics);
				proficiencies.add(Skills.SleightOfHand);
				break;
			case EARTHBORN:
				proficiencies.addAll(Arrays.asList(Skills.values()));
				proficiencies.addAll(Arrays.asList(Tools.values()));
				break;
			default:
				break;
			}
			return proficiencies;
		}

		@Override
		public LinkedHashSet<RaceFeature> getFeatures() {
			LinkedHashSet<RaceFeature> features = getMainRace().getFeatures();
			switch (this) {
			case REEFBORN:
				features.add(RaceFeature.WEAPONFAMILIARITY);
				features.add(RaceFeature.AGILEMOVEMENT);
				break;
			case EARTHBORN:
				features.add(RaceFeature.DIVERSEUPBRINGING);
				break;
			case CITYBORN:
				features.add(RaceFeature.BONUSLANGUAGEPROFICIENCY);
				features.add(RaceFeature.CULTURALIMPACT);
				break;
			case PILGRIM:
				features.add(RaceFeature.ROADSAVVY);
				break;
			case MODEL_ABU:
				features.add(RaceFeature.EXOTOUGHNESS);
				features.add(RaceFeature.HARDENEDFRAME);
				break;
			case MODEL_CTHD:
				features.add(RaceFeature.ATHLETICBUILD);
				break;
			case MODEL_JSYK:
				features.add(RaceFeature.UNOBTRUSIVESCANNERS);
				features.add(RaceFeature.OBSERVATIONUNIT);
				break;
			case CAPTAIN:
				features.add(RaceFeature.CAPTAINSKILLPROFICIENCY);
				features.add(RaceFeature.BRUTALPUNCHES);
				break;
			case VANDAL:
				features.add(RaceFeature.VANDALSKILLPROFICIENCY);
				features.add(RaceFeature.VANDALEVASION);
				break;
			case MOTHERMORPH:
				features.add(RaceFeature.TRANCE);
				features.add(RaceFeature.HOVER);
				break;
			case KNIGHTMORPH:
				features.add(RaceFeature.CHITINOUSPLATING);
				features.add(RaceFeature.HEAVYWEIGHT);
				break;
			case GOBLIN:
				features.add(RaceFeature.BLINK);
				break;
			case HOBGOBLIN:
				features.add(RaceFeature.AIMBOT);
				features.add(RaceFeature.SCANNERS);
				break;
			default:
				break;
			}
			features.addAll(getMainRace().getFeatures());
			return features;
		}

		@Override
		public Size getSize() {
			return getMainRace().getSize();
		}

		@Override
		public HashMap<Stats, Integer> getAbilityScoreIncreases() {
			HashMap<Stats, Integer> stats = getMainRace().getAbilityScoreIncreases();
			switch (this) {
			case REEFBORN:
				stats.put(Stats.Intelligence, 2);
				stats.put(Stats.Wisdom, 1);
				break;
			case EARTHBORN:
				stats.put(Stats.Dexterity, 1);
				stats.put(Stats.Charisma, 2);
				break;
			case MODEL_ABU:
				stats.put(Stats.Strength, 2);
				stats.put(Stats.Constitution, 1);
				break;
			case MODEL_CTHD:
				stats.put(Stats.Dexterity, 2);
				stats.put(Stats.Charisma, 1);
				break;
			case MODEL_JSYK:
				stats.put(Stats.Intelligence, 1);
				stats.put(Stats.Wisdom, 2);
				break;
			case CAPTAIN:
				stats.put(Stats.Strength, 2);
				break;
			case VANDAL:
				stats.put(Stats.Dexterity, 2);
				break;
			default:
				break;
			}
			return stats;
		}

		@Override
		public WeaponInterface getUnarmedStrikes() {
			switch (this) {
			case MODEL_CTHD:
			case CAPTAIN:
				return new WeaponInterface() {

					@Override
					public String toString() {
						return "Unarmed Strikes";
					}

					@Override
					public String name() {
						return toString();
					}

					@Override
					public boolean isFinesse() {
						return true;
					}

					@Override
					public DamageType getDamageType() {
						return DamageType.BLUDGEONING;
					}

					public WeaponType getWeaponType() {
						return WeaponType.SIMPLE_MELEE;
					}

					@Override
					public Die getDamageDie() {
						return Die.d4;
					}

					@Override
					public int getDamageDieNumber() {
						return 1;
					}

					@Override
					public ArrayList<WeaponProperties> getProperties() {
						return null;
					}

				};
			default:
				return null;
			}
		}
	}

	enum Size {
		TINY, SMALL, MEDIUM, Large, Gargantuan
	}

	enum RaceFeature implements Feature {
		AWOKENDARKVISION("Darkvision"), AWOKENANCESTRY("Awoken Ancestry"), WEAPONFAMILIARITY("Weapon Familiarity"),
		AGILEMOVEMENT("Agile Movement"), DIVERSEUPBRINGING("Diverse Upbringing"),
		BONUSLANGUAGEPROFICIENCY("Bonus Language Proficiency"), CULTURALIMPACT("Cultural Impact"),
		ROADSAVVY("Road Savvy"), EXODARKVISION("Darkvision"), EXOLIVINGMACHINE("Living Machine"),
		EXOTOUGHNESS("Exo Toughness"), HARDENEDFRAME("Hardened Frame"), ATHLETICBUILD("Athletic Build"),
		UNOBTRUSIVESCANNERS("Unobtrusive Scanners"), OBSERVATIONUNIT("Observation Unit"),
		POWERFULSTATURE("Powerful Stature"), HEAVYWEIGHT("Heavyweight"), STRONGGRIP("Strong Grip"),
		DARKVISION("Darkvision"), BRANCHINGNERVES("Branching Nerves"), CAPTAINSKILLPROFICIENCY("Skill Proficiency"),
		BRUTALPUNCHES("Brutal Punches"), VANDALSKILLPROFICIENCY("Skill Proficiency"), VANDALEVASION("Vandal Evasion"),
		TRANCE("Trance"), HOVER("Hover"), CHITINOUSPLATING("Chitinous Plating"), PSYCHOKINETICS("Psychokinetics"),
		NATURALLYSTEALTHY("Naturally Stealthy"), PSIONNIMBLENESS("Psion Nimbleness"),
		VEXLIVINGMACHINE("Living Machine"), SUPERIORMEMORY("Superior Memory"), TELEPORT("Teleport"), BLINK("Blink"),
		AIMBOT("Aimbot"), SCANNERS("Scanners"), MEMORY_BANK("Memory Bank");

		final String name;

		RaceFeature(String name) {
			this.name = name;
		}

		RaceFeature() {
			this.name = toString().substring(0, 1) + toString().substring(1).toLowerCase();
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public String getDescription() {
			switch (this) {
			case AWOKENDARKVISION:
				return "Born of Light and Darkness, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.";
			case AWOKENANCESTRY:
				return "You have advantage on all saving throws against Darkness effects.";
			case WEAPONFAMILIARITY:
				return "You are naturally proficient in sidearms.";
			case AGILEMOVEMENT:
				return "People of the Reef often have to crawl through small spaces or pass through heavily obstructed areas, and you�ve learned to do this quickly over time. Moving through a space smaller than you does not count as difficult terrain for you.";
			case DIVERSEUPBRINGING:
				return "You gain proficiency in one skill or one tool of your choice.";
			case BONUSLANGUAGEPROFICIENCY:
				return "You learn one language of Humanity of your choice.";
			case CULTURALIMPACT:
				return "You gain proficiency in one toolkit or artisan�s tool of your choice.";
			case ROADSAVVY:
				return "You gain proficiency in the Survival skill.";
			case EXODARKVISION:
				return "You have superior vision in dark and dim conditions because, even though the cameras you use for eyes were built to mimic human vision capabilities, the devs still gave them a slight buff. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.";
			case EXOLIVINGMACHINE:
				return "Even though you are technically a machine, you are considered a living creature. You are immune to incompatible diseases and viruses. You do not need to eat or breathe, but you can ingest food and drink if you wish. Instead of sleeping, you enter an inactive state for 4 hours each day. While inactive, you may �dream;� these dreams are a result of memory defragmentation. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep";
			case EXOTOUGHNESS:
				return "Your hit point maximum increases by 1, and it increases by 1 every time you gain a level.";
			case HARDENEDFRAME:
				return "While unarmored, your Armor Class is equal to 8 + your Constitution modifier + your Dexterity modifier (minimum 10).";
			case ATHLETICBUILD:
				return "Your unarmed attacks deal 1d4 damage, and you may choose to use either Strength or Dexterity for your attack and damage rolls. You must use the same ability score for both rolls.";
			case UNOBTRUSIVESCANNERS:
				return "You have scanners to a range of 5 feet. Consciously, you interpret your scanners as an intense gut feeling, or you may be unable to shake the feeling of being watched. This is to prevent your conscious mind from being overwhelmed with information.";
			case OBSERVATIONUNIT:
				return "You gain proficiency in either Perception or Investigation.";
			case POWERFULSTATURE:
				return "You have advantage on Strength saving throws against being knocked prone.";
			case HEAVYWEIGHT:
				return "You count as one size larger when determining what you can carry, lift, push, or pull.";
			case STRONGGRIP:
				return "You can use two-handed weapons in one hand without penalty, unless it has the bulky property. You can use your free hand to hold or manipulate simple objects, but can only wield a second weapon if it is a one-handed weapon.";
			case DARKVISION:
				return "You have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light.";
			case BRANCHINGNERVES:
				return "Even though you have four arms, your lower limbs cannot act entirely independently of your upper limbs. Because of this, you consider yourself to have only two hands when determining what you can hold or wield.";
			case CAPTAINSKILLPROFICIENCY:
				return "You gain proficiency in the Intimidation or Athletics skill.";
			case BRUTALPUNCHES:
				return "You hit particularly hard with your unarmed strikes, which deal 1d4 + your Strength modifier in bludgeoning damage. Your punches can overcome natural resistances to bludgeoning damage.";
			case VANDALSKILLPROFICIENCY:
				return "You gain proficiency in the Acrobatics or Sleight of Hand skill.";
			case VANDALEVASION:
				return "Upon failing a Dexterity saving throw, you may choose to succeed instead. Once you use this feature you can�t use it again until you complete a long rest.";
			case TRANCE:
				return "You don�t need to sleep. Instead, you meditate deeply remaining semiconscious, for 4 hours a day. While meditating, you can dream after a fashion; such dreams are actually mental exercises that have become reflexive through years of practice. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep.";
			case HOVER:
				return "You can naturally and easily hover 1 foot off the ground when you move, and you are immune to falling damage while not unconscious or stunned. As a bonus action, you may boost this hover up to 15 feet off a solid surface.";
			case CHITINOUSPLATING:
				return "You have a natural chitinous armor. While unarmored, your AC is 15, and you count as heavily armored";
			case PSYCHOKINETICS:
				return "You have a natural understanding of the way the Light works, and can use it to tap into Light energies to give yourself certain benefits. Your unarmed strikes deal energy damage instead of bludgeoning damage, and you can choose to add your Intelligence modifier to your unarmed strike attack and damage rolls instead of Strength. You can also take the Dash action as a bonus action.";
			case NATURALLYSTEALTHY:
				return "You can attempt to hide even when you are obscured only by a creature that is at least one size larger than you.";
			case PSIONNIMBLENESS:
				return "You can move through the space of any creature that is of a size larger than yours.";
			case VEXLIVINGMACHINE:
				return "Even though you are technically a machine, your core is a mixture of living creatures that together form your consciousness. You are immune to incompatible diseases or viruses. You do not eat or breathe. Instead of sleeping, you enter an inactive state for 4 hours each day. While inactive, you may �dream;� these dreams are a result of memory defragmentation. After resting in this way, you gain the same benefit that a human does from 8 hours of sleep.";
			case SUPERIORMEMORY:
				return "You are proficient in the History skill, and you can double your proficiency bonus for it.";
			case TELEPORT:
				return "You can teleport once as a bonus action or action to an unoccupied spot you can see within 15 feet of you. You regain usage of this feature at the end of your turn.";
			case BLINK:
				return "(Recharge 5-6). As a reaction to an attack being made against you, or being forced to make a saving throw, you may use your Teleport. If this removes you from the direction of the attack or area of effect, you are not affected.";
			case AIMBOT:
				return "Once per turn, you can choose to Aim without expending movement to do so.";
			case SCANNERS:
				return "As a bonus action, you can push your scanners to a range of 5 feet, learning the location of all living creatures or living machines within 5 feet of you.";
			case MEMORY_BANK:
				return "Ghosts can store items as data within their secondary memory. See Ghost Inventory for more information.";
			default:
				return "";
			}
		}

	}

}
