package guardian;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import guardian.Entity.Alignment;
import guardian.Ghost.GhostBackground;
import guardian.Races.MainRace;
import guardian.Races.Race;
import guardian.Skill.Skills;
import guardian.Stat.Stats;
import guardian.classes.CharacterClass;
import guardian.classes.CharacterSubClass.SubClasses;
import guardian.items.AddItem;
import guardian.items.NewWeapon;
import guardian.items.Weapon;

@SuppressWarnings("serial")
public class CharacterCreation extends JFrame implements ActionListener {

	Guardian character = new Guardian();

	JButton createCharacter = new JButton("Create Character");
	JButton next = new JButton("Next");
	JCheckBox advancedSheet = new JCheckBox("Advanced Character Sheet");
	JLabel guardianNameStats = new JLabel("Guardian");
	JLabel guardianNameSkills = new JLabel("Guardian");
	JLabel ghostNameStats = new JLabel("Ghost");
	JLabel ghostNameSkills = new JLabel("Ghost");

	JPanel infoPane = new JPanel();
	JPanel descriptionPane = new JPanel();
	JPanel backgroundPane = new JPanel();
	JPanel inventoryPane = new JPanel();
	JPanel healthPane = new JPanel();
	JPanel guardianStatsPane = new JPanel();
	JPanel guardianSkillsPane = new JPanel();
	JPanel ghostStatsPane = new JPanel();
	JPanel ghostSkillsPane = new JPanel();

	LinkedList<JPanel> panels = new LinkedList<JPanel>();
	JPanel displayedPanel;

	JComboBox<SubClasses> characterClass = new JComboBox<SubClasses>(SubClasses.values());
	JComboBox<GhostBackground> ghostBackground = new JComboBox<GhostBackground>(GhostBackground.values());
	JComboBox<Races> race = new JComboBox<Races>(Race.values());

	JSpinner age = new JSpinner(new SpinnerNumberModel(10, 3, 100000, 1));
	JSpinner heightFeet = new JSpinner(new SpinnerNumberModel(0, 0, 30, 1));
	JSpinner heightInches = new JSpinner(new SpinnerNumberModel(0, 0, 11, 1));
	JSpinner weight = new JSpinner(new SpinnerNumberModel(100, 1, 100000, 1));
	JTextField eyes = new JTextField();
	JTextField skin = new JTextField();
	JTextField hair = new JTextField();

	AddItem inventory = new AddItem(character.getInventory());

	JSpinner initiativeBonus = new JSpinner(new SpinnerNumberModel(0, 0, 20, 1));
	JSpinner maxHitPoints = new JSpinner(new SpinnerNumberModel(character.maxHitPoints, 1, 1000, 1));

	Map<Stats, JSpinner[]> guardianStats = new LinkedHashMap<Stats, JSpinner[]>();
	Map<Skills, JCheckBox[]> guardianSkills = new LinkedHashMap<Skills, JCheckBox[]>();

	Map<Stats, JSpinner[]> ghostStats = new LinkedHashMap<Stats, JSpinner[]>();
	Map<Skills, JCheckBox[]> ghostSkills = new LinkedHashMap<Skills, JCheckBox[]>();

	JButton guardianStatsButton = new JButton("Click to roll stats");
	JButton ghostStatsButton = new JButton("Click to roll stats");

	CharacterCreation() {
		super("New Character");

		this.init();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	void init() {
		initInfoPane();
		initDescriptionPane();
		initBackgroundPane();
		initGuardianStatsPane();
		initHealthPane();
		initInventoryPane();
		initGuardianSkillsPane();
		initGhostStatsPane();
		initGhostSkillsPane();

		for (JPanel panel : panels)
			if (this.getMinimumSize().height < panel.getPreferredSize().height)
				this.setMinimumSize(
						new Dimension(panel.getPreferredSize().height * 2 / 3, panel.getPreferredSize().height + 55));
		createCharacter.addActionListener(this);
		this.setLayout(new BorderLayout());
		displayedPanel = panels.pop();
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (panels.size() == 8) {
					guardianNameStats.setText(character.characterName);
					guardianNameSkills.setText(character.characterName);
					ghostNameStats.setText(character.ghost.characterName);
					ghostNameSkills.setText(character.ghost.characterName);
					if (((Race) race.getSelectedItem()).getMainRace() != MainRace.HUMAN)
						for (Stats stat : guardianStats.keySet()) {
							guardianStats.get(stat)[2]
									.setValue(((Race) race.getSelectedItem()).getAbilityScoreIncreases().get(stat));
							guardianStats.get(stat)[2].setEnabled(false);
						}
				}
				remove(displayedPanel);
				displayedPanel = panels.pop();
				add(displayedPanel, BorderLayout.CENTER);
				if (panels.size() == 0) {
					remove(next);
					add(createCharacter, BorderLayout.SOUTH);
				}
				revalidate();
				pack();
				repaint();
			}
		});
		this.add(displayedPanel, BorderLayout.CENTER);
		this.add(next, BorderLayout.SOUTH);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	void initInfoPane() {
		infoPane.setLayout(new GridLayout(0, 1));
		infoPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		infoPane.add(character.getCharacterName());
		JPanel characterClass = new JPanel(new GridLayout());
		characterClass.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Class",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		characterClass.setBackground(character.backgroundColor);
		characterClass.add(this.characterClass);
		this.characterClass.setMaximumRowCount(9);

		infoPane.add(characterClass);
		JPanel race = new JPanel(new GridLayout());
		race.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Race",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		race.setBackground(character.backgroundColor);
		race.add(this.race);
		this.race.setMaximumRowCount(9);
		this.race.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (((Races) CharacterCreation.this.race.getSelectedItem()).getMainRace() == MainRace.VEX)
					character.alignment.setModel(new DefaultComboBoxModel<Alignment>(Alignment.getLawful()));
				else
					character.alignment.setModel(new DefaultComboBoxModel<Alignment>(Alignment.values()));
			}

		});
		infoPane.add(race);
		JPanel alignment = new JPanel(new GridLayout());
		alignment.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Alignment",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		alignment.setBackground(character.backgroundColor);
		alignment.add(character.alignment);
		infoPane.add(alignment);
		infoPane.add(character.ghost.getCharacterName());
		JPanel background = new JPanel(new GridLayout());
		background.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Ghost Background", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		background.setBackground(character.ghost.backgroundColor);
		background.add(this.ghostBackground);
		infoPane.add(background);
		infoPane.add(advancedSheet);
		advancedSheet.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				advancedSheet.setSelected(false);
				advancedSheet.setEnabled(false);
				JOptionPane.showMessageDialog(advancedSheet,
						"The advanced character sheet is not available yet.\r\n"
								+ "When it is, you will not need to make any changes when you level up,\r\n"
								+ "the program will make all of the changes for you!",
						"", JOptionPane.ERROR_MESSAGE);
				// if(advancedSheet.isSelected())
				// advancedSheet.setSelected(JOptionPane.showConfirmDialog(advancedSheet, "",
				// "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) ==
				// JOptionPane.YES_OPTION);
			}
		});
		panels.add(infoPane);
	}

	void initDescriptionPane() {
		JPanel agePane = new JPanel(new GridLayout());
		agePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Age",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		agePane.add(age);

		JPanel heightPane = new JPanel();
		heightPane.setLayout(new BoxLayout(heightPane, BoxLayout.X_AXIS));
		heightPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Height",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		heightPane.add(heightFeet);
		heightPane.add(new JLabel("'"));
		heightPane.add(heightInches);
		heightPane.add(new JLabel("\""));

		JPanel weightPane = new JPanel(new GridLayout());
		weightPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Weight",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		weightPane.add(weight);

		JPanel eyesPane = new JPanel(new GridLayout());
		eyesPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Eyes",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		eyesPane.add(eyes);

		JPanel skinPane = new JPanel(new GridLayout());
		skinPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Skin",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		skinPane.add(skin);

		JPanel hairPane = new JPanel(new GridLayout());
		hairPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Hair",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		hairPane.add(hair);

		descriptionPane.setLayout(new GridLayout(0, 1));
		descriptionPane.add(agePane);
		descriptionPane.add(heightPane);
		descriptionPane.add(weightPane);
		descriptionPane.add(eyesPane);
		descriptionPane.add(skinPane);
		descriptionPane.add(hairPane);
		panels.add(descriptionPane);
	}

	void initBackgroundPane() {
		backgroundPane.setLayout(new GridLayout(0, 1));
		backgroundPane
				.add(new AddInfo("New Personality Trait", character.information.get("personalityTraits"), null).open());
		backgroundPane.add(new AddInfo("New Ideal", character.information.get("ideals"), null).open());
		backgroundPane.add(new AddInfo("New Bond", character.information.get("bonds"), null).open());
		backgroundPane.add(new AddInfo("New Flaw", character.information.get("flaws"), null).open());
		backgroundPane.add(new AddProficiency(character.proficiencies, null).open(true));
		backgroundPane.add(new AddInfo("New Language", character.information.get("languages"), null).open());
		backgroundPane.add(new AddInfo("New Charcter Note", character.information.get("notes"), null).open());
		panels.add(backgroundPane);
	}

	void initInventoryPane() {
		JPanel buttonPane = new JPanel(new GridLayout(1, 0));
		buttonPane.add(new NewWeapon(character).open);
		buttonPane.add(inventory.open());

		inventoryPane.setLayout(new BoxLayout(inventoryPane, BoxLayout.Y_AXIS));
		inventoryPane.add(buttonPane);
		panels.add(inventoryPane);
	}

	void initHealthPane() {
		JPanel initiativePane = new JPanel(new GridLayout());
		initiativePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Initiative Bonus", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		initiativePane.add(initiativeBonus);

		JPanel maxHitPointsPane = new JPanel(new GridLayout());
		maxHitPointsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Max Hit Points", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		maxHitPoints.setFont(Main.textFont);
		maxHitPointsPane.add(maxHitPoints);

		healthPane.setLayout(new GridLayout(0, 1));
		healthPane.add(initiativePane);
		healthPane.add(maxHitPointsPane);
		panels.add(healthPane);
	}

	void initGuardianStatsPane() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 2));
		for (Stats key : Stats.values()) {
			guardianStats.put(key, new JSpinner[3]);
			JSpinner[] stat = guardianStats.get(key);
			JPanel statPanel = new JPanel(new GridLayout(0, 2));
			statPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			statPanel.add(new JLabel(key.getName()));
			statPanel.add(stat[0] = new JSpinner(new SpinnerNumberModel(10, 3, 30, 1)));
			statPanel.add(new JLabel("Other Bonus"));
			statPanel.add(stat[1] = new JSpinner(new SpinnerNumberModel(0, -5, 10, 1)));
			statPanel.add(new JLabel("Race Bonus"));
			statPanel.add(stat[2] = new JSpinner(new SpinnerNumberModel(0, -5, 10, 1)));
			panel.add(statPanel);
		}

		guardianStatsButton.addActionListener(this);
		guardianStatsPane.setLayout(new BorderLayout());
		guardianStatsPane.add(guardianNameStats, BorderLayout.NORTH);
		guardianStatsPane.add(panel, BorderLayout.CENTER);
		guardianStatsPane.add(guardianStatsButton, BorderLayout.SOUTH);
		panels.add(guardianStatsPane);
	}

	void initGuardianSkillsPane() {
		JPanel guardianSkillsPane = new JPanel(new BorderLayout());
		this.guardianSkillsPane.setLayout(new GridLayout(0, 2));

		for (Skills skill : Skills.values()) {
			guardianSkills.put(skill, new JCheckBox[] { new JCheckBox(skill.toString()), new JCheckBox("Expertise") });
			this.guardianSkillsPane.add(guardianSkills.get(skill)[0]);
			this.guardianSkillsPane.add(guardianSkills.get(skill)[1]);
		}
		guardianSkillsPane.add(guardianNameSkills, BorderLayout.NORTH);
		guardianSkillsPane.add(this.guardianSkillsPane, BorderLayout.CENTER);
		panels.add(guardianSkillsPane);
	}

	void initGhostStatsPane() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(0, 2));
		for (Stats key : Stats.values()) {
			ghostStats.put(key, new JSpinner[3]);
			JSpinner[] stat = ghostStats.get(key);
			JPanel statPanel = new JPanel(new GridLayout(0, 2));
			statPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			statPanel.add(new JLabel(key.getName()));
			statPanel.add(stat[0] = new JSpinner(new SpinnerNumberModel(10, 3, 30, 1)));
			statPanel.add(new JLabel("Other Bonus"));
			statPanel.add(stat[1] = new JSpinner(new SpinnerNumberModel(0, -5, 10, 1)));
			statPanel.add(new JLabel("Race Bonus"));
			statPanel.add(stat[2] = new JSpinner(new SpinnerNumberModel(0, 0, 2, 1)));
			if (key != Stats.Dexterity && key != Stats.Intelligence)
				stat[2].setEnabled(false);
			else
				stat[2].setModel(new SpinnerNumberModel(1, 1, 2, 1));
			panel.add(statPanel);
		}

		ghostStatsButton.addActionListener(this);
		ghostStatsPane.setLayout(new BorderLayout());
		ghostStatsPane.add(ghostNameStats, BorderLayout.NORTH);
		ghostStatsPane.add(panel, BorderLayout.CENTER);
		ghostStatsPane.add(ghostStatsButton, BorderLayout.SOUTH);
		panels.add(ghostStatsPane);
	}

	void initGhostSkillsPane() {
		JPanel ghostSkillsPane = new JPanel(new BorderLayout());
		this.ghostSkillsPane.setLayout(new GridLayout(0, 2));

		for (Skills skill : Skills.values()) {
			ghostSkills.put(skill, new JCheckBox[] { new JCheckBox(skill.toString()), new JCheckBox("Expertise") });
			this.ghostSkillsPane.add(ghostSkills.get(skill)[0]);
			this.ghostSkillsPane.add(ghostSkills.get(skill)[1]);
		}
		this.ghostSkills.get(Skills.Technology)[0].setSelected(true);
		ghostSkillsPane.add(ghostNameSkills, BorderLayout.NORTH);
		ghostSkillsPane.add(this.ghostSkillsPane, BorderLayout.CENTER);
		panels.add(ghostSkillsPane);
	}

	int newStat(Die die) {
		int rolls[] = new int[4];
		rolls[0] = die.roll();
		rolls[1] = die.roll();
		rolls[2] = die.roll();
		rolls[3] = die.roll();
		Arrays.sort(rolls);
		return rolls[1] + rolls[2] + rolls[3];
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == createCharacter) {
			character.setCharacterDescription((Integer) age.getValue(), (Integer) heightFeet.getValue(),
					(Integer) heightInches.getValue(), (Integer) weight.getValue(), eyes.getText(), skin.getText(),
					hair.getText());
			character.initiativeBonus = (int) initiativeBonus.getValue();
			character.maxHitPoints = (int) maxHitPoints.getValue();
			character.setCharacterClass(new CharacterClass((SubClasses) characterClass.getSelectedItem()));
			for (BaseInterface proficiency : character.getCharacterClass().getSubClass().getProficiencies())
				character.proficiencies.put(proficiency, new Proficiency(proficiency, character.proficiencies));
			character.race = (Races) race.getSelectedItem();
			if (character.getRace().getUnarmedStrikes() != null)
				new Weapon(character.getRace().getUnarmedStrikes(), 0, character.getWeapons(), character.getInventory(),
						1, true, character);
			character.setHealthStats((Integer) initiativeBonus.getValue());
			character.setStats(guardianStats, false);
			character.setSkills(guardianSkills);
			character.ghost.setStats(ghostStats, true);
			character.ghost.setSkills(ghostSkills);

			character.ghost.init(GhostBackground.ARCHIVIST, 1);
			character.advancedSheet = advancedSheet.isSelected();
			Main.character = new MyCharacter(Main.selector.characters, character);
			Main.selector.setVisible(false);
			this.dispose();
		} else if (e.getSource() == guardianStatsButton) {
			guardianStatsButton.setText(newStat(Die.d6) + ", " + newStat(Die.d6) + ", " + newStat(Die.d6) + ", "
					+ newStat(Die.d6) + ", " + newStat(Die.d6) + ", " + newStat(Die.d6));
			guardianStatsButton.setIcon(null);
		} else if (e.getSource() == ghostStatsButton) {
			ghostStatsButton.setText(newStat(Die.d6) + ", " + newStat(Die.d6) + ", " + newStat(Die.d6) + ", "
					+ newStat(Die.d6) + ", " + newStat(Die.d6) + ", " + newStat(Die.d6));
			ghostStatsButton.setIcon(null);
		}
	}
}