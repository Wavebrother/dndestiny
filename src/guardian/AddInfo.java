package guardian;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AddInfo extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private ArrayList<Info> list;
	private JPanel panel;

	private JTextField area = new JTextField();

	private JButton add = new JButton("Add");
	private JButton done = new JButton("Done");

	public AddInfo(String title, ArrayList<Info> list, JPanel panel) {
		super(title);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});
		this.list = list;
		this.panel = panel;

		this.add.addActionListener(this);
		this.done.addActionListener(this);

		JPanel buttonPane = new JPanel(new GridLayout(1, 0));
		buttonPane.add(this.add);
		buttonPane.add(this.done);

		this.setLayout(new GridLayout(0, 1));
		this.add(this.area);
		this.add(buttonPane);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	public JButton open() {
		JButton open = new JButton(this.getTitle());
		open.setFont(Main.titleFont);
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
			}

		});
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.add || e.getSource() == this.done) {
			try {
				if (!this.area.getText().isEmpty()) {
					Guardian.newInfo(this.list, new JTextArea(this.area.getText()), this.panel);
				}
				this.area.setText("");
				this.area.requestFocus();
				if (e.getSource() == this.done)
					this.setVisible(false);
			} catch (RuntimeException error) {
				this.setVisible(false);
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					error.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
			}
		}
	}

}
