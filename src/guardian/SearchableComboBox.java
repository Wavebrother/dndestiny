package guardian;

import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class SearchableComboBox extends JComboBox<BaseInterface> {
	private static final long serialVersionUID = 1L;

	public BaseInterface[] items;
	private BaseInterface blankItem = new BaseInterface() {
		String name = "";
		@Override
		public String toString() {
			return name;
		}

		@Override
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String name() {
			return name;
		}
	};

	public SearchableComboBox() {
		super();
		this.setEditable(true);
		this.addActionListener(this);
		this.getEditor().getEditorComponent().addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				showPopup();
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (e.isTemporary()) {
					blankItem.setName("");
					filter(null);
				}
			}

		});
		this.getEditor().getEditorComponent().addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				filter(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}
		});
	}

	private void filter(KeyEvent e) {
		ArrayList<BaseInterface> filteredItems = new ArrayList<BaseInterface>();
		if (e != null)
			blankItem.setName(getEditor().getItem().toString());
		filteredItems.add(blankItem);
		for (int i = 1; i < items.length; i++) {
			if (items[i].toString().toLowerCase().contains(getEditor().getItem().toString().toLowerCase())) {
				filteredItems.add(items[i]);
			}
		}

		if (e == null || e.getKeyCode() != KeyEvent.VK_ENTER || filteredItems.size() > 2) {
			BaseInterface[] items = new BaseInterface[filteredItems.size()];
			for (int i = 0; i < items.length; i++)
				items[i] = filteredItems.get(i);
			setModel(new DefaultComboBoxModel<BaseInterface>(items));
			if (filteredItems.size() == 2 && (e == null || e.getKeyCode() != KeyEvent.VK_BACK_SPACE)) {
				String typed = blankItem.toString();
				blankItem.setName(items[1].toString());
				((JTextField) getEditor().getEditorComponent()).setText(blankItem.toString());
				((JTextField) getEditor().getEditorComponent()).select(typed.length(), blankItem.toString().length());
			}
			if (e != null)
				showPopup();
		}
		if ((e == null || e.getKeyCode() == KeyEvent.VK_ENTER) && filteredItems.size() == 2)
			setSelectedIndex(1);
	}

	public SearchableComboBox actionListener(ActionListener e) {
		super.addActionListener(e);
		return this;
	}

	public void setModel(BaseInterface[] objects) {
		this.items = new BaseInterface[objects.length + 1];
		this.items[0] = blankItem;
		for (int i = 1; i < this.items.length; i++)
			this.items[i] = objects[i - 1];
		super.setModel(new DefaultComboBoxModel<BaseInterface>(this.items));
	}
}
