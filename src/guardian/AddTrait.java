package guardian;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class AddTrait extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private SearchableComboBox features = new SearchableComboBox();
	private JCheckBox custom = new JCheckBox("Custom Feature");
	private JTextField name = new JTextField();
	private JTextField description = new JTextField();

	private ArrayList<Trait> list;
	private JPanel parent;

	private JButton add = new JButton("Add");
	private JButton done = new JButton("Done");

	public AddTrait(String title, Entity entity, ArrayList<Trait> list, JPanel parent, boolean spell) {
		super(title);
		this.list = list;
		this.parent = parent;
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});
		this.features.actionListener(this);

		this.add.addActionListener(this);
		this.done.addActionListener(this);

		JPanel buttonPane = new JPanel(new GridLayout(1, 0));
		buttonPane.add(this.add);
		buttonPane.add(this.done);

		JPanel namePanel = new JPanel(new GridLayout());
		namePanel.add(name);
		namePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel descriptionPanel = new JPanel(new GridLayout());
		descriptionPanel.add(description);
		descriptionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Description", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		this.custom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (custom.isSelected()) {
					AddTrait.this.remove(features);
					AddTrait.this.add(namePanel, 1);
					AddTrait.this.add(descriptionPanel, 2);
				} else {
					AddTrait.this.add(features, 1);
					AddTrait.this.remove(namePanel);
					AddTrait.this.remove(descriptionPanel);
				}
				AddTrait.this.revalidate();
				AddTrait.this.pack();
				AddTrait.this.repaint();
			}

		});
		this.setLayout(new GridLayout(0, 1));
		this.add(this.custom);
		this.add(this.features);
		this.add(buttonPane);
		if (spell) {
			this.features.setModel(Spell.values());
			this.custom.doClick();
			this.custom.setText("Custom Spell");
			this.custom.setEnabled(false);
		} else
			this.features
					.setModel(entity.getCharacterClass().getSubClass().getFeatures(20).toArray(new BaseInterface[0]));
		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	JButton open() {
		JButton open = new JButton(this.getTitle());
		open.setFont(Main.titleFont);
		open.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
			}

		});
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.add || e.getSource() == this.done) {
			try {
				Feature feature = null;
				Trait trait = null;
				if (!custom.isSelected()) {
					if (this.features.getSelectedItem() instanceof Feature
							&& !(feature = (Feature) this.features.getSelectedItem()).toString().isEmpty()) {
						list.add(trait = new Trait(feature, list));
					}
					this.features.setModel(new DefaultComboBoxModel<BaseInterface>(this.features.items));
					this.features.setSelectedIndex(0);
					this.features.showPopup();
				} else if (!name.getText().isEmpty()) {
					feature = new CustomFeature(name.getText(), description.getText());
					list.add(trait = new Trait(feature, list));
					name.setText("");
					description.setText("");
				}
				if (feature != null && parent != null) {
					parent.add(trait.display(parent));
					parent.revalidate();
					parent.repaint();
				}
				if (e.getSource() == this.done)
					this.setVisible(false);
			} catch (RuntimeException error) {
				this.setVisible(false);
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					error.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
			}
		}
	}
}
