package guardian;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class Check extends JButton implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Die die;
	private int rolls;
	private int modifier;

	public Check(Die die, int rolls, Icon icon, int modifier) {
		super(icon);
		this.die = die;
		this.rolls = rolls;
		this.modifier = modifier;

		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this) {
			int total = 0;
			StringBuilder output = new StringBuilder();
			output.append("You rolled ");
			for(int i = 0; i < rolls; i++) {
				int roll = this.die.roll();
				total += roll;
				output.append(roll);
				if(rolls > 3 && i + 1 < rolls)
					output.append(", ");
				if(rolls == 2 && i == 0)
					output.append(" ");
				if(i + 2 == rolls)
					output.append("and ");
			}
			output.append(" for a grand total of " + (total + modifier) + ".");
			
			JOptionPane.showMessageDialog(this, output.toString());
		}
	}

}
