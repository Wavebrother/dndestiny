package guardian;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;

public class CharacterSelector extends javax.swing.JFrame implements java.awt.event.ActionListener {

	private static final long serialVersionUID = 1L;

	private JComboBox<Guardian> characterSelection;
	private JSpinner fontSize = new JSpinner(new javax.swing.SpinnerNumberModel((int) ((Main.width / 1920.0) * 15), 6, 35, 1));
	private JButton select = new JButton("Select Character");
	private JButton copy = new JButton("Copy Character");
	private JButton create = new JButton("Create a New Character");
	private JButton delete = new JButton("Delete Character");

	private JButton dnDestinyProject = new JButton("D&Destiny Project Site");
	private JButton creditsButton = new JButton("Credits");
	private final JTextArea creditsLabel = new JTextArea();
	private static final java.io.InputStream creditsFile = Main.class.getResourceAsStream("/resources/credits");

	private JPanel buttonPanel = new JPanel(new GridLayout(0, 2));

	Guardian[] characters;

	public CharacterSelector(Guardian[] characters) {
		super("Character Selector - Version " + Main.version);
		this.characters = characters;

		try (BufferedReader readChangeLog = new BufferedReader(new java.io.InputStreamReader(creditsFile))) {
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = readChangeLog.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			sb.delete(sb.lastIndexOf("\n"), sb.length());
			creditsLabel.setText(sb.toString());
			creditsLabel.setFont(new Font(Font.MONOSPACED, Font.PLAIN, Main.textFont.getSize()));
			creditsLabel.setBackground(new java.awt.Color(0, 0, 0, 0));
			creditsLabel.setSelectionColor(null);
			creditsLabel.setSelectedTextColor(null);
			creditsLabel.setEditable(false);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.init();
		this.setLocationRelativeTo(null);
		this.setVisible(characters.length > 0);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	void init() {
		this.addWindowListener(new java.awt.event.WindowAdapter() {

			@Override
			public void windowClosing(java.awt.event.WindowEvent closeEvent) {
				Main.write(characters);
			}
		});
		characterSelection = new JComboBox<Guardian>(characters);
		select.addActionListener(this);
		copy.addActionListener(this);
		create.addActionListener(this);
		delete.addActionListener(this);
		dnDestinyProject.addActionListener(this);
		creditsButton.addActionListener(this);

		buttonPanel.add(create);
		if (characters.length > 0) {
			buttonPanel.add(copy);
			buttonPanel.add(select, 0);
			buttonPanel.add(delete);
		} else
			create.doClick();
		buttonPanel.add(creditsButton);
		buttonPanel.add(dnDestinyProject);

		JPanel fontPanel = new JPanel(new GridLayout());
		fontPanel.setBorder(BorderFactory.createTitledBorder("Font Size"));
		fontPanel.add(fontSize);

		this.setLayout(new BorderLayout());
		this.add(fontPanel, BorderLayout.NORTH);
		this.add(characterSelection, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);

		this.setIconImage(new javax.swing.ImageIcon(Main.logo).getImage());
		this.pack();
		this.setResizable(false);
	}

	@Override
	public void actionPerformed(java.awt.event.ActionEvent event) {
		if (event.getSource() == select) {
			Main.titleFont = new Font("Dialog", 1, (int) fontSize.getValue());
			Main.textFont = new Font("Dialog", 0, (int) fontSize.getValue());
			Main.character = new MyCharacter(characters, characterSelection.getSelectedIndex());
			this.setVisible(false);
		} else if (event.getSource() == create) {
			new CharacterCreation();
		} else if (event.getSource() == copy) {
			Guardian[] tempCharacters = new Guardian[characters.length + 1];
			int index = characterSelection.getSelectedIndex();
			boolean copied = false;
			for (int i = 0; i < tempCharacters.length; i++) {
				if (copied) {
					if (i >= 0) {
						tempCharacters[i] = characters[i - 1];
					}
				} else {
					tempCharacters[i] = characters[i];
				}
				if (!copied) {
					copied = i == index;
					if (copied) {
						i++;
						tempCharacters[i] = characters[i - 1].copy();
					}
				}
			}
			characters = tempCharacters;
			characterSelection.setModel(new DefaultComboBoxModel<Guardian>(characters));
			characterSelection.setSelectedIndex(index + 1);
		} else if (event.getSource() == delete) {
			if (JOptionPane.showConfirmDialog(null,
					"Are you sure you wish to delete " + characterSelection.getSelectedItem().toString() + "?", null,
					JOptionPane.YES_NO_OPTION) == 0) {
				Guardian[] tempCharacters = new Guardian[characters.length - 1];
				boolean deleted = false;
				for (int i = 0; i < tempCharacters.length; i++) {
					if (deleted) {
						if (i >= 0) {
							tempCharacters[i] = characters[i + 1];
						}
					} else {
						tempCharacters[i] = characters[i];
					}
					if (!deleted) {
						deleted = i == characterSelection.getSelectedIndex();
						if (deleted)
							i--;
					}
				}
				characters = tempCharacters;
				characterSelection.setModel(new DefaultComboBoxModel<Guardian>(characters));
				if (characters.length == 0) {
					delete.setEnabled(false);
					select.setEnabled(false);
				}
			}
		} else if (event.getSource() == creditsButton) {
			JOptionPane.showMessageDialog(creditsButton, creditsLabel, "Credits", JOptionPane.PLAIN_MESSAGE);
		} else if (event.getSource() == dnDestinyProject) {
			if (JOptionPane.showConfirmDialog(dnDestinyProject,
					"Do you wish to open a web borwser to go the D&Destiny project site?", "Open Web Browser?",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				try {
					java.awt.Desktop.getDesktop().browse(new java.net.URI("https://dndestinyproject.com/"));
				} catch (IOException | java.net.URISyntaxException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
