package guardian;

public enum Spell implements SpellInterface {
	;

	private String name;
	private String description;
	private final SpellType spellType;
	private final DamageType damageType;
	private final CastingTime castingTime;
	private final Duration duration;
	private final int[] range;

	Spell(String name, SpellType spellType, DamageType damageType, CastingTime castingTime, Duration duration,
			int... range) {
		this.name = name;
		this.spellType = spellType;
		this.damageType = damageType;
		this.castingTime = castingTime;
		this.duration = duration;
		this.range = range;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public SpellType getSpellType() {
		return spellType;
	}

	@Override
	public CastingTime getCastingTime() {
		return castingTime;
	}

	@Override
	public DamageType getDamageType() {
		return damageType;
	}

	@Override
	public Duration getDuration() {
		return duration;
	}

	@Override
	public int[] getRange() {
		return range;
	}

	public int getCost() {
		return 0;
	}

	public enum SpellType {
		GRENADE, MELEE;
	}

	public enum CastingTime {
		ACTION, BONUSACTION, FREEACTION
	}

	public static class Duration {
		public Duration(int time, String... descriptors) {

		}

		public Duration() {

		}
	}
}
