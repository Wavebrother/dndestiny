package guardian;

import java.awt.Image;
import java.util.LinkedHashMap;

import javax.swing.ImageIcon;

import guardian.items.Firearm.Firearms;
import guardian.items.Weapon.Weapons;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.itemEnums.Armor.ArmorLevel;

public interface BaseInterface {
	static LinkedHashMap<String, BaseInterface> getProficiencyList() {
		LinkedHashMap<String, BaseInterface> list = new LinkedHashMap<String, BaseInterface>();
		for (Weapons weapon : Weapons.values())
			list.put(weapon.toString(), weapon);
		for (Firearms firearm : Firearms.values())
			list.put(firearm.toString(), firearm);
		for (WeaponType type : WeaponType.values())
			list.put(type.toString(), type);
		for (ArmorLevel level : ArmorLevel.values())
			list.put(level.toString(), level);
		list.remove(ArmorLevel.GHOST.toString());
		list.remove(ArmorLevel.VEHICLE.toString());
		return list;
	}

	default String getName() {
		return toString();
	}

	String name();

	default void setName(String name) {
	}

	default String getDescription() {
		return "";
	}

	default ImageIcon getIcon(int size, double multiplier) {
		ImageIcon icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
		return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
				(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
	}
}
