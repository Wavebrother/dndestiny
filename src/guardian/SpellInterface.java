package guardian;

import guardian.Spell.CastingTime;
import guardian.Spell.Duration;
import guardian.Spell.SpellType;

public interface SpellInterface extends Feature {
	SpellType getSpellType();
	CastingTime getCastingTime();
	DamageType getDamageType();
	Duration getDuration();
	int[] getRange();
}
