package guardian.items.itemEnums;

import guardian.items.interfaces.ItemInterface;

public enum Resources implements ItemInterface {
	ARMOR_PARTS("Armor Parts"), ETHER("Ether"), HELIUM_FILAMENTS("Helium Filaments"), RELIC_IRON("Relic Iron"),
	SPINMETAL("Spinmetal"), SPIRIT_BLOOM("Spirit Bloom"), WEAPON_PART("Weapon Part"), WORMSPORE("Wormspore");
	private final String name;

	Resources(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String getDescription() {
		switch (this) {
		case ARMOR_PARTS:
			return "Hadronic essence, sapphire wire, and sheets of plasteel are used in various combinations to repair Guardian armor. A single unit of armor parts is comprised of all three materials and its enough to fix basic damage to Guardian armor.";
		case ETHER:
			return "Source of nutrition for eliksni creatures, harvested via servitors. Guardians often collect it from the reservoirs in defeated servitors and sell it to interested parties.";
		case HELIUM_FILAMENTS:
			return "Filaments of helium-3 fusion fuel, gathered from the lunar regolith by a helium coil. In addition to providing spot-energy much like a battery, helium filaments are often an additional component to fix or upgrade solar-based Guardian equipment.";
		case RELIC_IRON:
			return "A post-Collapse material of extraordinary density and only naturally occurring on Mars, relic iron is mostly used to create heavy armor for Titans and vehicles. It is also often associated with void Light and can be a required component for fixing or upgrading void-based Guardian equipment.";
		case SPINMETAL:
			return "A single branch of spinmetal is a filigree of bubbled metal grown by wild colonies of Golden Age cytoconstructors. It is used in almost everything that requires metal components, and quantities of it may be needed to fix or upgrade arc-based Guardian equipment.";
		case SPIRIT_BLOOM:
			return "Shavings from flower-shaped crystals with pharmaceutical and chemical applications. Spirit bloom is highly sought after by pharmaceutical companies and the Cryptarchy alike.";
		case WEAPON_PART:
			return "Screws, nuts, bolts, firing pins, old magazines, raw alloys - bits and pieces that can be used to repair firearm weapons. A single unit of weapon parts is enough to fix most problems that occur with firearms.";
		case WORMSPORE:
			return "A concentrated transmutation catalyst that reacts to both the Light and the Darkness. Often found around Hive infestations. A necessary component in some exotic pieces of Guardian equipment.";
		default:
			return "";
		}
	}

	@Override
	public int getMaxStack() {
		switch (this) {
		default:
			return 99;
		}
	}
}
