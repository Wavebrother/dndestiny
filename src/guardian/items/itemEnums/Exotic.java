package guardian.items.itemEnums;

import java.util.ArrayList;

import guardian.BaseInterface;
import guardian.DamageType;
import guardian.Die;
import guardian.Ghost.GhostSubClass;
import guardian.Races.MainRace;
import guardian.classes.CharacterSubClass.SubClasses;
import guardian.items.Firearm.Firearms;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Inventory;
import guardian.items.Item;
import guardian.items.Weapon.Weapons;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.WeaponProperties;
import guardian.items.interfaces.ItemInterface;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.interfaces.PerkInterface.Perk;

public class Exotic extends Item {
	public Exotic(ExoticInterface item, Inventory inventory, int amount) {
		super(item, inventory, amount);
	}

	public interface ExoticInterface extends ItemInterface {
		Rarity getRarity();

		default ArrayList<BaseInterface> getAttunableClasses() {
			ArrayList<BaseInterface> classes = new ArrayList<BaseInterface>();
			for (SubClasses clazz : SubClasses.values())
				classes.add(clazz);
			classes.add(GhostSubClass.GHOST);
			return classes;
		}

		default boolean requiresAttunement() {
			return false;
		}

		default boolean hasAttunement() {
			return false;
		}

		public static enum ExoticItem implements ExoticInterface {
			ANKH_OF_SAI_MOTA("Ankh of Sai Mota"), BRETOMARTS_PLEDGE("Bretomart's Pledge"), BROKEN_CROWN("Broken Crown"),
			BUTTERFLY_CODE("Butterfly Code"), CLAW_OF_COMMAND("Claw of Command"),
			COLOVANCES_CHOICE("Colovance's Choice"), COYOTES_LUCK("Coyote's Luck"),
			CORRUPTED_MEMORY_BANK("Corrupted Memory Bank"), CYCLOPEAN_ENIGMA("Cyclopean Enigma"),
			DREDGEN_YORS_ROSE("Dredgen Yor's Rose"), ECHO_OF_HONOR("Echo of Honor"), EFRIDEETS_EYE("Efrideet's Eye"),
			ELEMENTAL_TELEMETRY("Elemental Telemetry"), ENERGY_SHIELD_SURGE("Energy Shield Surge"),
			EXPANDED_MEMORY_BANK("Expanded Memory Bank"), EXPLOSIVE_ENGRAM("Explosive Engram"),
			FILAMENT_HEX("Filament Hex"), FIREBREAK_ETERNAL("Firebreak Eternal"), FIVE_OF_SWORDS("Five of Swords"),
			FLAMEL_CREST("Flamel Crest"), FORTUNA_PENDANT("Fortuna Pendant"),
			FOSSILIED_HERMAION_BLOSSOM("Fossilized Hermaion Blosson"), HEZEN_TOTEM("Hezen Totem"),
			HORN_OF_SIX_WARS("Horn of Six Wars"), HOURLESS_GLASS("Hourless Glass"),
			HUNTER_OF_THE_HEART("Hunter of the Heart"), IGLATHO_CYU("Iglatho-Cyu"),
			INCUBATION_SHELL("Incubation Shell"), INTACT_WARMIND_CORE("Intact Warmind Core"),
			JASPER_CARCANET("Jasper Carcanet"), JUDGEMENT_OF_MARS("Judgement of Mars"),
			KABRS_POCKET_WATCH("Kabr's Pocket Watch"), MOTE_OF_CLEANSING_LIGHT("Mote of Cleansing Light"),
			MOTE_OF_CONCENTRATED_LIGHT("Mote of Concentrated Light"), MOTE_OF_FOCUSED_LIGHT("Mote of Focused Light"),
			MOTE_OF_RATIANT_LIGHT("Mote of Radiant Light"), NEURAL_BEZOAR("Neural Bezoar"),
			NUMINOUS_WEB("Numinous Web"), ODDLY_COLORED_CUBE("Oddly Colored Cube"), PARASITIC_OPTIC("Parasitic Optic"),
			PRISMATIC_HEART("Prismatic Heart"), RING_OF_LIVING_STONE("Ring of Living Stone"),
			SAINT_14S_RING("Saint-14's Ring"), SCROLL_OF_EIMIN_TIN("Scroll of Eimin-Tin"),
			SILENT_ORCLE("Silent Oracle"), SISYPHUS_PROJECT("Sisyphus Project"), SIVA_CORE("Siva Core"),
			SKELETON_KEY("Skeleton Key"), SKYBURNERS_OBOL("Skyburner's Obol"), SOOTPEARL("Sootpearl"),
			SPECIMEN_TWELVE("Specimen Twelve"), SPECTRAL_DUST("Spectral Dust"), STONEBORN_RELIC("Stoneborn Relic"),
			SUNSCORCHES_LENS("Sunscorched Lens"), THANATONAUTS_LULLABY("Thanatonaut's Lullaby"),
			THRICE_WRAPPED_THING("Thrice-Wrapped Thing"), THE_MARAID("The Maraid"),
			THE_PAHANIN_ERRATA("The Pahanin Errata"), THE_TRAITORS_DIE("The Traitor's Die"),
			TOME_OF_NOTHING("Tome of Nothing"), TWO_SIDED_COIN("Two-sided Coin"), UNERRING_COMPASS("Unerring Compass"),
			WAX_SEALED_CASE("Wax-sealed Case"), WEI_NINGS_CONG("Wei Ning's Cong"),
			WHISPERING_SPHERE("Whispering Sphere"), YZOZS_PENDULUM("Yzoz's Pendulum");

			final String name;

			ExoticItem(String name) {
				this.name = name;
			}

			@Override
			public Rarity getRarity() {
				switch (this) {
				case COYOTES_LUCK:
				case ELEMENTAL_TELEMETRY:
				case EXPANDED_MEMORY_BANK:
				case FILAMENT_HEX:
				case FLAMEL_CREST:
				case FORTUNA_PENDANT:
					return Rarity.UNCOMMON;
				case CLAW_OF_COMMAND:
				case COLOVANCES_CHOICE:
				case EFRIDEETS_EYE:
				case EXPLOSIVE_ENGRAM:
					return Rarity.RARE;
				case ANKH_OF_SAI_MOTA:
				case CORRUPTED_MEMORY_BANK:
				case ECHO_OF_HONOR:
				case FOSSILIED_HERMAION_BLOSSOM:
					return Rarity.VERY_RARE;
				case DREDGEN_YORS_ROSE:
				case ENERGY_SHIELD_SURGE:
				case FIREBREAK_ETERNAL:
				case FIVE_OF_SWORDS:
					return Rarity.LEGENDARY;
				}
				return Rarity.COMMON;
			}

			@Override
			public ArrayList<BaseInterface> getAttunableClasses() {
				ArrayList<BaseInterface> classes = new ArrayList<BaseInterface>();
				switch (this) {
				case BROKEN_CROWN:
					classes.add(MainRace.ELIKSNI);
					break;
				case CORRUPTED_MEMORY_BANK:
				case ENERGY_SHIELD_SURGE:
				case EXPANDED_MEMORY_BANK:
					classes.add(GhostSubClass.GHOST);
					break;
				case FIREBREAK_ETERNAL:
					classes.add(SubClasses.SUNBREAKER);
					break;
				default:
					classes.addAll(ExoticInterface.super.getAttunableClasses());
				}
				return classes;
			}

			@Override
			public boolean hasAttunement() {
				switch (this) {
				case BUTTERFLY_CODE:
				case CLAW_OF_COMMAND:
				case CYCLOPEAN_ENIGMA:
				case EXPLOSIVE_ENGRAM:
				case FIVE_OF_SWORDS:
				case FORTUNA_PENDANT:
				case FOSSILIED_HERMAION_BLOSSOM:
					return false;
				default:
					return true;
				}
			}
		}

		enum Engram implements ExoticInterface {
			COMMON(false), COMMON_DECRYPTER(true), UNCOMMON(false), UNCOMMON_DECRYPTER(true), RARE(false),
			RARE_DECRYPTER(true), VERY_RARE(false), VERY_RARE_DECRYPTER(true), LEGENDARY(false),
			LEGENDARY_DECRYPTER(false);

			final boolean decrypter;

			Engram(boolean decrypter) {
				this.decrypter = decrypter;
			}

			@Override
			public Rarity getRarity() {
				if (decrypter)
					return Rarity.valueOf(name().substring(0, name().indexOf("_")));
				return Rarity.valueOf(name());
			}
		}

		enum SilkenCodex implements ExoticInterface {
			UNCOMMON, RARE, VERY_RARE, LEGENDARY;

			@Override
			public Rarity getRarity() {
				return Rarity.valueOf(name());
			}

			@Override
			public String toString() {
				switch (getRarity()) {
				case UNCOMMON:
					return "Open";
				case RARE:
					return "Shut";
				case VERY_RARE:
					return "Banish";
				case LEGENDARY:
					return "Collapse";
				default:
					return "";
				}
			}

			@Override
			public ArrayList<BaseInterface> getAttunableClasses() {
				ArrayList<BaseInterface> classes = new ArrayList<BaseInterface>();
				classes.add(SubClasses.VOIDWALKER);
				classes.add(SubClasses.SUNSINGER);
				classes.add(SubClasses.STORMCALLER);
				return classes;
			}

			@Override
			public boolean hasAttunement() {
				return true;
			}
		}

		enum DamnedLute implements ExoticInterface {
			NYAN_CAT, CRAZY_FROG, WHATS_NEW_PUSSYCAT, MAMBO_NO5, I_NEED_A_HERO, DONT_STOP_BELIEVING;

			@Override
			public Rarity getRarity() {
				return Rarity.COMMON;
			}

			@Override
			public String toString() {
				switch (this) {
				case CRAZY_FROG:
					return "The Damned Lute - Crazy Frog";
				case DONT_STOP_BELIEVING:
					return "The Damned Lute - Don't Stop Believing";
				case I_NEED_A_HERO:
					return "The Damned Lute - I Need a Hero";
				case MAMBO_NO5:
					return "The Damned Lute - Mambo No. 5";
				case NYAN_CAT:
					return "The Damned Lute - Nyan Cat";
				case WHATS_NEW_PUSSYCAT:
					return "The Damned Lute - What's New Pussycat";
				default:
					return "The Damned Lute";
				}
			}
		}

		enum RestorativeLight implements ExoticInterface {
			COMMON, UNCOMMON, RARE, VERY_RARE, LEGENDARY;

			@Override
			public Rarity getRarity() {
				return Rarity.valueOf(name());
			}

			@Override
			public String toString() {
				switch (getRarity()) {
				case COMMON:
					return "Restorative Light";
				case UNCOMMON:
					return "Greater Restorative Light";
				case RARE:
					return "Superior Restorative Light";
				case VERY_RARE:
					return "Supreme Restorative Light";
				case LEGENDARY:
					return "Perfect Restorative Light";
				default:
					return "";
				}
			}

			@Override
			public boolean hasAttunement() {
				return true;
			}
		}

		class RainbowBlade implements ExoticInterface, WeaponInterface {

			public final Weapons baseWeapon;

			RainbowBlade(Weapons weapon) {
				this.baseWeapon = weapon;
			}

			@Override
			public DamageType getDamageType() {
				return baseWeapon.getDamageType();
			}

			@Override
			public WeaponType getWeaponType() {
				return baseWeapon.getWeaponType();
			}

			@Override
			public Die getDamageDie() {
				return baseWeapon.getDamageDie();
			}

			@Override
			public int getDamageDieNumber() {
				return baseWeapon.getDamageDieNumber();
			}

			@Override
			public ArrayList<WeaponProperties> getProperties() {
				return baseWeapon.getProperties();
			}

			@Override
			public Rarity getRarity() {
				return Rarity.RARE;
			}

			@Override
			public String name() {
				return "Rainbow Blade";
			}
		}

		enum Memories implements ExoticInterface {
			FELWINTER, GHELEON, JOLDER, PERUN, RADEGHAST, SILIMAR, SKORRI, TIMUR;

			@Override
			public Rarity getRarity() {
				switch (this) {
				case FELWINTER:
					return Rarity.UNCOMMON;
				case GHELEON:
				case JOLDER:
				case PERUN:
				case SKORRI:
					return Rarity.RARE;
				case RADEGHAST:
				case SILIMAR:
				case TIMUR:
					return Rarity.VERY_RARE;
				default:
					return Rarity.COMMON;
				}
			}

			@Override
			public boolean hasAttunement() {
				return true;
			}

			@Override
			public boolean requiresAttunement() {
				return true;
			}
		}

		static enum ExoticWeapon implements ExoticInterface, WeaponInterface {
			FOURTH_HORSEMAN("4th Horseman"), ACE_OF_SPADES("Ace of Spades"), ANARCHY("Anarchy"), ARBALEST("Arbalest"),
			BAD_JUJU("Bad Juju"), BLACK_SPINDLE("Black Spindle"), BOOLEAN_GEMINI("Boolean Gemini"),
			BOREALIS("Borealis"), BRIAN("Brian"), CERBERUS("Cerberus +1"), CHAPERONE("Chaperone"),
			COLDHEART("Coldheart"), CRIMSON("Crimson"), DARCI("D.A.R.C.I."), DRAGONS_BREATH("Dragon's Breath"),
			DREGS_PROMISE("Dreg's Promise"), FABIAN_STRATEGY("Fabian Strategy"), FIGHTING_LION("Fighting Lion"),
			GJALLARHORN("Gjallarhorn"), GRAVITON_LANCE("Graviton Lance"), HARD_LIGHT("Hard Light"),
			HAWKMOON("Hawkmoon"), HEREAFTER("Hereafter"), HUCKLEBERRY("Huckleberry"), ICE_BREAKER("Ice Breaker"),
			INVECTIVE("Invective"), IZANAGIS_BURDEN("Izanagi's Burden"), JADE_RABBIT("Jade Rabbit"), JOTUUN("J�tuun"),
			KHVOSTOV_7G_0X("Khvostov 7G-0X"), THE_LAST_WORD("The Last Word"), LE_MONARQUE("Le Monarque"),
			LEGEND_OF_ACRIUS("Legend of Acrius"), LORD_OF_WOLVES("Lord of Wolves"), MALFEASANCE("Malfeasance"),
			MERCILESS("Merciless"), MIDA_MULTI_TOOL("MIDA Multi-Tool"), MONTE_CARLO("Monte Carlo"),
			NECROCHASM("Necrochasm"), NEMESIS_STAR("Nemesis Star"), NO_LAND_BEYOND("No Land Beyond"),
			NO_TIME_TO_EXPLAIN("No Time to Explain"), OUTBREAK_PRIME("Outbreak Prime"),
			PATIENCE_AND_TIME("Patience and Time"), PLAN_C("Plan C"), POCKET_INFINITY("Pocket Infinity"),
			POLARIS_LANCE("Polaris Lance"), PROMETHEUS_LENS("Prometheus Lens"), RAT_KING("Rat King"),
			RED_DEATH("Red Death"), REPLICA_ACE_OF_SPADES("Replica Ace of Spades"), RISKRUNNER("Riskrunner"),
			SHIN_MALPHURS_THE_LAST_WORD("Shin Malphur's The Last Word"), SKYBURNERS_OATH("Skyburner's Oath"),
			SLEEPER_SIMULANT("Sleeper Simulant"), SLOT_MACHINE("Slot Machine"), STURM("Sturm"), SUNSHOT("Sunshot"),
			SUPER_GOOD_ADVICE("Super Good Advice"), SUROS_REGIME("Suros Regime"), SWEET_BUSINESS("Sweet Business"),
			TELESTO("Telesto"), THE_COLONY("The Colony"), THE_FIRST_CURSE("The First Curse"),
			THE_PROSPECTOR("The Prospector"), THE_QUEENBREAKER("The Queenbreaker"),
			THE_WARDCLIFF_COIL("The Wardcliff Coil"), THORN("Thorn"), TLALOC("Tlaloc"),
			TOUCH_OF_MALICE("Touch of Malice"), TRACTOR_CANNON("Tractor Cannon"), TRESPASSER("Trespasser"),
			TRINITY_GHOUL("Trinity Ghoul"), TRUTH("Truth"), TWO_TAILED_FOX("Two-tailed Fox"),
			UNIVERSAL_REMOTE("Universal Remote"), VEX_MYTHOCLAST("Vex Mythoclast"), VIGILANCE_WING("Vigilance Wing"),
			WAVESPLITTER("Wavesplitter"), WISH_ENDER("Wish-Ender"), ZEN_METEOR("Zen Meteor"),
			ZHALO_SUPERCELL("Zhalo Supercell"), RADEGHASTS_BLADE("Radeghast's Blade");

			final String name;

			ExoticWeapon(String name) {
				this.name = name;
			}

			public WeaponInterface getBaseWeapon() {
				switch (this) {
				case FOURTH_HORSEMAN:
				case CHAPERONE:
				case INVECTIVE:
				case LEGEND_OF_ACRIUS:
				case LORD_OF_WOLVES:
				case TRACTOR_CANNON:
				case UNIVERSAL_REMOTE:
					return Firearms.Shotgun;
				case ACE_OF_SPADES:
				case CRIMSON:
				case HAWKMOON:
				case MALFEASANCE:
				case REPLICA_ACE_OF_SPADES:
				case SHIN_MALPHURS_THE_LAST_WORD:
				case STURM:
				case SUNSHOT:
				case THE_FIRST_CURSE:
				case THE_LAST_WORD:
				case THORN:
					return Firearms.Hand_Cannon;
				case ANARCHY:
				case FIGHTING_LION:
				case THE_COLONY:
				case THE_PROSPECTOR:
					return Firearms.Grenade_Launcher;
				case ARBALEST:
				case SLEEPER_SIMULANT:
				case THE_QUEENBREAKER:
					return Firearms.Linear_Fusion_Rifle;
				case BAD_JUJU:
				case GRAVITON_LANCE:
				case NO_TIME_TO_EXPLAIN:
				case OUTBREAK_PRIME:
				case RED_DEATH:
				case VIGILANCE_WING:
					return Firearms.Pulse_Rifle;
				case BLACK_SPINDLE:
				case BOREALIS:
				case DARCI:
				case HEREAFTER:
				case ICE_BREAKER:
				case IZANAGIS_BURDEN:
				case NO_LAND_BEYOND:
				case PATIENCE_AND_TIME:
				case ZEN_METEOR:
					return Firearms.Sniper_Rifle;
				case BOOLEAN_GEMINI:
				case JADE_RABBIT:
				case MIDA_MULTI_TOOL:
				case POLARIS_LANCE:
				case SKYBURNERS_OATH:
				case TLALOC:
				case TOUCH_OF_MALICE:
					return Firearms.Scout_Rifle;
				case BRIAN:
				case NEMESIS_STAR:
				case SLOT_MACHINE:
				case SUPER_GOOD_ADVICE:
					return Firearms.Light_Machine_Gun;
				case CERBERUS:
				case FABIAN_STRATEGY:
				case HARD_LIGHT:
				case KHVOSTOV_7G_0X:
				case MONTE_CARLO:
				case NECROCHASM:
				case SUROS_REGIME:
				case SWEET_BUSINESS:
				case ZHALO_SUPERCELL:
					return Firearms.Auto_Rifle;
				case COLDHEART:
				case PROMETHEUS_LENS:
				case WAVESPLITTER:
					return Firearms.Trace_Rifle;
				case DRAGONS_BREATH:
				case GJALLARHORN:
				case THE_WARDCLIFF_COIL:
				case TRUTH:
				case TWO_TAILED_FOX:
					return Firearms.Rocket_Launcher;
				case DREGS_PROMISE:
				case RAT_KING:
				case TRESPASSER:
					return Firearms.Sidearm;
				case HUCKLEBERRY:
				case RISKRUNNER:
					return Firearms.Submachine_Gun;
				case JOTUUN:
				case MERCILESS:
				case PLAN_C:
				case POCKET_INFINITY:
				case TELESTO:
				case VEX_MYTHOCLAST:
					return Firearms.Fusion_Rifle;
				case LE_MONARQUE:
				case TRINITY_GHOUL:
				case WISH_ENDER:
					return Firearms.Combat_Bow;
				case RADEGHASTS_BLADE:
					return Weapons.GREATSWORD;
				default:
					return null;
				}
			}

			public int getMagicalBonus() {
				switch (this) {
				case ANARCHY:
				case BLACK_SPINDLE:
				case BOOLEAN_GEMINI:
				case BOREALIS:
				case CRIMSON:
				case INVECTIVE:
				case JOTUUN:
				case LE_MONARQUE:
					return 1;
				case FOURTH_HORSEMAN:
				case BAD_JUJU:
				case GRAVITON_LANCE:
				case HAWKMOON:
				case IZANAGIS_BURDEN:
					return 2;
				case ACE_OF_SPADES:
				case COLDHEART:
				case GJALLARHORN:
				case ICE_BREAKER:
				case LEGEND_OF_ACRIUS:
					return 3;
				default:
					return 0;
				}
			}

			@Override
			public int[] getRange() {
				switch (this) {
				case FOURTH_HORSEMAN:
					return new int[] { 10, 15, 15 };
				case ANARCHY:
					return new int[] { 30, 30, 30 };
				case CERBERUS:
					return new int[] { 15, 20, 25 };
				case CHAPERONE:
					return new int[] { 20, 30, 40 };
				case GRAVITON_LANCE:
					return new int[] { 25, 90, 100 };
				case HARD_LIGHT:
					return new int[] { 20, 30, 45 };
				case INVECTIVE:
					return new int[] { 10, 20, 25 };
				case JOTUUN:
					return new int[] { 20, 40, 60 };
				case THE_LAST_WORD:
					return new int[] { 30, 50, 75 };
				default:
					return getBaseWeapon().getRange();
				}
			}

			@Override
			public boolean isFinesse() {
				switch (this) {
				case BOOLEAN_GEMINI:
				case CHAPERONE:
				case INVECTIVE:
				case LEGEND_OF_ACRIUS:
					return true;
				default:
					return getBaseWeapon().isFinesse();
				}
			}

			@Override
			public boolean isRanged() {
				return getBaseWeapon().isRanged();
			}

			@Override
			public boolean isThrown() {
				return getBaseWeapon().isThrown();
			}

			@Override
			public DamageType getDamageType() {
				switch (this) {
				case ANARCHY:
					return DamageType.ARC;
				case ARBALEST:
					return DamageType.KINETIC;
				case COLDHEART:
					return DamageType.COLD;
				case GJALLARHORN:
				case JOTUUN:
					return DamageType.SOLAR;
				case GRAVITON_LANCE:
					return DamageType.VOID;
				case HARD_LIGHT:
				case LEGEND_OF_ACRIUS:
					return DamageType.ENERGY;
				default:
					return getBaseWeapon().getDamageType();
				}
			}

			@Override
			public WeaponType getWeaponType() {
				return getBaseWeapon().getWeaponType();
			}

			@Override
			public Die getDamageDie() {
				switch (this) {
				case CERBERUS:
				case CRIMSON:
					return Die.d4;
				case INVECTIVE:
					return Die.d8;
				case ACE_OF_SPADES:
					return Die.d10;
				default:
					return getBaseWeapon().getDamageDie();
				}
			}

			@Override
			public int getDamageDieNumber() {
				switch (this) {
				case CERBERUS:
				case CRIMSON:
					return 2;
				default:
					return getBaseWeapon().getDamageDieNumber();
				}
			}

			@Override
			public ArrayList<WeaponProperties> getProperties() {
				ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
				switch (this) {
				case FOURTH_HORSEMAN:
					properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
					properties.add(WeaponProperties.HIGH_RECOIL);
					break;
				case ACE_OF_SPADES:
					properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
					break;
				case ARBALEST:
					properties.add(WeaponProperties.BULKY);
					properties.add(WeaponProperties.ENERGY_PROJECTILES);
					properties.add(WeaponProperties.LOADING);
					break;
				case BLACK_SPINDLE:
					properties.add(WeaponProperties.BULKY);
					properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
					break;
				case BOOLEAN_GEMINI:
					properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
					break;
				case BRIAN:
					properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
					break;
				case CERBERUS:
					properties.add(WeaponProperties.AUTOMATIC_FIRE);
					properties.add(WeaponProperties.BULKY);
					properties.add(WeaponProperties.HIGH_RECOIL);
					break;
				case CHAPERONE:
					properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
					// Stabilizer sights
					break;
				case DRAGONS_BREATH:
					properties.add(WeaponProperties.BULKY);
					properties.add(WeaponProperties.HEAVY);
					properties.add(WeaponProperties.HIGH_RECOIL);
					properties.add(WeaponProperties.PAYLOAD);
					break;
				case DREGS_PROMISE:
					properties.add(WeaponProperties.AUTOMATIC_FIRE);
					properties.add(WeaponProperties.LIGHT);
					break;
				case FABIAN_STRATEGY:
					properties.add(WeaponProperties.AUTOMATIC_FIRE);
					properties.add(WeaponProperties.BULKY);
					break;
				case HARD_LIGHT:
					// Armor-Piercing Rounds
					properties.add(WeaponProperties.AUTOMATIC_FIRE);
					properties.add(WeaponProperties.BULKY);
					break;
				case INVECTIVE:
					properties.add(WeaponProperties.BULKY);
					break;
				case IZANAGIS_BURDEN:
					properties.add(WeaponProperties.BULKY);
					break;
				case KHVOSTOV_7G_0X:
					// Lightweight
					break;
				case THE_LAST_WORD:
					properties.add(WeaponProperties.BULKY);
					break;
				default:
					properties.addAll(getBaseWeapon().getProperties());
					break;
				}
				return properties;
			}

			@Override
			public int getMaxStack() {
				switch (this) {
				case CHAPERONE:
					return 1;
				case ARBALEST:
					return 3;
				case FOURTH_HORSEMAN:
				case BLACK_SPINDLE:
				case INVECTIVE:
				case IZANAGIS_BURDEN:
					return 4;
				case CERBERUS:
				case COLDHEART:
				case FIGHTING_LION:
					return 5;
				case ANARCHY:
				case BOOLEAN_GEMINI:
				case BRIAN:
				case FABIAN_STRATEGY:
				case GRAVITON_LANCE:
				case KHVOSTOV_7G_0X:
					return 6;
				case ACE_OF_SPADES:
					return 8;
				default:
					return getBaseWeapon().getMaxStack();
				}
			}

			@Override
			public RangeBand getRangeBand() {
				switch (this) {
				case CHAPERONE:
					return RangeBand.MEDIUM;
				case LE_MONARQUE:
					return RangeBand.LONG;
				default:
					return getBaseWeapon().getRangeBand();
				}
			}

			public PerkInterface[][] getPerkSlotOptions() {
				switch (this) {
				case FOURTH_HORSEMAN:
					return new PerkInterface[][] { { Perk.FINAL_ROUND }, { Perk.MULLIGAN }, { Perk.THUNDERER } };
				default:
					return new PerkInterface[0][0];
				}
			}

			@Override
			public Rarity getRarity() {
				switch (this) {
				case ANARCHY:
				case ARBALEST:
				case BOOLEAN_GEMINI:
				case BOREALIS:
				case BRIAN:
				case CHAPERONE:
				case DARCI:
				case DRAGONS_BREATH:
				case DREGS_PROMISE:
				case HARD_LIGHT:
				case INVECTIVE:
				case JADE_RABBIT:
				case KHVOSTOV_7G_0X:
				case MONTE_CARLO:
				case NO_LAND_BEYOND:
				case RISKRUNNER:
				case TELESTO:
				case THE_QUEENBREAKER:
				case THE_WARDCLIFF_COIL:
				case TLALOC:
				case TRACTOR_CANNON:
				case UNIVERSAL_REMOTE:
					return Rarity.UNCOMMON;
				case BLACK_SPINDLE:
				case CERBERUS:
				case FABIAN_STRATEGY:
				case FIGHTING_LION:
				case FOURTH_HORSEMAN:
				case GRAVITON_LANCE:
				case HEREAFTER:
				case HUCKLEBERRY:
				case IZANAGIS_BURDEN:
				case JOTUUN:
				case LE_MONARQUE:
				case THE_LAST_WORD:
				case MALFEASANCE:
				case MERCILESS:
				case POLARIS_LANCE:
				case RAT_KING:
				case RED_DEATH:
				case SKYBURNERS_OATH:
				case SLOT_MACHINE:
				case SUNSHOT:
				case SUROS_REGIME:
				case SWEET_BUSINESS:
				case THE_PROSPECTOR:
				case TRUTH:
				case VIGILANCE_WING:
				case ZHALO_SUPERCELL:
					return Rarity.RARE;
				case BAD_JUJU:
				case COLDHEART:
				case CRIMSON:
				case HAWKMOON:
				case LORD_OF_WOLVES:
				case MIDA_MULTI_TOOL:
				case NEMESIS_STAR:
				case PATIENCE_AND_TIME:
				case PLAN_C:
				case POCKET_INFINITY:
				case RADEGHASTS_BLADE:
				case REPLICA_ACE_OF_SPADES:
				case STURM:
				case TRINITY_GHOUL:
				case TWO_TAILED_FOX:
				case WAVESPLITTER:
				case ZEN_METEOR:
					return Rarity.VERY_RARE;
				case ACE_OF_SPADES:
				case GJALLARHORN:
				case ICE_BREAKER:
				case LEGEND_OF_ACRIUS:
				case NECROCHASM:
				case NO_TIME_TO_EXPLAIN:
				case OUTBREAK_PRIME:
				case PROMETHEUS_LENS:
				case SHIN_MALPHURS_THE_LAST_WORD:
				case SLEEPER_SIMULANT:
				case SUPER_GOOD_ADVICE:
				case THE_COLONY:
				case THE_FIRST_CURSE:
				case THORN:
				case TOUCH_OF_MALICE:
				case TRESPASSER:
				case VEX_MYTHOCLAST:
				case WISH_ENDER:
					return Rarity.LEGENDARY;
				default:
					return Rarity.COMMON;
				}
			}

			@Override
			public ArrayList<BaseInterface> getAttunableClasses() {
				ArrayList<BaseInterface> classes = new ArrayList<BaseInterface>();
				switch (this) {
				case ACE_OF_SPADES:
					classes.add(SubClasses.GUNSLINGER);
					break;
				case FABIAN_STRATEGY:
					classes.add(SubClasses.STRIKER);
					classes.add(SubClasses.DEFENDER);
					classes.add(SubClasses.SUNBREAKER);
					break;
				case BAD_JUJU:
					classes.add(SubClasses.VOIDWALKER);
					classes.add(SubClasses.SUNSINGER);
					classes.add(SubClasses.STORMCALLER);
					break;
				default:
					return ExoticInterface.super.getAttunableClasses();
				}
				return classes;
			}

			@Override
			public boolean hasAttunement() {
				switch (this) {
				case ACE_OF_SPADES:
				case BAD_JUJU:
				case BOOLEAN_GEMINI:
				case BOREALIS:
				case COLDHEART:
				case DARCI:
				case FABIAN_STRATEGY:
				case FIGHTING_LION:
					return true;
				default:
					return false;
				}
			}
		}

		static enum ExoticArmor implements ExoticInterface {
			ACD0_FEEDBACK_FENCE_UNCOMMON("ACD/0 Feedback Fence"), ACD0_FEEDBACK_FENCE_RARE("ACD/0 Feedback Fence"),
			ACD0_FEEDBACK_FENCE_VERY_RARE("ACD/0 Feedback Fence"), ACHLYOPHAGE_SYMBIOTE("Achlyophage Symbiote"),
			ACTIUM_WAR_RIG("Actium War Rig"), AEON_GAUNTLETS("Aeon Gauntlets"), AEON_CHEST("Aeon Chest"),
			AEON_HELMET("Aeon Helmet"), AHAMKARAS_RIBCAGE("Ahamkara's Ribcage"), ALCHEMIST_RAIMENT("Alchemist Raiment"),
			AN_INSURMOUNTABLE_SKULLFORT("An Insurmountable Skullfort"), APOTHEOSIS_VEIL("Apotheosis Veil"),
			ASHEN_WAKE("Ashen Wake"), ASTROCYTE_VERSE("Astrocyte Verse"), ATS8_ARACHNID("ATS/8 Arachnid"),
			ATS8_TARANTELLA("ATS/8 Tarantella"), BOXING_GLOVES("Boxing Gloves"),
			CELESTIAL_NIGHTHAWK("Celestial Nighthawk"), CHROMATIC_FIRE("Chromatic Fire"),
			CONTRAVERSE_HOLD("Contraverse Hold"), CREST_OF_ALPHA_LUPI("Crest of Alpha Lupi"),
			CROWN_OF_TEMPESTS("Crown of Tempests"), DONT_TOUCH_ME("Don't Touch Me"),
			DOOM_FANG_PAULDRON("Doom Fang Pauldron"), DUNEMARCHERS("Dunemarchers"), ETERNAL_WARRIOR("Eternal Warrior"),
			EYE_OF_ANOTHER_WORLD("Eye of Another World"), FOETRACER("Foetracer"), FROST_EE5("Frost-EE5"),
			GEMINI_JESTER("Gemini Jester"), GEOMAG_STABILIZERS("Geomag Stabilizers"), GHOST_GHOST("Ghost Ghost"),
			GRAVITON_FORFEIT("Graviton Forfeit"), GWISIN_VEST("Gwisin Vest"), HALLOWFIRE_HEART("Hallowfire Heart"),
			HEART_OF_THE_PRAXIC_FIRE("Heart of the Praxic Fire"), HELM_OF_SAINT_14("Helm of Saint-14"),
			IMMOLATION_FISTS("Immolation Fists"), KARNSTEIN_ARMLETS("Karnstein Armlets"),
			KHEPRIS_STING("Khepri's Sting"), KNUCKLEHEAD_RADAR("Knucklehead Radar"), LUCKY_PANTS("Lucky Pants"),
			LUCKY_RASPBERRY("Lucky Raspberry"), MASK_OF_THE_QUIET_ONE("Mask of the Quiet One"),
			MASK_OF_THE_THIRD_MAN("Mask of the Third Man"), MECHANEERS_TRICKSLEEVES("Mechaneer's Tricksleeves"),
			MIND_FLAYERS_TREASURE("Mind Flayer's Treasure"), MK44_STAND_ASIDES("MK. 44 Stand Asides"),
			NO_BACKUP_PLANS("No Backup Plans"), OATHKEPPER("Oathkeeper"), OBSIDIAN_MIND("Obsidian Mind"),
			ONE_EYED_MASK("One-Eyed Mask"), OPHIDIA_SPATHE("Ophidia Spathe"), OPHIDIAN_ASPECT("Ophidian Aspect"),
			ORPHEUS_RIG("Orpheus Rig"), OVERGROWN_SHELL("Overgrown Shell"), PEACEKEEPERS("Peacekeepers"),
			PURIFIER_ROBES("Purifier Robes"), RADIANT_DANCE_MACHINES("Radiant Dance Machines"),
			RAIDEN_FLUX("Raiden Flux"), RUIN_WINGS("Ruin Wings"), SANGUINE_ALCHEMY("Sanguine Alchemy"),
			SERVITOR_SHELL("Servitor Shell"), SHARDS_OF_GALANOR("Shards of Galanor"), SHINOBUS_VOW("Shinobu's Vow"),
			SKYBURNERS_ANNEX("Skyburners Annex"), ST0MP_EE5("St0mp-EE5"), STARFIRE_PROTOCOL("Starfire Protocol"),
			SUNBRACERS("Sunbracers"), SYNTHOCEPS("Synthoceps"), THAGOMIZERS("Thagomizers"),
			THE_ARMAMENTARIUM("The Armamentarium"), THE_DRAGONS_SHADOW("The Dragon's Shadow"),
			THE_GLASSHOUSE("The Glasshouse"), THE_IMPOSSABLE_MACHINES("The Impossible Machines"), THE_RAM("The Ram"),
			THE_STAG("The Stag"), THE_TAIKONAUT("The Taikonaut"), TRANSVERSIVE_STEPS("Transversive Steps"),
			URSA_FURIOSA("Ursa Furiosa"), VERITYS_BROW("Verity's Brow"), VOIDFANG_VESTIMENTS("Voidfang Vestiments"),
			WINTERS_GUILE("Winter's Guile"), WORMGOD_CARESS("Wormgod Caress");

			final String name;

			ExoticArmor(String name) {
				this.name = name;
			}

			@Override
			public Rarity getRarity() {
				switch (this) {
				case ACD0_FEEDBACK_FENCE_UNCOMMON:
				case AHAMKARAS_RIBCAGE:
				case AN_INSURMOUNTABLE_SKULLFORT:
				case ATS8_ARACHNID:
				case CROWN_OF_TEMPESTS:
				case EYE_OF_ANOTHER_WORLD:
					return Rarity.UNCOMMON;
				case ACD0_FEEDBACK_FENCE_RARE:
				case ACTIUM_WAR_RIG:
				case AEON_CHEST:
				case AEON_GAUNTLETS:
				case ALCHEMIST_RAIMENT:
				case APOTHEOSIS_VEIL:
				case ASHEN_WAKE:
				case ATS8_TARANTELLA:
				case CONTRAVERSE_HOLD:
				case DUNEMARCHERS:
				case FROST_EE5:
				case GEMINI_JESTER:
					return Rarity.RARE;
				case ACD0_FEEDBACK_FENCE_VERY_RARE:
				case ACHLYOPHAGE_SYMBIOTE:
				case AEON_HELMET:
				case ASTROCYTE_VERSE:
				case CELESTIAL_NIGHTHAWK:
				case CHROMATIC_FIRE:
				case CREST_OF_ALPHA_LUPI:
				case DONT_TOUCH_ME:
				case DOOM_FANG_PAULDRON:
				case ETERNAL_WARRIOR:
					return Rarity.VERY_RARE;
				case GEOMAG_STABILIZERS:
					return Rarity.LEGENDARY;
				default:
					return Rarity.COMMON;
				}
			}

			static enum ArmorPiece {
				HELM, CHESTPLATE, GAUNTLETS, BOOTS;
			}

			@Override
			public ArrayList<BaseInterface> getAttunableClasses() {
				ArrayList<BaseInterface> classes = new ArrayList<BaseInterface>();
				switch (this) {
				case ACHLYOPHAGE_SYMBIOTE:
				case ATS8_ARACHNID:
				case CELESTIAL_NIGHTHAWK:
					classes.add(SubClasses.GUNSLINGER);
					break;
				case ATS8_TARANTELLA:
					classes.add(SubClasses.BLADEDANCER);
					break;
				case AHAMKARAS_RIBCAGE:
				case DONT_TOUCH_ME:
				case FROST_EE5:
				case GEMINI_JESTER:
					classes.add(SubClasses.GUNSLINGER);
					classes.add(SubClasses.BLADEDANCER);
					classes.add(SubClasses.NIGHTSTALKER);
					break;
				case ETERNAL_WARRIOR:
					classes.add(SubClasses.STRIKER);
					break;
				case DOOM_FANG_PAULDRON:
					classes.add(SubClasses.DEFENDER);
					break;
				case ASHEN_WAKE:
					classes.add(SubClasses.SUNBREAKER);
					break;
				case ACD0_FEEDBACK_FENCE_UNCOMMON:
				case ACD0_FEEDBACK_FENCE_RARE:
				case ACD0_FEEDBACK_FENCE_VERY_RARE:
				case ACTIUM_WAR_RIG:
				case AN_INSURMOUNTABLE_SKULLFORT:
				case DUNEMARCHERS:
					classes.add(SubClasses.STRIKER);
					classes.add(SubClasses.DEFENDER);
					classes.add(SubClasses.SUNBREAKER);
					break;
				case CONTRAVERSE_HOLD:
					classes.add(SubClasses.VOIDWALKER);
					break;
				case CROWN_OF_TEMPESTS:
					classes.add(SubClasses.STORMCALLER);
					break;
				case ALCHEMIST_RAIMENT:
				case APOTHEOSIS_VEIL:
				case CHROMATIC_FIRE:
				case GEOMAG_STABILIZERS:
					classes.add(SubClasses.VOIDWALKER);
					classes.add(SubClasses.SUNSINGER);
					classes.add(SubClasses.STORMCALLER);
					break;
				case ASTROCYTE_VERSE:
					classes.add(SubClasses.VOIDWALKER);
					classes.add(SubClasses.BLADEDANCER);
					break;
				case GHOST_GHOST:
					classes.add(GhostSubClass.GHOST);
					break;
				case AEON_CHEST:
				case AEON_GAUNTLETS:
				case AEON_HELMET:
				case CREST_OF_ALPHA_LUPI:
					classes.addAll(ExoticInterface.super.getAttunableClasses());
					classes.remove(GhostSubClass.GHOST);
					break;
				default:
					break;
				}
				return classes;
			}

			@Override
			public boolean hasAttunement() {
				switch (this) {
				case BOXING_GLOVES:
				case FOETRACER:
					return false;
				default:
					return true;
				}
			}
		}

		static enum Rarity {
			COMMON, UNCOMMON, RARE, VERY_RARE, LEGENDARY;
			@Override
			public String toString() {
				switch (this) {
				case VERY_RARE:
					return "Very Rare";
				default:
					return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
				}
			}
		}
	}
}
