package guardian.items.itemEnums;

import java.awt.Image;

import javax.swing.ImageIcon;

import guardian.Main;
import guardian.items.interfaces.ItemInterface;

public enum Ammunition implements ItemInterface {
	SIMPLE_MAGAZINE("Simple Magazine"), MARTIAL_MAGAZINE("Martial Magazine"), ROCKETS("Rockets"),
	STANDARD_ARROWS("Standard Arrows"), DRAGONFLY_ARROWS("Dragonfly Arrows"), ELEMENTAL_ARROWS("Elemental Arrows"),
	RELIC_TIPPED_ARROWS("Relic-tipped Arrows"), TRANSMAT_ARROWS("Transmat Arrows");
	String name;

	Ammunition(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int getMaxStack() {
		switch (this) {
		case ROCKETS:
			return 5;
		case DRAGONFLY_ARROWS:
		case ELEMENTAL_ARROWS:
		case RELIC_TIPPED_ARROWS:
		case TRANSMAT_ARROWS:
			return 200;
		case STANDARD_ARROWS:
			return 400;
		default:
			return 10;
		}
	}

	@Override
	public int getMemorySize() {
		return 1;
	}

	@Override
	public double getWeightPerItem() {
		switch (this) {
		case STANDARD_ARROWS:
		case DRAGONFLY_ARROWS:
		case ELEMENTAL_ARROWS:
		case RELIC_TIPPED_ARROWS:
		case TRANSMAT_ARROWS:
			return .05;
		case SIMPLE_MAGAZINE:
			return .5;
		case MARTIAL_MAGAZINE:
			return 1;
		case ROCKETS:
			return 2;
		default:
			return 0;
		}
	}

	@Override
	public ImageIcon getIcon(int size, double multiplier) {
		ImageIcon icon;
		switch (this) {
		default:
			icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
		}
		return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
				(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
	}
}
