package guardian.items.itemEnums;

import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

import guardian.BaseInterface;
import guardian.items.interfaces.ArmorInterface;

public enum Armor implements ArmorInterface {
	PADDED, LEATHER, SPINWEAVE, COBBLED, SPINWIRE, REINFORCED, PLASTWIRE, SPINPLATE, HALFPLAST, PLASTEEL, FORTIFIED,
	RELIC, NOSHELL, GENERALIST;
	private final static Map<String, Armor> guardianArmor = new HashMap<String, Armor>();
	private final static Map<String, Armor> ghostArmor = new HashMap<String, Armor>();
	private String description;
	private ImageIcon icon;

	public static Map<String, Armor> getGuardianArmor() {
		for (Armor armor : Armor.values())
			if (armor.isGuardianArmor())
				guardianArmor.put(armor.name(), armor);
		return guardianArmor;
	}

	public static Map<String, Armor> getGhostArmor() {
		for (Armor armor : Armor.values())
			if (!armor.isGuardianArmor())
				ghostArmor.put(armor.name(), armor);
		return ghostArmor;
	}

	@Override
	public String toString() {
		if(this == HALFPLAST)
			return "Half-plast";
		return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
	}
	
	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public int getMemorySize() {
		switch (this) {
		case GENERALIST:
			return 1;
		case PADDED:
			return 2;
		case LEATHER:
		case COBBLED:
		case SPINWIRE:
		case HALFPLAST:
			return 3;
		case SPINWEAVE:
		case REINFORCED:
		case PLASTWIRE:
		case SPINPLATE:
		case PLASTEEL:
			return 4;
		case FORTIFIED:
			return 5;
		case RELIC:
			return 6;
		default:
			return 0;
		}
	}

	@Override
	public double getWeightPerItem() {
		switch (this) {
		case COBBLED:
			return 17;
		case FORTIFIED:
			return 65;
		case GENERALIST:
			return 2;
		case SPINPLATE:
		case HALFPLAST:
			return 45;
		case LEATHER:
			return 15;
		case PADDED:
			return 13;
		case SPINWIRE:
		case PLASTWIRE:
			return 25;
		case PLASTEEL:
			return 60;
		case REINFORCED:
			return 50;
		case RELIC:
			return 70;
		case SPINWEAVE:
			return 18;
		default:
			return 0;
		}
	}

	@Override
	public ImageIcon getIcon(int size, double multiplier) {
		return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
				(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
	}

	@Override
	public boolean isStealth() {
		switch (this) {
		case PADDED:
		case REINFORCED:
		case SPINPLATE:
		case PLASTEEL:
		case FORTIFIED:
		case RELIC:
			return false;
		default:
			return true;
		}
	}

	@Override
	public boolean isGuardianArmor() {
		return getLevel() != ArmorLevel.GHOST;
	}

	@Override
	public int getMinStr() {
		switch (this) {
		case PLASTEEL:
			return 13;
		case FORTIFIED:
		case RELIC:
			return 15;
		default:
			return 0;
		}
	}

	@Override
	public int getBaseAC() {
		switch (this) {
		case PADDED:
		case LEATHER:
			return 11;
		case SPINWEAVE:
		case COBBLED:
		case NOSHELL:
			return 12;
		case SPINWIRE:
		case GENERALIST:
			return 13;
		case REINFORCED:
		case PLASTWIRE:
		case HALFPLAST:
			return 14;
		case SPINPLATE:
			return 15;
		case PLASTEEL:
			return 16;
		case FORTIFIED:
			return 17;
		case RELIC:
			return 18;
		default:
			return 0;
		}
	}

	@Override
	public int getMaxAC() {
		switch (this) {
		case COBBLED:
		case HALFPLAST:
			return 14;
		case SPINWIRE:
			return 15;
		case REINFORCED:
		case PLASTWIRE:
		case PLASTEEL:
			return 16;
		case SPINPLATE:
		case FORTIFIED:
			return 17;
		case RELIC:
			return 18;
		default:
			return 0;
		}
	}

	@Override
	public ArmorLevel getLevel() {
		switch (this) {
		case PADDED:
		case LEATHER:
		case SPINWEAVE:
			return ArmorLevel.LIGHT;
		case COBBLED:
		case SPINWIRE:
		case REINFORCED:
		case PLASTWIRE:
		case SPINPLATE:
			return ArmorLevel.MEDIUM;
		case HALFPLAST:
		case PLASTEEL:
		case FORTIFIED:
		case RELIC:
			return ArmorLevel.HEAVY;
		case NOSHELL:
		case GENERALIST:
			return ArmorLevel.GHOST;
		default:
			return null;
		}
	}

	@Override
	public void setItem(String description, ImageIcon icon) {
		this.description = description;
		this.icon = icon;
	}

	public enum ArmorLevel implements BaseInterface {
		LIGHT, MEDIUM, HEAVY, GHOST, VEHICLE, SHIELD;

		@Override
		public String toString() {
			switch (this) {
			case SHIELD:
				return "Shields";
			case GHOST:
				return "Ghost Shells";
			default:
				return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase() + " Armor";
			}
		}
	}

}
