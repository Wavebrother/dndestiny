package guardian.items.itemEnums;

import java.util.HashMap;
import java.util.Map;

import guardian.Main;
import guardian.items.interfaces.ItemInterface;

public enum Items implements ItemInterface {
	GLIMMER, SOLAR_BATTERY, CAMPING_COOKBOOK, CIVILIAN_CLOTHING, COMMS_DEVICE, COOKING_UTENSIL_SET,
	ETHER_CONTAINER_SMALL, ETHER_CONTAINER_MEDIUM, ETHER_CONTAINER_LARGE, FIRE_STARTER_KIT, FIREWOOD_BUNDLE, HANDCUFFS,
	HUNTING_TRAP, LED_LIGHT, PADLOCK, BIOMETRIC_LOCK, MOTION_ACTIVATED_CAMERA, PLASMA_CUTTER, QUIVER, QUIVER_XL,
	RATION_1_DAY, RELAY_BEACON, QUANTUM_RELAY_BEACON, SELFIE_STICK, SIGNAL_FLARE_GUN, SIGNAL_FLARE_STICK, SIGNAL_JAMMER,
	SURVEILLANCE_DRONE, TABLET_COMPUTER, VIDEO_GAME, VIDEO_GAME_SYSTEM, WATER_BOTTLE, WIRE_CABLE_50_FT,
	WEAPON_TELEMETRY_BASIC_50XP, WEAPON_TELEMETRY_ADVANCED_500XP, WEAPON_TELEMETRY_ROYAL_5000XP, ZAVALA_SNACKS;

	public static Map<String, ItemInterface> getItems() {
		Map<String, ItemInterface> items = new HashMap<String, ItemInterface>();
		for (ItemInterface item : Items.values())
			if (!item.toString().equals(""))
				items.put(item.toString(), item);
		for (ItemInterface item : Armor.values())
			if (!item.toString().equals(""))
				items.put(item.toString(), item);
		for (ItemInterface item : Ammunition.values())
			if (!item.toString().equals(""))
				items.put(item.toString(), item);
		for (ItemInterface item : Resources.values())
			if (!item.toString().equals(""))
				items.put(item.toString(), item);
		return items;
	}

	@Override
	public String toString() {
		switch (this) {
		case SOLAR_BATTERY:
			return "Battery, solar";
		case CIVILIAN_CLOTHING:
			return "Clothing, civilian";
		case ETHER_CONTAINER_SMALL:
			return "Ether Container (Small)";
		case ETHER_CONTAINER_MEDIUM:
			return "Ether Container (Medium)";
		case ETHER_CONTAINER_LARGE:
			return "Ether Container (Large)";
		case LED_LIGHT:
			return "LED Light";
		case MOTION_ACTIVATED_CAMERA:
			return "Motion-activated Camera";
		case QUIVER_XL:
			return "Quiver, XL";
		case RATION_1_DAY:
			return "Ration (1 day)";
		case SIGNAL_FLARE_GUN:
			return "Signal Flare, gun";
		case SIGNAL_FLARE_STICK:
			return "Signal Flare, stick";
		case VIDEO_GAME:
			return "Video Game, single";
		case VIDEO_GAME_SYSTEM:
			return "Video Game, system";
		case WIRE_CABLE_50_FT:
			return "Wire Cable, 50 ft.";
		case WEAPON_TELEMETRY_BASIC_50XP:
			return "Weapon Telemetry (Basic - 50XP)";
		case WEAPON_TELEMETRY_ADVANCED_500XP:
			return "Weapon Telemetry (Advanced - 500XP)";
		case WEAPON_TELEMETRY_ROYAL_5000XP:
			return "Weapon Telemetry (Royal - 5000XP)";
		default:
			return Main.enumString(name());
		}
	}

	@Override
	public int getMaxStack() {
		switch (this) {
		case ETHER_CONTAINER_SMALL:
			return 5;
		case SOLAR_BATTERY:
		case FIRE_STARTER_KIT:
		case FIREWOOD_BUNDLE:
		case PADLOCK:
		case RATION_1_DAY:
		case WIRE_CABLE_50_FT:
		case WEAPON_TELEMETRY_ADVANCED_500XP:
		case ZAVALA_SNACKS:
			return 10;
		case CIVILIAN_CLOTHING:
		case HUNTING_TRAP:
			return 50;
		case HANDCUFFS:
		case QUIVER:
		case QUIVER_XL:
		case SELFIE_STICK:
		case SIGNAL_FLARE_STICK:
		case WATER_BOTTLE:
		case WEAPON_TELEMETRY_BASIC_50XP:
			return 99;
		case GLIMMER:
			return Integer.MAX_VALUE;
		default:
			return 1;
		}
	}

	@Override
	public int getMemorySize() {
		switch (this) {
		case COOKING_UTENSIL_SET:
		case RELAY_BEACON:
		case SIGNAL_FLARE_GUN:
			return 3;
		case TABLET_COMPUTER:
			return 4;
		case SURVEILLANCE_DRONE:
		case VIDEO_GAME_SYSTEM:
			return 5;
		case QUANTUM_RELAY_BEACON:
			return 10;
		default:
			return 1;
		}
	}

	@Override
	public double getWeightPerItem() {
		switch (this) {
		case GLIMMER:
			return 0;
		case SIGNAL_FLARE_STICK:
			return .25;
		case SELFIE_STICK:
		case ZAVALA_SNACKS:
			return .5;
		case COMMS_DEVICE:
		case ETHER_CONTAINER_SMALL:
		case FIRE_STARTER_KIT:
		case LED_LIGHT:
		case RATION_1_DAY:
			return 2;
		case CIVILIAN_CLOTHING:
		case ETHER_CONTAINER_MEDIUM:
		case BIOMETRIC_LOCK:
		case MOTION_ACTIVATED_CAMERA:
		case QUIVER_XL:
		case SIGNAL_FLARE_GUN:
		case TABLET_COMPUTER:
		case WEAPON_TELEMETRY_BASIC_50XP:
		case WEAPON_TELEMETRY_ADVANCED_500XP:
			return 3;
		case WEAPON_TELEMETRY_ROYAL_5000XP:
			return 4;
		case ETHER_CONTAINER_LARGE:
			return 5;
		case SURVEILLANCE_DRONE:
		case VIDEO_GAME_SYSTEM:
		case WIRE_CABLE_50_FT:
			return 8;
		case PLASMA_CUTTER:
		case RELAY_BEACON:
			return 10;
		case FIREWOOD_BUNDLE:
			return 12;
		case COOKING_UTENSIL_SET:
		case QUANTUM_RELAY_BEACON:
			return 15;
		case HUNTING_TRAP:
			return 25;
		default:
			return 1;
		}
	}

	@Override
	public String getDescription() {
		switch (this) {
		case GLIMMER:
			return "Wealth is measured almost unilaterally across the solar system in terms of glimmer; white-blue, brightly shining programmable cubes of matter form the Golden Age that can be turned into any non-organic substance or object. In some far-flung, lost, or forgotten corners of the system, wealth may be measured in material goods instead, but these areas are few and far between.";
		case SOLAR_BATTERY:
			return "This battery is compatible with most modern devices, and can be recharged by setting it out in the sun.";
		case CAMPING_COOKBOOK:
			return "A cookbook. That you use for camping.";
		case CIVILIAN_CLOTHING:
			return "Clothing. Looks like those a civilian wears.";
		case COMMS_DEVICE:
			return "Connects you to the Vanguard communication network if within 6 miles of any Vanguard relay beacon. This can be used for communication purposes only. Ghosts inherently have access to this functionality. The quality of the connection can degrade based on the surroundings.";
		case COOKING_UTENSIL_SET:
			return "Contains a few different sizes of pots, pans, and several sets of silverware, as well as eco-friendly disinfectant to wash everything with. Good for cooking almost any recipe.";
		case ETHER_CONTAINER_SMALL:
			return "Special container for carrying ether.";
		case ETHER_CONTAINER_MEDIUM:
			return "Special container for carrying ether.";
		case ETHER_CONTAINER_LARGE:
			return "Special container for carrying ether.";
		case FIRE_STARTER_KIT:
			return "A kit for starting fires.";
		case FIREWOOD_BUNDLE:
			return "A bundle of flammable wood. Used as fuel for a fire.";
		case HANDCUFFS:
			return "A pair of lockable linked metal rings for securing a creature�s wrists in close proximity to each other, usually affixed in a way to hold the creature�s wrists behind their back, and with a chain between . Each ring has a rotating arm which adjusts via a ratchet, which prevents the ring from opening once it has been closed. Standard handcuffs can fit on the wrists on a Medium or smaller creature.";
		case HUNTING_TRAP:
			return "Colloquially called a bear trap, when you use your action to set it, this trap forms a saw-toothed steel ring that snaps shut when a creature steps on a pressure plate in the center. The trap is affixed by a heavy chain to an immobile object, such as a tree or a spike driven into the ground. A creature that steps on the plate must succeed on a DC 13 Dexterity saving throw or take 1d4 piercing damage and stop moving. Thereafter, until the creature breaks free of the trap, its movement is limited by the length of the chain (typically 3 feet long). A creature can use its action to make a DC 13 Strength check, freeing itself or another creature within its reach on a success. Each failed check deals 1 piercing damage to the trapped creature.";
		case LED_LIGHT:
			return "Bright light. May or may not require batteries.";
		case PADLOCK:
			return "Lock. Comes with the key to open it.";
		case BIOMETRIC_LOCK:
			return "Lock. Opens using biometic security.";
		case MOTION_ACTIVATED_CAMERA:
			return "A camera that takes a series of pictures when it detects motion in a 15-foot cone in front of it. It can transmit these images to a device within 200 feet of it.";
		case PLASMA_CUTTER:
			return "Good for cutting through up to 3 inches of steel or weaker metals. Loud and bright when used. Burns wood, melts plastic.";
		case QUIVER:
			return "Quiver. It holds 20 arrows.";
		case QUIVER_XL:
			return "Bigger quiver. It holds 50 arrows.";
		case RATION_1_DAY:
			return "Enough food for one person for one day";
		case RELAY_BEACON:
			return "When activated, this beacon can connect anyone within 6 miles to the Vanguard network. This beacon can be used for communication or to transmat items to and from a Guardian�s vault if the beacon, through one connection after the other, is able to link back to the Tower. These beacons can be blocked by things like dense physical terrain and signal jammers.";
		case QUANTUM_RELAY_BEACON:
			return "Advanced relay beacon that connects to the Tower on a quantum level. Ignores distance. Creatures within 6 miles of the beacon can connect to it.";
		case SELFIE_STICK:
			return "An eldritch abomonation that should be disposed of immediately after pickup. DO NOT KEEP!";
		case SIGNAL_FLARE_GUN:
			return "Device that produces a bright and usually colored light. Signal flare sticks are used by twisting the cap. Hitting a creature with a lit signal flare deals 1d4-1 fire damage.";
		case SIGNAL_FLARE_STICK:
			return "Device that produces a bright and usually colored light. Shoots a small ball of light that arcs through the air. Hitting a creature with a lit signal flare deals 1d4-1 fire damage.";
		case SIGNAL_JAMMER:
			return "Device that spams junk signals, corrupting all local signals and making it difficult to impossible to communicate via signal-based devices.";
		case SURVEILLANCE_DRONE:
			return "Those who don�t have Ghosts to scan the local area, or Guardians who don�t want to risk the use of their Ghost, will often use a surveillance drone to do so. Surveillance drones can connect to a tablet computer and be used to gather visual information within a 500-foot radius of the controller.";
		case TABLET_COMPUTER:
			return "Portable computer with sufficient specs for most daily tasks.";
		case VIDEO_GAME:
			return "A singular video game to be used with a compatible system.";
		case VIDEO_GAME_SYSTEM:
			return "A video game system that accepts compatible video games.";
		case WATER_BOTTLE:
			return "Holds 4 day�s worth of water. Self-filters most common contaminants. Dentproof, heatproof, bulletproof.";
		case WIRE_CABLE_50_FT:
			return "Cable made of wire approximately exactly 50 feet long.";
		case WEAPON_TELEMETRY_BASIC_50XP:
			return "Used for gathering usage data from a weapon. Interested parties, such as Banshee-44 and the various Foundries, will pay greatly for filled weapon telemetries. Attaches to the weapon on use. (Players earn an equal amount of XP for their weapon telemetries as they do for their character).";
		case WEAPON_TELEMETRY_ADVANCED_500XP:
			return "Used for gathering usage data from a weapon. Interested parties, such as Banshee-44 and the various Foundries, will pay greatly for filled weapon telemetries. Attaches to the weapon on use. (Players earn an equal amount of XP for their weapon telemetries as they do for their character).";
		case WEAPON_TELEMETRY_ROYAL_5000XP:
			return "Used for gathering usage data from a weapon. Interested parties, such as Banshee-44 and the various Foundries, will pay greatly for filled weapon telemetries. Attaches to the weapon on use. (Players earn an equal amount of XP for their weapon telemetries as they do for their character).";
		case ZAVALA_SNACKS:
			return "Delicious lemon shortbread treats shaped like Zavala�s head.";
		default:
			return "";
		}
	}

	public static double getGlimmerWeight(long glimmer) {
		double weight = 0;
		while (glimmer >= 100000) {
			weight += 40;
			glimmer -= 100000;
		}
		while (glimmer >= 10000) {
			weight += 20;
			glimmer -= 10000;
		}
		while (glimmer >= 1000) {
			weight += 10;
			glimmer -= 1000;
		}
		while (glimmer >= 100) {
			weight += 5;
			glimmer -= 100;
		}
		while (glimmer >= 10) {
			weight += 1;
			glimmer -= 10;
		}
		while (glimmer >= 1) {
			weight += 0.1;
			glimmer -= 1;
		}
		return weight;
	}

	public enum ItemType {
		AMMUNITION, ARROW, MISCELLANEOUS;

		@Override
		public String toString() {
			return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
		}
	}
}
