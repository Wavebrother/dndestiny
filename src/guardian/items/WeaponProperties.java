package guardian.items;

import guardian.BaseInterface;
import guardian.Main;

public enum WeaponProperties implements BaseInterface {
	HIGH_CALIBER_ROUNDS, HIGH_RECOIL, FULL_AUTO, AUTOMATIC_FIRE, HEAVY_HITTER, MOUNTED, PAYLOAD, BULKY,
	ENERGY_PROJECTILES, LOADING, FINESSE, HEAVY, LIGHT, REACH, THROWN, TWO_HANDED, VERSATILE, TRACKING,
	PAYLOAD_VOLATILE_LAUNCH, COMBAT_BOW, TRACE_RIFLE, GRENADE_LAUNCHER;

	@Override
	public String toString() {
		switch (this) {
		case HIGH_CALIBER_ROUNDS:
			return "High-Caliber Rounds";
		case TWO_HANDED:
			return "Two-Handed";
		case PAYLOAD_VOLATILE_LAUNCH:
			return "Payload";
		default:
			return Main.enumString(name());
		}
	}

	@Override
	public String getDescription() {
		switch (this) {
		case HIGH_CALIBER_ROUNDS:
			return "This weapon ignores damage soak values.";
		case HIGH_RECOIL:
			return "The kickback on this weapon is severe enough that you must use your Strength modifier when determining the attack and damage rolls of this weapon instead of your Dexterity modifier.";
		case FULL_AUTO:
			return "You can make two attacks with this weapon for every single attack you would normally make. You must have enough ammo in your magazine to do so.";
		case AUTOMATIC_FIRE:
			return "These weapons fire multiple bullets per shot, granting them a higher than average chance of dealing damage in their upper ranges. When you deal damage with this weapon, roll an additional damage die and drop the lowest roll from your total for that attack.";
		case HEAVY_HITTER:
			return "The rounds from this weapon hit hard, causing all damage rolls to have an inherent +1.";
		case MOUNTED:
			return "This weapon cannot be handheld, it must be fired via a vehicle, battery, or other special ordnance.";
		case PAYLOAD:
			return "The rounds from this weapon are highly explosive and as such, for this weapon's attack roll, you choose a spot you can see within this weapon's range to fire at. All creatures within 5 feet of that spot must make a Dexterity saving throw (DC = 8 + your proficiency + your Strength or Dexterity modifier, the same one you use for this weapon�s damage roll), taking full damage on a failure or half damage on a success.";
		case BULKY:
			return "Some weapons are just too heavy, cumbersome, or ungainly to use, and so despite proficiency with this weapon, this weapon's reload cost starts as an action.";
		case ENERGY_PROJECTILES:
			return "The projectiles from this weapon are made of raw energy and can damage all creatures in a line up to this weapon�s effective range. In order to do this, your attack must have killed the first target, and your attack roll must beat the AC of the next subsequent target.";
		case LOADING:
			return "Because of the time required to load this weapon, you can fire only one piece of ammunition from it when you use an action, bonus action, or reaction to fire it, regardless of the number of attacks you can normally make.";
		case FINESSE:
			return "When making an attack with a finesse weapon, you use your choice of your Strength or Dexterity modifier for the attack and damage rolls. You must use the same modifier for both rolls. ";
		case HEAVY:
			return "Small creatures have disadvantage on attack rolls with heavy weapons. A heavy weapon's size and bulk make it too large for a Small creature to use effectively.";
		case LIGHT:
			return "A light weapon is small and easy to handle, making it ideal for use when fighting with two weapons.";
		case REACH:
			return "This weapon adds 5 feet to your reach when you attack with it, as well as when determining your reach for opportunity attacks with it.";
		case THROWN:
			return "If a weapon has the thrown property, you can throw the weapon to make a ranged attack. If the weapon is a melee weapon, you use the same ability modifier for that attack roll and damage roll that you would use for a melee attack with the weapon. For example, if you throw a handaxe, you use your Strength, but if you throw a dagger, you can use either your Strength or your Dexterity, since the dagger has the finesse property.";
		case TWO_HANDED:
			return "This weapon requires two hands when you attack with it.";
		case VERSATILE:
			return "This weapon can be used with one or two hands. A damage value in parentheses appears with the property--the damage when the weapon is used with two hands to make a melee attack.";
		case TRACKING:
			return "Creatures that attempt to use reactions or other movement abilities to dodge out of the way of an attack from this weapon, but move 10 feet or less and do not end up behind full cover, are still hit as the target of your attack.";
		case PAYLOAD_VOLATILE_LAUNCH:
			return "The rounds from this weapon are highly explosive and as such, for this weapon's attack roll, you choose a spot you can see within this weapon's range to fire at. All creatures within 2.5 feet of that spot must make a Dexterity saving throw (DC = 8 + your proficiency + your Strength or Dexterity modifier, the same one you use for this weapon�s damage roll), taking full damage on a failure or half damage on a success.";
		case COMBAT_BOW:
			return "The combat bow is designed for use in the modern theatre of war. Its range is treated like a firearm�s range, with values of 20/100/200 (medium) that can be affected by the Aiming condition, and the combat bow is perk-compatible (see Chapter 6: Customization). The standard arrows it shoots deals kinetic damage. Unlike firearms, the combat bow is relatively silent.";
		case GRENADE_LAUNCHER:
			return "The shells the grenade launcher fires can be bounced or arced in such a way as to ignore the benefits creatures gain from shields. You cannot benefit from the Aiming condition when firing this weapon in this way.";
		case TRACE_RIFLE:
			return "Trace rifles fire a line of energy that always goes up to the weapon�s maximum range, and always must make an attack against the first target in the line. When Aiming with a trace rifle, you can make an attack up to the weapon�s extended range without disadvantage, but the extended range does not become equal to the maximum range of the weapon. Attacking between the extended and maximum ranges grants disadvantage, even while Aiming, and if you hit a target, the target takes half as much damage from the attack.";
		default:
			return "";

		}
	}
}
