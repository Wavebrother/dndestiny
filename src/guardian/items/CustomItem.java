package guardian.items;
import guardian.items.interfaces.ItemInterface;

public class CustomItem implements ItemInterface {
	private String name;
	private String description;
	private int memorySize;
	private int maxStack;
	private double weightPerItem;

	public CustomItem(String name, String description, int maxStack, double weightPerItem, int memorySize) {
		this.name = name;
		this.description = description;
		this.memorySize = memorySize;
		this.maxStack = maxStack;
		this.weightPerItem = weightPerItem;
	}

	@Override
	public String toString() {
		return name;
	}
	
	@Override 
	public String name() {
		return name;
	}

	@Override
	public int getMaxStack() {
		return maxStack;
	}

	@Override
	public int getMemorySize() {
		return memorySize;
	}

	@Override
	public double getWeightPerItem() {
		return weightPerItem;
	}

	@Override
	public String getDescription() {
		return description;
	}
}
