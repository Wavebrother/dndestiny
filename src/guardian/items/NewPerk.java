package guardian.items;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import guardian.Main;
import guardian.SearchableComboBox;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.PerkInterface.CustomPerk;

public class NewPerk extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private SearchableComboBox[] perks;
	public JButton open = new JButton();

	private JTextField name = new JTextField();
	private JTextField description = new JTextField();

	private JButton add = new JButton("Add");
	private JButton done = new JButton("Done");

	public NewPerk(SearchableComboBox[] perks) {
		super("New Perk");
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});
		this.perks = perks;

		this.add.addActionListener(this);
		this.done.addActionListener(this);

		JPanel buttonPane = new JPanel(new GridLayout(1, 0));
		buttonPane.add(this.add);
		buttonPane.add(this.done);

		JPanel namePanel = new JPanel(new GridLayout());
		namePanel.add(name);
		namePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel descriptionPanel = new JPanel(new GridLayout());
		descriptionPanel.add(description);
		descriptionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Description", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		this.setLayout(new GridLayout(0, 1));
		this.add(namePanel);
		this.add(descriptionPanel);
		this.add(buttonPane);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	public JButton open() {
		open = new JButton(this.getTitle());
		open.setFont(Main.titleFont);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
			}
		});
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.add || e.getSource() == this.done) {
			try {
				new CustomPerk(name.getText(), description.getText());
				for (SearchableComboBox perkBox : perks) {
					perkBox.setModel(PerkInterface.perks.values().toArray(new PerkInterface[0]));
					perkBox.repaint();
				}
				name.setText("");
				description.setText("");
				if (e.getSource() == this.done)
					this.setVisible(false);
			} catch (RuntimeException error) {
				this.setVisible(false);
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					error.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
			}
		}
	}

}
