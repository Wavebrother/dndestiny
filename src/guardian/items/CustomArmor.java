package guardian.items;
import java.awt.Image;

import javax.swing.ImageIcon;

import guardian.items.interfaces.ArmorInterface;
import guardian.items.itemEnums.Armor;

public class CustomArmor implements ArmorInterface {
	private String name;
	private String description;
	private Armor.ArmorLevel level;
	private int baseAC;
	private int maxAC;
	private int minStr;
	private int memorySize;
	private double weight;
	private boolean stealth;
	private ImageIcon icon;

	public CustomArmor(String name, String description, Armor.ArmorLevel level, int baseAC, int maxAC, int minStr,
			int memorySize, double weight, boolean stealth, ImageIcon icon) {
		setItem(description, icon);
		this.name = name;
		this.level = level;
		this.baseAC = baseAC;
		this.maxAC = maxAC;
		this.minStr = minStr;
		this.stealth = stealth;
		this.memorySize = memorySize;
		this.weight = weight;
	}

	@Override
	public String toString() {
		return name;
	}
	
	@Override 
	public String name() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public int getMemorySize() {
		return memorySize;
	}

	@Override
	public double getWeightPerItem() {
		return weight;
	}

	@Override
	public ImageIcon getIcon(int size, double multiplier) {
		return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
				(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
	}

	@Override
	public boolean isStealth() {
		return stealth;
	}

	@Override
	public boolean isGuardianArmor() {
		return level != Armor.ArmorLevel.GHOST;
	}

	@Override
	public int getMinStr() {
		return minStr;
	}

	@Override
	public int getMaxAC() {
		return maxAC;
	}

	@Override
	public int getBaseAC() {
		return baseAC;
	}

	@Override
	public Armor.ArmorLevel getLevel() {
		return level;
	}

	@Override
	public void setItem(String description, ImageIcon icon) {
		this.description = description;
		this.icon = icon;
	}

}
