package guardian.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.Entity;
import guardian.Main;
import guardian.items.itemEnums.Items;

public class Inventory {
	private ArrayList<Item> items = new ArrayList<Item>();
	String name;
	Entity entity;
	private int maxSpaces;
	private int width = (int) (Main.height / 2);
	JPanel panel = new JPanel();
	public boolean iconLayout = true;
	public double iconMultiplier = 4;
	private JPanel displayPanel = new JPanel(new BorderLayout());

	private Color backgroundColor;
	private Color borderColor;

	public Inventory(String name, int maxSpaces, boolean iconLayout) {
		this.name = name;
		this.maxSpaces = maxSpaces;
		this.iconLayout = iconLayout;
	}

	public void init(Color backgroundColor, Color borderColor, Entity entity) {
		this.backgroundColor = backgroundColor;
		this.borderColor = borderColor;
		this.entity = entity;
	}

	private int getSpace() {
		int itemSpace = 0;
		for (Item item : items) {
			if (item.getItem() == Items.GLIMMER) {
				int count = (int) item.amount.getValue();
				do {
					itemSpace++;
					count -= 62500;
				} while (count >= 62500);
			} else
				itemSpace += item.getItem().getMemorySize();
		}
		return maxSpaces - itemSpace;
	}

	public void moveItem(Inventory destination, Item item) {
		if (destination.addItem(item))
			items.remove(item);
		fillPanel();
	}

	private boolean hasSpace(int itemSize) {
		return (itemSize <= getSpace() || maxSpaces == 0);
	}

	public void setMaxSpaces(int maxSpaces) {
		this.maxSpaces = maxSpaces;
		fillPanel();
	}

	public int getMaxSpaces() {
		return maxSpaces;
	}

	public boolean addItem(Item item) {
		if (hasSpace(item.getItem().getMemorySize())) {
			if (item.getItem() == Items.GLIMMER && items.size() > 0) {
				int i = 0;
				while (items.size() > i && items.get(i).getItem() == Items.GLIMMER) {
					if ((int) items.get(i).amount.getValue() < Integer.MAX_VALUE) {
						if ((int) items.get(i).amount.getValue() + (int) item.amount.getValue() <= Integer.MAX_VALUE) {
							items.get(i).amount
									.setValue((int) items.get(i).amount.getValue() + (int) item.amount.getValue());
							item.amount = null;
							break;
						} else {
							item.amount.setValue((int) item.amount.getValue()
									- (Integer.MAX_VALUE - (int) items.get(i).amount.getValue()));
							items.get(i).amount.setValue(Integer.MAX_VALUE);
						}
					}
					i++;
				}
				if (item.amount != null)
					items.add(i, item);
				if (Main.character != null)
					Main.character.glimmer.setText(Main.character.character.getGlimmerFormatted());
			} else
				items.add(item);
			item.setInventory(this);
			if (entity != null) {
				displayPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
						"Inventory (" + getWeight() + "lbs)", TitledBorder.DEFAULT_JUSTIFICATION,
						TitledBorder.DEFAULT_POSITION, Main.titleFont));
				fillPanel();
			}
			return true;
		} else {
			JOptionPane.showMessageDialog(null,
					"This inventory does not have space for " + item.getItem().toString() + ".", name,
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	public ArrayList<Item> getAllItems() {
		ArrayList<Item> items = new ArrayList<Item>();
		items.addAll(this.items);
		items.addAll(entity.getWeapons());
		return items;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	double getWeight() {
		double weight = 0;
		long glimmer = 0;
		for (Item item : getAllItems()) {
			if (item.getItem() == Items.GLIMMER)
				glimmer += (int) item.amount.getValue();
			else
				weight += item.getWeight();
		}
		weight += Items.getGlimmerWeight(glimmer);
		return Math.round(weight * 10.0) / 10.0;
	}

	private boolean setLayout(boolean iconLayout) {
		fillPanel(this.iconLayout = iconLayout);
		return iconLayout;
	}

	public JPanel display(int width) {
		this.width = width;
		this.panel.setBackground(backgroundColor);
		fillPanel();
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());
		northPanel.setBackground(backgroundColor);
		JPanel iconSizePane = new JPanel(new BorderLayout());
		JSpinner iconSizeMultiplier = new JSpinner(new SpinnerNumberModel(iconMultiplier, 1.0, width / 15, .1));
		((JSpinner.DefaultEditor) iconSizeMultiplier.getEditor()).getTextField().setColumns(2);
		iconSizeMultiplier.setFont(Main.textFont);
		iconSizeMultiplier.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				iconMultiplier = (double) iconSizeMultiplier.getValue();
				fillPanel();
			}
		});
		iconSizePane.setBackground(backgroundColor);
		iconSizePane.add(new JLabel("Icon Size"), BorderLayout.WEST);
		((JLabel) iconSizePane.getComponent(0)).setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		((JLabel) iconSizePane.getComponent(0)).setFont(Main.titleFont);
		iconSizePane.add(iconSizeMultiplier, BorderLayout.CENTER);
		JCheckBox iconLayout = new JCheckBox("Icon Layout");
		iconLayout.setFont(Main.titleFont);
		iconLayout.setSelected(this.iconLayout);
		iconLayout.setBackground(backgroundColor);
		iconLayout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.character.getContentPane().revalidate();
				setLayout(iconLayout.isSelected());
				if (iconLayout.isSelected())
					northPanel.add(iconSizePane);
				else
					northPanel.remove(iconSizePane);
				northPanel.revalidate();
				northPanel.repaint();
			}
		});
		if (this.iconLayout)
			northPanel.add(iconSizePane, BorderLayout.WEST);
		northPanel.add(new AddItem(this).open(), BorderLayout.CENTER);
		northPanel.add(iconLayout, BorderLayout.EAST);

		displayPanel.add(northPanel, BorderLayout.NORTH);
		displayPanel.add(this.panel, BorderLayout.CENTER);
		displayPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
				"Inventory (" + getWeight() + "lbs)", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
				Main.titleFont));
		displayPanel.setBackground(backgroundColor);
		return displayPanel;
	}

	public void fillPanel() {
		fillPanel(iconMultiplier, iconLayout);
	}

	private void fillPanel(boolean iconLayout) {
		fillPanel(iconMultiplier, iconLayout);
	}

	private void fillPanel(double iconMultiplier, boolean iconLayout) {
		int size = Main.textFont.getSize();
		int columns = (int) (width / (size * iconMultiplier));
		this.panel.removeAll();
		if (iconLayout) {
			this.panel.setLayout(new GridBagLayout());
			int y = 0;
			int maxSpaces;
			if ((maxSpaces = this.maxSpaces) == 0) {
				if ((maxSpaces = getAllItems().size()) < columns)
					maxSpaces = columns;
			} else {
				maxSpaces = getAllItems().size() + getSpace();
			}
			for (int i = 0; i < maxSpaces; i++) {
				y = i / columns;
				int x = i % columns;
				GridBagConstraints constraints = new GridBagConstraints();
				constraints.fill = GridBagConstraints.NONE;
				constraints.gridy = y;
				constraints.gridx = x;
				if (i < getAllItems().size()) {
					this.panel.add(getAllItems().get(i).openIcon(size, iconMultiplier, displayPanel), constraints);
				} else {
					JPanel fillerPanel = new JPanel(new BorderLayout());
					fillerPanel.setBackground(backgroundColor);
					fillerPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
					fillerPanel.setPreferredSize(
							new Dimension((int) (size * iconMultiplier), (int) (size * iconMultiplier)));
					this.panel.add(fillerPanel, constraints);
				}
			}
		} else {
			this.panel.setLayout(new BoxLayout(this.panel, BoxLayout.Y_AXIS));
			for (Item item : getAllItems()) {
				JPanel openPanel = new JPanel(new GridLayout());
				openPanel.add(item.open(size, iconMultiplier, displayPanel));
				this.panel.add(openPanel);
			}
		}
		this.panel.revalidate();
		this.panel.repaint();
	}

	private ArrayList<Tool> getTools() {
		ArrayList<Tool> tools = new ArrayList<Tool>();
		for (Item item : this.items)
			if (item instanceof Tool)
				tools.add((Tool) item);
		return tools;
	}

	@SuppressWarnings("unused")
	private ArrayList<Item> getMiscellaneous() {
		ArrayList<Item> miscellaneous = new ArrayList<Item>();
		miscellaneous.addAll(items);
		miscellaneous.removeAll(getTools());
		return miscellaneous;
	}
}
