package guardian.items;

import java.util.ArrayList;

import guardian.DamageType;
import guardian.Die;
import guardian.items.Firearm.Firearms;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.interfaces.FirearmInterface;
import guardian.items.interfaces.PerkInterface;

public class CustomFirearm extends CustomWeapon implements FirearmInterface {
	private RangeBand rangeBand;
	public Firearms base;

	public CustomFirearm(String name, double weightPerItem, int memorySize, int closeRange, int midRange, int farRange,
			RangeBand rangeBand, boolean finesse, DamageType damageType, Firearms base, Die damageDie,
			int damageDieNumber, int shots, ArrayList<WeaponProperties> properties) {
		super(name, weightPerItem, memorySize, shots, closeRange, farRange, finesse, false, damageType, null, damageDie,
				damageDieNumber, properties);
		this.rangeBand = rangeBand;
		this.base = base;
		super.getRange()[1] = midRange;
	}

	@Override
	public WeaponType getWeaponType() {
		return base.getWeaponType();
	}

	@Override
	public boolean isThrown() {
		return false;
	}

	@Override
	public RangeBand getRangeBand() {
		return rangeBand;
	}

	@Override
	public String getRangeInfo() {
		return "Range: " + this.getRange()[0] + ", " + this.getRange()[1] + ", " + this.getRange()[2] + " ("
				+ getRangeBand() + ")";
	}

	@Override
	public PerkInterface[][] getPerkSlotOptions() {
		return new PerkInterface[][] { PerkInterface.perks.values().toArray(new PerkInterface[0]),
				PerkInterface.perks.values().toArray(new PerkInterface[0]),
				PerkInterface.perks.values().toArray(new PerkInterface[0]) };
	}
}
