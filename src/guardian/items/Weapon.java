package guardian.items;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import guardian.BaseInterface;
import guardian.Check;
import guardian.DamageType;
import guardian.Die;
import guardian.Entity;
import guardian.Main;
import guardian.Stat.Stats;
import guardian.items.Firearm.Firearms;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.itemEnums.Exotic.ExoticInterface.ExoticWeapon;
import guardian.items.itemEnums.Exotic.ExoticInterface.RainbowBlade;

public class Weapon extends Item implements ActionListener {
	private WeaponInterface weapon;
	private final Map<String, JPanel[]> panels = new HashMap<String, JPanel[]>();
	private JPanel buttonPanel = new JPanel();

	public Entity entity;
	final private JTextField attackField = new JTextField();
	final private JTextField damageField = new JTextField();
	public boolean proficient;

	final private JButton info = new JButton("\u24d8");
	final private JButton damageButton = new JButton("Damage");

	public int magicalBonus;

	public Weapon(WeaponInterface weapon, int magicalBonus, ArrayList<Weapon> weapons, Inventory inventory, int amount,
			boolean proficient, Entity entity) {
		super(weapon, inventory, amount);
		weapons.add(this);
		this.weapon = weapon;
		this.magicalBonus = magicalBonus;
		this.info.addActionListener(this);
		this.damageButton.addActionListener(this);
		this.buttonPanel.setLayout(new BoxLayout(this.buttonPanel, BoxLayout.X_AXIS));
		this.proficient = proficient;
		this.entity = entity;
	}

	@Override
	double getWeight() {
		return this.getItem().getWeightPerItem();
	}

	protected int getDamageBonus() {
		int bonus = 0;
		if ((this.weapon.isFinesse() && entity.getStats().get(Stats.Dexterity).getModifier() > entity.getStats()
				.get(Stats.Strength).getModifier()))
			bonus = entity.getStats().get(Stats.Dexterity).getModifier();
		else
			bonus = entity.getStats().get(Stats.Strength).getModifier();
		if (weapon instanceof ExoticWeapon)
			bonus += ((ExoticWeapon) weapon).getMagicalBonus();
		else
			bonus += this.magicalBonus;
		return bonus;
	}

	protected int getAttackBonus() {
		int bonus = 0;
		if (this.isProficient())
			bonus = entity.getProficiencyBonus();
		bonus += getDamageBonus();
		return bonus;
	}

	Die getDamageDie() {
		return getWeapon().getDamageDie();
	}

	int getDamageDieNumber() {
		return getWeapon().getDamageDieNumber();
	}

	private boolean isProficient() {
		if (proficient)
			return true;
		if (weapon instanceof RainbowBlade)
			return entity.hasProficiency(((RainbowBlade) weapon).baseWeapon);
		if (weapon instanceof ExoticWeapon)
			return entity.hasProficiency(((ExoticWeapon) weapon).getBaseWeapon());
		if (weapon instanceof CustomFirearm)
			return entity.hasProficiency(((CustomFirearm) weapon).base);
		return entity.hasProficiency(weapon);
	}

	public JPanel getName(JPanel parentPanel) {
		JPanel panel = new JPanel(new GridLayout());
		JTextField name = new JTextField(this.weapon.toString());
		if (weapon instanceof Weapons || weapon instanceof Firearms)
			name.setEditable(false);
		panel.add(name);
		name.setFont(Main.textFont);
		this.panels.put("name", new JPanel[] { parentPanel, panel });
		return panel;
	}

	public JPanel getAmount(JPanel parentPanel) {
		JPanel panel = new JPanel(new GridLayout());
		panel.add(this.amount);
		this.amount.setFont(Main.textFont);
		JPanel[] panels = { parentPanel, panel };
		this.panels.put("amount", panels);
		return panel;
	}

	public JPanel getAttack(JPanel parentPanel) {
		JPanel panel = new JPanel(new GridLayout());
		this.attackField.setText("+" + this.getAttackBonus());
		this.attackField.setEditable(false);
		panel.add(this.attackField);
		this.attackField.setFont(Main.textFont);
		JPanel[] panels = { parentPanel, panel };
		this.panels.put("attack", panels);
		return panel;
	}

	public JPanel getDamage(JPanel parentPanel) {
		JPanel panel = new JPanel(new GridLayout());
		this.damageField.setText(this.weapon.getDamageDieNumber() + this.weapon.getDamageDie().name() + " "
				+ this.weapon.getDamageType() + " +" + this.getDamageBonus());
		this.damageField.setEditable(false);
		panel.add(this.damageField);
		this.damageField.setFont(Main.textFont);
		JPanel[] panels = { parentPanel, panel };
		this.panels.put("damage", panels);
		return panel;
	}

	public JPanel getButtons(JPanel parentPanel) {
		JPanel panel = new JPanel(new GridLayout());
		JPanel infoPanel = new JPanel(new GridLayout());
		infoPanel.add(this.info);
		JPanel movePanel = new JPanel(new GridLayout(0, 1));
		movePanel.add(this.moveUp);
		movePanel.add(this.moveDown);
		JPanel buttonPanel = new JPanel(new GridLayout());
		buttonPanel.add(this.delete);
		buttonPanel.add(movePanel);
		JPanel rollPanel = new JPanel(new GridLayout());
		rollPanel.add(new Check(this.weapon.getDamageDie(), this.weapon.getDamageDieNumber(), Main.d20Icon,
				this.getDamageBonus()));
		this.buttonPanel.removeAll();
		this.buttonPanel.add(rollPanel);
		this.buttonPanel.add(infoPanel);
		this.buttonPanel.add(buttonPanel);
		panel.add(this.buttonPanel);
		JPanel[] panels = { parentPanel, panel };
		this.panels.put("buttons", panels);
		return panel;
	}

	public WeaponInterface getWeapon() {
		return weapon;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.info) {
			StringBuilder sb = new StringBuilder();
			if (this.weapon.isRanged() || this.weapon.isThrown())
				sb.append(this.weapon.getRangeInfo() + "\n");
			for (WeaponProperties property : this.weapon.getProperties()) {
				sb.append(property + ": " + property.getDescription() + "\n");
				if (!property.getDescription().contains("."))
					sb.append("\n");
			}
			if (this instanceof Firearm) {
				for (PerkInterface perk : ((Firearm) this).perks) {
					if (perk != null) {
						sb.append(perk.toString() + ": " + perk.getDescription() + "\n");
						if (!perk.getDescription().contains("."))
							sb.append("\n");
					}
				}
			}
			if (this.magicalBonus > 0)
				sb.append("Magical (+" + this.magicalBonus + ")\n");
			if (this.weapon.isThrown())
				sb.append("Thrown\n");
			if (this.weapon.isRanged())
				sb.append("Ranged\n");
			if (this.isProficient())
				sb.append("Proficient\n");
			JOptionPane.showMessageDialog(info, sb.toString().replace(".", ".\n"));
		} else if (e.getSource() == this.damageButton) {
			int damage = 0;
			StringBuilder sb = new StringBuilder();
			sb.append("You Rolled ");
			for (int i = 0; i < this.weapon.getDamageDieNumber(); i++) {
				int r = this.weapon.getDamageDie().roll();
				sb.append(r);
				sb.append(" and ");
				damage += r;
			}
			sb.delete(sb.length() - 5, sb.length());
			sb.append(". This results in a total of ");
			sb.append(damage);
			sb.append("(");
			if ((this.weapon.isFinesse() || this.weapon.isRanged()) && entity.getStats().get(Stats.Dexterity)
					.getModifier() > entity.getStats().get(Stats.Strength).getModifier())
				sb.append(damage + entity.getStats().get(Stats.Dexterity).getModifier());
			else
				sb.append(damage + entity.getStats().get(Stats.Strength).getModifier());
			sb.append(") ");
			sb.append(weapon.getDamageType());
			sb.append(" damage.");
			JOptionPane.showMessageDialog(damageButton, sb.toString());
		} else if (e.getSource() == this.delete) {
			entity.getWeapons().remove(this);
			for (JPanel[] panels : this.panels.values())
				panels[0].remove(panels[1]);
		} else if (e.getSource() == this.moveUp && entity.getWeapons().indexOf(this) != 0) {
			Collections.swap(entity.getWeapons(), entity.getWeapons().indexOf(this),
					entity.getWeapons().indexOf(this) - 1);
			for (JPanel[] panels : this.panels.values()) {
				Component[] panel = panels[0].getComponents();
				int index = Arrays.asList(panel).indexOf(panels[1]) - 1;
				JPanel swappedTrait = (JPanel) panel[index];
				panel[index] = panels[1];
				panel[index + 1] = swappedTrait;
				panels[0].removeAll();
				for (Component component : panel)
					panels[0].add(component);
			}
		} else if (e.getSource() == this.moveDown
				&& entity.getWeapons().indexOf(this) != entity.getWeapons().size() - 1) {
			Collections.swap(entity.getWeapons(), entity.getWeapons().indexOf(this),
					entity.getWeapons().indexOf(this) + 1);
			for (JPanel[] panels : this.panels.values()) {
				Component[] panel = panels[0].getComponents();
				int index = Arrays.asList(panel).indexOf(panels[1]) + 1;
				JPanel swappedTrait = (JPanel) panel[index];
				panel[index] = panels[1];
				panel[index - 1] = swappedTrait;
				panels[0].removeAll();
				for (Component component : panel)
					panels[0].add(component);
			}
		}
		Main.character.getContentPane().revalidate();
		Main.character.getContentPane().repaint();
	}

	public static enum Weapons implements WeaponInterface {
		DAGGER("Dagger"), LIGHT_HAMMER("Light Hammer"), GREATAXE("Greataxe"), GREATSWORD("Greatsword"),
		LONGSWORD("Longsword"), MAUL("Maul"), SHORTSWORD("Shortsword"), WARHAMMER("Warhammer");

		private String name;

		Weapons(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

		public static Map<String, WeaponInterface> getWeapons() {
			Map<String, WeaponInterface> weapons = new HashMap<String, WeaponInterface>();
			for (Weapons weapon : values())
				weapons.put(weapon.toString(), weapon);
			weapons.remove("");
			return weapons;
		}

		@Override
		public int getMemorySize() {
			switch (this) {
			case DAGGER:
			case LIGHT_HAMMER:
				return 2;
			case GREATAXE:
			case GREATSWORD:
			case LONGSWORD:
			case SHORTSWORD:
			case WARHAMMER:
				return 3;
			case MAUL:
				return 4;
			default:
				return 1;
			}
		}

		@Override
		public double getWeightPerItem() {
			switch (this) {
			case DAGGER:
				return 6;
			case LIGHT_HAMMER:
			case SHORTSWORD:
			case WARHAMMER:
				return 7;
			case LONGSWORD:
				return 8;
			case GREATSWORD:
				return 11;
			case GREATAXE:
				return 12;
			case MAUL:
				return 15;
			default:
				return 0;
			}
		}

		@Override
		public ImageIcon getIcon(int size, double multiplier) {
			ImageIcon icon;
			switch (this) {
			default:
				icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
			}
			return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
					(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
		}

		@Override
		public int[] getRange() {
			switch (this) {
			case DAGGER:
			case LIGHT_HAMMER:
				return new int[] { 20, 0, 60 };
			default:
				return null;
			}
		}

		@Override
		public boolean isFinesse() {
			switch (this) {
			case DAGGER:
			case SHORTSWORD:
				return true;
			default:
				return false;
			}
		}

		@Override
		public boolean isRanged() {
			return false;
		}

		@Override
		public boolean isThrown() {
			switch (this) {
			case DAGGER:
			case LIGHT_HAMMER:
				return true;
			default:
				return false;
			}
		}

		@Override
		public DamageType getDamageType() {
			switch (this) {
			case DAGGER:
			case SHORTSWORD:
				return DamageType.PIERCING;
			case LIGHT_HAMMER:
			case MAUL:
			case WARHAMMER:
				return DamageType.BLUDGEONING;
			case GREATAXE:
			case GREATSWORD:
			case LONGSWORD:
				return DamageType.SLASHING;
			default:
				return DamageType.KINETIC;
			}
		}

		@Override
		public WeaponType getWeaponType() {
			switch (this) {
			case DAGGER:
			case LIGHT_HAMMER:
				return WeaponType.SIMPLE_MELEE;
			case GREATAXE:
			case GREATSWORD:
			case LONGSWORD:
			case MAUL:
			case SHORTSWORD:
			case WARHAMMER:
				return WeaponType.MARTIAL_MELEE;
			}
			return null;
		}

		@Override
		public Die getDamageDie() {
			switch (this) {
			case DAGGER:
			case LIGHT_HAMMER:
				return Die.d4;
			case GREATSWORD:
			case MAUL:
			case SHORTSWORD:
				return Die.d6;
			case LONGSWORD:
			case WARHAMMER:
				return Die.d8;
			case GREATAXE:
				return Die.d12;
			default:
				return Die.d4;
			}
		}

		@Override
		public int getDamageDieNumber() {
			switch (this) {
			case GREATSWORD:
			case MAUL:
				return 2;
			default:
				return 1;
			}
		}

		@Override
		public ArrayList<WeaponProperties> getProperties() {
			ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
			if (isFinesse())
				properties.add(WeaponProperties.FINESSE);
			if (isThrown())
				properties.add(WeaponProperties.THROWN);
			switch (this) {
			case DAGGER:
			case LIGHT_HAMMER:
			case SHORTSWORD:
				properties.add(WeaponProperties.LIGHT);
				break;
			case GREATAXE:
			case GREATSWORD:
			case MAUL:
				properties.add(WeaponProperties.HEAVY);
				properties.add(WeaponProperties.TWO_HANDED);
				break;
			case LONGSWORD:
			case WARHAMMER:
				properties.add(WeaponProperties.VERSATILE);
				break;
			}
			return properties;
		}

		public enum WeaponType implements BaseInterface {
			SIMPLE_MELEE, MARTIAL_MELEE, SIMPLE_RANGED, MARTIAL_RANGED;

			@Override
			public String toString() {
				String output = "";
				switch (this) {
				case SIMPLE_MELEE:
				case SIMPLE_RANGED:
					output = "Simple ";
					break;
				case MARTIAL_MELEE:
				case MARTIAL_RANGED:
					output = "Martial ";
					break;
				}
				switch (this) {
				case SIMPLE_MELEE:
				case MARTIAL_MELEE:
					output += "Melee weapons";
					break;
				case SIMPLE_RANGED:
				case MARTIAL_RANGED:
					output += "Firearms";
					break;
				}
				return output;
			}
		}
	}
}
