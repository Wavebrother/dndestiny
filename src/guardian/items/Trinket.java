package guardian.items;

import java.util.ArrayList;
import java.util.Random;

import guardian.items.interfaces.ItemInterface;

public class Trinket implements ItemInterface {
	private static ArrayList<Trinket> trinkets = new ArrayList<Trinket>();
	static {
		new Trinket("A shard of an unknown Ghost.");
		new Trinket("A bullet shell with a name carved on it.");
		new Trinket("Empty lipstick case with the relief of a hibiscus flower.");
		new Trinket("Well-worn gloves made from unhackable glimmer, age unknown.");
		new Trinket("Silver flask with six tally marks etched into it.");
		new Trinket("A piece of colored glass.");
		new Trinket("A piece of leather with an embroidered emblem of a wolf on it.");
		new Trinket("Jewelry box shaped like an engram, no obvious keyhole.");
		new Trinket("Small cat statue that has a persistent, faint chime, and smells vaguely of mint.");
		new Trinket("An ancient piece of technology with the word �NOKIA� still embossed on the back.");
		new Trinket("A package of Twinkies.");
		new Trinket("An expired coupon for a spicy ramen shop.");
		new Trinket("A box of raisins.");
		new Trinket("A stick of celery.");
		new Trinket("A carefully preserved movie ticket from the year 2136.");
		new Trinket("A metal pin in the shape of a cartoon mouse.");
		new Trinket("A deck of old playing cards that are missing the ace of spades.");
		new Trinket("A copy of Hunter of the Heart, signed by the author.");
		new Trinket("A hand-knitted, blue hat with the letters �AK� in gold.");
		new Trinket("A strange coin made of a bronze-like metal with a green gem in the center, warm to the touch.");
		new Trinket("A photo of a woman sitting at the edge of a lake, grinning at the camera.");
		new Trinket("A bottlecap.");
		new Trinket(
				"A keychain photo with a picture of a kitten clinging to the edge with the words �hang in there!� in curly font.");
		new Trinket("An ultra rare Shaxx card from the Guardian trading card game.");
		new Trinket("A small nautilus fossil that came from Io.");
		new Trinket("A ragged doll made from llama wool, and filled with soft pine filigrees.");
		new Trinket("A box of glimmer-shaped sweets.");
		new Trinket("A necklace made from warbeast scales and teeth.");
		new Trinket("A whistle with the word �CALE� etched into it, next to an etching of a kale leaf.");
		new Trinket("A travel mug with the Crucible logo on it.");
		new Trinket("A spent air horn that can only make a pathetic toot of noise.");
		new Trinket("A rainbow rubber bracelet");
		new Trinket("A metal slinky.");
		new Trinket("A rubik�s cube but all the colored stickers are missing.");
		new Trinket("A tyedye t-shirt.");
		new Trinket(
				"A blue plastic card with a magnetic strip on the back and �BLOCKBUSTERS� written in yellow on the front.");
		new Trinket("A small jar of sourdough starter, estimated to be over 2,000 years old.");
		new Trinket("A clear plastic bag full of plastic chips.");
		new Trinket("A marble that looks like the Traveler.");
		new Trinket("An old memory stick that cannot be accessed by any modern technology, including your Ghost.");
		new Trinket(
				"A strange piece of black plastic with the words �AWESOME MIX VOL 1� written on the outside. Inside, there is a dark brown magnetic tape wrapped around two cylinders.");
		new Trinket(
				"A picture of an ancient astronaut with the letters �aeiou� drawn into a speech bubble next to it.");
		new Trinket("Fingerless gloves that look like an animal�s paw");
		new Trinket("A rock with seven numbers carved into it, with a space between the third and fourth numbers.");
		new Trinket("A 1:5 scale model of a Gjallarhorn that shoots foam darts.");
		new Trinket("A vial of rainbow glitter.");
		new Trinket("A small plastic figurine of a mouse-like creature with a lightning bolt for a tail.");
		new Trinket("A glass bottle with a toy colony ship inside.");
		new Trinket("An outdated work order for the installation and calibration of a cascade driver array.");
		new Trinket(
				"Slivers of glass used by the Vex. Isotopic dating suggests their age ranges across billions of years - past and future.");
		new Trinket(
				"An ancient recording device. There is no film, but the Cryptarchs say you can still see the past when you look through it.");
		new Trinket("An avocado with an unusual amount of radiation coming from it.");
		new Trinket("An artichoke that has been exposed to a Hive summoning ritual. Glows green.");
		new Trinket("A clear rubber ball with a plastic toy inside.");
		new Trinket("A styrofoam head.");
		new Trinket("A stuffed cat.");
		new Trinket("A polished rock that has an uncanny resemblance to Jupiter.");
		new Trinket("A small green toy helmet with an orange visor.");
		new Trinket("A package of Zavala Snacks.");
		new Trinket(
				"A plastic name tag with a name that is not yours printed on it. The name tag is blue with a yellow asterisk in the corner.");
		new Trinket(
				"A small plastic circular device with a long-dead display screen and a cute pixelated animal on the case.");
		new Trinket("A red flower that never wilts.");
		new Trinket("A red, green, and white card with text from an ancient language inside.");
		new Trinket("A belt buckle that says �BAMF�.");
		new Trinket("An embroidered patch with a single red and white stripe and the text �N7� on it.");
		new Trinket("A container of exo polish.");
		new Trinket("Shredded remains of a Fallen house banner.");
		new Trinket("The Cryptarchy�s best guess for this item is �magic Vex tetris block�.");
		new Trinket("A soft-knit Ghost-sized hat.");
		new Trinket("Pearl showing signs of having been in the vacuum of space. Dated to be roughly 50,000 years old.");
		new Trinket("A tiny shard of the Traveler, cold.");
		new Trinket("An old page of sheet music. The only parts still legible read �HOPE FOR THE FUTURE�.");
		new Trinket("A broken controller for a video game console.");
		new Trinket("A small bone that seems to whisper to you if you hold it right next to your ear.");
		new Trinket("A conch shell.");
		new Trinket("An unfinished blueprint for a weapon.");
		new Trinket("A sticker book.");
		new Trinket("A bunny keychain.");
		new Trinket("A small green crystal that becomes darker the longer you look at it.");
		new Trinket("A shard from a sunbreaker�s hammer that is always hot to the touch.");
		new Trinket("An old Swiss army knife, still good.");
		new Trinket("A silver locket with only a bit of ash inside.");
		new Trinket("A plastic purple unicorn.");
		new Trinket("A stuffed white cat attached to a pair of plastic bongos.");
		new Trinket("A ring that hosts an unknown symbol.");
		new Trinket("A map of a world that seems to be on the back of a turtle.");
		new Trinket("Feathers from a yellow bird.");
		new Trinket("A tiny goat made out of straw, with a bronze bell tied around its neck.");
		new Trinket("A pamphlet for the Cult of Osiris.");
		new Trinket("A piece of metal with the number 1-1-7 on it.");
		new Trinket("An invitation to a lecture by Master Rahool.");
		new Trinket("An ancient recipe for so-called �buffalo� chicken wings.");
		new Trinket("A hat with the SRL logo on it.");
		new Trinket("A briefcase filled with black and white circular chips.");
		new Trinket("A blue collar for a domesticated animal, the name it once bore now illegible due to time.");
		new Trinket("A toy car with a bat-shaped symbol on it.");
		new Trinket("A hat made out of spinmetal.");
		new Trinket("A scarf that has pockets at the ends.");
		new Trinket("A mask from a previous Festival of the Lost.");
		new Trinket("A book about how to play a game called Dragons in Dungeons.");
	}

	String trinket;

	private Trinket(String trinket) {
		this.trinket = trinket;
		trinkets.add(this);
	}

	public Trinket(Trinket trinket) {
		this.trinket = trinket.trinket;
	}

	@Override
	public String toString() {
		return trinket;
	}

	@Override
	public String getName() {
		return "Trinket";
	}

	@Override
	public String getDescription() {
		return trinket;
	}

	public static Trinket randomTrinket() {
		return trinkets.get(new Random().nextInt(trinkets.size()));
	}

	public static ArrayList<Trinket> trinkets() {
		ArrayList<Trinket> trinkets = new ArrayList<Trinket>();
		trinkets.addAll(Trinket.trinkets);
		return trinkets;
	}

	@Override
	public String name() {
		return this.toString();
	}
}
