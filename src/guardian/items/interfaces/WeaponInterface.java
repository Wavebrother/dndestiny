package guardian.items.interfaces;

import java.util.ArrayList;

import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.DamageType;
import guardian.Die;
import guardian.items.WeaponProperties;

public interface WeaponInterface extends ItemInterface {
	default int[] getRange() {
		return new int[3];
	}

	default String getRangeInfo() {
		return "Range: " + this.getRange()[0] + ", " + this.getRange()[2];
	}

	default boolean isFinesse() {
		return getProperties().contains(WeaponProperties.FINESSE);
	}

	default boolean isRanged() {
		switch(getWeaponType()) {
		case SIMPLE_MELEE:
		case MARTIAL_MELEE:
			return false;
		case SIMPLE_RANGED:
		case MARTIAL_RANGED:
			return true;
		}
		return false;
	}

	default boolean isThrown() {
		return false;
	}

	DamageType getDamageType();

	WeaponType getWeaponType();

	Die getDamageDie();

	default int getDamageDieNumber() {
		return 1;
	}

	ArrayList<WeaponProperties> getProperties();

	default RangeBand getRangeBand() {
		return null;
	}
}
