package guardian.items.interfaces;

public interface EnumInterface {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static <T extends Enum> T fromString(Class<T> class1, String name) {
		return (T) Enum.valueOf((Class<T>) class1, name.replaceAll("\\s", "_").replace("-", "_")
				.replace("/", "_").replace(",", "").replace(".", "").replace("(", "").replace(")", "").toUpperCase());
	}
}
