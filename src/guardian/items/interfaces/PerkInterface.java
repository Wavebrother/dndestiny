package guardian.items.interfaces;

import java.util.LinkedHashMap;
import java.util.Map;

import guardian.BaseInterface;
import guardian.Main;

public interface PerkInterface extends BaseInterface {
	final static Map<String, PerkInterface> perks = new LinkedHashMap<String, PerkInterface>();

	public enum Perk implements PerkInterface {
		ACCELERATED_COILS, APPENDED_MAGAZINE, ARMOR_PIERCING_ROUNDS, ARMY_OF_ONE, BATTLE_RUNNER, BLACK_POWDER,
		BLINDING_GRENADES, BOX_BREATHING, CASCADE, CLOSE_AND_OR_PERSONAL, CLOWN_CARTRIDGE, CLUSTER_BOMBS,
		CONCUSSION_GRENADES, CROWD_CONTROL, ENHANCED_BATTERY, EXTENDED_MAGAZINE, EYE_OF_THE_STORM, FEATHERWEIGHT_BOLT,
		FIELD_SCOUT, FINAL_ROUND, FIREFLY, FLARED_MAGWELL, FOCUSED_FIRE, FULL_DRAW, GLASS_HALF_FULL,
		GRENADES_AND_HORSESHOES, GUERILLA_FIGHTER, HAMMER_FORGED, HARD_LAUNCH, HEADSEEKER, HEAVY_DUTY_COILS,
		HEAVY_PAYLOAD, HIDDEN_HAND, HIGH_CALIBER_ROUNDS, HIP_FIRE, HOT_SWAP, LAST_RESORT, LIFE_SUPPORT, LIGHTWEIGHT,
		LUCK_IN_THE_CHAMBER, MULLIGAN, OUTLAW, PERFECT_BALANCE, PERFECTIONIST, PERSISTENCE, RANGEFINDER,
		REACTIVE_RELOAD, REPLENISH, RESCUE_MAGAZINE, RICOCHET_ROUNDS, SECRET_ROUND, SHOOT_TO_LOOT, SHOT_PACKAGE,
		SMALLBORE, SNAPSHOT, SPRAY_AND_PLAY, STABILIZER_SIGHTS, SUPERCHARGER, SURROUNDED, TAKE_A_KNEE, THIRD_EYE,
		THREAT_DETECTOR_SIGHTS, TRACKING, TRIPLE_TAP, TRIPOD, UNDERDOG, UNFLINCHING, VOLATILE_LAUNCH, ZEN_MOMENT,

		THUNDERER, MAVERICK, MOMENTO_MORI, ARC_TRAPS, HIGH_VOLTAGE_NODES, COMPONDING_FORCE, DISRUPTION_BREAK,
		STRING_OF_CURSES, WHITE_NAIL, ONE_WAY, OR_ANOTHER, THE_FUNDAMENTALS, IONIC_RETURN, PRECISION_SWITCH,
		AUTOMATIC_SWITCH, OLD_FASHIONED_RECIPE, SPREAD_SHOT_PACKAGE, FLUTED_BARREL, FULL_BORE, FOUR_HEADED_DOG,
		PRECISION_SLUG, THE_ROADBORN, COLD_FUSION, LONGEST_WINTER, CRUEL_REMEDY, PERSONAL_ASSISTANT, TARGET_ACQUIRED,
		NAPALM, SHOCK_ROUNDS, FRONT_LINES, THIN_THE_HERD, DELAYED_GRATIFICATION, WOLFPACK_ROUNDS, BLACK_HOLE, COSMOLOGY,
		ARC_CORE, SOLAR_CORE, VOID_CORE, VOLATILE_LIGHT, HOLDING_ACES, BLINDING_LIGHT, THE_HEREAFTER, RAMPAGE,
		RIDE_THE_BULL, NO_BACKPACK, ICE_BREAKER, INVECTIVE, HONED_EDGE, THE_FATE_OF_ALL_FOOLS, CHARGE_SHOT,
		SHIELD_DISORIENT, MODULAR_DESIGN, RPM_450, RPM_900, POISON_ARROWS, SHOCK_BLAST, LONG_MARCH, HOWL_OF_THE_PACK,
		RELEASE_THE_WOLVES, EXPLOSIVE_SHADOW, CONSERVE_MOMENTUM, MULTIMODE, MIDA_MULTI_TOOL, MONTE_CARLO_METHOD,
		CURSEBRINGER, EXTINCTION_CYCLE, STRANGE_GRAVITY, THE_MASTER, REWIND_AGAIN, THE_CORRUPTION_SPREADS, VIRULENCE,
		PATIENCE_AND_TIME, PLAN_C, POCKET_INFINITY, THE_PERFECT_FIFTH, FLAME_REFRACTION, PRISMATIC_INFERNO, VERMIN,
		RAT_PACK, RED_DEATH, ARC_CONDUCTOR, SUPERCONDUCTOR, FAN_FIRE, LAST_WORD, HANNIBAL, SLUG_RIFLE, DORNR�SCHEN,
		THE_HOUSE_ALWAYS_WINS, COUNTING_CARDS, ACCOMPLICE, STORM_AND_STRESS, SUNBURN, SUN_BLAST, SUPER_GOOD_ADVICE,
		SPINNING_UP, DUAL_SPEED_RECEIVER, SUROS_LEGACY, PAYDAY, BUSINESS_TIME, HARBINGERS_PULSE,
		INSECTOID_ROBOT_GRENADES, SPICY_BOYS, SERVE_THE_COLONY, DEAD_EYE, FIRST_CURSE, EXCAVATION,
		FULL_AUTO_TRIGGER_SYSTEM, COMBAT_SIGHTS, MARKSMAN_SIGHTS, MAD_SCIENTIST, MECHANIZED_AUTOLOADER,
		MARK_OF_THE_DEVOURER, OVERFLOW, TOUCH_OF_MALICE, TOUCH_OF_MERCY, REPULSOR_FORCE, THE_SCIENTIFIC_METHOD,
		PARTICLE_REPEATER, BE_THE_DANGER, UNREPENTANT, SPLIT_ELECTRON, LIGHTNING_ROD, PROTOTYPE_TRUESEEKER, TWINTAILS,
		PLAY_WITH_YOUR_PREY, UNIVERSAL_REMOTE, TIMELESS_MYTHOCLAST, HARSH_TRUTHS, LAST_STAND, HARMONIC_LASER,
		SUPERCHARGED_BATTERY, QUEENS_WRATH, COMPOUND_DRAW, DYNAMITE, WITH_A_LASER_BEAM, ZHALO_SUPERCELL,
		BOLTS_FROM_THE_BLUE;
		static {
//			StringBuilder sb = new StringBuilder();
			for (Perk perk : values()) {
				perks.put(perk.toString(), perk);
//				if (perk.description != null) {
//					sb.append("case " + perk.name() + ":\n");
//					sb.append("\treturn " + perk.getCost() + ";\n");
//				}
			}
//			System.out.println(sb.toString());
		}

		@Override
		public String toString() {
			switch (this) {
			case ARMOR_PIERCING_ROUNDS:
				return "Armor-Piercing Rounds";
			case CLOSE_AND_OR_PERSONAL:
				return "Close and/or Personal";
			case HIGH_CALIBER_ROUNDS:
				return "High-Caliber Rounds";
			case HIGH_VOLTAGE_NODES:
				return "High-Voltage Nodes";
			case OLD_FASHIONED_RECIPE:
				return "Old-Fashioned Recipe";
			case FOUR_HEADED_DOG:
				return "Four-Headed Dog";
			case RPM_450:
				return "450 RPM";
			case RPM_900:
				return "900 RPM";
			case MIDA_MULTI_TOOL:
				return "MIDA Multi-Tool";
			case SUROS_LEGACY:
				return "SUROS Legacy";
			case HARBINGERS_PULSE:
				return "Harbinger's Pulse";
			case QUEENS_WRATH:
				return "Queen's Wrath";
			default:
				return Main.enumString(name());
			}
		}

		@Override
		public String getDescription() {
			switch (this) {
			case ACCELERATED_COILS:
				return "The damage of this weapon becomes 1d10, and it loses the Loading property.";
			case APPENDED_MAGAZINE:
				return "This weapon gains an additional 4 shots in its maximum shot capacity, and gains the Bulky property.";
			case ARMOR_PIERCING_ROUNDS:
				return "The rounds from this weapon can piece most barriers, but is blocked by 1 inch of common metal, 2 inches of stone, half an inch of relic iron, or 3 feet of wood or dirt. These rounds can also damage all creatures in a line up to this weapon�s effective range. In order to do this, your attack must have killed the first target, and your attack roll must beat the AC of the next subsequent target.";
			case ARMY_OF_ONE:
				return "Every time you hit a hostile creature with this weapon, you gain 1 point of Reserves, to a maximum of 10. Once on your turn, you can exchange 3 points of Reserves to gain 1 melee ability charge, or 5 points of reserves to gain 1 grenade ability charge. You lose all Reserves if you go 1 minute without hitting a hostile creature.";
			case BATTLE_RUNNER:
				return "If you kill a creature with this weapon, you gain +10 movement until end of your next turn.";
			case BLACK_POWDER:
				return "This weapon's payload DC increases by 1.";
			case BLINDING_GRENADES:
				return "Creatures hit with a shot from this weapon, or who fail the payload saving throw of this weapon, must make a Wisdom saving throw, becoming blinded until the end of their next turn on a failed save.";
			case BOX_BREATHING:
				return "This weapon gains the Loading property, and a bonus +2 on attack and damage rolls.";
			case CASCADE:
				return "If you kill a creature with a melee attack and you are already wielding this weapon, you can reload it as a free action. This overcomes the bulky property.";
			case CLOSE_AND_OR_PERSONAL:
				return "If you hit a hostile creature with this weapon, you can add an additional damage die to the next melee attack you make before the end of your next turn.";
			case CLOWN_CARTRIDGE:
				return "When you reload this weapon, you can roll a d6. On 5-6, you gain a temporary +1 to this weapon�s maximum shot capacity. This bonus is lost if you spend the shot or if you reload this weapon and do not roll 5-6.";
			case CLUSTER_BOMBS:
				return "This weapon gains an additional damage die.";
			case CONCUSSION_GRENADES:
				return "Creatures hit with a shot from this weapon, or who fail the payload saving throw of this weapon, must make a Constitution saving throw. On a failed save, if the creature makes an attack on their next turn, that attack has disadvantage.";
			case CROWD_CONTROL:
				return "When you kill a hostile creature with this weapon, this weapon gains a bonus +2 to its damage rolls until you reload it, or you go one round without attacking a hostile creature with this weapon.";
			case ENHANCED_BATTERY:
				return "The damage die size of this weapon increases by one, and all of its ranges are increased by 10 feet.";
			case EXTENDED_MAGAZINE:
				return "This weapon gains a bonus +2 to its maximum shot capacity.";
			case EYE_OF_THE_STORM:
				return "If you are holding this weapon in your hands, and your energy shield capacity is less than your maximum shield capacity, you have a bonus +1 to attack rolls you make with this weapon. If your energy shields fall below half of their maximum, this increases to +2. If your energy shields are at 0, this increases to +3.";
			case FEATHERWEIGHT_BOLT:
				return "This weapon has the Automatic Fire property.";
			case FIELD_SCOUT:
				return "When you make an attack with this weapon and there are no allies within 30 feet of you that you are aware of, you gain a bonus +1 to attack and damage rolls with this weapon.";
			case FINAL_ROUND:
				return "If you make an attack with the last shot in this weapon�s magazine and your attack hits, it is considered a critical hit. This weapon requires a bonus action or action to reload. This does not supercede the bulky property.";
			case FIREFLY:
				return "If you kill a hostile creature while Aiming with this weapon, they explode. All other creatures within 5 feet of them must make a Dexterity saving throw, taking 2d6 energy damage on a failed, or half as much on a success.";
			case FLARED_MAGWELL:
				return "This perk removes the bulky property from this weapon, at the cost of a -1 penalty to attack rolls made with it.";
			case FOCUSED_FIRE:
				return "The range band of this weapon is increased, and the effective range of this weapon is reduced by 10 feet. While aiming, the damage die size of this weapon increases by one.";
			case FULL_DRAW:
				return "The damage die size of this weapon is increased by one, its range band is increased, and it gains the Loading property. If this weapon fires an arrow with a separate damage, that damage does not change.";
			case GLASS_HALF_FULL:
				return "While this weapon�s current shot capacity is equal to or less than half of its total shot capacity, the damage die size is increased by one.";
			case GRENADES_AND_HORSESHOES:
				return "The rounds fired from this weapon are equipped with scanners to a range of 5 feet. If, along the path of this weapon�s attack, the scanners detect a creature within 5 feet of it, you can choose to detonate the round from this weapon early.";
			case GUERILLA_FIGHTER:
				return "When firing this weapon from behind any incomplete cover, it costs no movement to begin Aiming.";
			case HAMMER_FORGED:
				return "All ranges of this weapon are increased by 5 feet.";
			case HARD_LAUNCH:
				return "The damage die of this weapon increases by one, its maximum shot capacity is reduced by 1, and it gains the bulky property.";
			case HEADSEEKER:
				return "While aiming, this weapon's damage becomes 1d10.";
			case HEAVY_DUTY_COILS:
				return "This weapon ignores damage soak values.";
			case HEAVY_PAYLOAD:
				return "The damage die size of this weapon is increased by one.";
			case HIDDEN_HAND:
				return "This weapon has a bonus +1 to attack rolls while you are Aiming.";
			case HIGH_CALIBER_ROUNDS:
				return "This weapon ignores damage soak values.";
			case HIP_FIRE:
				return "This weapon has a bonus +1 to attack rolls made with it while you are not Aiming.";
			case HOT_SWAP:
				return "It becomes a bonus action to switch to this weapon. Switching to this weapon grants a bonus +2 to hit on the next attack you make with it before end of your next turn.";
			case LAST_RESORT:
				return "If neither you nor your Ghost are carrying any other weapons, and neither of you have any spare magazines for this weapon, but there is at least one shot left in its shot capacity, you gain a bonus +5 to attack and damage rolls with this weapon.";
			case LIFE_SUPPORT:
				return "Once on your turn, if you kill a hostile creature with this weapon, you can make a shield recharge roll. You must attune to this weapon to gain this benefit. ";
			case LIGHTWEIGHT:
				return "While holding this weapon in your hands, you gain a bonus 5 feet of movement speed.";
			case LUCK_IN_THE_CHAMBER:
				return "When you make you attack with this weapon, add a special d6 to your attack roll called a Lucky d6. If this Lucky d6 lands on 6, you add a bonus +6 to your damage roll if you hit with your attack. Hit or miss, once this Lucky d6 is used, you remove it from your attack rolls until you reload this weapon. On your final shot in your weapon's shot capacity, if you still have a Lucky d6 remaining, you consider its roll to automatically be a 6.";
			case MULLIGAN:
				return "If you miss with an attack with this weapon, you can roll d6. On 5-6, you regain the shot you spent on your missed attack directly to this weapon�s current shot capacity, and perks that are normally interrupted by missing an attack are not interrupted.";
			case OUTLAW:
				return "If you kill a creature while Aiming with this weapon, you can reload it as a free action. This overcomes the bulky property.";
			case PERFECT_BALANCE:
				return "You have a bonus +1 to hit with this weapon.";
			case PERFECTIONIST:
				return "During an encounter, if you hit a hostile creature with every shot in this weapon�s shot capacity, you can reload this weapon as a free action. This overcomes the bulky property.";
			case PERSISTENCE:
				return "This weapon gains the bulky property. When you make an attack with this weapon, you can add a bonus +1 to the attack roll of your next attack with this weapon. The effect of this perk can stack with itself, but is lost if you take any other action, reaction, or bonus action to do anything other than Attack with this weapon. You must be attuned to this weapon to gain this benefit.";
			case RANGEFINDER:
				return "All ranges of this weapon increase by 10 feet.";
			case REACTIVE_RELOAD:
				return "Reloading this weapon grants a bonus +2 damage on the next attack you make with it. You must make this attack before the end of your next turn. This weapons requires a bonus action or action to reload. This does not supersede the bulky property.";
			case REPLENISH:
				return "Casting a grenade or super ability while holding this weapon in your hands refills up to half this weapon�s shot capacity. You must be attuned to this weapon in order to gain this benefit.";
			case RESCUE_MAGAZINE:
				return "When your energy shields drop to 0, your Ghost can use its reaction to reload this weapon from a stored magazine. Your Ghost can do this even from within its pocket backpack, but it must be attuned to this weapon in order to do so.";
			case RICOCHET_ROUNDS:
				return "When you make an attack with this weapon and miss, roll a d6. On 5-6, you can choose one other creature within 20 feet to attack instead, using the same attack roll for your first target. If your attack roll would hit the chosen creature, roll damage as normal. Only weapons without the Payload property can benefit from this perk.";
			case SECRET_ROUND:
				return "This weapon gains a bonus +1 to its damage rolls.";
			case SHOOT_TO_LOOT:
				return "If you hit a small item with a shot from this weapon, your Ghost can use its reaction to transmat that item into its memory using the smart rounds in this weapon. Your Ghost can do this even from within its pocket backpack, but it must be attuned to this weapon in order to do so. It can only transfer one item per shot, and the item must weight no more than 7 lbs. This weapon requires special ammunition that costs an additional 500 glimmer to purchase per magazine.";
			case SHOT_PACKAGE:
				return "All ranges of this weapon increase by 5 feet, it gains a bonus +1 to hit, and -1 to its maximum shot capacity.";
			case SMALLBORE:
				return "This weapon has a bonus +2 to hit, -2 to its maximum shot capacity, and gains the bulky property.";
			case SNAPSHOT:
				return "It costs 10 less feet of movement to begin Aiming with this weapon.";
			case SPRAY_AND_PLAY:
				return "When this weapon�s shot capacity is at 0, you can reload it as a bonus action. This overcomes the bulky property.";
			case STABILIZER_SIGHTS:
				return "Your movement speed is not reduced while Aiming with this weapon.";
			case SUPERCHARGER:
				return "This weapon has the automatic fire property.";
			case SURROUNDED:
				return "You have a bonus +2 to the damage rolls you make with this weapon when there are two or more hostile creatures within 5 feet of you.";
			case TAKE_A_KNEE:
				return "If you voluntarily go prone while wielding this weapon, you do not have disadvantage when attacking with this weapon, Aiming with it costs no movement, and you have a bonus +2 to hit.";
			case THIRD_EYE:
				return "This perk negates the effect of creatures within 10 feet of you having advantage on their attacks against you while you are Aiming.";
			case THREAT_DETECTOR_SIGHTS:
				return "When Aiming with this weapon, you gain bonus +5 on Wisdom (Perception) rolls to find hidden creatures that are not under the effects of active camouflage. ";
			case TRACKING:
				return "Creatures that attempt to use reactions or other movement abilities to dodge out of the way of an attack from this weapon, but move 10 feet or less and do not end up behind full cover, are still hit as the target of your attack.";
			case TRIPLE_TAP:
				return "If you hit with three shots from this weapon without reloading between shots, on the third shot, one shot is refunded to your shot capacity and you can immediately take a bonus action to make an attack with this weapon. On combat bows, an arrow is not refunded, but you can still make a bonus action attack if you have an arrow to attack with.";
			case TRIPOD:
				return "A rocket launcher with this perk has a shot capacity of 3.";
			case UNDERDOG:
				return "If two or more hostile creatures have attempted to attack you before the start of your turn, you gain a bonus +1 to attack and damage rolls with this weapon.";
			case UNFLINCHING:
				return "If you are Aiming with this weapon and are hit with an attack, you can use your reaction to negates the effect of creatures within 10 feet of your having advantage on attacks while you are Aiming. You also grant yourself a bonus +1 to AC until the start of your next turn when you do this.";
			case VOLATILE_LAUNCH:
				return "This weapon gains the Payload property, but with a diameter of 5 feet instead of radius.";
			case ZEN_MOMENT:
				return "Every time you hit a hostile creature with a shot from this weapon, you gain a bonus +1 to hit on your next attack. You must make your next attack before the end of your next turn. This bonus can stack to a maximum of +3 to hit, but if you do not hit a hostile creature on your turn, this bonus is lost.";
			case THUNDERER:
				return "This weapon has a bonus +1 to ranged attack rolls made with it. When you take the Attack action with this weapon, you can take one shot with it for every shot left in this weapon's current shot capacity.";
			case MAVERICK:
				return "This perk negates the effect of creatures within 10 feet of your having advantage on attacks while you are Aiming. If you hit a hostile creature while Aiming, your shot is refunded.";
			case MOMENTO_MORI:
				return "You can channel your Golden Gun super ability into this weapon. When you do, your Golden Gun gains a bonus +3 to attack and damage rolls, as well as the benefits of Firefly.";
			case ARC_TRAPS:
				return "When you attack with a shot from this weapon, choose a spot within 30 feet of you. The shot from this weapon lands and stays there for up to 1 minute, creating an arc node. If an arc node senses another arc node within 10 feet of it, the two nodes will immediately link and create a line of arc energy between them. Creatures who start their turn on this line, or who attempt to pass through this line for the first time on their turn, must make a Dexterity saving throw, taking 2d6 arc damage on a failed save or half as much on a success. Each arc node can connect to up to 3 other arc nodes of your choice. If you reload this weapon, or if more than a minute passes since two nodes were last connected to each other, all currently active arc nodes controlled by this weapon dissipate.";
			case HIGH_VOLTAGE_NODES:
				return "The damage from this weapon's arc nodes ignores damage soak values.";
			case COMPONDING_FORCE:
				return "This weapon deals an additional 2d6 damage to energy shields. This damage is not affected by any weaknesses, resistances, or immunities that the energy shield may grant.";
			case DISRUPTION_BREAK:
				return "If the damage from this weapon reduces a creature's energy shields to 0, they become weak to kinetic damage. If the creature has a natural resistance to kinetic damage, it loses that resistance instead. If the creature has a natural immunity to kinetic damage, it resists kinetic damage instead. This effect lasts until the end of your next turn.";
			case STRING_OF_CURSES:
				return "If you hit a creature with a shot from this weapon, you gain one Curse, up to a max of 10. On your turn, you can spend a number of curses, indicated in parentheses, to perform one of the following. You lose your Curses if you remove your attunement to this weapon, or if you are no longer carrying it. You cannot be attuned to any other weapon while attuned to this one.\r\n"
						+ "\tFetter (Up to 4). Choose a creature you can see. That creature has a bonus to its attack rolls against you equal to the amount of Curses you spend on this, and you gain a bonus to your AC equal to the amount of curses you spend on this. This effect lasts until the start of your next turn.\r\n"
						+ "\tSpite (4 Curses): A target that you hit with a shot from Bad Juju has disadvantage on its next attack.\r\n"
						+ "\tDebilitate (4 Curses). Both you and the target that you hit have to make an Intelligence saving throw (DC 16), rolling once on the Madness table on a failed save.\r\n"
						+ "\tSunder (6 Curses). The target must make a Charisma saving throw. On a failed save, their AC is reduced by 1 for the next hour. On a success, you roll once on the Madness table.\r\n"
						+ "\tEgotism (10 Curses). Make an Intelligence saving throw (DC 16). On a success, you regain 1 super ability charge. On a failed save, you take 4d6 psychic damage and cannot cast your super ability for 24 hours.";
			case WHITE_NAIL:
				return "If you hit a hostile creature with every shot in this weapon�s shot capacity while Aiming, this weapon's shot capacity is refunded.";
			case ONE_WAY:
				return "Movement speed isn't reduced while Aiming with this weapon. When you hit a hostile creature while Aiming with this weapon, you gain a bonus +10 movement speed that lasts until the end of your next turn, and you can use a bonus action to Disengage. This effect cannot stack with itself.";
			case OR_ANOTHER:
				return "You gain a bonus +1 AC until the end of your next turn when you hit a creature without Aiming with this weapon.";
			case THE_FUNDAMENTALS:
				return "As a bonus action, you may switch the damage this weapon deals between arc, solar, and void. ";
			case IONIC_RETURN:
				return "If you hit a creature protected by energy shields with this weapon, you can refund your shot to this weapon's shot capacity.";
			case PRECISION_SWITCH:
				return "Ranged firearm weapon attacks with this weapon have a bonus +1 to damage rolls, and its damage die is increased by one while Aiming. While Aiming, all ranges on this weapon increase by 10 feet.";
			case AUTOMATIC_SWITCH:
				return "This weapon gains a bonus +1 to attack rolls, its range band is reduced by one, and it gains the Automatic Fire property. ";
			case OLD_FASHIONED_RECIPE:
				return "As a bonus action, you can switch between the Precision Switch and Automatic Switch perks on this weapon.";
			case SPREAD_SHOT_PACKAGE:
				return "While Aiming with this weapon, it gains an additional damage die.";
			case FLUTED_BARREL:
				return "It is a bonus action or action to reload this weapon. This overcomes the bulky property.";
			case FULL_BORE:
				return "Before you make an ranged firearm weapon attack with this weapon, you can choose to go full bore and take a -5 penalty to the attack roll. If you hit, you gain a bonus +10 to the attack's damage.";
			case FOUR_HEADED_DOG:
				return "When you attack with this weapon without Aiming, you make a single attack that affects all creatures in a 15-foot cone instead of making a normal attack. For every target you hit, you make a separate damage roll. This only consumes one shot from this weapon's shot capacity regardless of how many targets you hit.";
			case PRECISION_SLUG:
				return "This weapon fires a single slug. While Aiming with this weapon, you have a bonus +1 to attack rolls you make with it.";
			case THE_ROADBORN:
				return "Hitting a creature while Aiming with this weapon grants a bonus +1 damage to your attack.";
			case COLD_FUSION:
				return "Creatures hit by this weapon have to make a Constitution saving throw (DC = 8 + your proficiency bonus + your Strength or Dexterity modifier). On a failed save, their movement speed is halved until the end of their next turn.";
			case LONGEST_WINTER:
				return "When you make an attack with this weapon, you can add a bonus +1 to the damage of your next attack with this weapon. The effects of this perk can stack with itself, but are lost if you reload this weapon, or take any other action, reaction, or bonus action to do anything other than Attack with this weapon.";
			case CRUEL_REMEDY:
				return "If you kill a creature while Aiming with this weapon, this weapon's shot capacity is completely refunded. Killing a creature with this weapon also allows you to make a shield recharge roll. If you are not at maximum health points, the amount rolled from your recharge applies to the health first, and the remainder goes to your shields.";
			case PERSONAL_ASSISTANT:
				return "When Aiming this weapon, you can see the current hit points of the creature you are aiming at, estimated to the nearest 10, displayed on this weapon's scope.";
			case TARGET_ACQUIRED:
				return "This weapon has a bonus +1 to attack and damage rolls against creatures who have less than their maximum hit points remaining.";
			case NAPALM:
				return "The round from this weapon is loaded with napalm that ignites on detonation, covering the ground within 5 feet of the target in blazing flames. For the next 3 rounds, creatures that start or end their turn within the flames, or who attempt to pass through them, must make a Constitution saving throw (DC = 8 + your proficiency bonus + your Strength modifier), taking 1d6 solar damage on a failed save, or half as much on a success.";
			case SHOCK_ROUNDS:
				return "The rounds in this weapon have enhanced target acquisition, granting this weapon the effects of the Tracking perk, as well as a bonus +1 to hit.";
			case FRONT_LINES:
				return "When 2 or more hostile creatures are within 10 feet of you, this weapon's damage die increases by one, and it gains a bonus +2 to ranged attack rolls made with it. If you kill a hostile creature with this weapon, up to half of this weapon's maximum shot capacity is refunded to its current shot capacity.";
			case THIN_THE_HERD:
				return "The damage from this weapon overcomes the resistances and immunities granted by a creature's energy shields. Killing a creature with a shot from this weapon grants a bonus +2 to damage with shots from this weapon until the end of your next turn.";
			case DELAYED_GRATIFICATION:
				return "As an action, you can shoot this weapon at a spot you can see within range and ready an action to detonate the grenade whenever a creature of your choosing moves within 5 feet of it. At the start of your next turn, the grenade detonates automatically if it has not already. With a successful DC 12 Dexterity check, you can bounce this grenade up to 10 feet around corners and will be notified by the primitive systems linked between the grenade and this weapon when a creature approaches within 5 feet of it.";
			case WOLFPACK_ROUNDS:
				return "On hit, the rounds from this weapon burst into trackers that seek out up to 3 creatures of your choosing within 5 feet of the target. These creatures must make the same payload saving throw, taking an additional 1d10 explosive damage on a failed save, or half as much on a success. You can choose the same creature multiple times, including the initial target.";
			case BLACK_HOLE:
				return "This weapon's maximum range increases by 300 feet. This weapon's effective and extended ranges are not altered by the Aiming condition.";
			case COSMOLOGY:
				return "If you kill a hostile creature with this weapon, the round explodes. All creatures within 5 feet of them must make a Dexterity saving throw (DC = 8 + your proficiency bonus + your Strength or Dexterity modifier), taking 2d6 void damage on a failed, or half as much on a success.";
			case ARC_CORE:
				return "This weapon deals arc damage.";
			case SOLAR_CORE:
				return "This weapon deals solar damage.";
			case VOID_CORE:
				return "This weapon deals void damage.";
			case VOLATILE_LIGHT:
				return "This weapon has the following features: \r\n"
						+ "\tWhen you make an attack with this weapon, you briefly shed bright light in a 5-foot radius, and dim light 10 feet beyond that.\r\n"
						+ "\tThis weapon can always make an attack up to its maximum range. When Aiming with this weapon, you can make an attack up to the weapon�s extended range without disadvantage, but the extended range does not become equal to the maximum range of the weapon. Attacking between the extended and maximum ranges grants disadvantage, even while Aiming.\r\n"
						+ "\tThe rounds from this weapon ricochet unpredictably, without regard for allies or enemies. When you make an attack with this weapon, roll a d6. On a 1, the rounds manage to ricochet back to you, dealing you 1d4 energy damage. On 3-4, it ricochets to another target within 30 feet of the original target if one exists, dealing that target 1d4 energy damage.";
			case HOLDING_ACES:
				return "This weapon rolls three Lucky d6s with its attack roll instead of one. Only one Lucky d6 can be applied (or lost on a miss) at a time. The amount of shots at the end of the weapon's shot capacity that automatically roll a 6 on one Lucky d6 is equal to the number of Lucky d6s remaining.";
			case BLINDING_LIGHT:
				return "If you hit a creature whiling Aiming with this weapon, the smart round you fired detonates, unleashing a bright, blinding light on your target. All creatures within 10 feet of the target must make a Wisdom saving throw. On a failed save, they are blinded until the end of their next turn. ";
			case THE_HEREAFTER:
				return "If you voluntarily go prone while wielding this weapon, you do not have disadvantage when attacking with this weapon, Aiming with it costs no movement, and you have a bonus +2 to hit. Additionally, if you are Aiming with this weapon and are hit with an attack, you can use your reaction to negates the effect of creatures within 10 feet of your having advantage on attacks while you are Aiming. You also grant yourself a bonus +1 to AC until the start of your next turn when you do this.";
			case RAMPAGE:
				return "Killing a hostile creature with this weapon adds a bonus +1 to all attacks with this weapon until the end of your next turn. This effect can stack up to 3 times, and can be persisted by hitting a hostile creature with an attack. You lose this effect if you miss with an attack or reload this weapon.";
			case RIDE_THE_BULL:
				return "For every attack you make with this weapon, your next attack with this weapon gains a bonus 1d4 damage at the cost of a -1 penalty to its attack roll. This bonus damage, and penalty effect, can stack on itself up to 3 times. You must make your next attack before the end of your next turn or the effect of this perk resets. This perk also resets if you reload this weapon.";
			case NO_BACKPACK:
				return "This weapon fires from a rechargeable battery instead of a magazine and thus cannot be reloaded. But if you do not attack with this weapon on your turn, it regains up to 2 shots in its battery.";
			case ICE_BREAKER:
				return "If you hit a creature, you can roll a d6. On 5-6 the bullet explodes on the creature. All creatures within 5 feet, including the target, must make a Dexterity saving throw. They take 2d6 solar damage on a failed save, or half as much on a success. The target makes their saving throw with disadvantage.";
			case INVECTIVE:
				return "If you do not make an attack with this weapon on your turn, 1d4 shots are refunded to this weapon�s magazine.";
			case HONED_EDGE:
				return "This weapon produces its ammo on a shot-by-shot basis, using glimmer loaded in the magazine rather than bullets. As a bonus action before you make your attack, you can switch this weapon�s firing mode to make one powerful attack, dealing 1d12 damage per shot left in this weapon's magazine. This consumes all remaining shots in the magazine as well.";
			case THE_FATE_OF_ALL_FOOLS:
				return "For every shot you take with this weapon while not Aiming, you gain a bonus +1 damage to the next attack you make with this weapon while Aiming. Hitting a creature while Aiming refunds one shot to this weapon's shot capacity and resets the effect of this perk. Reloading, switching weapons, or going more than two rounds without making an attack with this weapon also resets the effect of this perk.";
			case CHARGE_SHOT:
				return "This weapon gains the Tracking perk. If you hit a creature with this weapon, it deals damage and the target must make a Constitution saving throw. On a failed save, they begin burning for 1 minute. Creatures can repeat the saving throw at the end of their turn, ending the effect early on a success. Creatures who succeed on their saving throw, or for whom the effect ends, become immune to Burning for 24 hours.";
			case SHIELD_DISORIENT:
				return "This weapon overcomes the resistances and immunities a creature's energy shields grant it. If you hit a hostile creature that has energy shields with a shot from this weapon, this weapon deals normal damage, but all other hostile creatures within 5 feet of the target, including the target, must make a Wisdom saving throw (DC = 8 + your proficiency bonus + your Strength or Dexterity modifier). On a failed save, they become Blinded until the end of their next turn.";
			case MODULAR_DESIGN:
				return "Modular Design. As an action, you can switch this weapon between the following modes:\r\n"
						+ "Automatic. This weapon gains the damage, range, range band, and properties of an auto rifle while this perk is active.\r\n"
						+ "Burst Fire. This weapon gains the damage, range, range band, and properties of a pulse rifle while this perk is active.\r\n"
						+ "Semi-Auto. This weapon gains the damage, range, range band, and properties of a scout rifle while this perk is active.";
			case RPM_450:
				return "The damage die size of this weapon increases by one";
			case RPM_900:
				return "You have a bonus +1 to attack rolls you make with this weapon.";
			case POISON_ARROWS:
				return "Arrows fired from this weapon become imbued with poison. If you hit a creature with an arrow fired from this weapon, that creature must make a Constitution saving throw, becoming Poisoned for 1 minute on a failed save. Creatures can repeat the saving throw at the end of their turn, ending the effect early on a success. Creatures who succeed on their saving throw, or for whom the effect ends, become immune to being Poisoned for 24 hours.";
			case SHOCK_BLAST:
				return "This weapon has the Energy Projectiles property. Creatures hit by an attack with this weapon must make a Constitution saving throw. They have disadvantage on the next attack roll, skill check, or saving throw they make within the next minute on a failed save.";
			case LONG_MARCH:
				return "This weapon has scanners that can link with the computer in your armor. While attuned to this weapon, you are aware of all creatures within 10 feet of you, and you do not have disadvantage on attack rolls against invisible creatures.";
			case HOWL_OF_THE_PACK:
				return "If you kill a hostile creature with this weapon, until the start of your next turn, you and any Risen of your choice who end their turn within 15 feet of you can make a shield recharge roll once on their turn.";
			case RELEASE_THE_WOLVES:
				return "Killing a hostile creature with a shot from this weapon immediately grants this weapon the Full Auto property until the end of your next turn.";
			case EXPLOSIVE_SHADOW:
				return "When you hit the flesh of a creature with a shot from this weapon, the bullet, which contains trace amount of Darkness, becomes imbedded in the target. Creatures can remove embedded bullets during a short or long rest without interrupting the rest. If you hit a creature with at least four Malfeasance bullets already imbedded in them, the Darkness in the bullets congeals and erupts, causing the creature to make a Light ability saving throw (or a Constitution saving throw if they don�t have Light abilities). On a failed save they take 4d8 Darkness damage, which has the Light Sap (3) property, and any currently embedded bullets no longer count toward activating the effect of this perk.";
			case CONSERVE_MOMENTUM:
				return "If you hit a creature but do not kill it, you can make one additional attack with this weapon when you take your next Attack action with it. The effect of this perk can stack with itself to a maximum of 5 attacks, but is lost if you kill a creature with this weapon or do not make an attack with it before the end of your next turn.";
			case MULTIMODE:
				return "Multimode. Choose one of the following perks for this weapon to gain. You can change your chosen perk as a bonus action on your turn.\r\n"
						+ "Field Scout\r\n" + "Threat Detector Sights\r\n" + "Shoot to Loot\r\n"
						+ "Relentless Tracker\r\n" + "Rangefinder\r\n" + "Snapshot";
			case MIDA_MULTI_TOOL:
				return "While holding this weapon in your hands, you gain a bonus 5 feet of movement speed. You can use your bonus action to make an attack with this weapon on each of your turns, if you are already holding this weapon in your hands when you start your turn.";
			case MONTE_CARLO_METHOD:
				return "If you damage a hostile creature with a shot from this weapon, you can roll a d6. On 5-6, you regain 1 melee ability charge.";
			case CURSEBRINGER:
				return "If you kill a creature with this weapon, that creature explodes and all other targets within 5 feet must make a Constitution saving throw. They take 2d4 Darkness damage on a failed save, or half as much on a success.";
			case EXTINCTION_CYCLE:
				return "You can use your action to make three attacks with this weapon instead of one. The first time you do this, this perk functions like normal. Every subsequent time you do this, you lose one attack in this perk's effect until you reach the minimum of 1 attack per Attack action. Reloading this weapon resets the effect of this perk.";
			case STRANGE_GRAVITY:
				return "When you make an attack with this weapon, you can add a bonus +1 to the attack and damage rolls of this weapon, and the effective range of this weapon is increased by 5 feet for your next attack with it. The effect of this perk can stack with itself, but is lost if you take any other action, reaction, or bonus action to do anything other than Attack with this weapon.";
			case THE_MASTER:
				return "If you hit a hostile creature while Aiming with this weapon, you gain an additional +1 to attack and damage rolls with this weapon until the end of your next turn. This can stack up to 3 times. The effect of this perk is lost if you reload this weapon, stop Aiming, or do not attack a hostile creature on your turn.";
			case REWIND_AGAIN:
				return "If you hit a target while Aiming with this weapon, refund your shot to your shot capacity. If you are holding this weapon in your hands and a creature hits you with an attack, you can use your reaction to refund the creature's shot, but also cause them to miss. You cannot attack with this weapon again for 1d12 hours when you do this.";
			case THE_CORRUPTION_SPREADS:
				return "If you hit a creature with this weapon, that creature gains one level of SIVA infestation for 1 minute. If three levels accumulate during that time, the creature becomes SIVA-corrupted and the rounds from this weapon activate, tearing away matter and flesh to form SIVA nanites. The nanites attack one creature of your choosing within 10 feet of the corrupted creature for 2d4 necrotic damage. Every shot from this weapon that hits a SIVA-corrupted creatures spawns more SIVA nanites, and every successful attack against a SIVA-corrupted creature is considered a critical hit. Corrupted creatures can make a Constitution saving throw at the start of their turn (DC = 8 + your proficiency bonus + your Strength or Dexterity modifier), ending the corruption early on a success. Creatures for whom the corruption ends, or who do not gain three levels of infestation in 1 minute, become immune to SIVA-corruption for 24 hours.";
			case VIRULENCE:
				return "Killing a SIVA-corrupted creature unleashes a swarm of SIVA nanites that are considered friendly to you and your allies. The swarm lasts for 1 minute.";
			case PATIENCE_AND_TIME:
				return "When you begin Aiming with this weapon, you gain active camouflage until the end of your next turn. On each of your turns after, if you continue to Aim, you can use a bonus action to grant yourself active camouflage until the end of your next turn.";
			case PLAN_C:
				return "It becomes a bonus action to switch to this weapon. Switching to this weapon grants a bonus +2 to attack rolls you make with it until end of your turn. On your turn, if you switch to this weapon and take the Attack action, you ignore the loading property of this weapon until the end of your turn.";
			case POCKET_INFINITY:
				return "This weapon's Mulligan perk is successful on a roll of 4-6 instead of 5-6. You have a bonus +1 to attack and damage rolls with this weapon. This weapon gains the Full Auto property.";
			case THE_PERFECT_FIFTH:
				return "If you hit a hostile creature while Aiming with this weapon, your shot is refunded to the magazine. If you hit one or more hostile creatures four times within 1 hour, without reloading this weapon in between shots, your next shot, if it hits, causes an explosion. All creatures within 5 feet of your target must make a Dexterity saving throw (DC = 8 + your proficiency modifier + your Dexterity modifier), taking 3d6 explosive damage on a failed save, or half on a success.";
			case FLAME_REFRACTION:
				return "If you kill a hostile creature with this weapon, roll a d6. On 5-6, roll a d4 to and refund the amount rolled to your shot capacity. You cannot go over your maximum shot capacity when refunding shots.";
			case PRISMATIC_INFERNO:
				return "This weapon gains the Payload property with a radius of 0 feet, and can only affect the target of your attack. However, for every attack you make with this weapon the radius increases by 5 feet, to a maximum of a 15-foot radius. The effects of this perk are reset if you do not make an attack with this weapon on your turn, if you are no longer holding this weapon in your hands, or if you reload this weapon.";
			case VERMIN:
				return "If you kill a hostile creature with this weapon, you can use your bonus action to grant yourself the effects of active camouflage until the end of your next turn.";
			case RAT_PACK:
				return "As an action, you can attack once for each creature within 20 feet of you holding a Rat King weapon in their hands (besides yourself), up to a max of 4 total attacks. You can only add your ability modifier to the damage roll of one of these attacks.";
			case RED_DEATH:
				return "Killing a creature with this weapon allows you to make a shield recharge roll. If you are not at maximum health points, the amount rolled from your recharge applies to the health first, and the remainder goes to your shields.";
			case ARC_CONDUCTOR:
				return "After taking energy or arc damage, you become charged for 3 rounds. While charged, you gain resistance to energy and arc damage, and this weapon's damage becomes 1d6.";
			case SUPERCONDUCTOR:
				return "While charged, if you hit a hostile creature with a shot from this weapon, you can roll a d4. On a 3-4, a creature of your choice within 5 feet must make a Constitution saving throw (DC = 8 + your proficiency bonus + your Strength modifier), taking 2d6 arc damage on a failed save or half as much on a success.";
			case FAN_FIRE:
				return "As an action, you can make a ranged attack against a number of creatures within 10 feet of a point you can see within this weapon�s range. You must have ammunition for each target, and you make a separate attack roll for each target.";
			case LAST_WORD:
				return "If you hit a creature with a shot from this weapon, you can reload it as a bonus action, overcoming the bulky property. While not Aiming with this weapon, you gain a bonus +3 to attack and damage rolls with it. Finally, you can channel your Golden Gun super ability through this weapon to cast your Golden Gun as a reaction to taking damage from an attack made against you within the effective range of your Golden Gun.";
			case HANNIBAL:
				return "This weapon does an additional 1d6 damage to cabal creatures, and the explosive nature of this weapon�s shells overcomes the bonus provided to a creature's AC by physical shields.";
			case SLUG_RIFLE:
				return "If you make an attack with this weapon and miss, the target of your attack must make a Dexterity saving throw (DC = 8 + your proficiency modifier + your Dexterity modifier). On a failed save, they take half the damage from your attack. If you had disadvantage on your attack roll, you target has advantage on their saving throw against this, and vice-versa.";
			case DORNR�SCHEN:
				return "This weapon's energy projectiles fire in a straight line up to this weapon's maximum range, and ricochet off hard surfaces at a 45 degree angle of your choosing, continuing until they have covered a length of line equal to this weapon's maximum range. When you make an attack with this weapon, all creatures within the line must make a Dexterity saving throw, taking full damage on a failed save and half as much on a success.";
			case THE_HOUSE_ALWAYS_WINS:
				return "This weapon's damage is halved, and you do not add your ability modifier to this weapon's damage.";
			case COUNTING_CARDS:
				return "This weapon does an additional 7 solar damage for every damage die rolled that is a 7 or 8. This bonus damage is not affected by the perk The House Always Wins.";
			case ACCOMPLICE:
				return "If you kill a hostile creature with this weapon, you can choose one other weapon you're carrying to reload. This spends one magazine for that weapon as normal.";
			case STORM_AND_STRESS:
				return "If you kill a creature with a firearm that deals energy, arc, solar, or void damage, and you are carrying this weapon on your person, this weapon gains one use of the Final Round perk, to a maximum of 6 uses. Whenever you next make an attack with this weapon, before you make your attack roll, you can choose to spend one of your uses of Final Round and gain its benefits regardless of how many shots you have left in your shot capacity. All uses of this perk are lost if you remove your attunement to this weapon, or if you are no longer carrying it.";
			case SUNBURN:
				return "If you hit a hostile creature with this weapon, it and all other creatures within 5 feet must make a Dexterity saving throw (DC = 8 + your proficiency bonus + your Strength or Dexterity modifier). On a failed save, they take 1d6 solar damage and the ash from this weapon's attack clings to them, shining bright light in 5 feet from them and dim light 5 feet beyond that.";
			case SUN_BLAST:
				return "If you hit a hostile creature that has the ashes from the Sunburn perk clinging to them, you deal an additional 1d6 solar damage to them.";
			case SUPER_GOOD_ADVICE:
				return "If you attack with this weapon and miss, roll a d4. On 2-4, the shot is refunded. If you hit, roll a d6. On 5-6, the shot is refunded after dealing damage.";
			case SPINNING_UP:
				return "This weapon's rate of fire increases the longer the trigger is held. For every Attack action you take with this weapon, you can make one additional attack on your next Attack action with this weapon, to a maximum of 5 attacks with the Attack action. You must make the attack before the end of your next turn to gain this benefit. If you take an action, bonus action, or reaction to do anything other than Attack with this weapon, or if you reload this weapon, this perk's bonus is reset.";
			case DUAL_SPEED_RECEIVER:
				return "While Aiming with this weapon, it gains the Full Auto property, and its damage die is increased by one. ";
			case SUROS_LEGACY:
				return "While this weapon�s current shot capacity is equal to or less than half of its total shot capacity, the damage die size is increased by one. If you are attuned to this weapon, you also gain the benefits of the Life Support perk.";
			case PAYDAY:
				return "You have a bonus +1 to hit with this weapon while not Aiming. This weapon's maximum shot capacity is increased by 2.";
			case BUSINESS_TIME:
				return "This weapon's rate of fire increases the longer the trigger is held. For every Attack action you take with this weapon, you can make one additional attack on your next Attack action with this weapon, to a maximum of 5 attacks per Attack action. You must make the attack before the end of your next turn to gain this benefit. If you take an action, bonus action, or reaction to do anything other than Attack with this weapon, or if you reload this weapon, this perk's bonus is reset. \r\n"
						+ "Additionally, once on your turn after you have made an attack with this weapon, you can roll a d6. On 5-6, roll a d4 to determine how many shots are refunded to this weapon's current shot capacity. You cannot exceed a weapon's maximum shot capacity when refunding shots.";
			case HARBINGERS_PULSE:
				return "If you kill a creature with this weapon, this weapon, as well as one other weapon you are carrying, are reloaded using an appropriate magazine you have on your person. If you do not have an appropriate magazine on your person, but your Ghost has one and is within its pocket backpack or within 5 feet of you, it can use its reaction to use an appropriate magazine instead.";
			case INSECTOID_ROBOT_GRENADES:
				return "When a round from this weapon collides with a hard surface or creature, it shifts into an insect-like robot that chases after the nearest creature of your choice within 15 feet of it. The robots can move up to 15 feet on your turn and can climb most surfaces, including smooth walls and hanging upside-down on ceilings. If they reach a target they detonate, and the target must make a Dexterity saving throw. They take 4d6 explosive damage on a failed save and half as much on a success.";
			case SPICY_BOYS:
				return "The damage of this weapon's robotic grenades ignores damage soak values, gains a bonus +1 to its saving throw DC, and the robots can travel an additional 10 feet on your turn.";
			case SERVE_THE_COLONY:
				return "This weapon reloads itself from an appropriate magazine if you do not make an attack with it on your turn. If you do not have an appropriate magazine, it will consume 250 glimmer it can find, either raw or in items, to create a magazine for itself.";
			case DEAD_EYE:
				return "While Aiming with this weapon, all ranges of this weapon are increased by 10 feet, you have a bonus +1 to hit with this weapon, and your movement speed is not reduced from Aiming.";
			case FIRST_CURSE:
				return "If you kill a hostile creature with a shot from this weapon, you refund one shot to this weapon's shot capacity, and you gain a bonus +2 to attack and damage rolls with this weapon. This effect does not stack on itself, and is lost when you reload this weapon.";
			case EXCAVATION:
				return "As an action, you can fire two grenades from this weapon, each at a spot you can see within range, and ready an action to detonate both grenades before the start of your next turn whenever a creature of your choosing moves within your chosen spots. At the start of your next turn, if the grenades have not detonated, you can choose to use your action to continue readying to detonate them, or let them detonate automatically.";
			case FULL_AUTO_TRIGGER_SYSTEM:
				return "As an action, you can fire all rounds in this weapon's current shot capacity with Excavation.";
			case COMBAT_SIGHTS:
				return "While this perk is active, this weapon loses the Loading property, and its damage becomes 1d10.";
			case MARKSMAN_SIGHTS:
				return "While this perk is active, this weapon gains the HCR property, and its range becomes 10/120/340 (long).";
			case MAD_SCIENTIST:
				return "This weapon fires a volley of miniature rockets in a 20-foot cone. All creatures within the cone are subject to this weapon's payload saving throw and damage.";
			case MECHANIZED_AUTOLOADER:
				return "This weapon will generate ammunition for itself using any carried magazine you have. It does this if you do not make an attack with it on your turn.";
			case MARK_OF_THE_DEVOURER:
				return "When you damage a creature with this weapon, they take an additional 1d6 Darkness damage at the start of their turn for the next minute. If the creature has energy shields, this damage is applied directly to health instead of their energy shields. If the creature is a Risen, they can make a Light ability saving throw at the end of their turn (DC = 8 + your proficiency modifier + your Strength or Dexterity modifier), ending the effect early on a success. Creatures that die to this damage are subject to the effects of Light Sap (3). \r\n"
						+ "You cannot cast any Light abilities while attuned to this weapon.";
			case OVERFLOW:
				return "If you have at least one super ability charge, this weapon has a bonus +2 to attack and damage rolls, Aiming with this weapon costs 10 less feet of movement, and your movement is not reduced while Aiming.";
			case TOUCH_OF_MALICE:
				return "When you fire the final round in this weapon's magazine and hit a creature, roll one of your hit die without expending it. You take the amount rolled as health damage immediately, and the same amount is applied as bonus damage to your damage roll. The final round of the magazine is also automatically refunded on a hit or miss.";
			case TOUCH_OF_MERCY:
				return "If you kill a creature with this weapon, you can make a shield recharge roll, gaining the amount rolled as health points first, with any leftover being applied to your energy shields, up to your maximum shield capacity.";
			case REPULSOR_FORCE:
				return "This weapon fires a pulse of energy in a 15-foot cone. Creatures within the cone must make a Charisma saving throw, taking this weapon's damage on a failed save and also becoming suppressed and weakened for one minute. On a success, they take half as much damage and are not suppressed. Creatures can repeat the saving throw at the end of their turn, ending the effects early on a success. Creatures who succeed on their saving throw, or for whom the effect ends, become immune to being suppressed or weakened for 24 hours.";
			case THE_SCIENTIFIC_METHOD:
				return "Damaging a hostile creature with this weapon grants a bonus +5 to movement speed and allows you to reload this weapon as a bonus action until the end of your next turn.";
			case PARTICLE_REPEATER:
				return "This weapon's saving throws gain a bonus +1 to their DC. If you attack and hit a hostile creature within 5 feet of you with a shot from this weapon, that creature must succeed on a Strength saving throw or be pushed back 10 feet.";
			case BE_THE_DANGER:
				return "This weapon has the Automatic Fire property. You gain a bonus +5 to Charisma (Intimidation) checks you make while holding this weapon.";
			case UNREPENTANT:
				return "If you kill a hostile creature with this weapon, your next attack deals an additional 2d4 damage on a hit.";
			case SPLIT_ELECTRON:
				return "While you are Aiming, this weapon has a bonus +1 to attack and damage rolls. While you are not Aiming, when you make a ranged attack with this weapon, you make that attack against up to 3 creatures of your choosing within 5 feet of a point you can see. You make a single attack roll that affects all the chosen creatures, but roll damage separately for each hit.";
			case LIGHTNING_ROD:
				return "If you hit a creature while Aiming with this weapon, the next arrow you fire from this bow becomes imbued with arc energy. Wherever you next make a ranged attack with this bow, up to 3 creatures of your choice within 5 feet of your target must make a Constitution saving throw (DC = 8 + your proficiency bonus + your Dexterity modifier). They take 3d6 arc damage on a failed save, or half as much on a success. If you hit a creature with this imbued arrow, that creature takes normal damage and makes this saving throw with disadvantage if it is also one of your chosen creatures for this perk.";
			case PROTOTYPE_TRUESEEKER:
				return "While Aiming with this weapon, you can choose to mark a creature you can see. Now, until you fire this weapon, regardless of which direction you fire it in the rounds from this weapon will seek out the creature you have marked, dodging around obstacles or obstructions so long as the rounds have at least 15 feet of open space to maneuver. You lose your mark if you do not make an attack against the creature within 30 seconds of marking them.";
			case TWINTAILS:
				return "This weapon fires two rockets at once, one that is a Void Warhead and deals void damage, and one that is a Solar Warhead and deals solar damage. You choose up to two spots, one for each warhead, or one spot for both warheads, when you make an attack with this weapon. ";
			case PLAY_WITH_YOUR_PREY:
				return "The void warhead suppresses creatures that failed the payload saving throw against it. The solar warhead causes creatures that fail their payload saving throw against it to begin burning. Each effect lasts for 1 minute. Creatures can repeat the payload saving throw at the end of each of their turns (these saving throws are not affected by Javelin), ending any effect from a Twintail warhead on themselves on a success. Creatures that succeed on their initial payload saving throw, or for whom the effect ends, become immune to the effect of the warhead for the next 24 hours.";
			case UNIVERSAL_REMOTE:
				return "While Aiming, all ranges of this weapon increase by 10 feet, and you have a bonus +1 to attack and damage rolls with this weapon.";
			case TIMELESS_MYTHOCLAST:
				return "For the first attack you make with this weapon on your action, bonus action, or reaction, if you hit, you can roll a d4 after dealing damage. On 3-4, the local time around you is distorted, one shot is refunded, and you can fire this weapon again as if it were the first attack on your action, bonus action, or reaction. The effect of this perk can loop so long as you continue to roll 3-4. If you roll a 1-2, this loop ends, and you complete your action, bonus action, or reaction as normal.";
			case HARSH_TRUTHS:
				return "If a friendly creature that you are aware of is killed within 30 feet of you, you can make a shield recharge roll, and you gain a bonus +5 feet of movement until the end of your next turn.";
			case LAST_STAND:
				return "If you are aware of at least one allied creature that is on death saving throws within 30 feet of you, aiming this weapon costs no movement and it gains a +2 bonus to attack and damage rolls.";
			case HARMONIC_LASER:
				return "When you attack with this weapon, before you roll damage, roll a d6. On 1-2, this weapon does 1d4 damage. On 3-4, this weapon does 2d4 damage. On 5-6, this weapon does 3d4 damage.";
			case SUPERCHARGED_BATTERY:
				return "When the duration of your super ability ends, the next 5 attacks with this weapon deal 3d4 damage regardless of the roll of Harmonic Laser. You lose the effect of this perk if you reload this weapon or are no longer holding it in your hands, or if more than 1 minute has passed since your super ability's duration ended.";
			case QUEENS_WRATH:
				return "This weapon deals a bonus 4d6 damage to beasts and monstrosities. When Aiming with this weapon, you gain bonus +5 on Wisdom (Perception) rolls to find hidden creatures. Creatures that are invisible are detected and outline by this weapon's scope, up to a range of 30 feet. You do not have disadvantage on attack rolls against invisible creatures while Aiming with this weapon.";
			case COMPOUND_DRAW:
				return "Arrows fired from this bow can damage all creatures in a line up to this weapon�s effective range (before accounting for Aiming changes). In order to do this, your attack must have killed the first target, and your attack roll must beat the AC of the next subsequent target.";
			case DYNAMITE:
				return "During an encounter, if you hit a hostile creature with every shot in this weapon�s shot capacity, this weapon's shot capacity is refunded, and the next shot you take with this weapon uses a powerful round that deals an additional 1d12 damage.";
			case WITH_A_LASER_BEAM:
				return "The powerful round fired from Dynamite has a payload radius of 10 feet instead of 5.";
			case ZHALO_SUPERCELL:
				return "If you hit a creature with this weapon and there is at least one other creature within 5 feet of your target, roll a d6. On 5-6, that creature must make a Constitution saving throw, taking 1d8 arc damage on a failed save and chaining this effect to yet another creature within 5 feet, who must make the same saving throw. Up to three creatures can be affected in this way. If any creature succeeds on their saving throw, they take half as much damage and this effect ends.";
			case BOLTS_FROM_THE_BLUE:
				return "For every creature that fails their saving throw from the Zhalo Supercell perk, this weapon regains 1 shot in its shot capacity.";
			default:
				return "";
			}
		}

		public int getCost() {
			switch (this) {
			case ACCELERATED_COILS:
				return 500;
			case APPENDED_MAGAZINE:
				return 250;
			case ARMOR_PIERCING_ROUNDS:
				return 1200;
			case ARMY_OF_ONE:
				return 3000;
			case BATTLE_RUNNER:
				return 750;
			case BLACK_POWDER:
				return 1500;
			case BLINDING_GRENADES:
				return 2500;
			case BOX_BREATHING:
				return 1000;
			case CASCADE:
				return 500;
			case CLOSE_AND_OR_PERSONAL:
				return 500;
			case CLOWN_CARTRIDGE:
				return 1200;
			case CLUSTER_BOMBS:
				return 2000;
			case CONCUSSION_GRENADES:
				return 2500;
			case CROWD_CONTROL:
				return 2000;
			case ENHANCED_BATTERY:
				return 2000;
			case EXTENDED_MAGAZINE:
				return 500;
			case EYE_OF_THE_STORM:
				return 2200;
			case FEATHERWEIGHT_BOLT:
				return 1500;
			case FIELD_SCOUT:
				return 2000;
			case FINAL_ROUND:
				return 2000;
			case FIREFLY:
				return 2000;
			case FLARED_MAGWELL:
				return 1000;
			case FOCUSED_FIRE:
				return 1000;
			case FULL_DRAW:
				return 1200;
			case GLASS_HALF_FULL:
				return 2000;
			case GRENADES_AND_HORSESHOES:
				return 1200;
			case GUERILLA_FIGHTER:
				return 250;
			case HAMMER_FORGED:
				return 250;
			case HARD_LAUNCH:
				return 2000;
			case HEADSEEKER:
				return 1250;
			case HEAVY_DUTY_COILS:
				return 500;
			case HEAVY_PAYLOAD:
				return 3000;
			case HIDDEN_HAND:
				return 1000;
			case HIGH_CALIBER_ROUNDS:
				return 500;
			case HIP_FIRE:
				return 1000;
			case HOT_SWAP:
				return 2000;
			case LAST_RESORT:
				return 2000;
			case LIFE_SUPPORT:
				return 2500;
			case LIGHTWEIGHT:
				return 750;
			case LUCK_IN_THE_CHAMBER:
				return 1250;
			case MULLIGAN:
				return 500;
			case OUTLAW:
				return 750;
			case PERFECT_BALANCE:
				return 1500;
			case PERFECTIONIST:
				return 500;
			case PERSISTENCE:
				return 2200;
			case RANGEFINDER:
				return 750;
			case REACTIVE_RELOAD:
				return 2000;
			case REPLENISH:
				return 500;
			case RESCUE_MAGAZINE:
				return 500;
			case RICOCHET_ROUNDS:
				return 2200;
			case SECRET_ROUND:
				return 1000;
			case SHOOT_TO_LOOT:
				return 750;
			case SHOT_PACKAGE:
				return 1200;
			case SMALLBORE:
				return 1500;
			case SNAPSHOT:
				return 750;
			case SPRAY_AND_PLAY:
				return 1750;
			case STABILIZER_SIGHTS:
				return 500;
			case SUPERCHARGER:
				return 1500;
			case SURROUNDED:
				return 2200;
			case TAKE_A_KNEE:
				return 2200;
			case THIRD_EYE:
				return 1250;
			case THREAT_DETECTOR_SIGHTS:
				return 2750;
			case TRACKING:
				return 1200;
			case TRIPLE_TAP:
				return 3000;
			case TRIPOD:
				return 2500;
			case UNDERDOG:
				return 1000;
			case UNFLINCHING:
				return 1000;
			case VOLATILE_LAUNCH:
				return 2000;
			case ZEN_MOMENT:
				return 3000;
			default:
				return 0;
			}
		}
	}

	public class CustomPerk implements PerkInterface {
		private final String name;
		private final String description;

		public CustomPerk(String name, String description) {
			this.name = name;
			this.description = description;
			perks.putIfAbsent(name, this);
		}

		@Override
		public String name() {
			return name;
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public String getDescription() {
			return description;
		}
	}
}
