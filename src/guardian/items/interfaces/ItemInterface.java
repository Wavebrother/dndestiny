package guardian.items.interfaces;

import javax.swing.ImageIcon;

import guardian.BaseInterface;
import guardian.items.itemEnums.Items.ItemType;

public interface ItemInterface extends BaseInterface {

	default int getMaxStack() {
		return 1;
	}

	default int getMemorySize() {
		return 0;
	}

	default double getWeightPerItem() {
		return 0;
	}

	default void setItem(String description, ImageIcon icon) {
	}
	
	default ItemType getItemType() {
		return ItemType.MISCELLANEOUS;
	}
}
