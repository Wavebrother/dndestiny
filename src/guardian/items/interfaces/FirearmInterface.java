package guardian.items.interfaces;

public interface FirearmInterface extends WeaponInterface {
	PerkInterface[][] getPerkSlotOptions();
}
