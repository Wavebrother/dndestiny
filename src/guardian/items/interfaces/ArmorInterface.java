package guardian.items.interfaces;
import guardian.items.itemEnums.Armor;

public interface ArmorInterface extends ItemInterface {
	public boolean isStealth();

	public int getMinStr();

	public int getMaxAC();

	public int getBaseAC();

	public Armor.ArmorLevel getLevel();

	boolean isGuardianArmor();

}
