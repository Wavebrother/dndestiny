package guardian.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Collections;

import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.Main;
import guardian.items.interfaces.ItemInterface;
import guardian.items.itemEnums.Items;

public class Item implements ActionListener {
	private final ItemInterface item;
	private Inventory inventory;
	private final JPanel panel = new JPanel(new BorderLayout());
	private JDialog itemFrame;
	public JSpinner amount;

	private JPanel displayPanel;
	protected final JButton delete = new JButton("X");
	protected final JButton moveUp = new JButton("\u2191");
	protected final JButton moveDown = new JButton("\u2193");
	private Popup popup = PopupFactory.getSharedInstance().getPopup(panel, new JLabel(), 0, 0);

	public Item(ItemInterface item, Inventory inventory, int amount) {
		this.item = item;
		if (amount > item.getMaxStack())
			this.amount = new JSpinner(new SpinnerNumberModel(item.getMaxStack(), 1, item.getMaxStack(), 1));
		else
			this.amount = new JSpinner(new SpinnerNumberModel(amount, 1, item.getMaxStack(), 1));
		this.amount.getPreferredSize().width = 50;
		this.setInventory(inventory);
		this.delete.addActionListener(this);
		this.delete.setForeground(Color.RED);
		this.moveUp.addActionListener(this);
		this.moveUp.setFont(new Font(this.moveUp.getFont().getName(), this.moveUp.getFont().getStyle(),
				(this.moveUp.getFont().getSize() * 3) / 4));
		this.moveDown.addActionListener(this);
		this.moveDown.setFont(new Font(this.moveDown.getFont().getName(), this.moveDown.getFont().getStyle(),
				(this.moveDown.getFont().getSize() * 3) / 4));
	}

	double getWeight() {
		return this.getItem().getWeightPerItem() * (int) this.amount.getValue();
	}

	protected String getDisplayName() {
		StringBuilder sb = new StringBuilder();
		int amount = (int) this.amount.getValue();
		sb.append(amount);
		sb.append(" ");
		sb.append(this.getItem().toString());
		if (amount > 1)
			sb.append("s");
		return sb.toString();
	}

	protected JPanel display() {
		JPanel movePanel = new JPanel(new GridLayout(0, 1));
		movePanel.add(this.moveUp);
		movePanel.add(this.moveDown);
		JPanel buttonPanel = new JPanel(new GridLayout());
		buttonPanel.add(this.delete);
		buttonPanel.add(movePanel);
		this.panel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
		this.panel.removeAll();
		JLabel nameLabel = new JLabel(this.item.getName());
		nameLabel.setFont(Main.titleFont);
		nameLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 20));
		this.panel.add(nameLabel, BorderLayout.CENTER);
		this.panel.add(this.amount, BorderLayout.WEST);
		this.amount.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				displayPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
						"Inventory (" + getInventory().getWeight() + "lbs)", TitledBorder.DEFAULT_JUSTIFICATION,
						TitledBorder.DEFAULT_POSITION, Main.titleFont));
				if (item == Items.GLIMMER) {
					Main.character.glimmer.setText(Main.character.character.getGlimmerFormatted());
					getInventory().fillPanel();
				}
			}
		});
		this.panel.add(buttonPanel, BorderLayout.EAST);
		this.amount.setFont(Main.textFont);
		this.panel.setPreferredSize(null);
		return this.panel;
	}

	JButton openIcon(int iconSize, double multiplier, JPanel displayPanel) {
		JButton open = new JButton(item.getIcon(iconSize, multiplier));
		open.setMargin(new Insets(0, 0, 0, 0));
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showItem(open, iconSize, multiplier, displayPanel);
			}
		});
		open.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				popup.hide();
				JLabel nameLabel = new JLabel(item.toString());
				nameLabel.setFont(Main.textFont);
				nameLabel.setFont(
						new Font(Main.textFont.getFontName(), Main.textFont.getStyle(), Main.textFont.getSize() + 5));
				nameLabel.setBackground(getInventory().entity.backgroundColor);
				popup = PopupFactory.getSharedInstance()
						.getPopup(open, nameLabel,
								open.getLocationOnScreen().x - nameLabel.getPreferredSize().width / 2
										+ open.getPreferredSize().width / 2,
								open.getLocationOnScreen().y + open.getHeight());
				if (model.isRollover()) {
					popup.show();
				}
			}
		});
		open.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		return open;
	}

	JButton open(int iconSize, double multiplier, JPanel displayPanel) {
		JButton open = new JButton(item.toString());
		open.setFont(Main.textFont);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showItem(open, iconSize, multiplier, displayPanel);
			}
		});
		return open;
	}

	private void showItem(JButton open, int iconSize, double multiplier, JPanel displayPanel) {
		this.displayPanel = displayPanel;
		if (itemFrame == null || !itemFrame.isVisible()) {
			itemFrame = new JDialog(SwingUtilities.getWindowAncestor(getInventory().panel));
			itemFrame.getContentPane().setLayout(new BorderLayout());
			itemFrame.getContentPane().add(display(), BorderLayout.NORTH);
			JTextArea description = new JTextArea(item.getDescription());
			description.setEditable(false);
			description.setFont(Main.textFont);
			description.setMargin(new Insets(3, 5, 5, 5));
			description.setLineWrap(true);
			description.setWrapStyleWord(true);
			itemFrame.getContentPane().add(description, BorderLayout.CENTER);
			itemFrame.setIconImage(item.getIcon(iconSize, multiplier).getImage());
			itemFrame.pack();
			description.setSize(description.getPreferredSize());
			itemFrame.pack();
			itemFrame.getContentPane().setBackground(Main.backgroundColor);
			itemFrame.setBackground(Main.backgroundColor);
			itemFrame.setLocationRelativeTo(open);
			itemFrame.setVisible(true);
			itemFrame.addWindowListener(new java.awt.event.WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent closeEvent) {
				}
			});
		} else {
			itemFrame.dispose();
		}
	}

	@Override
	public String toString() {
		return this.getItem().toString();
	}

	public ItemInterface getItem() {
		return item;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == delete) {
			this.getInventory().getItems().remove(this);
			Main.character.glimmer.setText(Main.character.character.getGlimmerFormatted());
		} else if (e.getSource() == this.moveUp && this.getInventory().getItems().indexOf(this) != 0
				&& !(this.getInventory().getItems().get(this.getInventory().getItems().indexOf(this) - 1)
						.getItem() == Items.GLIMMER)) {
			Collections.swap(this.getInventory().getItems(), this.getInventory().getItems().indexOf(this),
					this.getInventory().getItems().indexOf(this) - 1);
		} else if (e.getSource() == this.moveDown
				&& this.getInventory().getItems().indexOf(this) != this.getInventory().getItems().size() - 1
				&& !(this.getItem() == Items.GLIMMER)) {
			Collections.swap(this.getInventory().getItems(), this.getInventory().getItems().indexOf(this),
					this.getInventory().getItems().indexOf(this) + 1);
		}
		displayPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Inventory (" + getInventory().getWeight() + "lbs)", TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION, Main.titleFont));
		this.getInventory().fillPanel();
		this.itemFrame.dispose();
		Main.character.getContentPane().repaint();
	}

}
