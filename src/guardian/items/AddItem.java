package guardian.items;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.BaseInterface;
import guardian.Main;
import guardian.SearchableComboBox;
import guardian.items.interfaces.ItemInterface;
import guardian.items.itemEnums.Items;

public class AddItem extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Inventory inventory;

	private SearchableComboBox items = new SearchableComboBox();
	private JCheckBox custom = new JCheckBox("Custom Item");
	private JTextField name = new JTextField();
	private JTextField description = new JTextField();
	private JSpinner maxStack = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
	private JSpinner weightPerItem = new JSpinner(new SpinnerNumberModel(1, 0, 100, .1));
	private JSpinner memorySize = new JSpinner(new SpinnerNumberModel(1, 1, 20, 1));
	private JSpinner amount = new JSpinner(new SpinnerNumberModel(1, 1, 1, 1));

	private JButton add = new JButton("Add");
	private JButton done = new JButton("Done");

	public AddItem(Inventory list) {
		super("New Item");
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});
		this.items.setModel(Items.values());
		this.items.actionListener(this);
		this.inventory = list;

		this.add.addActionListener(this);
		this.done.addActionListener(this);

		JPanel buttonPane = new JPanel(new GridLayout(1, 0));
		buttonPane.add(this.add);
		buttonPane.add(this.done);

		JPanel namePanel = new JPanel(new GridLayout());
		namePanel.add(name);
		namePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel descriptionPanel = new JPanel(new GridLayout());
		descriptionPanel.add(description);
		descriptionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Description", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel maxStackPanel = new JPanel(new GridLayout());
		maxStackPanel.add(maxStack);
		maxStack.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if ((int) maxStack.getValue() > (int) amount.getValue())
					amount.setModel(new SpinnerNumberModel((int) amount.getValue(), 1, (int) maxStack.getValue(), 1));
				else
					amount.setModel(new SpinnerNumberModel((int) maxStack.getValue(), 1, (int) maxStack.getValue(), 1));
			}
		});
		maxStackPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Max Stack Size", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel weightPerItemPanel = new JPanel(new GridLayout());
		weightPerItemPanel.add(weightPerItem);
		weightPerItemPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Weight Per Item", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel memorySizePanel = new JPanel(new GridLayout());
		memorySizePanel.add(memorySize);
		memorySizePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Memory Size", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		this.custom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (custom.isSelected()) {
					remove(items);
					add(namePanel, 1);
					add(descriptionPanel, 2);
					add(maxStackPanel, 3);
					add(weightPerItemPanel, 4);
					add(memorySizePanel, 5);
				} else {
					add(items, 1);
					remove(namePanel);
					remove(descriptionPanel);
					remove(maxStackPanel);
					remove(weightPerItemPanel);
					remove(memorySizePanel);
				}
				amount.setValue(1);
				revalidate();
				pack();
				repaint();
			}

		});
		this.setLayout(new GridLayout(0, 1));
		this.add(this.custom);
		this.add(this.items);
		this.add(this.amount);
		this.add(buttonPane);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	public JButton open() {
		JButton open = new JButton(this.getTitle());
		open.setFont(Main.titleFont);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
			}
		});
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.add || e.getSource() == this.done) {
			try {
				if (!custom.isSelected()) {
					ItemInterface item;
					if (this.items.getSelectedItem() instanceof ItemInterface
							&& !(item = (ItemInterface) this.items.getSelectedItem()).toString().isEmpty()) {
						inventory.addItem(new Item(item, inventory, (int) amount.getValue()));
					}
					this.items.setModel(new DefaultComboBoxModel<BaseInterface>(this.items.items));
					this.items.setSelectedIndex(0);
					this.items.showPopup();
				} else if (!name.getText().isEmpty()) {
					ItemInterface item = new CustomItem(name.getText(), description.getText(),
							(int) maxStack.getValue(), (double) weightPerItem.getValue(), (int) memorySize.getValue());
					inventory.addItem(new Item(item, inventory, (int) amount.getValue()));
					name.setText("");
					description.setText("");
					maxStack.setValue(1);
					weightPerItem.setValue(1);
					memorySize.setValue(1);
				}
				amount.setValue(1);

				if (e.getSource() == this.done)
					this.setVisible(false);
			} catch (RuntimeException error) {
				this.setVisible(false);
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					error.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
			}
		} else if (e.getSource() == this.items && this.items.getSelectedItem() instanceof Items
				&& !((Items) this.items.getSelectedItem()).toString().isEmpty()) {
			Items item = (Items) this.items.getSelectedItem();
			if ((int) amount.getValue() <= item.getMaxStack())
				amount.setModel(new SpinnerNumberModel((int) amount.getValue(), 1, item.getMaxStack(), 1));
			else
				amount.setModel(new SpinnerNumberModel(1, 1, item.getMaxStack(), 1));
		}
	}

}
