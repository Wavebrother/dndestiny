package guardian.items;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import guardian.BaseInterface;
import guardian.DamageType;
import guardian.Die;
import guardian.Entity;
import guardian.Main;
import guardian.Stat.Stats;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.interfaces.FirearmInterface;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.interfaces.PerkInterface.Perk;

public class Firearm extends Weapon {
	public int tier;
	public PerkInterface[] perks;

	public Firearm(WeaponInterface firearm, int magicalBonus, int tier, PerkInterface[] perks,
			ArrayList<Weapon> weapons, Inventory inventory, int shots, boolean proficient, Entity entity) {
		super(firearm, magicalBonus, weapons, inventory, shots, proficient, entity);
		this.tier = tier;
		this.perks = perks;
		this.amount = new JSpinner(new SpinnerNumberModel(shots, 1, getMagazine(), 1));
	}

	private int getMagazine() {
		int magazine = this.getWeapon().getMaxStack();
		if (checkPerks(Perk.APPENDED_MAGAZINE))
			magazine += 4;
		if (checkPerks(Perk.EXTENDED_MAGAZINE, Perk.PAYDAY))
			magazine += 2;
		if (checkPerks(Perk.HARD_LAUNCH, Perk.SHOT_PACKAGE))
			magazine -= 1;
		if (checkPerks(Perk.SMALLBORE))
			magazine -= 2;
		if (checkPerks(Perk.TRIPOD))
			return 3;
		return magazine;
	}

	@Override
	protected int getDamageBonus() {
		int bonus = super.getDamageBonus();
		if (checkPerks(Perk.BOX_BREATHING))
			bonus += 2;
		if (checkPerks(Perk.SECRET_ROUND, Perk.PRECISION_SWITCH, Perk.POCKET_INFINITY))
			bonus += 1;
		if (checkPerks(Perk.THE_HOUSE_ALWAYS_WINS)) {
			if ((this.getWeapon().isFinesse() && entity.getStats().get(Stats.Dexterity).getModifier() > entity
					.getStats().get(Stats.Strength).getModifier()))
				bonus -= entity.getStats().get(Stats.Dexterity).getModifier();
			else
				bonus -= entity.getStats().get(Stats.Strength).getModifier();
		}
		return bonus;
	}

	@Override
	protected int getAttackBonus() {
		int bonus = super.getAttackBonus();
		if (checkPerks(Perk.BOX_BREATHING, Perk.SMALLBORE))
			bonus += 2;
		if (checkPerks(Perk.PERFECT_BALANCE, Perk.SHOT_PACKAGE, Perk.THUNDERER, Perk.AUTOMATIC_SWITCH,
				Perk.SHOCK_ROUNDS, Perk.RPM_900, Perk.POCKET_INFINITY))
			bonus += 1;
		if (checkPerks(Perk.FLARED_MAGWELL))
			bonus -= 1;
		return bonus;
	}

	@Override
	Die getDamageDie() {
		Die die = this.getWeapon().getDamageDie();
		if (checkPerks(Perk.ACCELERATED_COILS, Perk.COMBAT_SIGHTS))
			die = Die.d10;
		if (checkPerks(Perk.ENHANCED_BATTERY, Perk.FULL_DRAW, Perk.HARD_LAUNCH, Perk.HEAVY_PAYLOAD, Perk.RPM_450))
			return die.increaseDie();
		return die;
	}

	@Override
	int getDamageDieNumber() {
		int number = this.getWeapon().getDamageDieNumber();
		if (checkPerks(Perk.CLUSTER_BOMBS))
			number++;
		if (checkPerks(Perk.COMBAT_SIGHTS))
			return 1;
		return number;
	}

	@SuppressWarnings("unused")
	private int[] getModifiedRange() {
		int[] ranges = { this.getWeapon().getRange()[0], this.getWeapon().getRange()[1],
				this.getWeapon().getRange()[2] };
		if (checkPerks(Perk.MARKSMAN_SIGHTS)) {
			ranges[0] = 10;
			ranges[1] = 120;
			ranges[2] = 340;
		}
		for (int i = 0; i < 3; i++) {
			if (checkPerks(Perk.ENHANCED_BATTERY, Perk.RANGEFINDER))
				ranges[i] += 10;
			if (checkPerks(Perk.HAMMER_FORGED, Perk.SHOT_PACKAGE))
				ranges[i] += 5;
		}
		if (checkPerks(Perk.FOCUSED_FIRE)) {
			this.getWeapon().getRange()[0] -= 10;
			if (this.getWeapon().getRange()[0] < 0)
				this.getWeapon().getRange()[0] = 0;
		}
		if (checkPerks(Perk.BLACK_HOLE))
			ranges[2] += 300;
		return ranges;
	}

	@SuppressWarnings("unused")
	private RangeBand getRangeBand() {
		int index = Arrays.asList(RangeBand.values()).indexOf(this.getWeapon().getRangeBand());
		if (checkPerks(Perk.FOCUSED_FIRE, Perk.AUTOMATIC_SWITCH) && index > 0)
			index--;
		if (checkPerks(Perk.FULL_DRAW) && index < 2)
			index++;
		if (checkPerks(Perk.MARKSMAN_SIGHTS))
			return RangeBand.LONG;
		return RangeBand.values()[index];
	}

	@SuppressWarnings("unused")
	private ArrayList<WeaponProperties> getProperties() {
		ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
		properties.addAll(this.getWeapon().getProperties());
		if (checkPerks(Perk.APPENDED_MAGAZINE, Perk.HARD_LAUNCH, Perk.PERSISTENCE, Perk.SMALLBORE))
			properties.add(WeaponProperties.BULKY);
		if (checkPerks(Perk.BOX_BREATHING, Perk.FULL_DRAW))
			properties.add(WeaponProperties.LOADING);
		if (checkPerks(Perk.FEATHERWEIGHT_BOLT, Perk.SUPERCHARGER, Perk.AUTOMATIC_SWITCH, Perk.BE_THE_DANGER))
			properties.add(WeaponProperties.AUTOMATIC_FIRE);
		if (checkPerks(Perk.VOLATILE_LAUNCH))
			properties.add(WeaponProperties.PAYLOAD_VOLATILE_LAUNCH);
		if (checkPerks(Perk.SHOCK_ROUNDS, Perk.CHARGE_SHOT))
			properties.add(WeaponProperties.TRACKING);
		if (checkPerks(Perk.SHOCK_BLAST))
			properties.add(WeaponProperties.ENERGY_PROJECTILES);
		if (checkPerks(Perk.POCKET_INFINITY))
			properties.add(WeaponProperties.FULL_AUTO);
		if (checkPerks(Perk.PRISMATIC_INFERNO))
			properties.add(WeaponProperties.PAYLOAD);
		if (checkPerks(Perk.MARKSMAN_SIGHTS))
			properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
		if (checkPerks(Perk.FLARED_MAGWELL, Perk.FLUTED_BARREL))
			properties.remove(WeaponProperties.BULKY);
		if (checkPerks(Perk.ACCELERATED_COILS, Perk.COMBAT_SIGHTS))
			properties.remove(WeaponProperties.LOADING);
		return properties;
	}

	private boolean checkPerks(Perk... perks) {
		boolean contains = false;
		List<PerkInterface> perkList = Arrays.asList(this.perks);
		for (PerkInterface perk : perks) {
			contains = perkList.contains(perk);
			if (contains)
				break;
		}
		return contains;
	}

	public enum Firearms implements FirearmInterface {
		Auto_Rifle("Auto Rifle"), Hand_Cannon("Hand Cannon"), Pulse_Rifle("Pulse Rifle"), Scout_Rifle("Scout Rifle"),
		Sidearm("Sidearm"), Submachine_Gun("Submachine Gun"), Trace_Rifle("Trace Rifle"), Combat_Bow("Combat Bow"),
		Fusion_Rifle("Fusion Rifle"), Grenade_Launcher("Grenade Launcher"), Light_Machine_Gun("Light Machine Gun"),
		Linear_Fusion_Rifle("Linear Fusion Rifle"), Rocket_Launcher("Rocket Launcher"), Shotgun("Shotgun"),
		Sniper_Rifle("Sniper");
		public static final int[] tierCosts = { 0, 500, 2000, 4500 };

		private String name;
		private String description;

		Firearms(String name) {
			this.name = name;
			// System.out.print(name().toUpperCase() + "(\"" + name + "\"), ");
		}

		public static Map<String, Firearms> getFirearms() {
			Map<String, Firearms> firearms = new HashMap<String, Firearms>();
			for (Firearms firearm : values())
				firearms.put(firearm.toString(), firearm);
			return firearms;
		}

		@Override
		public String toString() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String getDescription() {
			return description;
		}

		@Override
		public int getMemorySize() {
			switch (this) {
			case Combat_Bow:
				return 4;
			default:
				return 3;
			}
		}

		@Override
		public double getWeightPerItem() {
			switch (this) {
			case Sidearm:
				return 3;
			case Submachine_Gun:
				return 4;
			case Hand_Cannon:
				return 6;
			case Scout_Rifle:
			case Trace_Rifle:
				return 8;
			case Pulse_Rifle:
				return 9;
			case Auto_Rifle:
				return 10;
			case Linear_Fusion_Rifle:
			case Sniper_Rifle:
				return 12;
			case Combat_Bow:
			case Shotgun:
				return 15;
			case Fusion_Rifle:
			case Grenade_Launcher:
				return 18;
			case Light_Machine_Gun:
				return 21;
			case Rocket_Launcher:
				return 25;
			default:
				return 0;
			}
		}

		@Override
		public ImageIcon getIcon(int size, double multiplier) {
			ImageIcon icon;
			switch (this) {
			default:
				icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
			}
			return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
					(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
		}

		@Override
		public int[] getRange() {
			switch (this) {
			case Auto_Rifle:
				return new int[] { 40, 55, 80 };
			case Hand_Cannon:
				return new int[] { 30, 70, 90 };
			case Pulse_Rifle:
				return new int[] { 25, 90, 120 };
			case Scout_Rifle:
				return new int[] { 20, 120, 240 };
			case Sidearm:
				return new int[] { 20, 40, 50 };
			case Submachine_Gun:
				return new int[] { 15, 25, 35 };
			case Trace_Rifle:
				return new int[] { 30, 40, 60 };
			case Fusion_Rifle:
				return new int[] { 30, 40, 50 };
			case Grenade_Launcher:
				return new int[] { 20, 40, 60 };
			case Light_Machine_Gun:
				return new int[] { 35, 80, 120 };
			case Linear_Fusion_Rifle:
				return new int[] { 10, 120, 340 };
			case Rocket_Launcher:
				return new int[] { 30, 50, 120 };
			case Shotgun:
				return new int[] { 10, 20, 30 };
			case Sniper_Rifle:
				return new int[] { 0, 240, 600 };
			default:
				return new int[3];
			}
		}

		@Override
		public String getRangeInfo() {
			return "Range: " + this.getRange()[0] + ", " + this.getRange()[1] + ", " + this.getRange()[2] + " ("
					+ getRangeBand() + ")";
		}

		@Override
		public boolean isFinesse() {
			switch (this) {
			case Auto_Rifle:
			case Hand_Cannon:
			case Pulse_Rifle:
			case Trace_Rifle:
			case Fusion_Rifle:
			case Grenade_Launcher:
			case Light_Machine_Gun:
				return true;
			default:
				return false;
			}
		}

		@Override
		public boolean isRanged() {
			return true;
		}

		@Override
		public boolean isThrown() {
			return false;
		}

		@Override
		public DamageType getDamageType() {
			switch (this) {
			case Sidearm:
			case Trace_Rifle:
			case Fusion_Rifle:
			case Linear_Fusion_Rifle:
				return DamageType.ENERGY;
			case Grenade_Launcher:
			case Rocket_Launcher:
				return DamageType.EXPLOSIVE;
			default:
				return DamageType.KINETIC;
			}
		}

		@Override
		public WeaponType getWeaponType() {
			switch (this) {
			case Auto_Rifle:
			case Hand_Cannon:
			case Pulse_Rifle:
			case Scout_Rifle:
			case Sidearm:
			case Submachine_Gun:
			case Trace_Rifle:
				return WeaponType.SIMPLE_RANGED;
			case Combat_Bow:
			case Fusion_Rifle:
			case Grenade_Launcher:
			case Light_Machine_Gun:
			case Linear_Fusion_Rifle:
			case Rocket_Launcher:
			case Shotgun:
			case Sniper_Rifle:
				return WeaponType.MARTIAL_RANGED;
			}
			return null;
		}

		@Override
		public Die getDamageDie() {
			switch (this) {
			case Pulse_Rifle:
			case Submachine_Gun:
				return Die.d4;
			case Auto_Rifle:
			case Sidearm:
			case Trace_Rifle:
			case Fusion_Rifle:
			case Grenade_Launcher:
				return Die.d6;
			case Hand_Cannon:
			case Scout_Rifle:
			case Combat_Bow:
			case Light_Machine_Gun:
			case Rocket_Launcher:
				return Die.d8;
			case Linear_Fusion_Rifle:
			case Shotgun:
			case Sniper_Rifle:
				return Die.d12;
			default:
				return null;
			}
		}

		@Override
		public int getDamageDieNumber() {
			switch (this) {
			case Pulse_Rifle:
			case Fusion_Rifle:
			case Grenade_Launcher:
			case Rocket_Launcher:
				return 2;
			default:
				return 1;
			}
		}

		@Override
		public ArrayList<WeaponProperties> getProperties() {
			ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
			if (isFinesse())
				properties.add(WeaponProperties.FINESSE);
			switch (this) {
			case Auto_Rifle:
				properties.add(WeaponProperties.AUTOMATIC_FIRE);
				break;
			case Sidearm:
				properties.add(WeaponProperties.LIGHT);
				break;
			case Submachine_Gun:
				properties.add(WeaponProperties.AUTOMATIC_FIRE);
				properties.add(WeaponProperties.HIGH_RECOIL);
				properties.add(WeaponProperties.LIGHT);
				break;
			case Trace_Rifle:
				properties.add(WeaponProperties.TRACE_RIFLE);
				break;
			case Fusion_Rifle:
			case Linear_Fusion_Rifle:
				properties.add(WeaponProperties.LOADING);
				properties.add(WeaponProperties.ENERGY_PROJECTILES);
				break;
			case Grenade_Launcher:
				properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
				properties.add(WeaponProperties.GRENADE_LAUNCHER);
				break;
			case Light_Machine_Gun:
				properties.add(WeaponProperties.AUTOMATIC_FIRE);
				properties.add(WeaponProperties.BULKY);
				properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
				properties.add(WeaponProperties.HEAVY_HITTER);
				break;
			case Rocket_Launcher:
				properties.add(WeaponProperties.BULKY);
				properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
				properties.add(WeaponProperties.HIGH_RECOIL);
				properties.add(WeaponProperties.HEAVY);
				properties.add(WeaponProperties.PAYLOAD);
				break;
			case Shotgun:
				properties.add(WeaponProperties.HIGH_RECOIL);
				break;
			default:
				break;
			}
			return properties;
		}

		@Override
		public int getMaxStack() {
			switch (this) {
			case Combat_Bow:
				return 1;
			case Rocket_Launcher:
				return 2;
			case Linear_Fusion_Rifle:
				return 3;
			case Grenade_Launcher:
				return 4;
			case Pulse_Rifle:
			case Fusion_Rifle:
			case Light_Machine_Gun:
			case Shotgun:
			case Sniper_Rifle:
				return 5;
			case Hand_Cannon:
			case Scout_Rifle:
			case Sidearm:
			case Submachine_Gun:
				return 6;
			case Auto_Rifle:
			case Trace_Rifle:
				return 8;
			default:
				return 1;
			}
		}

		@Override
		public RangeBand getRangeBand() {
			switch (this) {
			case Auto_Rifle:
			case Hand_Cannon:
			case Sidearm:
			case Submachine_Gun:
			case Rocket_Launcher:
			case Shotgun:
				return RangeBand.CLOSE;
			case Pulse_Rifle:
			case Scout_Rifle:
			case Trace_Rifle:
			case Combat_Bow:
			case Fusion_Rifle:
			case Grenade_Launcher:
				return RangeBand.MEDIUM;
			case Linear_Fusion_Rifle:
			case Sniper_Rifle:
				return RangeBand.LONG;
			default:
				return null;
			}
		}

		@Override
		public PerkInterface[][] getPerkSlotOptions() {
			switch (this) {
			case Auto_Rifle:
				return new PerkInterface[][] {
						{ Perk.CLOSE_AND_OR_PERSONAL, Perk.EXTENDED_MAGAZINE, Perk.FOCUSED_FIRE },
						{ Perk.APPENDED_MAGAZINE, Perk.LIFE_SUPPORT, Perk.RANGEFINDER, Perk.SMALLBORE, Perk.THIRD_EYE },
						{ Perk.CROWD_CONTROL, Perk.GLASS_HALF_FULL, Perk.PERFECT_BALANCE, Perk.SURROUNDED } };
			case Hand_Cannon:
				return new PerkInterface[][] {
						{ Perk.FOCUSED_FIRE, Perk.HIP_FIRE, Perk.LUCK_IN_THE_CHAMBER }, { Perk.EXTENDED_MAGAZINE,
								Perk.HIGH_CALIBER_ROUNDS, Perk.LIGHTWEIGHT, Perk.SMALLBORE, Perk.SNAPSHOT },
						{ Perk.FIREFLY, Perk.OUTLAW, Perk.TRIPLE_TAP } };
			case Pulse_Rifle:
				return new PerkInterface[][] { { Perk.HEADSEEKER, Perk.MULLIGAN, Perk.SECRET_ROUND },
						{ Perk.HIDDEN_HAND, Perk.HIP_FIRE, Perk.RANGEFINDER, Perk.SMALLBORE },
						{ Perk.GLASS_HALF_FULL, Perk.OUTLAW, Perk.THIRD_EYE } };
			case Scout_Rifle:
				return new PerkInterface[][] { { Perk.FIELD_SCOUT, Perk.HIP_FIRE, Perk.ZEN_MOMENT },
						{ Perk.LIGHTWEIGHT, Perk.RANGEFINDER, Perk.SHOOT_TO_LOOT, Perk.SNAPSHOT, Perk.THIRD_EYE },
						{ Perk.FIREFLY, Perk.MULLIGAN, Perk.REACTIVE_RELOAD, Perk.UNFLINCHING } };
			case Sidearm:
				return new PerkInterface[][] {
						{ Perk.GUERILLA_FIGHTER, Perk.HAMMER_FORGED, Perk.RANGEFINDER, Perk.RESCUE_MAGAZINE },
						{ Perk.BATTLE_RUNNER, Perk.LAST_RESORT, Perk.LIGHTWEIGHT, Perk.SNAPSHOT,
								Perk.STABILIZER_SIGHTS },
						{ Perk.LIFE_SUPPORT, Perk.PERFECT_BALANCE, Perk.REACTIVE_RELOAD, Perk.ZEN_MOMENT } };
			case Submachine_Gun:
				return new PerkInterface[][] { { Perk.EYE_OF_THE_STORM, Perk.HAMMER_FORGED, Perk.HIP_FIRE },
						{ Perk.CLOSE_AND_OR_PERSONAL, Perk.CROWD_CONTROL, Perk.SURROUNDED },
						{ Perk.BATTLE_RUNNER, Perk.LIFE_SUPPORT, Perk.LIGHTWEIGHT } };
			case Trace_Rifle:
				return new PerkInterface[][] {
						{ Perk.APPENDED_MAGAZINE, Perk.ENHANCED_BATTERY, Perk.EXTENDED_MAGAZINE, Perk.SUPERCHARGER },
						{ Perk.FOCUSED_FIRE, Perk.PERSISTENCE, Perk.RANGEFINDER, Perk.REPLENISH },
						{ Perk.HEAVY_DUTY_COILS, Perk.SNAPSHOT, Perk.STABILIZER_SIGHTS, Perk.THIRD_EYE } };
			case Combat_Bow:
				return new PerkInterface[][] { { Perk.FULL_DRAW, Perk.HIP_FIRE, Perk.MULLIGAN },
						{ Perk.FIELD_SCOUT, Perk.GUERILLA_FIGHTER, Perk.LIGHTWEIGHT, Perk.SNAPSHOT,
								Perk.THREAT_DETECTOR_SIGHTS },
						{ Perk.THIRD_EYE, Perk.TRIPLE_TAP, Perk.UNFLINCHING, Perk.ZEN_MOMENT } };
			case Fusion_Rifle:
				return new PerkInterface[][] { { Perk.ACCELERATED_COILS, Perk.ENHANCED_BATTERY, Perk.LIGHTWEIGHT },
						{ Perk.APPENDED_MAGAZINE, Perk.BATTLE_RUNNER, Perk.HEAVY_DUTY_COILS, Perk.RANGEFINDER,
								Perk.SMALLBORE, Perk.SNAPSHOT },
						{ Perk.CASCADE, Perk.HOT_SWAP, Perk.OUTLAW } };
			case Grenade_Launcher:
				return new PerkInterface[][] {
						{ Perk.BLINDING_GRENADES, Perk.CONCUSSION_GRENADES, Perk.HARD_LAUNCH, Perk.RICOCHET_ROUNDS,
								Perk.VOLATILE_LAUNCH },
						{ Perk.FLARED_MAGWELL, Perk.HIP_FIRE, Perk.HOT_SWAP, Perk.SNAPSHOT },
						{ Perk.BATTLE_RUNNER, Perk.CLOSE_AND_OR_PERSONAL, Perk.CROWD_CONTROL, Perk.UNFLINCHING } };
			case Light_Machine_Gun:
				return new PerkInterface[][] { { Perk.ARMY_OF_ONE, Perk.HEADSEEKER, Perk.RANGEFINDER },
						{ Perk.ARMOR_PIERCING_ROUNDS, Perk.EXTENDED_MAGAZINE, Perk.MULLIGAN, Perk.SPRAY_AND_PLAY },
						{ Perk.LAST_RESORT, Perk.LIFE_SUPPORT, Perk.PERFECT_BALANCE, Perk.PERSISTENCE } };
			case Linear_Fusion_Rifle:
				return new PerkInterface[][] {
						{ Perk.HIP_FIRE, Perk.SMALLBORE, Perk.SNAPSHOT }, { Perk.HEAVY_DUTY_COILS, Perk.MULLIGAN,
								Perk.PERFECTIONIST, Perk.RANGEFINDER, Perk.STABILIZER_SIGHTS },
						{ Perk.FINAL_ROUND, Perk.FIREFLY, Perk.HIDDEN_HAND } };
			case Rocket_Launcher:
				return new PerkInterface[][] { { Perk.BLACK_POWDER, Perk.GRENADES_AND_HORSESHOES, Perk.TRACKING },
						{ Perk.BATTLE_RUNNER, Perk.CLOSE_AND_OR_PERSONAL, Perk.HEAVY_PAYLOAD, Perk.TRIPOD },
						{ Perk.CLOWN_CARTRIDGE, Perk.CLUSTER_BOMBS, Perk.SPRAY_AND_PLAY } };
			case Shotgun:
				return new PerkInterface[][] {
						{ Perk.BATTLE_RUNNER, Perk.CLOSE_AND_OR_PERSONAL, Perk.LIFE_SUPPORT, Perk.SHOT_PACKAGE },
						{ Perk.HAMMER_FORGED, Perk.HIGH_CALIBER_ROUNDS, Perk.SMALLBORE },
						{ Perk.CASCADE, Perk.CROWD_CONTROL, Perk.FIELD_SCOUT, Perk.FINAL_ROUND } };
			case Sniper_Rifle:
				return new PerkInterface[][] { { Perk.FIELD_SCOUT, Perk.MULLIGAN, Perk.SNAPSHOT, Perk.TAKE_A_KNEE },
						{ Perk.BOX_BREATHING, Perk.CROWD_CONTROL, Perk.HIGH_CALIBER_ROUNDS, Perk.SHOOT_TO_LOOT,
								Perk.THREAT_DETECTOR_SIGHTS },
						{ Perk.FINAL_ROUND, Perk.FIREFLY, Perk.TRIPLE_TAP } };
			default:
				return null;
			}
		}

		public enum RangeBand implements BaseInterface {
			CLOSE, MEDIUM, LONG;
			@Override
			public String toString() {
				return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
			}
		}
	}

}
