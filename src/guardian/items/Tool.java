package guardian.items;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import guardian.Entity;
import guardian.Main;
import guardian.items.interfaces.ItemInterface;

public class Tool extends Item {
	private JCheckBox proficiency;
	private JCheckBox expertise;

	private Tool(Tools item, Inventory list, int amount, boolean proficiency, boolean expertise, JPanel parentPanel) {
		super(item, list, amount);
		this.proficiency = new JCheckBox("Proficiency");
		this.proficiency.setSelected(proficiency);
		this.expertise = new JCheckBox("Expertise");
		this.expertise.setSelected(expertise);
	}

	@SuppressWarnings("unused")
	private String getDisplayName(Entity entity) {
		if (this.proficiency.isSelected()) {
			if (this.expertise.isSelected())
				return super.getDisplayName() + " (+" + (entity.getProficiencyBonus() * 2) + ")";
			else
				return super.getDisplayName() + " (+" + entity.getProficiencyBonus() + ")";
		} else
			return super.getDisplayName();
	}

	@Override
	protected JPanel display() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel proficiency = new JPanel(new GridLayout(2, 1));
		proficiency.add(this.proficiency);
		proficiency.add(this.expertise);
		this.proficiency.setFont(Main.textFont);
		this.expertise.setFont(Main.textFont);
		panel.add(proficiency, BorderLayout.WEST);
		panel.add(super.display(), BorderLayout.CENTER);
		return panel;
	}

	public enum Tools implements ItemInterface {
		ARMORSMITHING_TOOLS("Armorsmithing Tools"), CAMPING_SET("Camping Set"), MEDICAL_SUPPLIES("Medical Supplies"),
		ALIEN_MEDICAL_SUPPLIES("Alien Medical Supplies"), THIEVES_TOOLS("Thieves' Tools"),
		VEHICLE_TOOLS("Vehicle Tools"), WEAPONSMITHING_TOOLS("Weaponsmithing Tools");

		private final String name;

		Tools(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public int getMemorySize() {
			switch (this) {
			case MEDICAL_SUPPLIES:
			case ALIEN_MEDICAL_SUPPLIES:
				return 3;
			case ARMORSMITHING_TOOLS:
				return 5;
			case CAMPING_SET:
			case THIEVES_TOOLS:
			case WEAPONSMITHING_TOOLS:
				return 10;
			case VEHICLE_TOOLS:
				return 25;
			default:
				return 0;
			}
		}

		@Override
		public double getWeightPerItem() {
			switch (this) {
			case THIEVES_TOOLS:
				return 3;
			case MEDICAL_SUPPLIES:
			case ALIEN_MEDICAL_SUPPLIES:
				return 10;
			case ARMORSMITHING_TOOLS:
				return 25;
			case WEAPONSMITHING_TOOLS:
				return 35;
			case CAMPING_SET:
				return 95;
			case VEHICLE_TOOLS:
				return 150;
			default:
				return 0;
			}
		}

		@Override
		public String getDescription() {
			switch (this) {
			case ARMORSMITHING_TOOLS:
				return "A typical armorsmithing kit is comprised of tools such as a screwdriver set, sewing needles of various sizes, a handheld spot welder, soldering rod, clamps, superglue, and replacement parts for circuit boards. When used in conjunction with armor parts and other resources, these kits are diverse enough that they can fix most armor.";
			case CAMPING_SET:
				return "Comes with a weatherproof sleeping bag, two-person tent, anchoring stakes, tarp, cooking utensils, and a small nest for a Ghost.";
			case MEDICAL_SUPPLIES:
				return "Gauze, antibacterials, general-purpose antibiotics, exo-grade replacement plating, liquid welders, and more fill this handy, travel-sized box with a friendly pair of green snakes on the lid. Most wounds sustained in the field by Humans can be treated with this toolkit.";
			case ALIEN_MEDICAL_SUPPLIES:
				return "Used for treating basic wounds sustained by alien species, from the cabal to the vex. Contains no metal tools due to a very common allergy in psions.";
			case THIEVES_TOOLS:
				return "While this set does include more traditional theieves tools, such as a file, a set of lock picks, and a small mirror mounted on a metal handle, it also includes tools for the modern era, including a tablet loaded with a hacker's API and a bundle of cables for connecting it to almost every device.";
			case VEHICLE_TOOLS:
				return "Often called a portable garage, a vehicle repair toolkit has everything from a basic monkey wrench to a glimmer-made engine hoist. Considered a must for anyone who owns a jumpship or sparrow.";
			case WEAPONSMITHING_TOOLS:
				return "Not too dissimilar from armorsmithing tools, a weaponsmithing kit has one vital difference: a portable CNC machine and matching program for a tablet computer, though Ghosts can often act as a substitute for the program and interface with the CNC machine directly. A CNC machine is particularly required for weaponsmithing because, while armor parts are generally universal despite the vast differences in assembly, Guardian weapons tend to be highly specialized and unique from Guardian to Guardian, and their parts are just as specialized.";
			default:
				return "";
			}
		}

		@Override
		public ImageIcon getIcon(int size, double multiplier) {
			ImageIcon icon;
			switch (this) {
			default:
				icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
			}
			return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
					(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
		}
	}
}
