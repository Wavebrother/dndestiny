package guardian.items;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import guardian.BaseInterface;
import guardian.DamageType;
import guardian.Die;
import guardian.Entity;
import guardian.Main;
import guardian.Stat.Stats;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.interfaces.ArmorInterface;
import guardian.items.interfaces.ItemInterface;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.itemEnums.Armor.ArmorLevel;

public class Vehicle extends Entity {
	Vehicles vehicle;

	Firearm vehicleFirearm;

	Vehicle(Vehicles vehicle) {
		this.vehicle = vehicle;
		if (vehicle.getWeapon() != null)
			this.vehicleFirearm = new Firearm(vehicle.getWeapon(), 0, 0, new PerkInterface[0], null, null, vehicle.getWeapon().getMaxStack(), true, this);
		this.getStats().get(Stats.Strength).value = vehicle.getStrength();
		this.getStats().get(Stats.Dexterity).value = vehicle.getDexterity();
		this.armor = vehicle.getArmor();
		this.setMaxHitPoints(vehicle.getMaxHealth());
	}

	public enum Vehicles implements ItemInterface {
		ARCADIA("Arcadia"), BULKFREIGHTER("Bulk Freighter"), DRAKE("Drake"), HAWK("Hawk"), KESTREL("Kestrel"),
		PHAETON("Phaeton"), REGULUS("Regulus"), S20CAVALIER("S-20 Cavalier"), S20NOMAD("S-20 Nomad"),
		S20SEEKER("S-20 Seeker");
		String name;

		Vehicles(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

		public int getStrength() {
			switch (this) {
			case S20NOMAD:
				return 14;
			case S20SEEKER:
				return 15;
			case S20CAVALIER:
			case KESTREL:
			case HAWK:
				return 16;
			case ARCADIA:
			case DRAKE:
				return 17;
			case PHAETON:
			case BULKFREIGHTER:
				return 18;
			case REGULUS:
				return 20;
			}
			return 0;
		}

		public int getDexterity() {
			switch (this) {
			case BULKFREIGHTER:
				return 10;
			case S20CAVALIER:
				return 12;
			case S20SEEKER:
				return 14;
			case DRAKE:
				return 15;
			case REGULUS:
				return 16;
			case S20NOMAD:
			case PHAETON:
			case HAWK:
				return 18;
			case ARCADIA:
				return 20;
			case KESTREL:
				return 22;
			}
			return 0;
		}

		public int getMaxHealth() {
			switch (this) {
			case S20NOMAD:
				return 20;
			case S20SEEKER:
				return 25;
			case S20CAVALIER:
				return 30;
			case BULKFREIGHTER:
				return 75;
			case KESTREL:
			case HAWK:
				return 180;
			case ARCADIA:
			case PHAETON:
			case REGULUS:
				return 200;
			case DRAKE:
				return 220;
			}
			return 0;
		}

		public ArmorInterface getArmor() {
			return new ArmorInterface() {
				@Override
				public String toString() {
					return Vehicles.this.toString();
				}

				@Override
				public String name() {
					return toString();
				}

				@Override
				public String getDescription() {
					return Vehicles.this.getDescription();
				}

				@Override
				public boolean isStealth() {
					return false;
				}

				@Override
				public int getMinStr() {
					return 0;
				}

				@Override
				public int getMaxAC() {
					return 0;
				}

				@Override
				public int getBaseAC() {
					switch (Vehicles.this) {
					case S20NOMAD:
						return 12;
					case S20SEEKER:
						return 13;
					case S20CAVALIER:
						return 14;
					case BULKFREIGHTER:
						return 15;
					case ARCADIA:
					case KESTREL:
					case DRAKE:
					case HAWK:
						return 16;
					case PHAETON:
						return 17;
					case REGULUS:
						return 18;
					default:
						return 0;
					}
				}

				@Override
				public ArmorLevel getLevel() {
					return ArmorLevel.VEHICLE;
				}

				@Override
				public boolean isGuardianArmor() {
					return false;
				}
			};
		}

		@Override
		public String getDescription() {
			switch (this.getType()) {
			case SPARROW:
				return "Quick, lightweight, and relatively inconspicuous, sparrows are the bread-and-butter of Guardian ground transportation. They�re small enough that they can be transmatted, but too large to store in a Ghost�s personal memory.\r\n"
						+ "Boost. Sparrow drivers can double the speed of their sparrow on their turn. If they do, they have disadvantage on all vehicle Dexterity checks and saving throws, and advantage on all vehicle Strength checks and saving throws.\r\n"
						+ "Lightweight. Sparrows measure their carrying capacity in pounds, rather than tonnes.";
			case JUMPSHIP:
				return "An expensive and precious commodity, jumpships are the fastest form of in-atmosphere travel and, should they be upgraded with a near-lightspeed drive (NLS drive), give Guardians access to the stars.\r\n"
						+ "Driver Cover. Jumpships provide full, complete cover for its driver and all occupants.\r\n"
						+ "Space-grade hull. Jumpships with at least half their HP remaining can transport creatures and objects safely through the vacuum of space.\r\n"
						+ "VTOL. A jumpship is capable of making vertical take-offs and landings.";
			case BULKFREIGHTER:
				return "Also called pilgrim cars, bulk freighters are large hovercraft used for transporting materials or personnel over land. They are slow but heavily armored, easy to make, and can be linked together to form massive trains.\r\n"
						+ "Driver Cover. Bulk freighters provide full, complete cover for their driver and all occupants.\r\n"
						+ "Heavyweight. Bulk freighters count as one size large when determining what they can carry, lift, push, or pull.";
			case DRAKE:
				return "Drakes are heavily armored all-terrain tanks. They can fit a single occupant, the driver, who has access to the powerful main cannon and rocket launchers with both dumbfire and targeting modes. \r\n"
						+ "Attack. The drake is armed with a heavy cannon and missile swarm. \r\n"
						+ "Driver Cover. Drakes provide full, complete cover for their driver and all occupants.\r\n"
						+ "Targeting Mode. If the drake is Aiming, creatures have disadvantage on their saving throws against its missile swarm.";
			case HAWK:
				return "A hawk is a troop carrier aircraft that is used by the Vanguard and The Last City denizens alike. They are typically used to make cargo deliveries across the City, or to transport Guardians to their patrol zones/Crucible matches, if they have no jumpships of their own.\r\n"
						+ "Attack. The hawk is armed with a heavy machine gun.\r\n"
						+ "Driver Cover. Hawks provide full, complete cover for its driver and all occupants.\r\n"
						+ "Heavyweight. The hawk counts as one size larger when determining what it can carry, lift, push, or pull.\r\n"
						+ "Space-grade hull. Hawks with at least half their HP remaining can transport creatures and objects safely through the vacuum of space.\r\n"
						+ "VTOL. A hawk is capable of making vertical take-offs and landings.";
			}
			return "How?";
		}

		@SuppressWarnings("unused")
		private int getSpeed() {
			switch (this) {
			case S20CAVALIER:
				return 40;
			case S20NOMAD:
				return 50;
			case S20SEEKER:
				return 45;
			case ARCADIA:
				return 820;
			case KESTREL:
				return 9000;
			case PHAETON:
				return 750;
			case REGULUS:
				return 700;
			case BULKFREIGHTER:
				return 25;
			case DRAKE:
				return 35;
			case HAWK:
				return 600;
			}
			return 0;
		}

		@SuppressWarnings("unused")
		private ConstructionSize getConstructionSize() {
			switch (this) {
			case S20CAVALIER:
			case S20NOMAD:
			case S20SEEKER:
				return ConstructionSize.Medium;
			case ARCADIA:
			case KESTREL:
			case PHAETON:
			case REGULUS:
			case DRAKE:
			case HAWK:
				return ConstructionSize.Giant;
			case BULKFREIGHTER:
				return ConstructionSize.Large;
			}
			return null;
		}

		@SuppressWarnings("unused")
		private int getDamageSoak() {
			switch (this) {
			case S20CAVALIER:
				return 6;
			case S20NOMAD:
				return 4;
			case S20SEEKER:
				return 5;
			case ARCADIA:
				return 20;
			case KESTREL:
				return 15;
			case PHAETON:
				return 20;
			case REGULUS:
				return 30;
			case BULKFREIGHTER:
				return 18;
			case DRAKE:
				return 12;
			case HAWK:
				return 20;
			}
			return 0;
		}

		@Override
		public ImageIcon getIcon(int size, double multiplier) {
			ImageIcon icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
			return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
					(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
		}

		private VehicleType getType() {
			switch (this) {
			case S20CAVALIER:
			case S20NOMAD:
			case S20SEEKER:
				return VehicleType.SPARROW;
			case ARCADIA:
			case KESTREL:
			case PHAETON:
			case REGULUS:
				return VehicleType.JUMPSHIP;
			case BULKFREIGHTER:
				return VehicleType.BULKFREIGHTER;
			case DRAKE:
				return VehicleType.DRAKE;
			case HAWK:
				return VehicleType.HAWK;
			}
			return null;
		}

		private VehicleFirearms getWeapon() {
			switch (this) {
			case DRAKE:
				return VehicleFirearms.MISSILESWARM;
			default:
				break;
			}
			return null;
		}

		public enum VehicleType implements BaseInterface {
			SPARROW, JUMPSHIP, BULKFREIGHTER, DRAKE, HAWK;

			@Override
			public String toString() {
				if (this == BULKFREIGHTER)
					return "Bulk Freighter";
				return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase() + "s";
			}
		}

		private enum ConstructionSize {
			Tiny, Small, Medium, Large, Huge, Giant, Massive, Titanic, Gargantuan, Cosmic
		}

		private enum VehicleFirearms implements WeaponInterface {
			HEAVYMACHINEGUN, HOMINGMISSILE, MISSILESWARM;
			private final ArrayList<ArrayList<PerkInterface>> perkSlotOptions = new ArrayList<ArrayList<PerkInterface>>();

			@Override
			public String toString() {
				switch (this) {
				case HEAVYMACHINEGUN:
					return "Heavy Machine Gun";
				case HOMINGMISSILE:
					return "Homing Missile";
				case MISSILESWARM:
					return "Missile Swarm";
				}
				return "";
			}

			@Override
			public String getDescription() {
				return "";
			}

			@Override
			public int getMemorySize() {
				return 0;
			}

			@Override
			public double getWeightPerItem() {
				return 0;
			}

			@Override
			public ImageIcon getIcon(int size, double multiplier) {
				ImageIcon icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
				return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
						(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()),
						Image.SCALE_SMOOTH));
			}

			@Override
			public int[] getRange() {
				switch (this) {
				case HEAVYMACHINEGUN:
					return new int[] { 200, 2000, 5000 };
				case HOMINGMISSILE:
					return new int[] { 0, 5000, 10000 };
				case MISSILESWARM:
					return new int[] { 80, 200, 500 };
				}
				return null;
			}

			@Override
			public String getRangeInfo() {
				return "Range: " + this.getRange()[0] + ", " + this.getRange()[1] + ", " + this.getRange()[2];
			}

			@Override
			public boolean isFinesse() {
				return false;
			}

			@Override
			public boolean isRanged() {
				return true;
			}

			@Override
			public boolean isThrown() {
				return false;
			}

			@Override
			public DamageType getDamageType() {
				return DamageType.KINETIC;
			}

			@Override
			public WeaponType getWeaponType() {
				return WeaponType.SIMPLE_RANGED;
			}

			@Override
			public Die getDamageDie() {
				return Die.d12;
			}

			@Override
			public int getDamageDieNumber() {
				return 1;
			}

			@Override
			public ArrayList<WeaponProperties> getProperties() {
				ArrayList<WeaponProperties> properties = new ArrayList<WeaponProperties>();
				properties.add(WeaponProperties.HIGH_CALIBER_ROUNDS);
				properties.add(WeaponProperties.MOUNTED);
				return properties;
			}

			@Override
			public int getMaxStack() {
				switch (this) {
				case HEAVYMACHINEGUN:
					return 10;
				case HOMINGMISSILE:
					return 1;
				case MISSILESWARM:
					return 4;
				}
				return 1;
			}

			@Override
			public RangeBand getRangeBand() {
				switch (this) {
				case HEAVYMACHINEGUN:
					return RangeBand.MEDIUM;
				case HOMINGMISSILE:
					break;
				case MISSILESWARM:
					break;
				}
				return null;
			}

			@SuppressWarnings("unused")
			public ArrayList<ArrayList<PerkInterface>> getPerkSlotOptions() {
				return perkSlotOptions;
			}
		}
	}
}
