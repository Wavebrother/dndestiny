package guardian.items;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.DamageType;
import guardian.Die;
import guardian.Guardian;
import guardian.Main;
import guardian.SearchableComboBox;
import guardian.items.Firearm.Firearms;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Weapon.Weapons;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.interfaces.PerkInterface;
import guardian.items.interfaces.WeaponInterface;

public class NewWeapon extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Guardian character;

	private SearchableComboBox weaponName = new SearchableComboBox();
	private JCheckBox custom = new JCheckBox("Custom Weapon");
	private JTextField nameField = new JTextField();
	private JSpinner maxStack = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
	private JSpinner weightPerItem = new JSpinner(new SpinnerNumberModel(1, 0, 100, .1));
	private JSpinner memorySize = new JSpinner(new SpinnerNumberModel(1, 1, 20, 1));
	private JCheckBox firearm = new JCheckBox("Firearm");
	private JSpinner closeRange = new JSpinner(new SpinnerNumberModel(10, 0, 1000, 10));
	private JSpinner midRange = new JSpinner(new SpinnerNumberModel(10, 0, 10000, 10));
	private JSpinner farRange = new JSpinner(new SpinnerNumberModel(10, 0, 10000, 10));
	private SearchableComboBox rangeBand = new SearchableComboBox();
	private SearchableComboBox damageType = new SearchableComboBox();
	private SearchableComboBox weaponType = new SearchableComboBox();
	private JComboBox<Die> damageDie = new JComboBox<Die>(Die.weaponValues());
	private JSpinner damageDieNumber = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
	private JSpinner shots = new JSpinner(new SpinnerNumberModel(1, 1, 15, 1));
	private JCheckBox finesse = new JCheckBox("Finesse");
	private JCheckBox thrown = new JCheckBox("Thrown");
	private JSpinner amount = new JSpinner(new SpinnerNumberModel(1, 1, 1, 1));
	private JSpinner magicalBonus = new JSpinner(new SpinnerNumberModel(0, 0, 10, 1));
	private JCheckBox proficiency = new JCheckBox("Proficiency");

	private JSpinner weaponTier = new JSpinner(new SpinnerNumberModel(0, 0, 3, 1));
	private SearchableComboBox[] perks = { new SearchableComboBox().actionListener(this),
			new SearchableComboBox().actionListener(this), new SearchableComboBox().actionListener(this) };
	private NewPerk customPerk = new NewPerk(perks);

	private JPanel firearmPanel = new JPanel();
	private JPanel perksPanel = new JPanel(new GridLayout(0, 1));
	private JPanel contentPanel = new JPanel(new BorderLayout());

	private JButton create = new JButton("Create Weapon");
	public JButton open = new JButton("New Weapon");

	public NewWeapon(Guardian character) {
		super(Main.character);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				create.doClick();
			}
		});
		this.open.setFont(Main.titleFont);
		this.open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setLocationRelativeTo(open);
				setVisible(true);
			}
		});
		this.setTitle("New Weapon");
		this.character = character;

		this.create.addActionListener(this);
		this.create.setFont(Main.titleFont);

		this.setLayout(new BorderLayout());

		WeaponInterface[] weapons = new WeaponInterface[Weapons.getWeapons().size() + Firearms.getFirearms().size()];
		for (int i = 0; i < Weapons.getWeapons().size() + Firearms.getFirearms().size(); i++) {
			if (i < Weapons.getWeapons().size())
				weapons[i] = (WeaponInterface) Weapons.getWeapons().values().toArray()[i];
			else
				weapons[i] = (WeaponInterface) Firearms.getFirearms().values().toArray()[i
						- Weapons.getWeapons().size()];
		}
		this.weaponName.setModel(weapons);
		this.weaponName.actionListener(this);
		JPanel weaponPane = new JPanel(new GridLayout(0, 2));
		JPanel weaponNamePane = new JPanel(new GridLayout());
		weaponNamePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		weaponNamePane.add(this.weaponName);

		JPanel namePane = new JPanel(new GridLayout());
		namePane.add(nameField);
		namePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel closeRangePane = new JPanel(new GridLayout());
		closeRangePane.add(closeRange);
		closeRangePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Close Range", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel midRangePane = new JPanel(new GridLayout());
		midRangePane.add(midRange);
		midRangePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Mid Range", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel farRangePane = new JPanel(new GridLayout());
		farRangePane.add(farRange);
		farRangePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Far Range", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel rangeBandPane = new JPanel(new GridLayout());
		rangeBandPane.add(rangeBand);
		rangeBandPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Range Band", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		rangeBand.setModel(RangeBand.values());

		JPanel damageTypePane = new JPanel(new GridLayout());
		damageTypePane.add(damageType);
		damageTypePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Damage Type", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		damageType.setModel(DamageType.values());

		JPanel weaponTypePane = new JPanel(new GridLayout());
		weaponTypePane.add(weaponType);
		weaponTypePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Weapon Type", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		weaponType.setModel(WeaponType.values());

		JPanel damageDiePane = new JPanel(new GridLayout());
		damageDiePane.add(damageDie);
		damageDiePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Damage Die", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel damageDieNumberPane = new JPanel(new GridLayout());
		damageDieNumberPane.add(damageDieNumber);
		damageDieNumberPane.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Damage Die Number",
						TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel shotsPane = new JPanel(new GridLayout());
		shotsPane.add(shots);
		shotsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Shots",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel maxStackPane = new JPanel(new GridLayout());
		maxStackPane.add(maxStack);
		maxStack.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if ((int) maxStack.getValue() > (int) amount.getValue())
					amount.setModel(new SpinnerNumberModel((int) amount.getValue(), 1, (int) maxStack.getValue(), 1));
				else
					amount.setModel(new SpinnerNumberModel((int) maxStack.getValue(), 1, (int) maxStack.getValue(), 1));
			}
		});
		maxStackPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Max Stack Size", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel weightPerItemPane = new JPanel(new GridLayout());
		weightPerItemPane.add(weightPerItem);
		weightPerItemPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Weight Per Item", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel memorySizePane = new JPanel(new GridLayout());
		memorySizePane.add(memorySize);
		memorySizePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Memory Size", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		this.firearm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				perksPanel.removeAll();
				if (firearm.isSelected()) {
					weaponType.setModel(Firearms.values());
					weaponPane.remove(maxStackPane);
					weaponPane.add(customPerk.open(), 11);
					weaponPane.add(shotsPane, 7);
					weaponPane.add(rangeBandPane, 3);
					weaponPane.add(midRangePane, 2);
					for (int i = 0; i < 3; i++) {
						perks[i].setModel(PerkInterface.perks.values().toArray(new PerkInterface[0]));
						if (i < (int) weaponTier.getValue())
							perksPanel.add(perks[i]);
						if ((int) weaponTier.getValue() > 0)
							firearmPanel.add(perksPanel);
					}
				} else {
					weaponType.setModel(WeaponType.values());
					weaponPane.remove(customPerk.open);
					weaponPane.remove(shotsPane);
					weaponPane.remove(rangeBandPane);
					weaponPane.remove(midRangePane);
					weaponPane.add(maxStackPane, 7);
				}
				getContentPane().revalidate();
				pack();
				getContentPane().repaint();
			}
		});
		this.firearm.addActionListener(this);

		JPanel amountPane = new JPanel(new GridLayout());
		amountPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Amount",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		amountPane.add(this.amount);

		JPanel magicalPane = new JPanel(new GridLayout());
		magicalPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Magical Bonus", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		magicalPane.add(this.magicalBonus);

		weaponPane.add(weaponNamePane);
		weaponPane.add(amountPane);
		weaponPane.add(magicalPane);
		weaponPane.add(proficiency);

		JPanel weaponTierPane = new JPanel(new GridLayout());
		weaponTierPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Weapon Tier", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		weaponTierPane.add(this.weaponTier);
		this.weaponTier.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				actionPerformed(new ActionEvent(weaponName, ActionEvent.ACTION_PERFORMED, "weaponTierChanged"));
				if ((int) weaponTier.getValue() > 0)
					firearmPanel.add(perksPanel);
				else
					firearmPanel.remove(perksPanel);
				getContentPane().revalidate();
				pack();
				getContentPane().repaint();
			}
		});

		perksPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Weapon Perks", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		firearmPanel.setLayout(new BoxLayout(firearmPanel, BoxLayout.Y_AXIS));
		firearmPanel.add(weaponTierPane);
		contentPanel.add(weaponPane, BorderLayout.CENTER);

		JPanel checkBoxes = new JPanel(new GridLayout(1, 0));
		checkBoxes.add(custom);

		this.custom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (custom.isSelected()) {
					weaponPane.remove(weaponNamePane);
					checkBoxes.add(firearm);
					weaponPane.add(namePane, 0);
					weaponPane.add(closeRangePane, 1);
					weaponPane.add(farRangePane, 2);
					weaponPane.add(damageTypePane, 3);
					weaponPane.add(weaponTypePane, 4);
					weaponPane.add(damageDiePane, 5);
					weaponPane.add(damageDieNumberPane, 6);
					weaponPane.add(maxStackPane, 7);
					weaponPane.add(weightPerItemPane, 8);
					weaponPane.add(memorySizePane, 9);
					if (firearm.isSelected()) {
						weaponPane.add(customPerk.open(), 11);
						weaponPane.add(shotsPane, 7);
						weaponPane.add(rangeBandPane, 3);
						weaponPane.add(midRangePane, 2);
						weaponPane.remove(maxStackPane);
					}
				} else {
					checkBoxes.remove(firearm);
					weaponPane.remove(namePane);
					weaponPane.remove(closeRangePane);
					weaponPane.remove(farRangePane);
					weaponPane.remove(damageTypePane);
					weaponPane.remove(weaponTypePane);
					weaponPane.remove(damageDiePane);
					weaponPane.remove(damageDieNumberPane);
					weaponPane.remove(maxStackPane);
					weaponPane.remove(weightPerItemPane);
					weaponPane.remove(memorySizePane);
					weaponPane.remove(customPerk.open);
					weaponPane.remove(shotsPane);
					weaponPane.remove(rangeBandPane);
					weaponPane.remove(midRangePane);
					weaponPane.add(weaponNamePane, 0);
				}
				amount.setValue(1);
				getContentPane().revalidate();
				pack();
				getContentPane().repaint();
			}

		});

		this.add(checkBoxes, BorderLayout.NORTH);
		this.add(contentPanel, BorderLayout.CENTER);
		this.add(this.create, BorderLayout.SOUTH);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setMinimumSize(new Dimension(this.getHeight(), this.getHeight()));
		this.setMinimumSize(this.getSize());
		this.setResizable(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.create) {
			try {
				if (this.weaponName.getSelectedIndex() != 0
						|| (custom.isSelected() && !nameField.getText().isEmpty())) {
					Weapon weapon = null;
					DamageType damageType = DamageType.KINETIC;
					if (this.damageType.getSelectedIndex() > 0)
						damageType = (DamageType) this.damageType.getSelectedItem();
					if (this.weaponName.getSelectedItem() instanceof Firearms
							|| (firearm.isSelected() && custom.isSelected())) {
						PerkInterface[] perks = new PerkInterface[3];
						for (int i = 0; i < (int) this.weaponTier.getValue(); i++) {
							if (this.perks[i].getSelectedItem() instanceof PerkInterface)
								perks[i] = (PerkInterface) this.perks[i].getSelectedItem();
						}
						if (firearm.isSelected()) {
							Firearms base = Firearms.Auto_Rifle;
							if (this.weaponType.getSelectedIndex() > 0)
								base = (Firearms) this.weaponType.getSelectedItem();
							RangeBand rangeBand = RangeBand.CLOSE;
							if (this.rangeBand.getSelectedIndex() > 0)
								rangeBand = (RangeBand) this.rangeBand.getSelectedItem();
							weapon = new Firearm(
									new CustomFirearm(nameField.getText(), (double) weightPerItem.getValue(),
											(int) memorySize.getValue(), (int) closeRange.getValue(),
											(int) this.midRange.getValue(), (int) farRange.getValue(), rangeBand,
											finesse.isSelected(), damageType, base, (Die) damageDie.getSelectedItem(),
											(int) damageDieNumber.getValue(), (int) shots.getValue(),
											new ArrayList<WeaponProperties>()),
									(int) this.magicalBonus.getValue(), (int) weaponTier.getValue(), perks,
									this.character.getWeapons(), character.getInventory(), (int) shots.getValue(),
									proficiency.isSelected(), this.character);
						} else {
							weapon = new Firearm((WeaponInterface) this.weaponName.getSelectedItem(),
									(int) this.magicalBonus.getValue(), (int) weaponTier.getValue(), perks,
									this.character.getWeapons(), character.getInventory(), (int) shots.getValue(),
									proficiency.isSelected(), this.character);
						}
					} else if (this.custom.isSelected() && !this.firearm.isSelected()) {
						WeaponType weaponType = WeaponType.SIMPLE_MELEE;
						if (this.weaponType.getSelectedIndex() > 0)
							weaponType = (WeaponType) this.weaponType.getSelectedItem();
						weapon = new Weapon(
								new CustomWeapon(nameField.getText(), (double) weightPerItem.getValue(),
										(int) memorySize.getValue(), (int) maxStack.getValue(),
										(int) closeRange.getValue(), (int) farRange.getValue(), finesse.isSelected(),
										thrown.isSelected(), damageType, weaponType, (Die) damageDie.getSelectedItem(),
										(int) damageDieNumber.getValue(), new ArrayList<WeaponProperties>()),
								(int) this.magicalBonus.getValue(), this.character.getWeapons(),
								character.getInventory(), (int) this.amount.getValue(), proficiency.isSelected(),
								this.character);
					} else if (this.weaponName.getSelectedItem() instanceof Weapons) {
						weapon = new Weapon((WeaponInterface) this.weaponName.getSelectedItem(),
								(int) this.magicalBonus.getValue(), this.character.getWeapons(),
								character.getInventory(), (int) this.amount.getValue(), proficiency.isSelected(),
								this.character);
					}
					if (Main.character != null) {
						Main.character.weaponNamePane.add(weapon.getName(Main.character.weaponNamePane));
						Main.character.weaponAmountPane.add(weapon.getAmount(Main.character.weaponAmountPane));
						Main.character.weaponAttackBonusPane
								.add(weapon.getAttack(Main.character.weaponAttackBonusPane));
						Main.character.weaponDamagePane.add(weapon.getDamage(Main.character.weaponDamagePane));
						Main.character.weaponButtonPane.add(weapon.getButtons(Main.character.weaponButtonPane));
						Main.character.getContentPane().revalidate();
						Main.character.getContentPane().repaint();
					}
				}
			} catch (RuntimeException error) {
				File crash = new File(
						"crashLogs\\" + Main.version.toString() + "\\" + System.currentTimeMillis() + "crash.log");
				crash.getParentFile().mkdirs();
				try (PrintStream ps = new PrintStream(crash)) {
					error.printStackTrace(ps);
				} catch (FileNotFoundException crashError) {
					crashError.printStackTrace();
				}
			}
			this.setVisible(false);
		} else if (e.getSource() == this.weaponName || e.getSource() == firearm) {
			if (this.weaponName.getSelectedItem() instanceof Firearms
					|| (firearm.isSelected() && custom.isSelected())) {
				perksPanel.removeAll();
				for (int i = 0; i < 3; i++) {
					if (!custom.isSelected()) {
						PerkInterface[] perks = new PerkInterface[((Firearms) this.weaponName.getSelectedItem())
								.getPerkSlotOptions()[i].length];
						for (int j = 0; j < perks.length; j++)
							perks[j] = ((Firearms) this.weaponName.getSelectedItem()).getPerkSlotOptions()[i][j];
						this.perks[i].setModel(((Firearms) this.weaponName.getSelectedItem()).getPerkSlotOptions()[i]);
					} else
						this.perks[i].setModel(PerkInterface.perks.values().toArray(new PerkInterface[0]));
					if (i < (int) weaponTier.getValue())
						perksPanel.add(this.perks[i]);
				}
				if ((int) weaponTier.getValue() > 0)
					firearmPanel.add(perksPanel);
				contentPanel.add(firearmPanel, BorderLayout.SOUTH);
			} else {
				firearmPanel.remove(perksPanel);
				contentPanel.remove(firearmPanel);
			}
			if (this.weaponName.getSelectedIndex() != 0)
				this.amount.setModel(new SpinnerNumberModel(1, 1,
						((WeaponInterface) this.weaponName.getSelectedItem()).getMaxStack(), 1));
		}
		this.pack();
		this.getContentPane().revalidate();
		this.getContentPane().repaint();
	}
}
