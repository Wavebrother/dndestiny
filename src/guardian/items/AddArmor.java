package guardian.items;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

import guardian.BaseInterface;
import guardian.Entity;
import guardian.Main;
import guardian.SearchableComboBox;
import guardian.items.interfaces.ArmorInterface;
import guardian.items.itemEnums.Armor;
import guardian.items.itemEnums.Armor.ArmorLevel;

public class AddArmor extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;

	private Entity entity;
	private SearchableComboBox items = new SearchableComboBox();
	private JCheckBox custom = new JCheckBox("Custom Armor");
	private JTextField name = new JTextField();
	private JTextField description = new JTextField();
	private JComboBox<ArmorLevel> armorLevels = new JComboBox<ArmorLevel>();
	private JSpinner baseAC = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
	private JSpinner maxAC = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
	private JSpinner minStr = new JSpinner(new SpinnerNumberModel(1, 1, 1000, 1));
	private JCheckBox stealth = new JCheckBox("Stealth Disadvantage");
	private JSpinner weight = new JSpinner(new SpinnerNumberModel(1, 0, 100, .1));
	private JSpinner memorySize = new JSpinner(new SpinnerNumberModel(1, 1, 20, 1));

	private JButton open = new JButton();
	private JButton done = new JButton("Done");

	public AddArmor(Entity entity, boolean guardian) {
		this.entity = entity;
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent closeEvent) {
				done.doClick();
			}
		});
		if (guardian)
			this.items.setModel(Armor.getGuardianArmor().values().toArray(new Armor[0]));
		else
			this.items.setModel(Armor.getGhostArmor().values().toArray(new Armor[0]));
		this.items.actionListener(this);

		this.done.addActionListener(this);
		open.setText(entity.armor.toString() + " (" + entity.getArmorClass() + ")");
		open.setFont(Main.titleFont);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(true);
			}
		});

		JPanel namePanel = new JPanel(new GridLayout());
		namePanel.add(name);
		namePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel descriptionPanel = new JPanel(new GridLayout());
		descriptionPanel.add(description);
		descriptionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Description", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel baseACPanel = new JPanel(new GridLayout());
		baseACPanel.add(baseAC);
		baseACPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Base Armor Class", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		Armor randomExample = Armor.values()[new Random().nextInt(Armor.values().length)];
		baseAC.setToolTipText("What is the armor class without the deterity modifier added? Ex: " + randomExample + ": "
				+ randomExample.getBaseAC());

		JPanel maxACPanel = new JPanel(new GridLayout());
		maxACPanel.add(maxAC);
		maxACPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Max Armor Class", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		Armor maxACExample = randomExample;
		if (maxACExample.getMaxAC() == 0)
			maxACExample = Armor.SPINWIRE;
		maxAC.setToolTipText("What is the maximum armor class with the deterity modifier added? Ex: " + maxACExample
				+ ": " + maxACExample.getMaxAC() + " Set this to 0 if there is no maximum.");

		JPanel minStrPanel = new JPanel(new GridLayout());
		minStrPanel.add(minStr);
		minStrPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Minimum Strength", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		Armor minStrExample = randomExample;
		if (minStrExample.getMinStr() == 0)
			minStrExample = Armor.FORTIFIED;
		minStr.setToolTipText("What is the minimum strength required for this armor? Ex: " + minStrExample + ": "
				+ minStrExample.getMaxAC() + " Set this to 0 if there is no minimum.");

		JPanel weightPanel = new JPanel(new GridLayout());
		weightPanel.add(weight);
		weightPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Weight",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		JPanel memorySizePanel = new JPanel(new GridLayout());
		memorySizePanel.add(memorySize);
		memorySizePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED),
				"Memory Size", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));

		this.custom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (custom.isSelected()) {
					AddArmor.this.remove(items);
					AddArmor.this.add(namePanel, 1);
					AddArmor.this.add(descriptionPanel, 2);
					AddArmor.this.add(baseACPanel, 3);
					AddArmor.this.add(maxACPanel, 4);
					AddArmor.this.add(minStrPanel, 5);
					AddArmor.this.add(weightPanel, 6);
					AddArmor.this.add(memorySizePanel, 7);
				} else {
					AddArmor.this.add(items, 1);
					AddArmor.this.remove(namePanel);
					AddArmor.this.remove(descriptionPanel);
					AddArmor.this.remove(baseACPanel);
					AddArmor.this.remove(maxACPanel);
					AddArmor.this.remove(minStrPanel);
					AddArmor.this.remove(weightPanel);
					AddArmor.this.remove(memorySizePanel);
				}
				AddArmor.this.revalidate();
				AddArmor.this.pack();
				AddArmor.this.repaint();
			}

		});
		this.setLayout(new GridLayout(0, 1));
		this.add(this.custom);
		this.add(this.items);
		this.add(this.done);

		this.setIconImage(new ImageIcon(Main.logo).getImage());
		this.pack();
		this.setMinimumSize(this.getSize());
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setLocationRelativeTo(null);
		this.setVisible(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
	}

	public JButton open() {
		return open;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.done) {
			if (!custom.isSelected()) {
				ArmorInterface item;
				if (this.items.getSelectedItem() instanceof ArmorInterface
						&& !(item = (ArmorInterface) this.items.getSelectedItem()).toString().isEmpty()) {
					entity.armor = item;
				}
				this.items.setModel(new DefaultComboBoxModel<BaseInterface>(this.items.items));
				this.items.setSelectedIndex(0);
				this.items.showPopup();
			} else if (!name.getText().isEmpty()) {
				entity.armor = new CustomArmor(name.getText(), description.getText(),
						(ArmorLevel) armorLevels.getSelectedItem(), (int) baseAC.getValue(), (int) maxAC.getValue(),
						(int) minStr.getValue(), (int) memorySize.getValue(), (double) weight.getValue(),
						!stealth.isSelected(), new ImageIcon());
				name.setText("");
				description.setText("");
				baseAC.setValue(1);
				maxAC.setValue(1);
				minStr.setValue(1);
				weight.setValue(1);
				memorySize.setValue(1);
			}
			open.setText(entity.armor.toString() + " (" + entity.getArmorClass() + ")");
			open.repaint();
			this.setVisible(false);
		}
	}

}
