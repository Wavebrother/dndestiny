package guardian.items;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import guardian.DamageType;
import guardian.Die;
import guardian.Main;
import guardian.items.Firearm.Firearms.RangeBand;
import guardian.items.Weapon.Weapons.WeaponType;
import guardian.items.interfaces.WeaponInterface;

public class CustomWeapon implements WeaponInterface {
	private String name;
	private int memorySize;
	private double weightPerItem;
	private int maxStack;
	private String description;

	private int[] range = new int[3];
	private boolean finesse;
	private boolean thrown;
	private DamageType damageType;
	private WeaponType weaponType;
	private Die damageDie;
	private int damageDieNumber;
	private ArrayList<WeaponProperties> properties;

	public CustomWeapon(String name, double weightPerItem, int memorySize, int maxStack, int closeRange, int farRange, boolean finesse,
			boolean thrown, DamageType damageType, WeaponType weaponType, Die damageDie, int damageDieNumber,
			ArrayList<WeaponProperties> properties) {
		this.name = name;
		this.range[0] = closeRange;
		this.range[2] = farRange;
		this.finesse = finesse;
		this.thrown = thrown;
		this.damageType = damageType;
		this.damageDie = damageDie;
		this.damageDieNumber = damageDieNumber;
		this.properties = properties;
		this.weaponType = weaponType;
		this.memorySize = memorySize;
		this.weightPerItem = weightPerItem;
		this.maxStack = maxStack;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public int getMemorySize() {
		return memorySize;
	}

	@Override
	public double getWeightPerItem() {
		return weightPerItem;
	}

	@Override
	public ImageIcon getIcon(int size, double multiplier) {
		ImageIcon icon = new ImageIcon(Main.class.getResource("/resources/logo.png"));
		return new ImageIcon(icon.getImage().getScaledInstance((int) ((multiplier * size) - 4),
				(int) ((multiplier * size - 4) * icon.getIconHeight() / icon.getIconWidth()), Image.SCALE_SMOOTH));
	}

	@Override
	public int[] getRange() {
		return this.range;
	}

	@Override
	public String getRangeInfo() {
		return "Range: " + this.getRange()[0] + ", " + this.getRange()[2];
	}

	@Override
	public boolean isFinesse() {
		return finesse;
	}

	@Override
	public boolean isThrown() {
		return thrown;
	}

	@Override
	public DamageType getDamageType() {
		return damageType;
	}

	@Override
	public WeaponType getWeaponType() {
		return weaponType;
	}

	@Override
	public Die getDamageDie() {
		return damageDie;
	}

	@Override
	public int getDamageDieNumber() {
		return damageDieNumber;
	}

	@Override
	public ArrayList<WeaponProperties> getProperties() {
		return properties;
	}

	@Override
	public RangeBand getRangeBand() {
		return null;
	}

	@Override
	public int getMaxStack() {
		return maxStack;
	}
}
