package guardian;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Info extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	JTextArea info;
	private ArrayList<Info> list;
	private JPanel parentPanel = new JPanel();
	private JButton delete = new JButton("X");
	private JButton moveUp = new JButton("\u2191");
	private JButton moveDown = new JButton("\u2193");

	Info(JTextArea area, ArrayList<Info> list) {
		super(new BorderLayout());
		this.info = new JTextArea(area.getText());
		this.info.setMargin(new Insets(3, 5, 5, 5));
		this.list = list;
		this.setBorder(BorderFactory.createLineBorder(Color.RED));
		this.delete.addActionListener(this);
		this.delete.setForeground(Color.RED);
		this.moveUp.addActionListener(this);
		this.moveUp.setFont(new Font(this.moveUp.getFont().getName(), this.moveUp.getFont().getStyle(),
				(this.moveUp.getFont().getSize() * 3) / 4));
		this.moveDown.addActionListener(this);
		this.moveDown.setFont(new Font(this.moveDown.getFont().getName(), this.moveDown.getFont().getStyle(),
				(this.moveDown.getFont().getSize() * 3) / 4));
		JPanel movePanel = new JPanel(new GridLayout(0, 1));
		movePanel.add(this.moveUp);
		movePanel.add(this.moveDown);
		JPanel buttonPanel = new JPanel(new GridLayout());
		buttonPanel.add(this.delete);
		buttonPanel.add(movePanel);
		this.add(this.info, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.EAST);
		this.info.setLineWrap(true);
		this.info.setWrapStyleWord(true);
	}

	JPanel display(JPanel panel) {
		this.parentPanel = panel;
		this.info.setFont(Main.textFont);
		return this;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == delete) {
			this.list.remove(this);
			this.parentPanel.remove(this);
		} else if (e.getSource() == this.moveUp && this.list.indexOf(this) != 0) {
			Collections.swap(this.list, this.list.indexOf(this), this.list.indexOf(this) - 1);
			Component[] panel = this.parentPanel.getComponents();
			int index = Arrays.asList(panel).indexOf(this) - 1;
			JPanel swappedTrait = (JPanel) panel[index];
			panel[index] = this;
			panel[index + 1] = swappedTrait;
			this.parentPanel.removeAll();
			for (Component component : panel)
				this.parentPanel.add(component);
		} else if (e.getSource() == this.moveDown && this.list.indexOf(this) != this.list.size() - 1) {
			Collections.swap(this.list, this.list.indexOf(this), this.list.indexOf(this) + 1);
			Component[] panel = this.parentPanel.getComponents();
			int index = Arrays.asList(panel).indexOf(this) + 1;
			JPanel swappedTrait = (JPanel) panel[index];
			panel[index] = this;
			panel[index - 1] = swappedTrait;
			this.parentPanel.removeAll();
			for (Component component : panel)
				this.parentPanel.add(component);
		}
		Main.character.getContentPane().revalidate();
		Main.character.getContentPane().repaint();
	}

}
