package guardian;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import guardian.Races.MainRace;

public class Stat implements ActionListener {
	private final Stats stat;
	public int value;
	private int modifiedValue;
	int otherModifier;
	int raceModifier;
	Entity entity;

	JCheckBox savingThrowsBox = new JCheckBox();

	JButton check = new JButton(Main.d20Icon);

	public Stat(Stats stat, int value, int otherModifier, int raceModifier, boolean savingThrows, Entity entity) {
		this.stat = stat;
		this.value = value;
		this.otherModifier = otherModifier;
		this.raceModifier = raceModifier;
		this.entity = entity;
		this.savingThrowsBox.setSelected(savingThrows);
		this.savingThrowsBox.addActionListener(this);
		check.addActionListener(this);
	}

	public Stat(Stats stat, int value, int otherModifier, boolean savingThrows, Entity entity) {
		this.stat = stat;
		this.value = value;
		this.otherModifier = otherModifier;
		this.entity = entity;
		this.savingThrowsBox.setSelected(savingThrows);
		this.savingThrowsBox.addActionListener(this);
		check.addActionListener(this);
	}

	String getName() {
		return stat.getName();
	}

	JPanel display() {
		JPanel panel = new JPanel(new BorderLayout());
		JTextField valueLabel = new JTextField();
		valueLabel.setColumns(3);
		valueLabel.setEditable(false);
		valueLabel.setHorizontalAlignment(JLabel.CENTER);
		valueLabel.setBorder(BorderFactory.createEmptyBorder());
		panel.add(valueLabel, BorderLayout.CENTER);
		setModifiedValue();
		panel.add(displaySavingThrows(), BorderLayout.SOUTH);
		if (this.getModifier() > 0)
			panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(entity.borderColor),
					this.stat + " (+" + this.getModifier() + ")", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, Main.titleFont));
		else
			panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(entity.borderColor),
					this.stat + " (" + this.getModifier() + ")", TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, Main.titleFont));
		valueLabel.setText(modifiedValue + "");
		valueLabel.setFont(Main.titleFont);
		valueLabel.setFont(new Font("Sitka Display", valueLabel.getFont().getStyle(), Main.titleFont.getSize() * 4));
		this.savingThrowsBox.setBackground(entity.backgroundColor);
		valueLabel.setBackground(entity.backgroundColor);
		panel.setBackground(entity.backgroundColor);
		return panel;
	}

	private JPanel displaySavingThrows() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBackground(entity.backgroundColor);
		panel.add(new JLabel("Saving Throws"));
		panel.add(savingThrowsBox);
		if (savingThrowsBox.isSelected()) {
			if (this.getModifier() + entity.getProficiencyBonus() > 0)
				savingThrowsBox.setText("+" + (this.getModifier() + entity.getProficiencyBonus()));
			else
				savingThrowsBox.setText("" + (this.getModifier() + entity.getProficiencyBonus()));
		} else {
			if (this.getModifier() > 0)
				savingThrowsBox.setText("+" + this.getModifier());
			else
				savingThrowsBox.setText("" + this.getModifier());
		}
		return panel;
	}

	private void setModifiedValue() {
		this.modifiedValue = value + otherModifier;
		if (entity.race.getMainRace() == MainRace.HUMAN || entity.race.getName().equals("Ghost"))
			this.modifiedValue += raceModifier;
		else
			modifiedValue += entity.getRace().getAbilityScoreIncreases().get(stat);
	}

	public int getModifier() {
		setModifiedValue();
		return modifiedValue / 2 - 5;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == check) {
			// Check checkFrame = new Check(this.getModifier(), this.name);
			// checkFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		} else if (e.getSource() == savingThrowsBox) {
			if (savingThrowsBox.isSelected()) {
				if (this.getModifier() + entity.getProficiencyBonus() > 0)
					savingThrowsBox.setText("+" + (this.getModifier() + entity.getProficiencyBonus()));
				else
					savingThrowsBox.setText("" + (this.getModifier() + entity.getProficiencyBonus()));
			} else {
				if (this.getModifier() > 0)
					savingThrowsBox.setText("+" + this.getModifier());
				else
					savingThrowsBox.setText("" + this.getModifier());
			}
		}
		Main.character.getContentPane().revalidate();
		Main.character.getContentPane().repaint();
	}

	public enum Stats {
		Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma;
		String getName() {
			return toString().substring(0, 1).toUpperCase() + toString().substring(1);
		}
	}
}
