package guardian;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import guardian.Skill.Skills;
import guardian.Stat.Stats;
import guardian.classes.CharacterClass;
import guardian.classes.CharacterSubClass;
import guardian.classes.CharacterSubClassPath;
import guardian.classes.Grenade;
import guardian.classes.SubClassFeature;
import guardian.items.AddArmor;
import guardian.items.Item;
import guardian.items.Vehicle.Vehicles.VehicleType;
import guardian.items.interfaces.WeaponInterface;
import guardian.items.itemEnums.Ammunition;
import guardian.items.itemEnums.Armor;
import guardian.items.itemEnums.Armor.ArmorLevel;
import guardian.items.itemEnums.Items;

public final class Ghost extends Entity implements ActionListener {
	private Guardian guardian;

	private JButton open;
	private JDialog display;
	@SuppressWarnings("unused")
	private JPanel panel = new JPanel();

	public Ghost(Entity entity, Background background, int hitDice) {
		super(entity);
		this.race = new Races() {
			@Override
			public String name() {
				return null;
			}

			@Override
			public String getName() {
				return "Ghost";
			}

			@Override
			public Size getSize() {
				return Size.TINY;
			}

			@Override
			public LinkedHashMap<Speed, Integer> getSpeed() {
				LinkedHashMap<Speed, Integer> speed = new LinkedHashMap<Speed, Integer>();
				speed.put(Speed.WALKING, 0);
				speed.put(Speed.SWIMMING, 0);
				speed.put(Speed.FLYING, 30);
				return speed;
			}

			@Override
			public HashMap<Stats, Integer> getAbilityScoreIncreases() {
				HashMap<Stats, Integer> stats = Races.super.getAbilityScoreIncreases();
				for (Stats stat : Stats.values())
					stats.put(stat, 0);
				stats.put(Stats.Intelligence, 2);
				stats.put(Stats.Dexterity, 1);
				return stats;
			}

			@Override
			public LinkedHashSet<RaceFeature> getFeatures() {
				LinkedHashSet<RaceFeature> features = new LinkedHashSet<RaceFeature>();
				features.add(RaceFeature.NATURALLYSTEALTHY);
				features.add(RaceFeature.MEMORY_BANK);
				return features;
			}

			@Override
			public WeaponInterface getUnarmedStrikes() {
				return null;
			}

			@Override
			public MainRace getMainRace() {
				return null;
			}
		};
		backgroundColor = new Color(169, 229, 208);
		init(background, hitDice);
	}

	Ghost() {
		super();
		this.race = new Races() {
			@Override
			public String name() {
				return null;
			}

			@Override
			public String getName() {
				return "Ghost";
			}

			@Override
			public Size getSize() {
				return Size.TINY;
			}

			@Override
			public LinkedHashMap<Speed, Integer> getSpeed() {
				LinkedHashMap<Speed, Integer> speed = new LinkedHashMap<Speed, Integer>();
				speed.put(Speed.WALKING, 0);
				speed.put(Speed.SWIMMING, 0);
				speed.put(Speed.FLYING, 30);
				return speed;
			}

			@Override
			public LinkedHashSet<RaceFeature> getFeatures() {
				LinkedHashSet<RaceFeature> features = new LinkedHashSet<RaceFeature>();
				features.add(RaceFeature.NATURALLYSTEALTHY);
				features.add(RaceFeature.MEMORY_BANK);
				return features;
			}

			@Override
			public WeaponInterface getUnarmedStrikes() {
				return null;
			}

			@Override
			public MainRace getMainRace() {
				return null;
			}
		};
		new Item(Items.GLIMMER, getInventory(),
				(Die.d4.roll() + Die.d4.roll() + Die.d4.roll() + Die.d4.roll() + Die.d4.roll()) * 100);
		new Item(Ammunition.SIMPLE_MAGAZINE, getInventory(), 5);
		backgroundColor = new Color(169, 229, 208);
	}

	void init(Background background, int hitDice) {
		this.setCharacterClass(new CharacterClass(GhostSubClass.GHOST, hitDice, hitDice));

		getStats().get(Stats.Dexterity).savingThrowsBox.setSelected(true);
		getStats().get(Stats.Intelligence).savingThrowsBox.setSelected(true);
		for (Stat stat : getStats().values())
			stat.savingThrowsBox.setEnabled(false);
		this.getInventory().init(this.backgroundColor, this.borderColor, this);
		this.getInventory();
		this.setBackground(background);
		getInventory().setMaxSpaces(this.getStats().get(Stats.Intelligence).value * 5);
		this.armor = Armor.GENERALIST;
		if (characterName.isEmpty())
			open = new JButton("Ghost");
		else
			open = new JButton(characterName);
		open.addActionListener(this);
		open.setFont(Main.titleFont);
	}

	JPanel open(Guardian guardian) {
		this.guardian = guardian;
		this.getCharacterClass().setLevel(guardian.getLevel());
		JPanel panel = new JPanel(new GridLayout());
		panel.add(open);
		return panel;
	}

	@Override
	public int getProficiencyBonus() {
		return guardian.getProficiencyBonus();
	}

	public int getLevel() {
		return guardian.getLevel();
	}

	@Override
	public int getMaxHitPoints() {
		if (advancedSheet)
			return super.getMaxHitPoints() - 1;
		else
			return super.getMaxHitPoints();
	}

	@Override
	JPanel getCharacterName() {
		JPanel panel = new JPanel(new GridLayout());
		JTextField characterNameComponent = new JTextField(characterName);
		characterNameComponent.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				characterName = characterNameComponent.getText();
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
			}
		});
		panel.add(characterNameComponent);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Ghost Name",
				TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
		panel.setBackground(backgroundColor);
		characterNameComponent.setFont(Main.textFont);
		return panel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == open) {
			if (display != null)
				display.dispose();
			display = new Display();
		}
	}

	public enum GhostBackground implements Background {
		ARCHIVIST, HACKER, SHADOW, SLEUTH, SURVIVALIST;

		@Override
		public String toString() {
			return super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
		}

		@Override
		public String getDescription() {
			switch (this) {
			case ARCHIVIST:
				return "Your Ghost has spent long hours pouring over books and data files -- which is saying a lot, seeing as how it can read the average book in 2 seconds.";
			case HACKER:
				return "Your Ghost doesn't balk at the challenge of navigating firewalls or deciphering the nuances of DOS.";
			case SHADOW:
				return "Your Ghost has learned to stay out of the way and remain unseen, and it keeps a close eye on its surroundings to stay alert.";
			case SLEUTH:
				return "Finding their Guardian took more than a lot of flying and scanning, and your Ghost is adept at exercising these supplemental skills.";
			case SURVIVALIST:
				return "Above all else, your Ghost values staying alive to fight another day. Death of the Guardian means little; death of the Ghost means everything.";
			}
			return "How?";
		}

		@Override
		public void setSkills(Entity ghost) {
			switch (this) {
			case ARCHIVIST:
				ghost.getSkills().get(Skills.Arcana).proficiency.setSelected(true);
				ghost.getSkills().get(Skills.Nature).proficiency.setSelected(true);
				break;
			case HACKER:
				ghost.getSkills().get(Skills.Investigation).proficiency.setSelected(true);
				ghost.getSkills().get(Skills.Technology).expertise.setSelected(true);
				break;
			case SHADOW:
				ghost.getSkills().get(Skills.Deception).proficiency.setSelected(true);
				ghost.getSkills().get(Skills.Stealth).proficiency.setSelected(true);
				break;
			case SLEUTH:
				ghost.getSkills().get(Skills.Deception).proficiency.setSelected(true);
				ghost.getSkills().get(Skills.Persuasion).proficiency.setSelected(true);
				break;
			case SURVIVALIST:
				ghost.getSkills().get(Skills.Survival).proficiency.setSelected(true);
				ghost.getSkills().get(Skills.Stealth).proficiency.setSelected(true);
				break;
			}
		}
	}

	public enum GhostSubClass implements CharacterSubClass {
		GHOST;

		@Override
		public String toString() {
			return "Ghost";
		}

		@Override
		public JPanel displayName(Entity entity) {
			JPanel classPane = new JPanel(new GridLayout());
			JTextField name = new JTextField(this.toString());
			name.setEditable(false);
			classPane.add(name);
			classPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(entity.borderColor),
					"Class", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			classPane.setBackground(entity.backgroundColor);
			return classPane;
		}

		@Override
		public ArrayList<Stats> getSavingThrows() {
			ArrayList<Stats> stats = new ArrayList<Stats>();
			stats.add(Stats.Dexterity);
			stats.add(Stats.Intelligence);
			return stats;
		}

		@Override
		public ArrayList<Feature> getFeatures(int guardianLevel) {
			ArrayList<Feature> features = new ArrayList<Feature>();
			features.add(SubClassFeature.POCKET_BACKPACK);
			features.add(SubClassFeature.SLIPPERY_SAVE);
			features.add(SubClassFeature.GHOST_CUNNING_ACTION);
			features.add(SubClassFeature.AMMO_SYNTHESYS);
			return features;
		}

		@Override
		public String getDescription() {
			return null;
		}

		@Override
		public Die getHitDie() {
			return Die.d4;
		}

		@Override
		public Grenade[] getGrenades(int level) {
			return null;
		}

		@Override
		public int getShield(int level) {
			return 0;
		}

		@Override
		public Stats getLightAbility() {
			return null;
		}

		@Override
		public LinkedHashSet<BaseInterface> getProficiencies() {
			LinkedHashSet<BaseInterface> proficiencies = new LinkedHashSet<BaseInterface>();
			proficiencies.add(ArmorLevel.GHOST);
			proficiencies.add(VehicleType.JUMPSHIP);
			return proficiencies;
		}

		@Override
		public CharacterSubClassPath[] getPaths() {
			return new CharacterSubClassPath[0];
		}
	}

	private class Display extends JDialog {
		private static final long serialVersionUID = 1L;

		private JPanel featuresPane = new JPanel(new GridLayout(0, 1));

		private JPanel topPane = new JPanel(new GridLayout(1, 0));
		private JPanel leftPane = new JPanel(new GridLayout(0, 1));
		private JScrollPane leftScrollPane = new JScrollPane(leftPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		private JPanel centerPane = new JPanel(new GridLayout());
		private JScrollPane centerScrollPane = new JScrollPane(centerPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		public JPanel rightPane = new JPanel(new GridLayout());
		private JScrollPane rightScrollPane = new JScrollPane(rightPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		Display() {
			super(Main.character);
			setTitle(characterName);
			init();
		}

		private void init() {
			initTopPane();
			initLeftPane();
			initCenterPane();
			initRightPane();
			this.setIconImage(new ImageIcon(Main.logo).getImage());
			// this.setExtendedState(JFrame.MAXIMIZED_BOTH);
			this.pack();
			this.getContentPane().setBackground(backgroundColor);
			// this.setAlwaysOnTop(true);
			this.setBackground(backgroundColor);
			this.setResizable(false);
			this.setLocationRelativeTo(null);
			this.setVisible(true);
		}

		private JPanel initStatsPane() {
			JPanel statsPane = new JPanel(new GridLayout(0, 1));
			statsPane.add(getStats().get(Stats.Strength).display());
			statsPane.add(getStats().get(Stats.Dexterity).display());
			statsPane.add(getStats().get(Stats.Constitution).display());
			statsPane.add(getStats().get(Stats.Intelligence).display());
			statsPane.add(getStats().get(Stats.Wisdom).display());
			statsPane.add(getStats().get(Stats.Charisma).display());
			statsPane.getPreferredSize().width = (int) (statsPane.getPreferredSize().width * 1.4);
			statsPane.setBackground(backgroundColor);
			return statsPane;
		}

		private JPanel initSkillsPane() {
			JPanel leftPane = new JPanel(new GridLayout(0, 1));
			leftPane.add(new JLabel(getSkills().get(Skills.Acrobatics).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.AnimalHandling).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Arcana).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Athletics).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Deception).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.History).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Insight).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Intimidation).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Investigation).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Medicine).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Nature).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Perception).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Performance).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Persuasion).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Religion).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.SleightOfHand).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Stealth).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Survival).getName()));
			leftPane.add(new JLabel(getSkills().get(Skills.Technology).getName()));
			leftPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 7));
			leftPane.setBackground(backgroundColor);

			JPanel rightPane = new JPanel(new GridLayout(0, 1));
			rightPane.add(getSkills().get(Skills.Acrobatics).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.AnimalHandling).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Arcana).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Athletics).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Deception).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.History).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Insight).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Intimidation).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Investigation).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Medicine).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Nature).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Perception).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Performance).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Persuasion).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Religion).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.SleightOfHand).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Stealth).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Survival).displayStats(Ghost.this));
			rightPane.add(getSkills().get(Skills.Technology).displayStats(Ghost.this));
			rightPane.setBackground(backgroundColor);

			JPanel skillsPane = new JPanel();
			skillsPane.setLayout(new BorderLayout());
			skillsPane.add(leftPane, BorderLayout.WEST);
			skillsPane.add(rightPane, BorderLayout.CENTER);
			skillsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Skills",
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			skillsPane.setBackground(backgroundColor);
			return skillsPane;
		}

		private JPanel initStatPane() {
			JPanel proficiencyPane = new JPanel(new GridLayout(1, 0));
			proficiencyPane.setBorder(
					BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Proficiency Bonus",
							TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			proficiencyPane.add(new JLabel(getProficiencyBonus() + ""));

			JPanel passivePerceptionPane = new JPanel(new GridLayout(1, 0));
			passivePerceptionPane.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createLineBorder(borderColor), "Passive Wisdom (Perception)",
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			passivePerceptionPane.add(getPassivePerception());
			proficiencyPane.setBackground(backgroundColor);
			passivePerceptionPane.setBackground(backgroundColor);

			JPanel statPane = new JPanel(new BorderLayout());
			statPane.add(initStatsPane(), BorderLayout.WEST);

			JPanel statPaneRight = new JPanel();
			statPaneRight.setLayout(new BoxLayout(statPaneRight, BoxLayout.Y_AXIS));
			statPaneRight.add(proficiencyPane);
			statPaneRight.add(passivePerceptionPane);
			statPaneRight.add(initSkillsPane());
			statPaneRight.setBackground(backgroundColor);

			statPane.add(statPaneRight, BorderLayout.CENTER);
			statPane.setBackground(backgroundColor);
			return statPane;
		}

		private void initFeaturesPane() {
			featuresPane.setLayout(new BoxLayout(featuresPane, BoxLayout.Y_AXIS));
			featuresPane.removeAll();
			for (Trait feature : getFeatures()) {
				featuresPane.add(feature);
			}
			featuresPane.setBackground(backgroundColor);
		}

		private void initTopPane() {
			topPane.add(new CharacterEditor(Ghost.this).open(false));
			topPane.add(getCharacterName());
			topPane.add(getCharacterBackground());

			topPane.setBackground(backgroundColor);
			this.getContentPane().add(topPane, BorderLayout.NORTH);
		}

		private void initLeftPane() {
			leftPane.setLayout(new BoxLayout(leftPane, BoxLayout.Y_AXIS));
			leftPane.add(initStatPane());
			leftPane.setBackground(backgroundColor);
			this.getContentPane().add(leftScrollPane, BorderLayout.WEST);
			leftPane.getPreferredSize().width += 20;
		}

		private void initCenterPane() {
			centerPane.setLayout(new BoxLayout(centerPane, BoxLayout.Y_AXIS));

			JPanel armorPane = new JPanel(new GridLayout());
			armorPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Armor",
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			armorPane.add(new AddArmor(Ghost.this, false).open());

			JPanel initiativePane = new JPanel(new GridLayout());
			initiativePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
					"Initiative", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			JTextField initiativeLabel = new JTextField(getInitiative() + "");
			initiativeLabel.setColumns(5);
			initiativeLabel.setFont(Main.textFont);
			initiativeLabel.setEditable(false);
			initiativeLabel.setBackground(backgroundColor);
			initiativePane.add(initiativeLabel);

			JPanel speedPane = new JPanel(new GridLayout(1, 0));
			speedPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Speed",
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			for (Speed speedName : getRace().getSpeed().keySet()) {
				JPanel panel = new JPanel();
				panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
				panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
						speedName.getName(), TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION,
						Main.titleFont));
				panel.setBackground(backgroundColor);
				JTextField speedLabel = new JTextField(getRace().getSpeed().get(speedName).toString());
				speedLabel.setColumns(6);
				speedLabel.setFont(Main.textFont);
				speedLabel.setEditable(false);
				speedLabel.setBackground(backgroundColor);
				panel.add(speedLabel);
				speedPane.add(panel);
			}

			JPanel armorInitiativePane = new JPanel();
			armorInitiativePane.setLayout(new BoxLayout(armorInitiativePane, BoxLayout.X_AXIS));
			armorInitiativePane.add(armorPane);
			armorInitiativePane.add(initiativePane);

			JPanel armorInitiativeSpeedPane = new JPanel();
			armorInitiativeSpeedPane.setLayout(new BoxLayout(armorInitiativeSpeedPane, BoxLayout.Y_AXIS));
			armorInitiativeSpeedPane.add(armorInitiativePane);
			armorInitiativeSpeedPane.add(speedPane);

			JPanel maxHitPointsPane = new JPanel(new GridLayout());
			maxHitPointsPane.setBorder(
					BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Maximum Hit Points",
							TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			JSpinner maxHitPoints = new JSpinner(new SpinnerNumberModel(Ghost.this.getMaxHitPoints(), 1, 1000, 1));
			JSpinner currentHitPoints = new JSpinner(
					new SpinnerNumberModel(getCurrentHitPoints(), 0, Ghost.this.getMaxHitPoints(), 1));
			JSpinner temporaryHitPoints = new JSpinner(new SpinnerNumberModel(getTemporaryHitPoints(), 0, 1000, 1));
			maxHitPointsPane.add(maxHitPoints);
			maxHitPoints.setModel(
					new SpinnerNumberModel((int) maxHitPoints.getValue(), (int) currentHitPoints.getValue(), 1000, 1));
			maxHitPoints.setFont(Main.textFont);
			maxHitPoints.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					currentHitPoints.setModel(new SpinnerNumberModel((int) (Integer) currentHitPoints.getValue(), 0,
							(int) (Integer) maxHitPoints.getValue(), 1));
					((JSpinner.DefaultEditor) currentHitPoints.getEditor()).getTextField().setColumns(9);
					Ghost.this.setMaxHitPoints((int) maxHitPoints.getValue());
				}
			});

			JPanel currentHitPointsPane = new JPanel(new GridLayout());
			currentHitPointsPane.setBorder(
					BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor), "Current Hit Points",
							TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			currentHitPointsPane.add(currentHitPoints);
			currentHitPoints.setModel(new SpinnerNumberModel((int) (Integer) currentHitPoints.getValue(), 0,
					(int) (Integer) maxHitPoints.getValue(), 1));
			currentHitPoints.setFont(Main.textFont);
			currentHitPoints.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					maxHitPoints.setModel(new SpinnerNumberModel((int) maxHitPoints.getValue(),
							(int) currentHitPoints.getValue(), 1000, 1));
					((JSpinner.DefaultEditor) maxHitPoints.getEditor()).getTextField().setColumns(10);
					Ghost.this.setCurrentHitPoints((int) currentHitPoints.getValue());
				}
			});

			JPanel healthPane1 = new JPanel();
			healthPane1.setLayout(new BoxLayout(healthPane1, BoxLayout.X_AXIS));
			healthPane1.add(maxHitPointsPane);
			healthPane1.add(currentHitPointsPane);
			healthPane1.add(armorInitiativeSpeedPane);

			JPanel temporaryHitPointsPane = new JPanel(new GridLayout());
			temporaryHitPointsPane.setBorder(BorderFactory.createTitledBorder(
					BorderFactory.createLineBorder(borderColor), "Temporary Hit Points",
					TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			temporaryHitPointsPane.add(temporaryHitPoints);
			temporaryHitPoints.setFont(Main.textFont);

			((JSpinner.DefaultEditor) maxHitPoints.getEditor()).getTextField().setColumns(10);
			((JSpinner.DefaultEditor) currentHitPoints.getEditor()).getTextField().setColumns(9);
			((JSpinner.DefaultEditor) temporaryHitPoints.getEditor()).getTextField().setColumns(11);

			JPanel hitDiePane = new JPanel();
			hitDiePane.setLayout(new BoxLayout(hitDiePane, BoxLayout.Y_AXIS));
			hitDiePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
					"Hit Dice: " + getCharacterClass().getSubClass().getHitDie(), TitledBorder.DEFAULT_JUSTIFICATION,
					TitledBorder.DEFAULT_POSITION, Main.titleFont));
			hitDiePane.add(getCharacterClass().getHitDice());

			JCheckBox[][] deathSaves = new JCheckBox[2][3];
			deathSaves[0][0] = new JCheckBox();
			deathSaves[0][0].setSelected(getDeathSaves()[0][1]);
			deathSaves[0][1] = new JCheckBox();
			deathSaves[0][1].setSelected(getDeathSaves()[0][1]);
			deathSaves[0][2] = new JCheckBox();
			deathSaves[0][2].setSelected(getDeathSaves()[0][2]);
			deathSaves[1][0] = new JCheckBox();
			deathSaves[1][0].setSelected(getDeathSaves()[1][1]);
			deathSaves[1][1] = new JCheckBox();
			deathSaves[1][1].setSelected(getDeathSaves()[1][1]);
			deathSaves[1][2] = new JCheckBox();
			deathSaves[1][2].setSelected(getDeathSaves()[1][2]);

			JPanel deathSuccessesPane = new JPanel(new GridLayout());
			deathSuccessesPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
					"Successes", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			deathSuccessesPane.add(deathSaves[0][0]);
			deathSuccessesPane.add(deathSaves[0][1]);
			deathSuccessesPane.add(deathSaves[0][2]);
			for (Component comp : deathSuccessesPane.getComponents())
				comp.setBackground(backgroundColor);

			JPanel deathFailsPane = new JPanel(new GridLayout());
			deathFailsPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
					"Fails", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			deathFailsPane.add(deathSaves[1][0]);
			deathFailsPane.add(deathSaves[1][1]);
			deathFailsPane.add(deathSaves[1][2]);

			JPanel deathSavesPane = new JPanel(new GridLayout(0, 1));
			deathSavesPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
					"Death Saves", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			deathSavesPane.add(deathSuccessesPane);
			deathSavesPane.add(deathFailsPane);
			for (Component comp : deathFailsPane.getComponents())
				comp.setBackground(backgroundColor);

			JPanel healthPane2 = new JPanel();
			healthPane2.setLayout(new BoxLayout(healthPane2, BoxLayout.X_AXIS));
			healthPane2.add(temporaryHitPointsPane);
			healthPane2.add(hitDiePane);
			healthPane2.add(deathSavesPane);

			initFeaturesPane();
			JPanel featuresPane = new JPanel(new BorderLayout());
			featuresPane.add(
					new AddTrait("New Feature", Ghost.this, Ghost.this.features, this.featuresPane, false).open(),
					BorderLayout.NORTH);
			featuresPane.add(this.featuresPane, BorderLayout.CENTER);
			featuresPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(borderColor),
					"Features", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, Main.titleFont));
			featuresPane.setBackground(backgroundColor);

			centerPane.add(healthPane1);
			centerPane.add(healthPane2);
			centerPane.add(featuresPane);

			armorPane.setBackground(backgroundColor);
			initiativePane.setBackground(backgroundColor);
			speedPane.setBackground(backgroundColor);
			maxHitPointsPane.setBackground(backgroundColor);
			currentHitPointsPane.setBackground(backgroundColor);
			temporaryHitPointsPane.setBackground(backgroundColor);
			hitDiePane.setBackground(backgroundColor);
			deathSuccessesPane.setBackground(backgroundColor);
			deathFailsPane.setBackground(backgroundColor);
			deathSavesPane.setBackground(backgroundColor);
			healthPane1.setBackground(backgroundColor);
			healthPane2.setBackground(backgroundColor);
			featuresPane.setBackground(backgroundColor);
			centerPane.setBackground(backgroundColor);
			this.getContentPane().add(centerScrollPane, BorderLayout.CENTER);
		}

		private void initRightPane() {
			rightPane.setLayout(new BoxLayout(rightPane, BoxLayout.Y_AXIS));
			rightPane.add(getInventory().display((int) (Main.height / 3)));
			rightPane.setBackground(backgroundColor);

			this.getContentPane().add(rightScrollPane, BorderLayout.EAST);
		}

	}
}
