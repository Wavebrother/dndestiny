package guardian;

public interface Language extends BaseInterface{
		String getSpeakers();

		Script getScript();

	enum ModernLanguages implements Language {
		HUMANCOMMON, ENGLISH, RUSSIAN, CHINESE, GUARDIAN;

		@Override
		public String toString() {
			switch (this) {
			case CHINESE:
				return "Chinese";
			case ENGLISH:
				return "English";
			case GUARDIAN:
				return "Guardian";
			case HUMANCOMMON:
				return "Human Commom";
			case RUSSIAN:
				return "Russian";
			}
			return "How?";
		}

		@Override
		public String getSpeakers() {
			switch (this) {
			case CHINESE:
				return "Western and Southern Asians";
			case ENGLISH:
				return "Eastern Europeans";
			case GUARDIAN:
				return "Guardians";
			case HUMANCOMMON:
				return "Citizens of The Last City";
			case RUSSIAN:
				return "Northern and Central Asians";
			}
			return "";
		}

		@Override
		public Script getScript() {
			switch (this) {
			case CHINESE:
				return Script.CHARACTERS;
			case RUSSIAN:
				return Script.CYRILLIC;
			case ENGLISH:
			case GUARDIAN:
			case HUMANCOMMON:
			}
			return Script.LATIN;
		}

	}

	public enum AncientLanguages implements Language {
		FRENCH, GERMAN, SWEDISH, JAPANESE, TAIWANESE, MANDARIN, ARABIC, HEBREW, HINDU;

		@Override
		public String toString() {
			switch (this) {
			case ARABIC:
				return "Arabic";
			case FRENCH:
				return "French";
			case GERMAN:
				return "German";
			case HEBREW:
				return "Hebrew";
			case HINDU:
				return "Hindu";
			case JAPANESE:
				return "Japanese";
			case MANDARIN:
				return "Mandarin";
			case SWEDISH:
				return "Swedish";
			case TAIWANESE:
				return "Taiwanese";
			}
			return "How?";
		}

		@Override
		public String getSpeakers() {
			return "Nobody";
		}

		@Override
		public Script getScript() {
			switch (this) {
			case ARABIC:
			case HEBREW:
				return Script.SEMITIC;
			case HINDU:
				return Script.DEVANAGARI;
			case JAPANESE:
				return Script.HIRAGANA;
			case MANDARIN:
			case TAIWANESE:
				return Script.CHARACTERS;
			case FRENCH:
			case GERMAN:
			case SWEDISH:
			}
			return Script.LATIN;
		}
	}

	enum ExoticLanguages implements Language {
		ELIKSNI, KRILL, ULURANT, HEXINARY;

		@Override
		public String toString() {
			switch (this) {
			case ELIKSNI:
				return "Eliksni";
			case HEXINARY:
				return "Hexinary";
			case KRILL:
				return "Krill";
			case ULURANT:
				return "Ulurant";
			}
			return "How?";
		}

		@Override
		public String getSpeakers() {
			switch (this) {
			case ELIKSNI:
				return "Eliksni, Reefborn Awoken";
			case HEXINARY:
				return "Vex, Servitors";
			case KRILL:
				return "Hive";
			case ULURANT:
				return "Cabal";
			}
			return "";
		}

		@Override
		public Script getScript() {
			switch (this) {
			case ELIKSNI:
				return Script.ELIKSNI;
			case HEXINARY:
				return Script.D_GGHL;
			case KRILL:
				return Script.KRILL;
			case ULURANT:
				return Script.ULURANT;
			}
			return null;
		}

	}

	enum Script {
		LATIN, CYRILLIC, CHARACTERS, HIRAGANA, SEMITIC, DEVANAGARI, ELIKSNI, KRILL, ULURANT, D_GGHL;
	}
}
